<section class="content"  id="categories_selectlable">
    <!-- COLOR PALETTE -->
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"> 
                {{ __('marketplace.categories.CATEGORIES_LABEL') }}
            </h3>
            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 250px;">
                  <input name="table_search" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }} {{ __('marketplace.categories.CATEGORY_LABEL') }}" v-model=cats_query  v-on:keyup="search_category()" type="text">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </div>
                </div>
            </div>
        </div>    

      <div class="box-body">
        
          
        <div class="row">
            <div class="col-md-12">
                <button type="button" v-on:click="set_select_all()" class="btn btn-primary ">
                    {{__('general.SELECT_ALL_LABEL') }} 
                
                </button>
            </div>
        </div>

              

        <table class="table">
            <thead>
                <tr>
                    <th width="10px"><input :checked=selected type="checkbox" v-on:click="set_select_all()"></th>
                    <th> {{ __('general.TITLE_LABEL') }}</th>
                    <th width="10%">{{ __('general.ACTIONS_LABEL') }}</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody >
              
                <tr v-if="cats.length > 0 " v-for="item in cats">
                     @verbatim
                    <td>
                        <input :checked=item.selected  type="checkbox"  :value=item.id name="cats[]" v-on:click="set_select_item(item)">
                    </td>
                   
                    <td>
                        <strong>{{item.name}} </strong>
                    </td>
                    @endverbatim
                    <td>
                        <a v-if="item.selected == true"  v-on:click="set_select_item(item)" class="btn btn-warning" >
                            {{__('general.SELECTED_LABEL') }} 
                        </a>
                        <a v-if="item.selected == false" v-on:click="set_select_item(item)"  class="btn btn-primary " >
                       
                            {{__('general.SELECT_LABEL') }} 
                        </a>
                      
                    </td>
                </tr>
             
            </tbody>
        </table>

        <div >
            <button v-if="next_page_cats!=null"  v-on:click="load_more()"  class="btn btn-block btn-primary" type="button">
                {{__('general.LOAD_MORE_LABEL') }} 
                   
            </button>
        </div>
         

      
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
