<div   style="display: none" :class=get_active_class() role="dialog" tabindex="-1" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" v-on:click="close_modal()" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title"> 
                    {{ __('media.MEDIA_LABEL') }}
                </h3>
            </div>
            <div class="modal-body gray" style="overflow: auto;max-height: 700px;"  >

                <div class="box-body">

        
    
                    <div style="padding: 20px;" class="clearfix">
                        <div>
                            <div id="holder"  v-on:click="open_uploader($event)" class="dropzone dz-clickable">
                                <div class="dz-default dz-message">
                                    <div class="dropzone-preupload-title">
                                      {{ __('media.DROP_FILES_LABEL') }}
                                    </div> 
                                    <p>
                                      {{ __('media.UNLOADED_FILES_LABEL') }}
                                    </p>
                                </div>
                                <input type="file" v-on:change="upload($event)" c  ref="id_files" id="id_files" class="hide" >
                            </div>
                            <br>

                            @verbatim
                           
                            <div v-if="progresss > 0" style="margin-top:25px;" class="progress">
                             <div class="progress-bar" role="progressbar" :aria-valuenow=progresss
                              aria-valuemin="0" aria-valuemax="100" :style="progress_status()" >
                                <span class="sr-only">{{progresss}} Complete</span>
                              </div>
                            </div>
                            @endverbatim
                        </div>

                    </div>                          
                        
                         
                            
                     
                    <div style="padding: 20px;" class="clearfix">
                    
                        <div class="col-md-8">
                            <div class="input-group mb-12">
                              <input type="text" class="form-control" v-model=dir_query placeholder="{{ __('general.NAME_LABEL') }}">
                              <span class="input-group-btn">
                                <button type="button" v-on:click="create_dir()" class="btn btn-defaul dropdown-toggle" >
                                    {{ __('general.NEW_LABEL') }}
                                    {{ __('media.MEDIA_DIRECTORY_LABEL') }}
                                </button>     
                              </span>
                            </div>
                        </div>
                    </div>
                    <div style="padding: 20px;" class="clearfix">
                        <div  class="col-md-5">
                            <div class="form-group ">
                            
                                <div class="input-group">
                                    <input type="text" v-model=current_path  class="form-control" placeholder="">
                                    <span class="input-group-btn">
                                        <button  v-on:click="navigate_query()" class="btn btn-primary" type="button">
                                            Go!
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="padding: 20px;" class="clearfix">      

                        <table class="table" style="max-width: 100%">

                            <tbody>
                                <tr >
                                    <td width="75%" colspan="3" >
                                        <strong v-on:click="move_to_parent()" class="btn btn-primary" style="cursor: pointer;">
                                        {{ __('media.PARENT_LABEL') }}
                                        </strong>
                                    </td>
                                    
                                </tr> 
                              

                                <tr v-if="files.length > 0" v-for="item in files">
                                    @verbatim
                                    <td class="app-timeline-item" width="20px;">
                                        <div class=" padding-top-20" style="height: 102px;">
                                            <div class="contact contact-rounded contact-bordered contact-xlg margin-bottom-0">
                                              <img  v-if="item.dir == false" style="height: 58px" :src="image_src(item)" >
                                              <img  v-if="item.dir == true" v-on:click="navigate(item.file)" style="height: 58px" src="/assets/img/nav.png" >
                                            </div>
                                        </div>
                                    </td>
                                    <td width="35%" >
                                        
                                        <strong >{{prettyfy_name(item.file)}}</strong>
                                         @endverbatim
                                        <div  v-if="item.editable == true" class="form-group ">
                                            <input type="text" v-model=item.file  class=" form-control">
                                        </div>
                                    </td>

                                 
                                    <td>
                                        


                                        <button v-if="item.dir == false" type="button"  v-on:click="set_image(item)"  class="btn btn-primary btnx">
                                            <i class="fa fa-clipboard"></i>
                                        </button>
                                       
                                        <a  v-if="item.editable == true"  v-on:click="save_name(item)"  class="btn btn-primary">
                                          {{ __('general.SAVE_LABEL') }}
                                         
                                        </a>
                                      
                                        <a  v-if="item.editable == false" v-on:click="make_editable(item)" class="btn btn-primary">
                                            <i class="fa fa-pencil-square-o"></i>
                                            {{ __('general.EDIT_LABEL') }}
                                        
                                        </a>
                                        
                                        <a v-on:click="delete_file(item)"  class="btn btn-danger" >
                                            <i class="fa fa-trash"></i>           
                                        </a>

                                    </td>
                                </tr>
                               
                            </tbody>
                        </table>

                    </div>
                           
                         
                    
                    


                  
                <!-- /.row -->
                </div>
            </div>
        </div>
    </div>
</div>

