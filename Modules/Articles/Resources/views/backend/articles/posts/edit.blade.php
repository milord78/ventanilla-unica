@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">


       <h3  style="margin: 15px" class="box-title">  
          {{  __('blog.post.POST_LABEL') }}
          @if (empty($item->id))
            {{ __('general.ADD_LABEL') }}
          @else
           {{ __('general.EDIT_LABEL') }}
          @endif
       </h3>
 
      <form role="form" method="post" action="{{$route}}">
           @csrf
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#basic" data-toggle="tab" aria-expanded="true">
                    {{ __('general.BASIC_LABEL') }}
                  </a>
                </li>
                <li>
                  <a href="#seo" data-toggle="tab" aria-expanded="false">
                  {{ __('general.SEO_LABEL') }}
                  </a>
                </li>
                <li>
                  <a href="#categories" data-toggle="tab" aria-expanded="false">
                    {{ __('blog.categories.CATEGORIES_LABEL') }}
                  </a>
                </li>
                <li>
                  <a href="#tags" data-toggle="tab" aria-expanded="false">
                  {{ __('tags.TAGS_LABEL') }}
                  </a>
                </li>
              
              </ul>
              <div class="tab-content no-padding" >
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                    <div style="margin: 15px">
                      
                       <div class="form-group">
                        <label>{{ __('general.TITLE_LABEL') }}</label>
                        <input type="text" name="title"  value="{{$item->title}}" required class="form-control" >
                        <br>
                       </div>
                       <div style="height: 620px;" class="form-group">
                        <label>{{ __('marketplace.articles.CONTENT_LABEL') }}</label>
                        <textarea name="content" id="mytextarea"  class="hide form-control" >{{$item->content}}</textarea>
                        <br>
                      </div>
                     

                      <div id="media_modal_module">

                          <div class="form-group">
                            <label >{{ __('general.THUMBNAIL_LABEL') }}</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button v-on:click="open_modal('featured')" class="btn btn-primary btn-outline-secondary" type="button">
                                       {{ __('media.SELECT_MEDIA_LABEL') }}
                                    </button>
                                </span>
                                <input class="form-control" name="thumbnail"  value="{{$item->thumbnail}}" ref="featured" type="text">
                            </div> 
                          </div>  

                          <div class="form-group">
                            <label >{{ __('general.FEATURED_IMAGE_LABEL') }}</label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button v-on:click="open_modal('thumb')"  class="btn btn-primary btn-outline-secondary" type="button">
                                        {{ __('media.SELECT_MEDIA_LABEL') }}
                                    </button>
                                </span>
                                <input class="form-control" name="featured_image"   value="{{$item->featured_image}}"  ref="thumb" type="text">
                            </div> 
                          </div>  
                         @include('articles::backend.articles.posts.includes.media_modal_selectable')
                      </div>

                       <div class="form-group">
                        <label> {{ __('general.PUBLISH_LABEL') }} <input type="checkbox" name="publish"    @if (!empty($item->publish) and $item->publish == true ) checked="checked" @endif > </label>
                       
                        <br>
                       </div>
                       <div class="form-group">
                        <label> {{ __('marketplace.articles.IS_ON_FEED_LABEL') }} <input type="checkbox" name="on_feed"    @if (!empty($item->on_feed) and $item->on_feed == true ) checked="checked" @endif > </label>
                       
                        <br>
                       </div>
                       <div class="form-group">
                        <label> {{ __('marketplace.articles.IS_FEATURED_LABEL') }} <input type="checkbox" name="featured"    @if (!empty($item->featured) and $item->featured == true ) checked="checked" @endif > </label>
                       
                        <br>
                       </div>
                    </div>
                </div>
                <div id="seo" class="tab-pane ">
                    <div style="margin: 15px">
                       <div class="form-group">
                        <label>{{ __('general.SLUG_LABEL') }}</label>
                        <input type="text" name="slug"  value="{{$item->slug}}" required class="form-control" >
                        <br>
                      </div>
                      <div class="form-group">
                        <label>{{ __('general.META_TITLE_LABEL') }}</label>
                        <input type="text" name="meta_title"  value="{{$item->meta_title}}" required class="form-control" >
                        <br>
                      </div>
                      <div class="form-group">
                        <label>{{ __('general.META_DESCRIPTION_LABEL') }}</label>
                        <textarea name="meta_description" class="form-control" >{{$item->meta_description}}</textarea>
                        <br>
                      </div>
                    </div>
                </div>
                <div id="categories" class="tab-pane ">
                    <div class="inner" style="margin: 15px">
                      @include('articles::backend.articles.posts.includes.categories_selectable')
                    </div>
                </div>
                <div id="tags" class="tab-pane ">
                    <div class="inner" style="margin: 15px">
                      @include('articles::backend.articles.posts.includes.tags_selectable')
                    </div>
                </div>
              </div>

              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>
            <a href="{{ route('admin_articles_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
            @if (!empty($item->id))
              <a href="{{ route('admin_articles_delete',['id' => $item->id]) }}" class="btn btn-danger">
                {{ __('general.DELETE_LABEL') }}
              </a>
            @endif
        </div>

      </form>
   
    
  </div>
  
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/js/admin/media_modal_module.js') }}"></script>
<script src="{{ asset('assets/js/admin/blog_related_cartegories.js') }}"></script>
<script src="{{ asset('assets/js/admin/blog_module_related_tags.js') }}"></script>


<script type="text/javascript">

  string_to_slug = function (str) 
  {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;~#";
    var to   = "aaaaeeeeiiiioooouuuunc--------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
  }
  var ready = function(e)
  { 


    copy_title = function(e)
    {
      value = $(this).val()
      
      $("input[name=slug]").val(string_to_slug(value));
      $("input[name=meta_title]").val(value);

      
    }
    $("input[name=title]").on('keyup',copy_title);
  } 
  $(window).on('load',ready);

    tinymce.init({
      selector: '#mytextarea',
      height: 600
    });
</script>



@endsection
