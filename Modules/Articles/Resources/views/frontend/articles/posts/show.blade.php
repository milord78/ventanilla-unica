@extends('layouts.app2')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="articles">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h1>{{$item->title}}</h1>
            <div>
              <p class="meta">
               <time class="updated" datetime="{{date('d-m-Y', strtotime($item->created))}}">{{date('F j, Y g:i a', strtotime($item->created))}}</time> por {{$item->autor->firstname}} {{$item->autor->lastname}}
              </p>
            </div>
            {!!$item->content!!}

            <div>
                <div class="c-author-excerpt">

                    <figure> 
                        <img alt="" src="{{$item->autor->logos}}" class="avatar avatar-96 photo" width="96" height="96"></figure>
                    <div class="right">
                      <h4>
                        <a href="{{route(
                          'frontend_articles_autor',
                          ['slug'=>$item->autor->nick]
                        )}}" title="Articulo por  {{$item->autor->firstname}} {{$item->autor->lastname}}" rel="author">
                          {{$item->autor->firstname}} {{$item->autor->lastname}}
                        </a>
                      </h4>
                        <p>
                          {{$item->autor->description}}
                        </p>
                        <p>
                        <a href="{$item->autor->website}}"> 
                          @{{$item->autor->nick}}
                        </a>
                    </p>
                    </div>
                </div>
            </div>
      
          </div>
          <div class="col-md-4">
            <div class="article_listing">
              <ul>
                @foreach(list_featured_blog_news($item->id,4) as $key => $item2)
                <li>
                  <article> 
                      <a href="{{route('frontend_articles_show',['slug'=>$item2->slug])}}">
                        <div class="left">
                          <figure> 
                            <img class="lazyautosizes img-responsive" src="{{$item2->thumbnail}}" >
                          </figure>
                        </div>
                        <div class="right">
                          <h3 class="title">{{$item2->title}}</h3>
                        </div>
                      </a>
                  </article>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
