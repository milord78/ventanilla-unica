@extends('layouts.app2')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="articles">
      <div class="container">
        <div class="row">
          <div class="col-md-8">

            <div>
              <div class="c-author-excerpt">
                <figure> 
                  <img alt="" src="{{$autor->logos}}" class="avatar avatar-96 photo" width="96" height="96">
                </figure>
                <div class="right">
                  <h4>
                    <span  title="Articulo por {{$autor->firstname}} {{$autor->lastname}}" rel="author">
                      {{$autor->firstname}} {{$autor->lastname}}
                    </span>
                  </h4>
                  <p>
                    {{$autor->description}}
                  </p>
                  <p>
                    <a href="{{$autor->website}}"> 
                      @{{$autor->nick}}
                    </a>
                  </p>
                </div>
              </div>
            </div>
            <div class="article_listing">
              <ul>
                @foreach($items as $key => $item)
                <li>
                  <article> 
                      <a href="{{route('')}}">
                        <div class="left">
                          <figure> 
                            <img class="lazyautosizes img-responsive" src="{{$item->thumbnail}}" >
                          </figure>
                        </div>
                        <div class="right">
                          <p class="meta">
                            <time class="updated" datetime="{{date('d-m-Y', strtotime($item->created))}}<">{{date('F j, Y g:i a', strtotime($item->created))}}</time> by {{$item->autor->firstname}} {{$item->autor->lastname}}
                          </p>
                          <h3 class="title">{{$item->title}}</h3>
                          <p>{{$item->excerpt}}</p>
                        </div>
                      </a>
                  </article>
                </li>
                @endforeach
              </ul>
            </div>
            <div class="pagination">
                <div class="col-md-12"> 
                  @if($items->currentPage()> 1)
                      <a class="btn btn-primary" href="{{ $items->url(1) }}">Anterior</a>
                  @endif
                  @if( $items->lastPage() > 1)
                      @if($items->currentPage() < $items->lastPage())
                          <a class="btn btn-primary" href="{{ $items->url($items->currentPage()+1) }}">Siguiente</a>
                      @endif
                  @endif
                </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="article_listing">
              <ul>
                @foreach(list_featured_blog_news(0,4) as $key => $item)
                <li>
                  <article> 
                      <a href="{{route('frontend_articles_show',['slug'=>$item->slug])}}">
                        <div class="left">
                          <figure> 
                            <img class="lazyautosizes img-responsive" src="{{$item->thumbnail}}" >
                          </figure>
                        </div>
                        <div class="right">
                          <h3 class="title">{{$item->title}}</h3>
                        </div>
                      </a>
                  </article>
                </li>
                @endforeach
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
