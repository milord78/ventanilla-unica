<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;
 
class CreateArticlesTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('autor_id')->nullable();
            $table->foreign('autor_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('publish')->default(true);
            $table->boolean('featured')->default(false);
            $table->boolean('on_feed')->default(true);
            $table->string('thumbnail');
            $table->string('featured_image');
            $table->string('title');
            $table->string('meta_title');
            $table->text('meta_description')->nullable();
            $table->text('slug');
            $table->text('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
