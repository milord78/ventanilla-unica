<?php

namespace  Modules\Articles\Http\Controllers\Admin;

use Modules\Articles\Entities\ArticlesCategories as PostsCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesCategoriesController extends Controller
{
  
    public function __construct()
    {

        $this->middleware('auth');
         $this->middleware('admin_check');
       
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $items = PostsCategories::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('articles::backend/articles/categories/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       
        $q = $request->input('title');

         if(!empty($q))
        {
            $items = PostsCategories::orderBy('id', 'desc')->where('title', $q )->paginate(15);
        }else
        {
            $items = PostsCategories::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('articles::backend/articles/categories/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = PostsCategories::find( $id);
        return view('articles::backend/articles/categories/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item= PostsCategories::find( $id);
      
       
        $route = route('admin_articles_categories_update',['id' => $item->id]);
        return view('articles::backend/articles/categories/edit',compact('item','route'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_articles_categories_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->title = null;
        $item->thumbnail = null;
        $item->meta_title = null;
        $item->meta_description = null;
        $item->slug = null;
        $item->publish = true;
 
        return view('articles::backend/articles/categories/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'meta_title' => 'required',

        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   


        $inder_data['publish']= (isset($inder_data['publish']))?true:false;

  

        if(PostsCategories::where(
            [
                ['slug','=',$inder_data['slug']],
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_articles_categories_edit",['id'=>$id]
            )->with('success','articles.categories.UPDATE_SUCESSFULLY_LABEL');
           

            PostsCategories::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_articles_categories_new")
                ->with('error','articles.categories.EMAIL_EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        PostsCategories::find($id)->delete();
        return redirect()->route("admin_articles_categories_index")
                        ->with('success','articles.categories.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'title' => 'required',
            'slug' => 'required',
            'meta_title' => 'required',
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
        $inder_data['publish']= (isset($inder_data['publish']))?true:false;
        if(PostsCategories::where('slug', $inder_data['slug'])->count() == 0)
        {
        
            $item= PostsCategories::create($inder_data);

            return redirect()->route("admin_articles_categories_edit",['id'=>$item->id])
                        ->with('success','articles.categories.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_articles_categories_new")
                        ->with('error','articles.categories.SLUG_EXISTS_LABEL');  
        }

    }

}
