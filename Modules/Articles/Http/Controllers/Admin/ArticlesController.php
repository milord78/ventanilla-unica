<?php

namespace Modules\Articles\Http\Controllers\Admin;
use Modules\Articles\Entities\Articles as PostsItems;
use Modules\Articles\Entities\ArticlesCategories as PostsCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ArticlesController extends Controller
{
  
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('admin_check');
       
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $items = PostsItems::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('articles::backend/articles/posts/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       
        $q = $request->input('title');

         if(!empty($q))
        {
            
            $items = PostsItems::orderBy('id', 'desc')->where('title', $q )->paginate(15);
        }else
        {
            $items = PostsItems::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('articles::backend/articles/posts/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = PostsItems::find( $id);
        return view('articles::backend/articles/posts/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = PostsItems::find( $id);
   
       
        $route = route('admin_articles_update',['id' => $item->id]);
        return view('articles::backend/articles/posts/edit',compact('item','route'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_articles_store');
        
        $item= new \stdClass();
        $item->id = null;
        $item->title = null;
        $item->content = null;
        $item->excerpt = null;
        $item->thumbnail = null;
        $item->thumbnail_text = null;
        $item->featured_image = null;
        $item->featured_image_text = null;
        $item->featured = false;
        $item->on_feed = true;
        $item->meta_title = null;
        $item->meta_description = null;
        $item->slug = null;
        $item->publish = true;
        $item->publish_date = null;
        $categories = PostsCategories::orderBy('title', 'asc')->get();


        return view(
            'articles::backend/articles/posts/edit',
            compact(
                'route',
                'item',
                'categories'
            )
        );
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
            'title'=>'required',
            'content'=>'required',
            'slug'=>'required',
            'meta_title'=>'required',
       
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   

        $inder_data['publish']= (isset($inder_data['publish']))?true:false;
        $inder_data['featured']= (isset($inder_data['featured']))?true:false;
        $inder_data['on_feed']= (isset($inder_data['on_feed']))?true:false;


        if(PostsItems::where(
            [
                ['slug','=',$inder_data['slug']],
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_articles_edit",['id'=>$id]
            )->with('success','articles.articles.UPDATE_SUCESSFULLY_LABEL');
           

            PostsItems::find($id)->update($inder_data);

            $cats = isset($inder_data['cats'])?$inder_data['cats']:[];
            $related = isset($inder_data['related'])?$inder_data['related']:[];
            $tags = isset($inder_data['tags'])?$inder_data['tags']:[];

     
       
            save_articles_categories_related($id,$cats);
            save_articles_tags_related($id,$tags);
            
            

            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_articles_new")
                ->with('error','articles.articles.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        PostsItems::find($id)->delete();
        return redirect()->route("admin_articles_index")
                        ->with('success','articles.articles.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
          
            'content'=>'required',
            'slug'=>'required',
            'meta_title'=>'required',
       
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
        $inder_data['publish']= (isset($inder_data['publish']))?true:false;
        $inder_data['featured']= (isset($inder_data['featured']))?true:false;
        $inder_data['on_feed']= (isset($inder_data['on_feed']))?true:false;
        $inder_data['in_offer']= (isset($inder_data['in_offer']))?true:false;
         $user_id = \Auth::user()->id;
            $inder_data['autor_id'] = $user_id;

        $inder_data['hits'] = 0;
        if(PostsItems::where('slug', $inder_data['slug'])->count() == 0)
        {
        
            $item= PostsItems::create($inder_data);
            $id = $item->id;
            $cats = isset($inder_data['cats'])?$inder_data['cats']:[];
            $related = isset($inder_data['related'])?$inder_data['related']:[];
            $tags = isset($inder_data['tags'])?$inder_data['tags']:[];

            

             
            save_articles_categories_related($id,$cats);
            save_articles_tags_related($id,$tags);
            

            return redirect()->route("admin_articles_edit",['id'=>$id])
                        ->with('success','articles.articles.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_articles_new")
                        ->with('error','articles.articles.EXISTS_LABEL');  
        }

    }

}
