<?php

namespace Modules\Articles\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Tags;
use Modules\Articles\Entities\ArticlesRelatedTags;

class PostsTagsController extends Controller
{
    //
    public function __construct()
    {
       
    }



    public function get_related(Request $request)
    {
        $id = $request->input('id');
        $items = ArticlesRelatedTags::where([['article_id','=',$id]])->get();
        $items_ids = [];
        foreach ($items as $key => $value) 
        {
            array_push($items_ids , $value->tags_id);
        }

       
     
        return response()->json( $items_ids);
    }
    

   
}
