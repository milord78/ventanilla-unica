<?php

namespace Modules\Articles\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Articles\Entities\ArticlesCategories;
use Modules\Articles\Entities\ArticlesRelatedCategories;
use Modules\Articles\Entities\Articles as PostsItems;

class ArticlesCategoriesController extends Controller
{
    //
    public function __construct()
    {
       
    }

    
    public function list(Request $request)
    {
    	
        $pages = $request->input('pages',150);
        $items = PostsCategories::orderBy('id', 'desc')->paginate($pages);
        return response()->json( $items);

    }


    public function search(Request $request)
    {   

        $q = $request->input('name');
        $exclude = $request->input('exclude');

        if( empty($exclude) && !empty($q))
        {
            $items = PostsCategories::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }

        if( !empty($exclude) && !empty($q))
        {
            $items = PostsCategories::where([
                ['name','=', $q],
                ['id','!=',  $exclude ],
            ])->paginate(15);
        }
          

        if( empty($exclude) && empty($q))
        {
            $items = PostsCategories::paginate(15);
        }

     
        return response()->json( $items);
       
    }
    
    public function get_related(Request $request)
    {
        $id = $request->input('id');
        $items = PostsRelatedCategories::where([['post_id','=',$id]])->get();
        $items_ids = [];
        foreach ($items as $key => $value) 
        {
            array_push($items_ids , $value->category_id);
        }
        return response()->json($items_ids);
    }
    
    public function posts_categories(Request $request)
    {
        $id = $request->input('category_id');
        $items = PostsRelatedCategories::where([['category_id','=',$id]])->get();
        $items_ids = [];


        foreach ($items as $key => $value) 
        {
            array_push($items_ids , $value->post_id);
        }
        
        $items = PostsItems::orderBy('id', 'desc')->whereIn('id',$items_ids)->paginate(15);
        return response()->json($items);
    }
    
    public function details(Request $request)
    {
        $id = $request->input('id');
        return response()->json( PostsCategories::find($id));
    }
}
