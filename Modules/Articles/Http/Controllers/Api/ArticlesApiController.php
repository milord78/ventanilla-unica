<?php

namespace Modules\Articles\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Articles\Entities\Articles as PostsItems;


class ArticlesApiController extends Controller
{
    //
    public function __construct()
    {
       
    }

    
    public function list(Request $request)
    {
    	
        $exclude = $request->input('exclude');
         if( !empty($exclude))
        {
            $items = PostsItems::orderBy('id', 'desc')->where([
                ['id','!=',  $exclude ],
            ])->paginate(15);
        }else{
            $items = PostsItems::orderBy('id', 'desc')->paginate(15);
        }
        
        return response()->json( $items);

    }


    public function search(Request $request)
    {   

        $q = $request->input('query');
        $exclude = $request->input('exclude');

        if( empty($exclude) && !empty($q))
        {
            $items = PostsItems::orderBy('id', 'desc')->where('title', $q )->paginate(15);
        }

        if( !empty($exclude) && !empty($q))
        {
            $items = PostsItems::orderBy('id', 'desc')->where([
                ['title','=', $q],
                ['id','!=',  $exclude ],
            ])->paginate(15);
        }
          

        if( empty($exclude) && empty($q))
        {
            $items = PostsItems::paginate(15);
        }

     
        return response()->json( $items);
       
    }
    

    
    public function details(Request $request)
    {
        $id = $request->input('id');
        return response()->json( PostsItems::find($id));
    }
 
   
}
