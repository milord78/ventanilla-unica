<?php

namespace Modules\Articles\Http\Controllers\Frontend;
use Modules\Articles\Entities\Articles as PostsItems;
use Modules\Articles\Entities\ArticlesCategories as PostsCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class ArticlesController extends Controller
{
  
    public function __construct()
    {

       
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $items = PostsItems::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('articles::frontend/articles/posts/list',compact('items','q'));
    }



    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function autor(Request $request,$slug)
    {
        $autor = User::where('nick',$slug)->get();
        if(count($autor)>0)
        {
            $autor = 0;
        }else
        {
            return view('errors_page.404_frontend',[]);
        }

        $items = PostsItems::leftJoin('users', 'posts.author_id', '=', 'users.id')->select('posts.*')->where('users.nick',$slug)->orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('articles::frontend/articles/posts/autor',compact('items','q','autor'));
    }

    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       
        $q = $request->input('title');

         if(!empty($q))
        {
            
            $items = PostsItems::orderBy('id', 'desc')->where('title', $q )->paginate(15);
        }else
        {
            $items = PostsItems::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('articles::frontend/articles/posts/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        $item = PostsItems::where('slug', $slug)->get();
        if(count($item )>0)
        {
            $item = $item[0];
        }else
        {
            return view('errors_page.404_frontend',[]);
        }
        return view('articles::frontend/articles/posts/show',compact('item'));
    }



}
