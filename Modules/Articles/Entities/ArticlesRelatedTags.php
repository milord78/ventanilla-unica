<?php

namespace Modules\Articles\Entities;

use Illuminate\Database\Eloquent\Model;

class ArticlesRelatedTags extends Model
{
   
  	protected $table = 'articles_related_tags';

    protected $fillable = [

		'tag_id',
		'article_id',
    ];     
}
