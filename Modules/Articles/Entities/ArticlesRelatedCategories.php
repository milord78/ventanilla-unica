<?php

namespace Modules\Pages\Entities;

use Illuminate\Database\Eloquent\Model;

class ArticlesRelatedCategories extends Model
{
  	protected $table = 'articles_related_categories';

    protected $fillable = [

		'category_id',
		'article_id',
    ];     
}
