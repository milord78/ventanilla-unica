<?php

namespace Modules\Articles\Entities;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
	protected $table = 'articles';

    protected $fillable = [
		'publish',
		'title',
		'slug',
		'meta_description',
		'meta_title',
		'content',
		'featured',
		'on_feed',
		'thumbnail',
		'featured_image',
		'autor_id',
    ]; 


    public function autor()
    {
        return $this->belongsTo('App\User','autor_id');
    }           
}
