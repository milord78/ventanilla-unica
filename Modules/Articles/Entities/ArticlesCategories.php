<?php

namespace Modules\Articles\Entities;

use Illuminate\Database\Eloquent\Model;

class ArticlesCategories extends Model
{
  	protected $table = 'articles_categories';

    protected $fillable = [
        'title',
        'publish',
        'slug',
        'meta_title',
        'meta_description',
    ];        
}
