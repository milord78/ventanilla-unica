<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('articles')->group(function() {
    Route::get('/', 'ArticlesController@index');
});


Route::prefix('api')->group(function() 
{
  
  	Route::prefix('blog')->group(function() 
  	{
		Route::prefix('categories')->group(function ()
		{
		  Route::post('/list', ['uses' => 'Api\ArticlesCategoriesController@list'])->name('admin_api_articles_categories_list');
		  Route::post('/search', ['uses' => 'Api\ArticlesCategoriesController@search'])->name('admin_api_articles_search_categories');
		  Route::post('/related', ['uses' => 'Api\ArticlesCategoriesController@get_related'])->name('admin_api_articles_search_categories_related');
		});

		Route::prefix('tags')->group(function ()
		{
		  Route::post('/related', ['uses' => 'Api\PostsTagsController@get_related'])->name('admin_api_articles_tags_related');
		});

		Route::prefix('articles')->group(function ()
		{
		  	Route::post('/list', ['uses' => 'Api\ArticlesApiController@list'])->name('admin_api_articles_list');
		  	Route::post('/search', ['uses' => 'Api\ArticlesApiController@search'])->name('admin_api_articles_search');
		});
	});
});


Route::prefix('admin')->group(function() 
{
  
  	Route::prefix('blog')->group(function() 
  	{

		Route::prefix('/categories')->group(function () 
		{
			Route::get('/index', ['uses' => 'Admin\ArticlesCategoriesController@index'])->name('admin_articles_categories_index');
			Route::get('/search', ['uses' => 'Admin\ArticlesCategoriesController@search'])->name('admin_articles_categories_search');
			Route::get('/show/{id}', ['uses' => 'Admin\ArticlesCategoriesController@show'])->name('admin_articles_categories_show');
			Route::get('/new', ['uses' => 'Admin\ArticlesCategoriesController@new'])->name('admin_articles_categories_new');
			Route::get('/edit/{id}', ['uses' => 'Admin\ArticlesCategoriesController@edit'])->name('admin_articles_categories_edit');
			Route::post('/store', ['uses' => 'Admin\ArticlesCategoriesController@store'])->name('admin_articles_categories_store');
			Route::post('/update/{id}', ['uses' => 'Admin\ArticlesCategoriesController@update'])->name('admin_articles_categories_update');
			Route::get('/delete/{id}', ['uses' => 'Admin\ArticlesCategoriesController@delete'])->name('admin_articles_categories_delete');
		});
		
		// articles routes
		Route::prefix('/articles')->group(function () 
		{
			Route::get('/index', ['uses' => 'Admin\ArticlesController@index'])->name('admin_articles_index');
			Route::get('/search', ['uses' => 'Admin\ArticlesController@search'])->name('admin_articles_search');
			Route::get('/show/{id}', ['uses' => 'Admin\ArticlesController@show'])->name('admin_articles_show');
			Route::get('/new', ['uses' => 'Admin\ArticlesController@new'])->name('admin_articles_new');
			Route::get('/edit/{id}', ['uses' => 'Admin\ArticlesController@edit'])->name('admin_articles_edit');
			Route::post('/store', ['uses' => 'Admin\ArticlesController@store'])->name('admin_articles_store');
			Route::post('/update/{id}', ['uses' => 'Admin\ArticlesController@update'])->name('admin_articles_update');
			Route::get('/delete/{id}', ['uses' => 'Admin\ArticlesController@delete'])->name('admin_articles_delete');
		});
		
	});
});

Route::prefix('/blog')->group(function () 
{
	Route::get('/', ['uses' => 'Frontend\ArticlesController@index'])->name('frontend_articles_index');
	Route::get('/search', ['uses' => 'Frontend\ArticlesController@search'])->name('frontend_articles_search');
	Route::get('/noticia/{slug}', ['uses' => 'Frontend\ArticlesController@show'])->name('frontend_articles_show');
	Route::get('/autor/{slug}', ['uses' => 'Frontend\ArticlesController@autor'])->name('frontend_articles_autor');
});