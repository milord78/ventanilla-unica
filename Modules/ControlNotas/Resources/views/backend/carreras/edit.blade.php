@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">


       <h3  style="margin: 15px" class="box-title">  
          {{ __('controlnotas::rating.carreras.CARRERAS_LABEL') }}
          @if (empty($item->id))
            {{ __('general.ADD_LABEL') }}
          @else
           {{ __('general.EDIT_LABEL') }}
          @endif
       </h3>
 
      <form role="form" method="post" action="{{$route}}">
           @csrf
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
              
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                    <div style="margin: 15px">
                      
                       <div class="form-group">
                        <label>{{ __('controlnotas::rating.carreras.CARRERA_LABEL') }}</label>
                        <input type="text" name="carrera"  value="{{$item->carrera}}" required class="form-control" >
                        <br>
                       </div>

                       <div class="form-group">
                          <label>{{ __('controlnotas::rating.plantel.PLANTEL_LABEL') }}</label>
                          <select name="plantel_id" class="form-control">
                            @foreach ($planteles as $key => $value)  
                              <option @if($item->plantel_id==$value->id) selected="selected" @endif value="{{$value->id}}">{{$value->nombre}}</option>
                            @endforeach
                          </select>
                          <br>
                       </div>

                  
                    </div>
                </div>
      
              </div>

              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }} Carrera</button>
            <a href="{{ route('admin_carreras_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
            @if (!empty($item->id))
              <a href="{{ route('admin_carreras_delete',['id' => $item->id]) }}" class="btn btn-danger">
                {{ __('general.DELETE_LABEL') }} Carrera
              </a>
            @endif
        </div>

      </form>
   
    
  </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">

  string_to_slug = function (str) 
  {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;~#";
    var to   = "aaaaeeeeiiiioooouuuunc--------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
  }
  var ready = function(e)
  { 


    copy_title = function(e)
    {
      value = $(this).val()
      
      $("input[name=slug]").val(string_to_slug(value));
      $("input[name=meta_title]").val(value);

      
    }
    $("input[name=name]").on('keyup',copy_title);
  } 
  $(window).on('load',ready);

</script>
@endsection
