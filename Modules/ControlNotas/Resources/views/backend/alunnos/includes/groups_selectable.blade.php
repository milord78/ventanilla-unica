<section class="content"  id="users_selectlable">
    <!-- COLOR PALETTE -->
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
            <h3 class="box-title"> 
            Aspirante
            </h3>
     
        </div>    

      <div class="box-body">
        
          
        <div class="row">
            <div class="col-md-12">
             
            </div>
        </div>

              

        <table class="table">
            <thead>
                <tr>
                   
                    <th>{{ __('general.NAME_LABEL') }}</th>
                    <th>{{ __('Email') }}</th>
                    <th width="10%">{{ __('general.ACTIONS_LABEL') }}</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tbody >
              
                <tr v-if="cats.length > 0 " v-for="item in cats">
                    @verbatim
           
                   
                    <td>
                    <input :checked=item.selected  type="checkbox"  class="hide" :value=item.id name="groups[]" v-on:click="set_select_item(item)">
                        <strong>{{item.firstname}} </strong>
                    </td>
                    <td>
                        <strong>{{item.email}} </strong>
                    </td>
                 
                    @endverbatim
                    <td>
                        <a v-if="item.selected == true"  v-on:click="set_select_item(item)" class="btn btn-warning" >
                            {{__('general.SELECTED_LABEL') }}  
                        </a>
                        <a v-if="item.selected == false" v-on:click="set_select_item(item)"  class="btn btn-primary " >
                       
                            {{__('general.SELECT_LABEL') }} Aspirante
                        </a>
                        <br>
                        <a  :href=item.link_details class="btn btn-success " >
                       
                            {{__('Ver ficha') }} Aspirante
                        </a>
                      
                    </td>
                </tr>
             
            </tbody>
        </table>

        <div >
            <button v-if="next_page_cats!=null"  v-on:click="load_more()"  class="btn btn-block btn-primary" type="button">
                {{__('general.LOAD_MORE_LABEL') }} 
                   
            </button>
        </div>
         

      
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
</section>
