@extends('layouts.backend')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">
      <div class="box-header" style="padding: 15px">
        <h3 class="box-title">Aspirantes inscritos</h3>
        <div class="box-tools">
          <form method="get" action="{{route('admin_alunnos_search')}}">
            <div class="input-group input-group-sm" style="width: 550px;">
               <input type="text" name="name" value="{{$q}}" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }} aspirantes inscritos">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                <a href="{{ route('admin_alunnos_new') }}" class="btn btn-primary">
                Inscribir Aspirantes 
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
        <table class="table">
          <thead>
            <tr>
              <th class="col2"># </th>
            
              <th>{{ __('controlnotas::rating.alunnos.FISTNAME_LABEL') }}</th>
              <th>{{ __('controlnotas::rating.alunnos.LASTNAME_PAT_LABEL') }}</th>
              <th>{{ __('controlnotas::rating.alunnos.LASTNAME_MAT_LABEL') }}</th>
              <th>{{ __('controlnotas::rating.alunnos.CARRERA_LABEL') }}</th>
              <th>{{ __('controlnotas::rating.ciclos.CICLO_LABEL') }}</th>
              <th>{{ __('controlnotas::rating.estado.ESTADO_LABEL') }}</th>
              <th>{{ __('Semestre') }}</th>
              <th>{{ __('controlnotas::rating.grupo.GRUPO_LABEL') }}</th>
                
              <th  style="width: 60px">{{ __('general.ACTIONS_LABEL') }}</th>
            </tr>
          </thead>
          <tbody>
            @foreach($items  as $index => $item)
            <tr>
              <td>{{$item->user()->firstname}}</td>
              <td>{{$item->user()->midname}}</td>
              <td>{{$item->user()->lastname}}}</td>
              <td>{{$item->carrera()->carrera}}</td>
              <td>{{$item->ciclo()->ciclo}}</td>
              <td>{{$item->estado()->estado}}</td>
              <td>{{$item->semestre}}</td>
              <td>
                <a href="{{ route('admin_alunnos_edit',['id'=>$item['id']]) }}" class="btn btn-block btn-success">{{ __('general.EDIT_LABEL') }}</a>
                <a href="{{ route('admin_alunnos_delete',['id'=>$item['id']]) }}" class="btn btn-block btn-danger">{{ __('general.DELETE_LABEL') }}</a>
              </td>
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>

@endsection
