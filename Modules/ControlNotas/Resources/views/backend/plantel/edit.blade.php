@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">


       <h3  style="margin: 15px" class="box-title">  
       @if (empty($item->id))
           Configurar
          @else
           {{ __('general.EDIT_LABEL') }}
          @endif
          {{ __('controlnotas::rating.plantel.PLANTEL_LABEL') }}
        
       </h3>
 
      <form role="form" method="post" action="{{$route}}">
           @csrf
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
            
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                    <div style="margin: 15px">
                      
                    <div class="row">
                        <div class="col-md-3">
                            <div class="avatar" style="background-image:url({{$item->logos()}}); background-size: cover;height: 200px;width: 100%; border-radius: 5px;">
                            </div>
                            <br>
                        </div>
                        <div class="col-md-8">
                            
                          <label for="exampleInputEmail1">{{__('Logo') }}</label>
                          <input type="file" name="thumbnail">
                        
                        </div>
                    </div>



                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.NAME_LABEL') }}</label>
                        <input type="text" name="nombre"  value="{{$item->nombre}}" required class="form-control" >
                        <br>
                      </div>
                      

                      <div class="form-group">
                        <label for="exampleInputEmail1">{{ __('portada del registro de aspirantes') }}</label>
                        <textarea  name="description" id="mytextarea"  class="hide form-control"  >{{$item->description}}</textarea>
                      </div> 
                  


                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.TYPE_LABEL') }}</label>
                        <select name="tipo" class="form-control">
                          @foreach ($tipos as $key => $value)  
                            <option @if($item->tipo==$value) selected="selected" @endif value="{{$value}}">{{($value)}}</option>
                          @endforeach
                        </select>
                        <br>
                      </div>
                      
                      <div class="form-group">
                        <label>Número</label>
                        <select name="numero" class="form-control">
                          @foreach ($numeros as $key => $value)  
                            <option @if($item->numero==$value) selected="selected" @endif value="{{$value}}">{{($value)}}</option>
                          @endforeach
                        </select>
                        <br>
                      </div>
                      
                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.TURNO_LABEL') }}</label>
                       

                          @foreach ($turnos as $key => $value)  
                          <div>
                            <input type="radio" name="turno"  @if($item->turno==$value) checked="checked" @endif  value="{{$value}}" />
                            <label>{{(ucfirst($value))}}</label>
                            <br>
                          </div>
   
                          @endforeach
                        
                        <br>
                      </div>
    
                      
                                                             
                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.CLAVE_LABEL') }}</label>
                        <input type="text" name="contrasena"  value="{{$item->contrasena}}" required class="form-control" >
                        <br>
                      </div>
                                 
                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.FECHA_TERMINO_DE_SOLICITUD_LABEL') }}</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="fecha_termino_de_solicitud" class="form-control pull-right datepicker" id="datepicker" value="{{$item->fecha_termino_de_solicitud}}" >
                        </div>
                                
                        


                        <br>
                       </div>
                       <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.FECHA_INICIO_SEMESTRAL_LABEL') }}</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="fecha_inicio_semestral" class="form-control pull-right datepicker" id="datepicker3" value="{{$item->fecha_inicio_semestral}}" >
                        </div>
                                
                        
                        <br>
                       </div>
                       <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.FECHA_TERMINO_SEMESTRAL_LABEL') }}</label>
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" name="fecha_termino_semestral" class="form-control pull-right datepicker" id="datepicker3" value="{{$item->fecha_termino_semestral}}" >
                        </div>
                                
                        
                        <br>
                       </div>     
                      




                                        
                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.POSTAL_ADDRESS_LABEL') }}</label>
                        <input type="text" name="direccion_postal"  value="{{$item->direccion_postal}}" required class="form-control" >
                        <br>
                      </div>
                                            
                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.PHONE_LABEL') }}</label>
                        <input type="text" name="telefono"  value="{{$item->telefono}}" required class="form-control" >
                        <br>
                      </div>
                                            
                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.MOBILE_PHONE_LABEL') }}</label>
                        <input type="text" name="telefono_movil"  value="{{$item->telefono_movil}}" required class="form-control" >
                        <br>
                      </div>
                                            
                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.EMAIL_LABEL') }}</label>
                        <input type="text" name="email"  value="{{$item->email}}" required class="form-control" >
                        <br>
                      </div>     


                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.WEBSITE_LABEL') }}</label>
                        <input type="text" name="website"  value="{{$item->website}}" required class="form-control" >
                        <br>
                      </div>     

                      <div class="form-group">
                        <label>{{ __('controlnotas::rating.plantel.FAN_PAGE_FACEBOOK_LABEL') }}</label>
                        <input type="text" name="fan_page_facebook"  value="{{$item->fan_page_facebook}}" required class="form-control" >
                        <br>
                      </div>     
 


                      <div class="box-body">
                       
                        <button id="aviso_de_privacidad" type="button" class="btn btn-primary">{{__('controlnotas::rating.plantel.CARGAR_AVISO_DE_PRIVACIDAD_LABEL') }}</button>
                        <input type="file" class="hide" name="aviso_de_privacidad">
                        
                        @if($item->aviso_de_privacidad!=null) 
                        

                        <a href="{{$item->aviso_de_privacidad}}" class="btn btn-primary">
                          {{__('general.SEE_LABEL') }}&nbsp;
                         {{__('controlnotas::rating.plantel.CARGAR_AVISO_DE_PRIVACIDAD_LABEL') }}
                        </a>  
                        @endif  

                      

                      </div>

                      


                      <div class="box-body">
                        <button id="reglamento_del_plantel" type="button" class="btn btn-primary">{{__('controlnotas::rating.plantel.CARGAR_REGLAMENTO_DEL_PLANEL_LABEL') }}</button>
     
                        @if($item->reglamento_del_plantel!=null) 
                        <a href="{{$item->reglamento_del_plantel}}" class="btn btn-primary">
                          {{__('general.SEE_LABEL') }}&nbsp;
                         {{__('controlnotas::rating.plantel.CARGAR_REGLAMENTO_DEL_PLANEL_LABEL') }}
                        </a>  
                        @endif  
                        <input type="file" class="hide" name="reglamento_del_plantel">
                      </div>

                      



                      <div class="box-body">
                        <button id="firma_de_reglamento" type="button" class="btn btn-primary">{{__('controlnotas::rating.plantel.CARGAR_FIRMA_DE_REGLAMENTO_LABEL') }}</button>
     
                        @if($item->firma_de_reglamento!=null) 
                        <a href="{{$item->firma_de_reglamento}}" class="btn btn-primary">
                          {{__('general.SEE_LABEL') }}&nbsp;
                         {{__('controlnotas::rating.plantel.CARGAR_FIRMA_DE_REGLAMENTO_LABEL') }}
                        </a>  
                        @endif  
                        <input type="file" class="hide" name="firma_de_reglamento">
                      </div>

                      <div class="box-body">
                        <button id="firma_de_authorizacion" type="button" class="btn btn-primary">{{__('controlnotas::rating.plantel.CARGAR_FIRMA_DE_AUTORIZACION_LABEL') }}</button>
     
                        @if($item->firma_de_authorizacion!=null) 
                        <a href="{{$item->firma_de_authorizacion}}" class="btn btn-primary">
                          {{__('general.SEE_LABEL') }}&nbsp;
                         {{__('controlnotas::rating.plantel.CARGAR_FIRMA_DE_AUTORIZACION_LABEL') }}
                        </a>  
                        @endif  
                        <input type="file" class="hide" name="firma_de_authorizacion">
                      </div>

                      <div class="box-body">
                        <button id="firma_de_veracidad" type="button" class="btn btn-primary">{{__('controlnotas::rating.plantel.CARGAR_FIRMA_DE_VERACIDAD_LABEL') }}</button>
     
                        @if($item->firma_de_veracidad!=null) 
                        <a href="{{$item->firma_de_veracidad}}" class="btn btn-primary">
                          {{__('general.SEE_LABEL') }}&nbsp;
                         {{__('controlnotas::rating.plantel.CARGAR_FIRMA_DE_VERACIDAD_LABEL') }}
                        </a>  
                        @endif  
                        <input type="file" class="hide" name="firma_de_veracidad">
                      </div>



                    </div>
                </div>
           
              </div>

              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>
       
        </div>

      </form>
   
    
  </div>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script type="text/javascript">

  var ready = function(e)
  { 
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      language: 'es'
    });

    var aviso_de_privacidad = function(e){
      $("input[name=aviso_de_privacidad]").trigger('click');
    }
    $("#aviso_de_privacidad").on('click',aviso_de_privacidad)

    var reglamento_del_plantel = function(e){
      $("input[name=reglamento_del_plantel]").trigger('click');
    }
    $("#reglamento_del_plantel").on('click',reglamento_del_plantel)


    var firma_de_veracidad = function(e){
      $("input[name=firma_de_veracidad]").trigger('click');
    }
    $("#firma_de_veracidad").on('click',firma_de_veracidad)


    var firma_de_authorizacion = function(e){
      $("input[name=firma_de_authorizacion]").trigger('click');
    }
    $("#firma_de_authorizacion").on('click',firma_de_authorizacion)


    var firma_de_reglamento = function(e){
      $("input[name=firma_de_reglamento]").trigger('click');
    }
    $("#firma_de_reglamento").on('click',firma_de_reglamento)


    


     
   
  } 
  $(window).on('load',ready);

  tinymce.init({
      selector: '#mytextarea',
      height: 600
    });
</script>
@endsection
