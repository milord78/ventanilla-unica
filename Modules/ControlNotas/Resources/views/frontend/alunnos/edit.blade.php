@extends('layouts.user_panel')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">


       <h3  style="margin: 15px" class="box-title">  
         
          @if (empty($item->id))
           Registrar nueva inscripcion escolar
          @else
           {{ __('general.EDIT_LABEL') }}  inscripcion escolar
          @endif
       </h3>
 
      <form role="form" method="post" action="{{$route}}">
           @csrf
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
            
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                    <div style="margin: 25px">
                  

                       <div class="form-group">
                        <label>{{ __('controlnotas::rating.alunnos.CARRERA_LABEL') }}</label>
                        <select name="carrera_id" class="form-control">
                          @foreach ($carreras as $key => $value)  
                            <option @if($item->carrera_id==$value->id) selected="selected" @endif value="{{$value->id}}">{{($value->carrera)}}</option>
                          @endforeach
                        </select>
                        <br>
                       </div>

                       <div class="form-group">
                        <label>{{ __('controlnotas::rating.alunnos.CICLO_LABEL') }}</label>
                        <select name="ciclo_id" class="form-control">
                          @foreach ($ciclos as $key => $value)  
                            <option @if($item->ciclo_id==$value->id) selected="selected" @endif value="{{$value->id}}">{{($value->ciclo)}}</option>
                          @endforeach
                        </select>
                        
                        <br>
                       </div>

                       <div class="form-group">
                        <label>Seleccionar Semestre</label>
                        <select name="semestre" class="form-control">
                          @for ($i=1; $i<=6;$i++)  
                            <option @if($item->semestre==$i) selected="selected" @endif value="{{$i}}">{{($i)}} Semestre</option>
                          @endfor
                        </select>
                        
                        <br>
                       </div>




                              
                       <div class="form-group">
                        <label>{{ __('controlnotas::rating.alunnos.ID_GRUPO_LABEL') }}</label>

                        <select name="grupo_id" class="form-control">
                          @foreach ($grupos as $key => $value)  
                            <option @if($item->grupo_id==$value->id) selected="selected" @endif value="{{$value->id}}">{{($value->grupo)}}</option>
                          @endforeach
                        </select>
                        
       
                        <br>

                       </div>
                      
              

                       
                       <div class="form-group">
                          <label>{{ __('controlnotas::rating.alunnos.TURNO_LABEL') }}</label>
                          <select name="turno" class="form-control">
                            @foreach ($turnos as $key => $value)  
                              <option @if($item->turno==$value['id']) selected="selected" @endif value="{{$value['id']}}">{{__($value['label'])}}</option>
                            @endforeach
                          </select>
                          <br>
                       </div>
                    </div>
                </div>
       
              </div>

              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">

        
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>
            <a href="{{ route('frontend_alunnos_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
            @if (!empty($item->id))
              <a href="{{ route('frontend_alunnos_delete',['id' => $item->id]) }}" class="btn btn-danger">
                {{ __('general.DELETE_LABEL') }}
              </a>
            @endif
        </div>

      </form>
   
    
  </div>
@endsection
