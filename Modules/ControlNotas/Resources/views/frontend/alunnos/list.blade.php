@extends('layouts.user_panel')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">
      <div class="box-header" style="padding: 15px">
        <h3 class="box-title">Semestres cursados</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
        <table class="table">
          <thead>
            <tr>
              <th class="col2"># </th>
              

            
              <th>{{ __('controlnotas::rating.alunnos.CARRERA_LABEL') }}</th>
              <th>{{ __('controlnotas::rating.ciclos.CICLO_LABEL') }}</th>
              <th>{{ __('controlnotas::rating.estado.ESTADO_LABEL') }}</th>
              <th>{{ __('Semestre') }}</th>
              <th>{{ __('controlnotas::rating.grupo.GRUPO_LABEL') }}</th>
                

         
            </tr>
          </thead>
          <tbody>
            @foreach($items  as $index => $item)
            <tr>
              <td>{{$item['id']}}</td>
              <td>{{$item->carrera()->carrera}}</td>
              <td>{{$item->ciclo()->ciclo}}</td>
              <td>{{$item->estado()->estado}}</td>
              <td>{{$item->semestre}}</td>
             
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>

@endsection
