<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 

    'alunnos'=>[
        'ALUNNOS_LABEL'                         =>  'Alunnos',
        'ALUNNO_LABEL'                          =>  'Alunno',
        'CREATED_SUCCESSFULLY_LABEL'            =>  'Alunno creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'              =>  'Alunno actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'             =>  'Alunno borrado exitosamente',
        'EXISTS_LABEL'                          =>  'Alunno existe',

        'FISTNAME_LABEL'                        =>  'Nombre',
        'LASTNAME_PAT_LABEL'                    =>  'Apellido Paterno',
        'LASTNAME_MAT_LABEL'                    =>  'Apellido Materno',
        'CARRERA_LABEL'                         =>  'Carrera',
        'CICLO_LABEL'                           =>  'Ciclo',
        'FECHA_LABEL'                           =>  'Fecha',
        'HORA_LABEL'                            =>  'Hora',
        'ID_GRUPO_LABEL'                        =>  'Grupo',
        'FECHA_NAC_LABEL'                       =>  'Fecha Nacimiento',
        'SEXO_LABEL'                            =>  'Sexo',
        'TURNO_LABEL'                           =>  'Turno',
        'SEXO_HOMBRE_LABEL'                     =>  'Masculino',
        'SEXO_MUJER_LABEL'                      =>  'Femenino',
        'MATUTINO_LABEL'                        =>  'Matutino',
        'VESPERTINO_LABEL'                      =>  'Vespertino',
        'SEXO_MUJER_LABEL'                      =>  'Femenino',
        'ESTADO_CIVIL_LABEL'                    =>  'Estado Civil',
        'SOLTERO_LABEL'                         =>  'Soltero',
        'CASADO_LABEL'                          =>  'Casado',
        'UNION_LIBRE_LABEL'                     =>  'Unión Libre',
        'DIVORCIADO_LABEL'                      =>  'Divorciado',
        'VIUDO_LABEL'                           =>  'Viudo',

    ],

    'avanzado'=>[
        'AVANZADO_LABEL'                        =>  'Avanzado',
        'AVANZADOS_LABEL'                       =>  'Avanzado',
        'CREATED_SUCCESSFULLY_LABEL'            =>  'Alunno creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'              =>  'Alunno actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'             =>  'Alunno borrado exitosamente',
        'EXISTS_LABEL'                          =>  'Alunno existe',

        'EDO_NAC_LABEL'                         =>  'Edo Nac',
        'LOCA_NAC_LABEL'                        =>  'Loca Nac',
        'MINI_NAC_LABEL'                        =>  'Mini NaC',
        'CURP_LABEL'                            =>  'Curp',
        'CONSTANCIAS_LABEL'                     =>  'Contancias',
        'EDO_ASP_LABEL'                         =>  'Estado',
        'MUNI_ASP_LABEL'                        =>  'Municipio',
        'MODALIDAD_LABEL'                       =>  'Modalidad',
        'REGIMEN_LABEL'                         =>  'Regimen',
        'EDO_PRO_LABEL'                         =>  'Estado de Procedencia',
        'MUNI_PRO_LABEL'                        =>  'Municipio de Procedencia',
        
        
    ],



    'carreras'=>[
        'CARRERA_LABEL'                         =>  'Carrera',
        'CARRERAS_LABEL'                        =>  'Carreras',
        'CREATED_SUCCESSFULLY_LABEL'            =>  'Carrera creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'              =>  'Carrera actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'             =>  'Carrera borrado exitosamente',
        'EXISTS_LABEL'                          =>  'Carrera existe',
        
    ],


    'ciclos'=>[
        'CICLO_LABEL'                           =>  'Ciclo',
        'CICLOS_LABEL'                          =>  'Ciclos',
        'CREATED_SUCCESSFULLY_LABEL'            =>  'Ciclo creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'              =>  'Ciclo actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'             =>  'Ciclo borrado exitosamente',
        'EXISTS_LABEL'                          =>  'Ciclo existe',
        'ACTIVE_LABEL'                          =>  'Activo',
    ],

    'estado'=>[
        'ESTADO_LABEL'                          =>  'Estado',
        'ESTADOS_LABEL'                         =>  'Estados',
        'CREATED_SUCCESSFULLY_LABEL'            =>  'Estado creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'              =>  'Estado actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'             =>  'Estado borrado exitosamente',
        'EXISTS_LABEL'                          =>  'Estado existe',
    ],

    'grupo'=>[
        'GRUPO_LABEL'                           =>  'Grupo',
        'GRUPOS_LABEL'                          =>  'Grupos',
        'CREATED_SUCCESSFULLY_LABEL'            =>  'Grupo creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'              =>  'Grupo actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'             =>  'Grupo borrado exitosamente',
        'EXISTS_LABEL'                          =>  'Grupo existe',
    ],


    'plantel'=>[
        'PLANTEL_LABEL'                         =>  'Plantel',
        'PLANTELS_LABEL'                        =>  'Planteles',
        'CREATED_SUCCESSFULLY_LABEL'            =>  'Plantel creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'              =>  'Plantel actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'             =>  'Plantel borrado exitosamente',
        'EXISTS_LABEL'                          =>  'Plantel existe',
        'NAME_LABEL'                            =>  'Nombre',
        'POSTAL_ADDRESS_LABEL'                  =>  'Dirección Postal',
        'PHONE_LABEL'                           =>  'Teléfono',
        'MOBILE_PHONE_LABEL'                    =>  'Teléfono Movil',
        'EMAIL_LABEL'                           =>  'Email',
        'WEBSITE_LABEL'                         =>  'Sitio web',
        'FAN_PAGE_FACEBOOK_LABEL'               =>  'Fan page',
        'CLAVE_LABEL'                           =>  'Clave Plantel (C.C.T.)',
        'FECHA_TERMINO_DE_SOLICITUD_LABEL'      =>  'Fecha término de solicitud',
        'FECHA_INICIO_SEMESTRAL_LABEL'          =>  'Fecha inicio semestral',
        'FECHA_TERMINO_SEMESTRAL_LABEL'         =>  'Fecha término semestral',

        'CARGAR_AVISO_DE_PRIVACIDAD_LABEL'      =>  'Cargar aviso de privacidad',
        'CARGAR_REGLAMENTO_DEL_PLANEL_LABEL'    =>  'Cargar Reglamento del plantel',

        'CARGAR_FIRMA_DE_REGLAMENTO_LABEL'      =>  'Cargar Firma de Reglamento',
        'CARGAR_FIRMA_DE_AUTORIZACION_LABEL'    =>  'Cargar Firma de Autorizacion',
        'CARGAR_FIRMA_DE_VERACIDAD_LABEL'       =>  'Cargar Firma de Veracidad',
        
        'TURNO_LABEL'                           => 'Turno',
        'NUMBER_LABEL'                          => 'Number',
        'TYPE_LABEL'                            => 'Tipo',

    ],


    'tramites'=>[
        'TRAMITES_LABEL'                         =>  'Tramites',
        'TRAMITE_LABEL'                          =>  'Tramite',
        'CREATED_SUCCESSFULLY_LABEL'             =>  'Tramite creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'               =>  'Tramite actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'              =>  'Tramite borrado exitosamente',
        'EXISTS_LABEL'                           =>  'Tramite existe',
    ],
    'examentes'=>[
        'TRAMITES_LABEL'                         =>  'Tramites',
        'TRAMITE_LABEL'                          =>  'Tramite',
        'CREATED_SUCCESSFULLY_LABEL'             =>  'Tramite creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'               =>  'Tramite actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'              =>  'Tramite borrado exitosamente',
        'EXISTS_LABEL'                           =>  'Tramite existe',
    ],




];
