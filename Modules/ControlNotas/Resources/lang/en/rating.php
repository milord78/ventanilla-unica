<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 



    'alunnos'=>[
                
        'ALUNNOS_LABEL'                     =>  'Alunnos',
        'ALUNNO_LABEL'                      =>  'Alunno',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Alunno  created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Alunno  updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Alunno  deleted successfully',
        'EXISTS_LABEL'                      =>  'Alunno  exists',
     
    
    ],
    
];
