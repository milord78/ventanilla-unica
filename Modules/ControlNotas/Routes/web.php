<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('tags')->group(function() {
    Route::get('/', 'TagsController@index');
});

Route::prefix('api')->group(function() {
	Route::prefix('tags')->group(function ()
	{
	  
	  Route::post('/list', ['uses' => 'Api\TagsController@list'])->name('admin_api_blog_tags_list');
	  
	  Route::post('/search', ['uses' => 'Api\TagsController@search'])->name('admin_api_blog_search_tags');

	  Route::post('/get_related', ['uses' => 'Api\TagsController@get_related'])->name('admin_api_blog_tags_related');


	});
});



Route::prefix('admin')->group(function() {


   	Route::prefix('/alumnos')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Admin\AlunnosController@index'])->name('admin_alunnos_index');
		Route::get('/search', ['uses' => 'Admin\AlunnosController@search'])->name('admin_alunnos_search');
		Route::get('/show/{id}', ['uses' => 'Admin\AlunnosController@show'])->name('admin_alunnos_show');
		Route::get('/new', ['uses' => 'Admin\AlunnosController@new'])->name('admin_alunnos_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\AlunnosController@edit'])->name('admin_alunnos_edit');
		Route::post('/store', ['uses' => 'Admin\AlunnosController@store'])->name('admin_alunnos_store');
		Route::post('/update/{id}', ['uses' => 'Admin\AlunnosController@update'])->name('admin_alunnos_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\AlunnosController@delete'])->name('admin_alunnos_delete');
	
	});

	Route::prefix('/plantel')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Admin\PlantelController@index'])->name('admin_plantel_index');
		Route::get('/search', ['uses' => 'Admin\PlantelController@search'])->name('admin_plantel_search');
		Route::get('/show/{id}', ['uses' => 'Admin\PlantelController@show'])->name('admin_plantel_show');
		Route::get('/new', ['uses' => 'Admin\PlantelController@new'])->name('admin_plantel_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\PlantelController@edit'])->name('admin_plantel_edit');
		Route::post('/store', ['uses' => 'Admin\PlantelController@store'])->name('admin_plantel_store');
		Route::post('/update/{id}', ['uses' => 'Admin\PlantelController@update'])->name('admin_plantel_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\PlantelController@delete'])->name('admin_plantel_delete');
	
	});

	Route::prefix('/avanzados')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Admin\AvanzadosController@index'])->name('admin_avanzados_index');
		Route::get('/search', ['uses' => 'Admin\AvanzadosController@search'])->name('admin_avanzados_search');
		Route::get('/show/{id}', ['uses' => 'Admin\AvanzadosController@show'])->name('admin_avanzados_show');
		Route::get('/new', ['uses' => 'Admin\AvanzadosController@new'])->name('admin_avanzados_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\AvanzadosController@edit'])->name('admin_avanzados_edit');
		Route::post('/store', ['uses' => 'Admin\AvanzadosController@store'])->name('admin_avanzados_store');
		Route::post('/update/{id}', ['uses' => 'Admin\AvanzadosController@update'])->name('admin_avanzados_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\AvanzadosController@delete'])->name('admin_avanzados_delete');
	
	});

	Route::prefix('/carreras')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Admin\CarrerasController@index'])->name('admin_carreras_index');
		Route::get('/search', ['uses' => 'Admin\CarrerasController@search'])->name('admin_carreras_search');
		Route::get('/show/{id}', ['uses' => 'Admin\CarrerasController@show'])->name('admin_carreras_show');
		Route::get('/new', ['uses' => 'Admin\CarrerasController@new'])->name('admin_carreras_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\CarrerasController@edit'])->name('admin_carreras_edit');
		Route::post('/store', ['uses' => 'Admin\CarrerasController@store'])->name('admin_carreras_store');
		Route::post('/update/{id}', ['uses' => 'Admin\CarrerasController@update'])->name('admin_carreras_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\CarrerasController@delete'])->name('admin_carreras_delete');
	
	});


	Route::prefix('/ciclos')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Admin\CiclosController@index'])->name('admin_ciclos_index');
		Route::get('/search', ['uses' => 'Admin\CiclosController@search'])->name('admin_ciclos_search');
		Route::get('/show/{id}', ['uses' => 'Admin\CiclosController@show'])->name('admin_ciclos_show');
		Route::get('/new', ['uses' => 'Admin\CiclosController@new'])->name('admin_ciclos_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\CiclosController@edit'])->name('admin_ciclos_edit');
		Route::post('/store', ['uses' => 'Admin\CiclosController@store'])->name('admin_ciclos_store');
		Route::post('/update/{id}', ['uses' => 'Admin\CiclosController@update'])->name('admin_ciclos_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\CiclosController@delete'])->name('admin_ciclos_delete');
	
	});

	Route::prefix('/estados')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Admin\EstadosController@index'])->name('admin_estados_index');
		Route::get('/search', ['uses' => 'Admin\EstadosController@search'])->name('admin_estados_search');
		Route::get('/show/{id}', ['uses' => 'Admin\EstadosController@show'])->name('admin_estados_show');
		Route::get('/new', ['uses' => 'Admin\EstadosController@new'])->name('admin_estados_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\EstadosController@edit'])->name('admin_estados_edit');
		Route::post('/store', ['uses' => 'Admin\EstadosController@store'])->name('admin_estados_store');
		Route::post('/update/{id}', ['uses' => 'Admin\EstadosController@update'])->name('admin_estados_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\EstadosController@delete'])->name('admin_estados_delete');
	
	});


	Route::prefix('/grupos')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Admin\GruposController@index'])->name('admin_grupos_index');
		Route::get('/search', ['uses' => 'Admin\GruposController@search'])->name('admin_grupos_search');
		Route::get('/show/{id}', ['uses' => 'Admin\GruposController@show'])->name('admin_grupos_show');
		Route::get('/new', ['uses' => 'Admin\GruposController@new'])->name('admin_grupos_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\GruposController@edit'])->name('admin_grupos_edit');
		Route::post('/store', ['uses' => 'Admin\GruposController@store'])->name('admin_grupos_store');
		Route::post('/update/{id}', ['uses' => 'Admin\GruposController@update'])->name('admin_grupos_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\GruposController@delete'])->name('admin_grupos_delete');
	
	});

});
	


Route::prefix('account')->group(function() {


	Route::prefix('/inscripciones')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Frontend\AlunnosController@index'])->name('frontend_alunnos_index');
		Route::get('/new', ['uses' => 'Frontend\AlunnosController@new'])->name('frontend_alunnos_new');
		Route::get('/edit/{id}', ['uses' => 'Frontend\AlunnosController@edit'])->name('frontend_alunnos_edit');
		Route::post('/store', ['uses' => 'Frontend\AlunnosController@store'])->name('frontend_alunnos_store');
		Route::post('/update/{id}', ['uses' => 'Frontend\AlunnosController@update'])->name('frontend_alunnos_update');
		Route::get('/delete/{id}', ['uses' => 'Frontend\AlunnosController@delete'])->name('frontend_alunnos_delete');
	
	});

	Route::prefix('/plantel')->group(function () 
	{
		// user routes
		Route::get('/index', ['uses' => 'Frontend\PlantelController@index'])->name('frontend_plantel_index');
		Route::get('/search', ['uses' => 'Frontend\PlantelController@search'])->name('frontend_plantel_search');

	
	});

});
	