<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('plantel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum(
                'tipo',
                ['cETis','CBtis']
            );
            $table->enum(
                'numero',
                ['112','95','120','80']
            );
            $table->enum(
                'turno',
                ['matutino','matutino y vespertino']
            );

            $table->unsignedBigInteger('grupo_id')->nullable();
            $table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('cascade');
            $table->unsignedBigInteger('ciclo_id')->nullable();
            $table->foreign('ciclo_id')->references('id')->on('ciclos')->onDelete('cascade');
            $table->string('nombre')->nullable();;
            $table->text('contrasena')->nullable();;
            $table->date('fecha_termino_de_solicitud')->nullable();;
            $table->date('fecha_inicio_semestral')->nullable();;
            $table->date('fecha_termino_semestral')->nullable();;
            $table->string('direccion_postal')->nullable();;
            $table->string('telefono')->nullable();;
            $table->string('telefono_movil')->nullable();;
            $table->string('email')->nullable();;
            $table->string('website')->nullable();;
            $table->string('fan_page_facebook')->nullable();;
            $table->string('aviso_de_privacidad')->nullable();;
            $table->string('reglamento_del_plantel')->nullable();;
            $table->text('logo')->nullable();
            $table->text('description')->nullable();

            $table->text('firma_de_reglamento')->nullable();
            $table->text('firma_de_authorizacion')->nullable();
            $table->text('firma_de_veracidad')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plantel');
    }
}
