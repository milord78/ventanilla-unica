<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvanzadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('avanzado', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->unsignedBigInteger('edo_asp_id')->nullable();
            $table->foreign('edo_asp_id')->references('id')->on('estados')->onDelete('cascade');
            $table->unsignedBigInteger('edo_pro_id')->nullable();
            $table->foreign('edo_pro_id')->references('id')->on('estados')->onDelete('cascade');

            $table->string('loca_nac');
            $table->string('mini_nac');
            $table->string('curp');
            $table->string('constancias');

            $table->string('muni_asp');

            $table->string('muni_pro');
            $table->string('modalidad');
            $table->string('regimen');


            $table->timestamps();

   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avanzado');
    }
}
