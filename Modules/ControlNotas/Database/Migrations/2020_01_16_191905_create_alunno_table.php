<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunnos', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');


            $table->unsignedBigInteger('carrera_id')->nullable();
            $table->foreign('carrera_id')->references('id')->on('carreras')->onDelete('cascade');

            $table->unsignedBigInteger('ciclo_id')->nullable();
            $table->foreign('ciclo_id')->references('id')->on('ciclos')->onDelete('cascade');

            $table->unsignedBigInteger('grupo_id')->nullable();
            $table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('cascade');

            $table->date('fecha');
            $table->string('hora');


        
            $table->enum(
                'turno',
                ['matutino','vespertino']
            );
          
            $table->enum(
                'estatus',
                [
                    'Pendiente',
                    'Procesando',
                    'Regular',
                    'Irregular',
                    'Reinscribir al siguiente semestre',
                    
                ]
            );
       
            $table->enum(
                'semestre',
                [
                    1,
                    2,
                    3,
                    4,
                    5,
                    6,
                ]
            );




            $table->timestamps();

   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunnos');
    }
}
