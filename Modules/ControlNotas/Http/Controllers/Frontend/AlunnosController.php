<?php

namespace  Modules\ControlNotas\Http\Controllers\Frontend;
use Modules\ControlNotas\Entities\Alunnos;
use Modules\ControlNotas\Entities\Grupos;
use Modules\ControlNotas\Entities\Carreras;
use Modules\ControlNotas\Entities\Ciclos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlunnosController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Alunnos::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::frontend/alunnos/list',compact('items','q'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        
        $grupos = Grupos::orderBy('id', 'desc')->get();
        $carreras = Carreras::orderBy('id', 'desc')->get();
        $ciclos = Ciclos::orderBy('id', 'desc')->get();
   

        $estatus = [
            ['id'=>'pendiente','label'=> 'Pendiente',],
            ['id'=>'procesando','label'=>  'Procesando',],
            ['id'=>'regular','label'=>  'Regular',],
            ['id'=>'irregular','label'=> 'Irregular',],
            ['id'=>'Reinscribir al siguiente semestre','label'=>  'Reinscribir al siguiente semestre',],
  
        ];
        
        $turnos = [
            ['id'=>'matutino','label'=>"controlnotas::rating.alunnos.MATUTINO_LABEL"],
            ['id'=>'vespertino','label'=>"controlnotas::rating.alunnos.VESPERTINO_LABEL"]
        ];
 

        return view('controlnotas::frontend/alunnos/edit',compact('item','route',
            'grupos',
            'carreras',
            'ciclos',
            'estatus',
            'turnos',
        ));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('frontend_alunnos_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->carrera_id = null;
        $item->ciclo_id = null;
        $item->fecha = null;
        $item->hora = null;
        $item->grupo_id = null;
        $item->estatus = null;
        $item->turno = null;
        $item->semestre = null;

        $grupos = Grupos::orderBy('id', 'desc')->get();
        $carreras = Carreras::orderBy('id', 'desc')->get();
        $ciclos = Ciclos::orderBy('id', 'desc')->get();


        $estatus = [
            ['id'=>'pendiente','label'=> 'Pendiente',],
            ['id'=>'procesando','label'=>  'Procesando',],
            ['id'=>'regular','label'=>  'Regular',],
            ['id'=>'irregular','label'=> 'Irregular',],
            ['id'=>'Reinscribir al siguiente semestre','label'=>  'Reinscribir al siguiente semestre',],
  
        ];
        
        $turnos = [
            ['id'=>'matutino','label'=>"controlnotas::rating.alunnos.MATUTINO_LABEL"],
            ['id'=>'vespertino','label'=>"controlnotas::rating.alunnos.VESPERTINO_LABEL"]
        ];


 
        return view('controlnotas::frontend/alunnos/edit',compact('route','item','grupos','carreras','ciclos','estatus','turnos'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
           
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
        $user_id = \Auth::user()->id;    
        if(Alunnos::where(
            [
                ['id','!=', $id],
                ['user_id','!=', $user_id ]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "frontend_alunnos_edit",['id'=>$id]
            )->with('success','controlnotas::rating.alunnos.UPDATE_SUCESSFULLY_LABEL');
           
            Alunnos::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("frontend_Alunnos_new")
                ->with('error','controlnotas::rating.alunnos.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
     
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
     
        $inder_data['user_id'] = \Auth::user()->id;    
        if(Alunnos::where('user_id', $inder_data['user_id'])->count() == 0)
        {
            $item = Alunnos::create($inder_data);

            return redirect()->route("frontend_alunnos_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.alunnos.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("frontend_alunnos_new")
                        ->with('error','controlnotas::rating.alunnos.SLUG_EXISTS_LABEL');  
        }

    }

}
