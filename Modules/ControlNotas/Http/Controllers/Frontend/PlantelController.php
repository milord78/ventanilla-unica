<?php

namespace  Modules\ControlNotas\Http\Controllers\Frontend;

use Modules\ControlNotas\Entities\Alunnos;
use Modules\ControlNotas\Entities\Grupos;
use Modules\ControlNotas\Entities\Ciclos;
use Modules\ControlNotas\Entities\Plantel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlantelController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
   
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Plantel::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::frontend/plantel/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Plantel::orderBy('id', 'desc')->where('nombre', $q )->paginate(15);
        }else
        {
            $items = Plantel::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::frontend/plantel/list',compact('items','q'));
    }


}
