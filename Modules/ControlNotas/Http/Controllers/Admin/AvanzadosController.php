<?php

namespace  Modules\ControlNotas\Http\Controllers\Admin;

use Modules\ControlNotas\Entities\Avanzados;
use Modules\ControlNotas\Entities\Estados;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AvanzadosController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Avanzados::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::backend/avanzados/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Avanzados::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Avanzados::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::backend/avanzados/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Avanzados::find( $id);
        return view('controlnotas::backend/avanzados/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = Avanzados::find( $id);
        $estados = Estados::orderBy('id', 'desc')->get();
        
        $route = route('admin_avanzados_update',['id' => $item->id]);
        return view('controlnotas::backend/avanzados/edit',compact('item','route','estados'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_avanzados_store');
        

         $item = new \stdClass();
         $item->id = null;
         $item->edo_asp_id = null;
         $item->edo_pro_id = null;
         $item->loca_nac = null;
         $item->mini_nac = null;
         $item->curp = null;
         $item->constancias = null;
         $item->muni_asp = null;
         $item->muni_pro = null;
         $item->modalidad = null;
         $item->regimen = null;

        $estados = Estados::orderBy('id', 'desc')->get();
        

        return view('controlnotas::backend/avanzados/edit',compact('route','item','estados'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   


        if(Avanzados::where(
            [
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_avanzados_edit",['id'=>$id]
            )->with('success','controlnotas::avanzado.UPDATE_SUCESSFULLY_LABEL');
           

            Avanzados::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_avanzados_new")
                ->with('error','controlnotas::avanzado.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Avanzados::find($id)->delete();
        return redirect()->route("admin_avanzados_index")
                        ->with('success','controlnotas::rating.avanzado.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
        ]);

        $inder_data = $request->except(['_token','_method','submit']);

        if(Avanzados::where('edo_nac', $inder_data['edo_nac'])->count() == 0)
        {
        
            $item = Avanzados::create($inder_data);

            return redirect()->route("admin_avanzados_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.avanzado.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_avanzados_new")
                        ->with('error','controlnotas::rating.avanzado.SLUG_EXISTS_LABEL');  
        }

    }

}
