<?php

namespace  Modules\ControlNotas\Http\Controllers\Admin;

use Modules\ControlNotas\Entities\Alunnos;
use Modules\ControlNotas\Entities\Grupos;
use Modules\ControlNotas\Entities\Ciclos;
use Modules\ControlNotas\Entities\Plantel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlantelController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Plantel::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::backend/plantel/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Plantel::orderBy('id', 'desc')->where('nombre', $q )->paginate(15);
        }else
        {
            $items = Plantel::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::backend/plantel/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Plantel::find( $id);
        return view('controlnotas::backend/plantel/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = Plantel::find( $id);
        $route = route('admin_plantel_update',['id' => $item->id]);
               
        $grupos = Grupos::orderBy('id', 'desc')->get();
        $ciclos = Ciclos::orderBy('id', 'desc')->get();

        $tipos = ['CETis','CBtis'];
        $numeros = ['112','95','120','80'];
        $turnos = ['matutino','matutino y vespertino'];
        
        


        return view(
            'controlnotas::backend/plantel/edit',
            compact('item','route','grupos','ciclos','tipos','numeros','turnos')
        );
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_plantel_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->tipo= null;
        $item->numero= null;
        $item->turno= null;
        $item->grupo_id= null;
        $item->ciclo_id= null;
        $item->nombre= null;
        $item->contrasena= null;
        $item->fecha_termino_de_solicitud= null;
        $item->fecha_inicio_semestral= null;
        $item->fecha_termino_semestral= null;
        $item->direccion_postal= null;
        $item->telefono= null;
        $item->telefono_movil= null;
        $item->email= null;
        $item->website= null;
        $item->logo = null;
        $item->description = null;
        $item->fan_page_facebook= null;
        $item->aviso_de_privacidad= null;
        $item->reglamento_del_plantel= null;
        $item->firma_de_reglamento= null;
        $item->firma_de_authorizacion= null;
        $item->firma_de_veracidad= null;
        

        


        $grupos = Grupos::orderBy('id', 'desc')->get();
        $ciclos = Ciclos::orderBy('id', 'desc')->get();

        $tipos = ['CETis','CBtis'];
        $numeros = ['112','95','120','80'];
        $turnos = ['matutino','matutino y vespertino'];


        return view('controlnotas::backend/plantel/edit',compact(
            'route','item',
            'grupos',
            'ciclos',
            'tipos',
            'numeros',
            'turnos'
        ));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
      

        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
     
        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   
        if(!empty($request->file('aviso_de_privacidad')))
        {
            $file_name = $request->file('aviso_de_privacidad')->getClientOriginalName();
            $path = $request->file('aviso_de_privacidad')->store('public');
            $inder_data['aviso_de_privacidad'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   

        if(!empty($request->file('reglamento_del_plantel')))
        {
            $file_name = $request->file('reglamento_del_plantel')->getClientOriginalName();
            $path = $request->file('reglamento_del_plantel')->store('public');
            $inder_data['reglamento_del_plantel'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   

        if(!empty($request->file('firma_de_reglamento')))
        {
            $file_name = $request->file('firma_de_reglamento')->getClientOriginalName();
            $path = $request->file('firma_de_reglamento')->store('public');
            $inder_data['firma_de_reglamento'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   


        if(!empty($request->file('firma_de_authorizacion')))
        {
            $file_name = $request->file('firma_de_authorizacion')->getClientOriginalName();
            $path = $request->file('firma_de_authorizacion')->store('public');
            $inder_data['firma_de_authorizacion'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   


        if(!empty($request->file('firma_de_veracidad')))
        {
            $file_name = $request->file('firma_de_veracidad')->getClientOriginalName();
            $path = $request->file('firma_de_veracidad')->store('public');
            $inder_data['firma_de_veracidad'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   



        if(Plantel::where(
            [
              
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_plantel_edit",['id'=>$id]
            )->with('success','controlnotas::rating.plantel.UPDATE_SUCESSFULLY_LABEL');
           

            Plantel::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_plantel_new")
                ->with('error','controlnotas::rating.plantel.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Plantel::find($id)->delete();
        return redirect()->route("admin_plantel_index")
                        ->with('success','controlnotas::rating.plantel.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
     
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
  
        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   
        if(Plantel::where('nombre', $inder_data['nombre'])->count() == 0)
        {

            



                if(!empty($request->file('aviso_de_privacidad')))
                {
                    $file_name = $request->file('aviso_de_privacidad')->getClientOriginalName();
                    $path = $request->file('aviso_de_privacidad')->store('public');
                    $inder_data['aviso_de_privacidad'] = $path;
                    //$inder_data['thumbnail_name'] = $file_name;
                }   

                if(!empty($request->file('reglamento_del_plantel')))
                {
                    $file_name = $request->file('reglamento_del_plantel')->getClientOriginalName();
                    $path = $request->file('reglamento_del_plantel')->store('public');
                    $inder_data['reglamento_del_plantel'] = $path;
                    //$inder_data['thumbnail_name'] = $file_name;
                }   

                if(!empty($request->file('firma_de_reglamento')))
                {
                    $file_name = $request->file('firma_de_reglamento')->getClientOriginalName();
                    $path = $request->file('firma_de_reglamento')->store('public');
                    $inder_data['firma_de_reglamento'] = $path;
                    //$inder_data['thumbnail_name'] = $file_name;
                }   


                if(!empty($request->file('firma_de_authorizacion')))
                {
                    $file_name = $request->file('firma_de_authorizacion')->getClientOriginalName();
                    $path = $request->file('firma_de_authorizacion')->store('public');
                    $inder_data['firma_de_authorizacion'] = $path;
                    //$inder_data['thumbnail_name'] = $file_name;
                }   


                if(!empty($request->file('firma_de_veracidad')))
                {
                    $file_name = $request->file('firma_de_veracidad')->getClientOriginalName();
                    $path = $request->file('firma_de_veracidad')->store('public');
                    $inder_data['firma_de_veracidad'] = $path;
                    //$inder_data['thumbnail_name'] = $file_name;
                }   



                


            $item = Plantel::create($inder_data);

            return redirect()->route("admin_plantel_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.plantel.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_plantel_new")
                        ->with('error','controlnotas::rating.plantel.SLUG_EXISTS_LABEL');  
        }

    }

}
