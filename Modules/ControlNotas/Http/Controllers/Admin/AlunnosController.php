<?php

namespace  Modules\ControlNotas\Http\Controllers\Admin;

use Modules\ControlNotas\Entities\Alunnos;
use Modules\ControlNotas\Entities\Grupos;
use Modules\ControlNotas\Entities\Carreras;
use Modules\ControlNotas\Entities\Ciclos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlunnosController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Alunnos::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::backend/alunnos/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Alunnos::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Alunnos::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::backend/alunnos/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Alunnos::find( $id);
        return view('controlnotas::backend/alunnos/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = Alunnos::find( $id);
        $route = route('admin_alunnos_update',['id' => $item->id]);
        $grupos = Grupos::orderBy('id', 'desc')->get();
        $carreras = Carreras::orderBy('id', 'desc')->get();
        $ciclos = Ciclos::orderBy('id', 'desc')->get();
     

        $grupos = Grupos::orderBy('id', 'desc')->get();
        $carreras = Carreras::orderBy('id', 'desc')->get();
        $ciclos = Ciclos::orderBy('id', 'desc')->get();
        $generos = [
            ['id'=>'hombre','label'=>"controlnotas::rating.alunnos.SEXO_HOMBRE_LABEL"],
            ['id'=>'mujer','label'=>"controlnotas::rating.alunnos.SEXO_MUJER_LABEL"]
        ];

        $escolaridad = [
            ['id'=>'primaria','label'=>"Primaria"],
            ['id'=>'secundaria','label'=>"Secundaria"],
            ['id'=>'bachillerato','label'=>"Bachillerato"],
            ['id'=>'universitario','label'=>"Universitario"],
        ];

        $tipo_sanguineos = [
            ['id'=>'A+','label'=>'A+'],
            ['id'=>'A-','label'=> 'A-',],
            ['id'=>'B+','label'=> 'B+',],
            ['id'=>'B-','label'=> 'B-',],
            ['id'=>'AB+','label'=> 'AB+',],
            ['id'=>'AB-','label'=> 'AB-',],
            ['id'=>'O+','label'=> 'O+',],
            ['id'=>'O-','label'=> 'O-',],
        ];

        $estatus = [
            ['id'=>'pendiente','label'=> 'Pendiente',],
            ['id'=>'procesando','label'=>  'Procesando',],
            ['id'=>'regular','label'=>  'Regular',],
            ['id'=>'irregular','label'=> 'Irregular',],
            ['id'=>'Reinscribir al siguiente semestre','label'=>  'Reinscribir al siguiente semestre',],
  
        ];
        
        $turnos = [
            ['id'=>'matutino','label'=>"controlnotas::rating.alunnos.MATUTINO_LABEL"],
            ['id'=>'vespertino','label'=>"controlnotas::rating.alunnos.VESPERTINO_LABEL"]
        ];

        


        return view('controlnotas::backend/alunnos/edit',compact(
            'item','route','grupos','carreras','ciclos',
    
            'generos',
            'escolaridad',
            'tipo_sanguineos',
            'estatus',
            'turnos',
    ));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_alunnos_store');
       
        $item = new \stdClass();
        $item->id = null;

        $item->carrera_id = null;
        $item->ciclo_id = null;
        $item->semestre = null;
        $item->turno = null;
        $item->grupo_id = null;
        $item->user_id = null;

  



        


        $grupos = Grupos::orderBy('id', 'desc')->get();
        $carreras = Carreras::orderBy('id', 'desc')->get();
        $ciclos = Ciclos::orderBy('id', 'desc')->get();
        $generos = [
            ['id'=>'hombre','label'=>"controlnotas::rating.alunnos.SEXO_HOMBRE_LABEL"],
            ['id'=>'mujer','label'=>"controlnotas::rating.alunnos.SEXO_MUJER_LABEL"]
        ];

        $escolaridad = [
            ['id'=>'primaria','label'=>"Primaria"],
            ['id'=>'secundaria','label'=>"Secundaria"],
            ['id'=>'bachillerato','label'=>"Bachillerato"],
            ['id'=>'universitario','label'=>"Universitario"],
        ];

        $tipo_sanguineos = [
            ['id'=>'A+','label'=>'A+'],
            ['id'=>'A-','label'=> 'A-',],
            ['id'=>'B+','label'=> 'B+',],
            ['id'=>'B-','label'=> 'B-',],
            ['id'=>'AB+','label'=> 'AB+',],
            ['id'=>'AB-','label'=> 'AB-',],
            ['id'=>'O+','label'=> 'O+',],
            ['id'=>'O-','label'=> 'O-',],
        ];

        $estatus = [
            ['id'=>'pendiente','label'=> 'Pendiente',],
            ['id'=>'procesando','label'=>  'Procesando',],
            ['id'=>'regular','label'=>  'Regular',],
            ['id'=>'irregular','label'=> 'Irregular',],
            ['id'=>'Reinscribir al siguiente semestre','label'=>  'Reinscribir al siguiente semestre',],
  
        ];
        
        $turnos = [
            ['id'=>'matutino','label'=>"controlnotas::rating.alunnos.MATUTINO_LABEL"],
            ['id'=>'vespertino','label'=>"controlnotas::rating.alunnos.VESPERTINO_LABEL"]
        ];
 
        return view('controlnotas::backend/alunnos/edit',compact('route','item','grupos','carreras','ciclos','generos','turnos','tipo_sanguineos','estatus','escolaridad'));
    
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
           
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   
        if(Alunnos::where(
            [
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_alunnos_edit",['id'=>$id]
            )->with('success','controlnotas::rating.alunnos.UPDATE_SUCESSFULLY_LABEL');
           
            Alunnos::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_Alunnos_new")
                ->with('error','controlnotas::rating.alunnos.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        Alunnos::find($id)->delete();
        return redirect()->route("admin_alunnos_index")
                        ->with('success','controlnotas::rating.alunnos.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
     
        ]);

        $inder_data = $request->except(['_token','_method','submit']);

        if(Alunnos::where('edo_nac', $inder_data['edo_nac'])->count() == 0)
        {
            $item = Alunnos::create($inder_data);

            return redirect()->route("admin_alunnos_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.alunnos.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_alunnos_new")
                        ->with('error','controlnotas::rating.alunnos.SLUG_EXISTS_LABEL');  
        }

    }

}
