<?php

namespace  Modules\ControlNotas\Http\Controllers\Admin;

use Modules\ControlNotas\Entities\Estados;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EstadosController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Estados::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::backend/estados/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Estados::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Estados::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::backend/estados/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Estados::find( $id);
        return view('controlnotas::backend/estados/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item= Estados::find( $id);
       
 
        $route = route('admin_estados_update',['id' => $item->id]);
        return view('controlnotas::backend/estados/edit',compact('item','route'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {
        $route = route('admin_estados_store');
        $item = new \stdClass();
        $item->id = null;
        $item->estado = null;
        return view('controlnotas::backend/estados/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
          
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);

        if(Estados::where(
            [
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_estados_edit",['id'=>$id]
            )->with('success','controlnotas::rating.estado.UPDATE_SUCESSFULLY_LABEL');
           

            Estados::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_estados_new")
                ->with('error','controlnotas::rating.estado.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Estados::find($id)->delete();
        return redirect()->route("admin_estados_index")
                        ->with('success','controlnotas::rating.estado.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
        
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
       
        if(Estados::where('estado', $inder_data['estado'])->count() == 0)
        {
            $item = Estados::create($inder_data);

            return redirect()->route("admin_estados_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.estado.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_estados_new")
                        ->with('error','controlnotas::rating.estado.SLUG_EXISTS_LABEL');  
        }

    }

}
