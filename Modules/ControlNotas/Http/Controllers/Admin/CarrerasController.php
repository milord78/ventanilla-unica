<?php

namespace  Modules\ControlNotas\Http\Controllers\Admin;

use Modules\ControlNotas\Entities\Plantel;
use Modules\ControlNotas\Entities\Carreras;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarrerasController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Carreras::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::backend/carreras/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Carreras::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Carreras::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::backend/carreras/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Carreras::find( $id);
        return view('controlnotas::backend/carreras/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = Carreras::find( $id);
        $route = route('admin_carreras_update',['id' => $item->id]);
        $planteles = Plantel::orderBy('id', 'desc')->get();
        return view('controlnotas::backend/carreras/edit',compact('item','route','planteles'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_carreras_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->carrera = null;
        $item->plantel_id = null;
        $planteles = Plantel::orderBy('id', 'desc')->get();
        return view('controlnotas::backend/carreras/edit',compact('route','item','planteles'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
           

        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   
        if(Carreras::where(
            [
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_carreras_edit",['id'=>$id]
            )->with('success','controlnotas::rating.carreras.UPDATE_SUCESSFULLY_LABEL');
           

            Carreras::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_carreras_new")
                ->with('error','controlnotas::rating.carreras.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Carreras::find($id)->delete();
        return redirect()->route("admin_carreras_index")
                        ->with('success','controlnotas::rating.carreras.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
          
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
      
        if(Carreras::where('carrera', $inder_data['carrera'])->count() == 0)
        {
            $item = Carreras::create($inder_data);

            return redirect()->route("admin_carreras_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.carreras.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_tags_new")
                        ->with('error','controlnotas::rating.carreras.SLUG_EXISTS_LABEL');  
        }

    }

}
