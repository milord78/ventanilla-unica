<?php

namespace  Modules\ControlNotas\Http\Controllers\Admin;

use Modules\ControlNotas\Entities\Ciclos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CiclosController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Ciclos::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::backend/ciclos/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Ciclos::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Ciclos::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::backend/ciclos/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Ciclos::find( $id);
        return view('controlnotas::backend/ciclos/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = Ciclos::find( $id);

        $route = route('admin_ciclos_update',['id' => $item->id]);
        return view('controlnotas::backend/ciclos/edit',compact('item','route'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_ciclos_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->ciclo = null;
        $item->activo = false;
        return view('controlnotas::backend/ciclos/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
         

        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);

        $inder_data['activo']= (isset($inder_data['activo']))?true:false;
        
            $redirect_url = redirect()->route(
                "admin_ciclos_edit",['id'=>$id]
            )->with('success','controlnotas::rating.ciclos.UPDATE_SUCESSFULLY_LABEL');
           

            Ciclos::find($id)->update($inder_data);
            return $redirect_url;
       
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Ciclos::find($id)->delete();
        return redirect()->route("admin_ciclos_index")
                        ->with('success','controlnotas::rating.ciclos.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
     
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
            
        $inder_data['activo']= (isset($inder_data['activo']))?true:false;
        
            $item = Ciclos::create($inder_data);

            return redirect()->route("admin_ciclos_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.ciclos.CREATED_SUCCESSFULLY_LABEL');  
      

    }

}
