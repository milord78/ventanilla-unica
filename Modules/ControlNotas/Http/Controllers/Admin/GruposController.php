<?php

namespace  Modules\ControlNotas\Http\Controllers\Admin;

use Modules\ControlNotas\Entities\Grupos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GruposController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Grupos::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('controlnotas::backend/grupos/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Grupos::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Grupos::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('controlnotas::backend/grupos/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Grupos::find( $id);
        return view('controlnotas::backend/grupos/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item= Grupos::find( $id);
       
        $item->publish = (bool)$item->publish ;
        
        $route = route('admin_grupos_update',['id' => $item->id]);
        return view('controlnotas::backend/grupos/edit',compact('item','route'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_grupos_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->grupo = null;

        return view('controlnotas::backend/grupos/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
         
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   

        if(Grupos::where(
            [
             
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_grupos_edit",['id'=>$id]
            )->with('success','controlnotas::rating.grupo.UPDATE_SUCESSFULLY_LABEL');
           

            Grupos::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_grupos_new")
                ->with('error','controlnotas::rating.grupo.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Grupos::find($id)->delete();
        return redirect()->route("admin_grupos_index")
                        ->with('success','controlnotas::rating.grupo.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
       
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
       
        if(Grupos::where('grupo', $inder_data['grupo'])->count() == 0)
        {
        
            $item = Grupos::create($inder_data);

            return redirect()->route("admin_grupos_edit",['id'=>$item->id])
                        ->with('success','controlnotas::rating.grupo.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_grupos_new")
                        ->with('error','controlnotas::rating.grupo.SLUG_EXISTS_LABEL');  
        }

    }

}
