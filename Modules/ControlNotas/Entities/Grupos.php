<?php

namespace Modules\ControlNotas\Entities;

use Illuminate\Database\Eloquent\Model;

class Grupos extends Model
{
  	protected $table = 'grupos';
    protected $fillable = [
        'grupo',
    ];
}
