<?php

namespace Modules\ControlNotas\Entities;

use Illuminate\Database\Eloquent\Model;

class Carreras extends Model
{
  	protected $table = 'carreras';
    protected $fillable = [
        'carrera',
        'plantel_id',
    ];
    public function plantel()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Plantel','plantel_id');
    }    
}
