<?php

namespace Modules\ControlNotas\Entities;

use Illuminate\Database\Eloquent\Model;

class Alunnos extends Model
{
  	protected $table = 'alunnos';
    protected $fillable = [
		'carrera_id',
		'ciclo_id',
		'grupo_id',
		'semestre',
		'fecha',
		'hora',
		'turno',
		'user_id',
		'estatus',

	];
	
	public function carrera()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Carreras','carrera_id');
	}    
	
	public function ciclos()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Ciclos','ciclo_id');
    }    
	public function grupo()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Grupo','grupo_id');
    }    

	public function user()
    {
        return $this->belongsTo('App\Model\User','user_id');
	}    
	
}
