<?php

namespace Modules\ControlNotas\Entities;

use Illuminate\Database\Eloquent\Model;

class Avanzados extends Model
{
  	protected $table = 'avanzado';
    protected $fillable = [
        'edo_asp_id',
        'edo_pro_id',
        'loca_nac',
        'mini_nac',
        'curp',
        'constancias',
        'muni_asp',
        'muni_pro',
        'modalidad',
        'regimen',

    ];

    public function carrera()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Estados','edo_asp_id');
	}    
	
	public function ciclos()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Estados','edo_pro_id');
    }    

}
