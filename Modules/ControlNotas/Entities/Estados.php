<?php

namespace Modules\ControlNotas\Entities;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
  	protected $table = 'estados';
    protected $fillable = [
        'estado',
    ];
}
