<?php

namespace Modules\ControlNotas\Entities;

use Illuminate\Database\Eloquent\Model;

class Ciclos extends Model
{
  	protected $table = 'ciclos';
    protected $fillable = [
        'ciclo',
        'activo'
    ];
}
