<?php

namespace Modules\ControlNotas\Entities;

use Illuminate\Database\Eloquent\Model;

class Plantel extends Model
{
  	protected $table = 'plantel';
    protected $fillable = [
        'tipo',
        'numero',
        'turno',
        'grupo_id',
        'ciclo_id',
        'nombre',
        'contrasena',
        'fecha_termino_de_solicitud',
        'fecha_inicio_semestral',
        'fecha_termino_semestral',
        'direccion_postal',
        'telefono',
        'telefono_movil',
        'email',
        'website',
        'fan_page_facebook',
        'aviso_de_privacidad',
        'reglamento_del_plantel',
        'logo',
        'description',
        'firma_de_reglamento',
        'firma_de_authorizacion',
        'firma_de_veracidad',
    ];

    public function grupo()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Grupos','grupo_id');
    }    
    
    public function ciclos()
    {
        return $this->belongsTo('Modules\ControlNotas\Entities\Ciclos','ciclo_id');
    }    

    public function logos()
    {
        if($this->logo==null)
        {
            return asset('assets/img/profileicon-1487703856.png');

        }else{
            
            return "/".str_replace("public","images",$this->logo);
        }
    }
}

