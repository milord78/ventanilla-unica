<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('statistics')->group(function() {
    Route::get('/', 'StatisticsController@index');
});

Route::prefix('api')->group(function() 
{
    Route::prefix('statistics')->group(function() 
    {
        Route::post('/users', ['uses' => 'Api\StatisticsController@users'])->name('admin_api_statistics_list');

        Route::post('/users_by_month', ['uses' => 'Api\StatisticsController@users_by_month'])->name('admin_api_statistics_users_by_month');

        Route::post('/users_by_year', ['uses' => 'Api\StatisticsController@users_by_year'])->name('admin_api_statistics_users_by_year');

        Route::post('/suscriptions', ['uses' => 'Api\StatisticsController@suscriptions'])->name('admin_api_statistics_suscriptions');

        Route::post('/suscriptions_by_month', ['uses' => 'Api\StatisticsController@suscriptions_by_month'])->name('admin_api_statistics_suscriptions_by_month');
		
		Route::post('/suscriptions_by_year', ['uses' => 'Api\StatisticsController@suscriptions_by_year'])->name('admin_api_statistics_suscriptions_by_year');

    });
});
