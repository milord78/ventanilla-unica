<?php

namespace Modules\Statistics\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function users()
    {
        $items = DB::table('users')
             ->select('role', DB::raw('count(*) as total'))
             ->groupBy('role')
             ->where('role',"user")
             ->get();
         return response()->json( $items);
    }

    public function users_by_month()
    {

        $items = DB::table('users')
             ->select('role', 
                DB::raw('count(role) as total'),
                DB::raw('YEAR(created_at) year, MONTH(created_at) month')
                )->groupBy(['month','role','created_at'])
                ->where('role',"user")
                ->get();
         return response()->json( $items);
    }
    public function users_by_year()
    {

        $items = DB::table('users')
             ->select('role', 
                DB::raw('count(role) as total'),
                DB::raw('YEAR(created_at) year, MONTH(created_at) month')
                )->groupBy(['year','role','created_at'])
                ->where('role',"user")
                ->get();
         return response()->json( $items);
    }


    public function suscriptions(Request $request)
    {
        $status = $request->input('cancelled',false);
        $items = DB::table('user_suscriptions')
             ->select('users.role', DB::raw('count(users.role) as total'))
             ->leftjoin('users', 'users.id','=', 'user_suscriptions.user_id')
             ->groupBy('users.role')
             ->where('user_suscriptions.cancelled',$status)
             ->get();
         return response()->json( $items);
    }

    public function suscriptions_by_month(Request $request)
    {
        $status = $request->input('cancelled',false);
        $items = DB::table('user_suscriptions')
             ->select(
                'users.role', 
                DB::raw('count(users.role) as total'),
                DB::raw('YEAR(user_suscriptions.created_at) year, MONTH(user_suscriptions.created_at) month')
                )
             ->leftjoin('users', 'users.id','=', 'user_suscriptions.user_id')
             ->groupBy(['month','users.role','user_suscriptions.created_at'])
             ->where('user_suscriptions.cancelled', $status )
             ->get();
         return response()->json( $items);
    }
    public function suscriptions_by_year(Request $request)
    {
        $status = $request->input('cancelled',false);
        $items = DB::table('user_suscriptions')
             ->select(
                'users.role', 
                DB::raw('count(users.role) as total'),
                DB::raw('YEAR(user_suscriptions.created_at) year, MONTH(user_suscriptions.created_at) month')
                )
             ->leftjoin('users', 'users.id','=', 'user_suscriptions.user_id')
             ->groupBy(['year','users.role','user_suscriptions.created_at'])
             ->where('user_suscriptions.cancelled',$status)
             ->get();
         return response()->json( $items);
    }


}
