<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('notifications')->group(function() {
    Route::get('/', 'NotificationsController@index');
});

Route::prefix('api')->group(function() {
	Route::prefix('notifications')->group(function() {
    	Route::get('/list', 'Api\NotificationsController@index')->name('api_notifications_index');
    });
});



Route::prefix('account')->group(function() {
	Route::prefix('notifications')->group(function() {
    	Route::get('/list', 'Frontend\NotificationsController@index')->name('frontend_notifications_index');;
    	Route::get('/view/{id}', 'Admin\NotificationsController@navigate')->name('frontend_notifications_view');;
    });
});



Route::prefix('admin')->group(function() {
	Route::prefix('notifications')->group(function() {
    	Route::get('/list', 'Admin\NotificationsController@index')->name('admin_notifications_index');
    	Route::get('/view/{id}', 'Admin\NotificationsController@navigate')->name('admin_notifications_view');
    });
    Route::prefix('system')->group(function() {
        Route::prefix('notifications')->group(function() {
            Route::get('/list', 'Admin\SystemNotificationController@index')->name('admin_system_notifications_index');
            Route::get('/view/{id}', 'Admin\SystemNotificationController@navigate')->name('admin_system_notifications_view');
        });
    });
});