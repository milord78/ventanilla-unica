<?php

namespace Modules\Notifications\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Notifications\Entities\Notification;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $items = Notification::where([
            [
                'autor_id','=',\Auth::user()->id
            ],
            [
                'readed','=',0
            ],   
        ]
          
        )->orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('notifications::frontend/notifications/list',compact('items','q'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function navigate($id)
    {
        $item = Notification::find($id);
        $item->update(['readed'=>true]);
        $redirect_url = redirect($item ->link);
        return $redirect_url;
    }


}

