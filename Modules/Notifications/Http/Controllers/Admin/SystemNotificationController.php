<?php

namespace Modules\Notifications\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Notifications\Entities\SystemNotification;

class SystemNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $items = SystemNotification::where(
            'readed',
            false
        )->orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('notifications::backend/system_notifications/list',compact('items','q'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function navigate($id)
    {
        $item = SystemNotification::find($id);
        $item->update(['readed'=>true]);
        $redirect_url = redirect($item ->link);
        return $redirect_url;
    }


}
