@extends('layouts.backend')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="">
      <div class="box-header" style="padding: 15px">
            
        <h3 class="box-title">
          {{ __('notifications::notifications.notification.NOTIFICATION_LABEL') }}
        </h3>
      </div>

      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">

        <table class="table">
          <thead>
            <tr>
             
              <th>{{ __('notifications::notifications.notification.MESSAGE_LABEL') }}</th>
             
              <th  style="width: 60px">{{ __('general.ACTIONS_LABEL') }}</th>
            </tr>
          </thead>
          <tbody>
            @foreach($items  as $index => $item)
            <tr>
       
              <td>{{$item['title']}}</td>

             
            
            
              <td>
                <a href="{{ route('admin_notifications_view',['id'=>$item['id']]) }}" class="btn btn-block btn-success">
                  {{ __('general.VIEW_LABEL') }}
                </a>
              </td>
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>

@endsection
