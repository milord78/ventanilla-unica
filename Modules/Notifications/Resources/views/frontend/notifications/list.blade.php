@extends('layouts.user_panel')

@section('content')
<div class="app-heading app-heading-bordered app-heading-page">
    <div class="icon icon-lg">
        <span class="icon-list3"></span>
    </div>
    <div class="title">            
        <h1>  
        {{ __('notifications::notifications.notification.NOTIFICATION_LABEL') }}
       </h1>
    </div>
</div>

    
<div class="app-heading-container app-heading-bordered bottom" >
    <ul class="breadcrumb">
        <li><a href="#">Dashboard</a></li>
        <li class="active">
        {{ __('notifications::notifications.notification.NOTIFICATION_LABEL') }}
        </li>
    </ul>
</div>
<div class="container">
  <div class="block"> 
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="">
      <div class="box-header" style="padding: 15px">
        
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">

   

    

        <table class="table">
          <thead>
            <tr>
             
              <th>{{ __('notifications::notifications.notification.MESSAGE_LABEL') }}</th>
          
              <th  style="width: 60px">{{ __('general.ACTIONS_LABEL') }}</th>
            </tr>
          </thead>
          <tbody>
            @foreach($items  as $index => $item)
            <tr>
            
              <td> {{ __($item['message'])}}</td>

             
              
            
              <td>
                <a href="{{ route('frontend_notifications_view',['id'=>$item['id']]) }}" class="btn btn-block btn-success">
                  {{ __('general.VIEW_LABEL') }}
                </a>
              </td>
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>
  </div>
</div>
@endsection
