<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 



    'notification'=>[
                
        'MESSAGE_LABEL'                 =>  'Message',
        'NOTIFICATIONS_LABEL'                =>  'Notifications',
        'NOTIFICATION_LABEL'                 =>  'Notification',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Notification  created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Notification  updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Notification  deleted successfully',
        'EXISTS_LABEL'                      =>  'Notification  exists',
        'MESSAGE_COUNT_LABEL'              =>  'Message Adviable:',
      
    ],
    
];
