<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 

    'notification'=>[
            
        'MESSAGE_LABEL'                 =>  'Mensaje',
        'NOTIFICATIONS_LABEL'                =>  'Notificacion',
        'NOTIFICATION_LABEL'                 =>  'Notificaciones',
        'CREATED_SUCCESSFULLY_LABEL'       =>  'Notificacion creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Notificacion actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Notificacion borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Notificacion existe',
         'MESSAGE_COUNT_LABEL'              =>  'Mensajes disponbiles:',
       
    ],


    

];
