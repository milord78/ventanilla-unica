<?php

namespace Modules\Notifications\Entities;

use Illuminate\Database\Eloquent\Model;

class SystemNotification extends Model
{


  	protected $table = 'system_notifications';
    protected $fillable = [
		'autor_id',
		'message',
		'link',
		'readed',
    ];
}
