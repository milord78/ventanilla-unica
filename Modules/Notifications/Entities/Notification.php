<?php

namespace Modules\Notifications\Entities;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{


  	protected $table = 'notifications';
    protected $fillable = [
		'autor_id',
		'message',
		'link',
		'readed',
    ];
}
