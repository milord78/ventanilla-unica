<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::prefix('admin')->group(function() {
   	Route::prefix('contact-form')->group(function () 
	{
		Route::get('/index', ['uses' => 'Admin\ContactFormController@index'])->name('admin_contact_form_index');
		
		Route::get('/search', ['uses' => 'Admin\ContactFormController@search'])->name('admin_contact_form_search');

		Route::get('/show/{id}', ['uses' => 'Admin\ContactFormController@show'])->name('admin_contact_form_show');

		Route::get('/delete/{id}', ['uses' => 'Admin\ContactFormController@delete'])->name('admin_contact_form_delete');
	});
});
	