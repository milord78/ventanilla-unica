<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 



    'contactform'=>[
        'CONTACT_FORMS_LABEL'               =>  'Contacts Message',
        'CONTACT_FORM_LABEL'                =>  'Contact Message',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'User Contact Message created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'User Contact Message updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'User Contact Message deleted successfully',
        'EXISTS_LABEL'                      =>  'User Contact Message exists',
        'PHONE_LABEL'                       =>  'Phone',
        'WEBSITE_LABEL'                     =>  'Website',
        'MESSAGE_LABEL'                     =>  'Message',
  
    ],


];
