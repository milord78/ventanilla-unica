<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 


    'contactform'=>[
            

        'CONTACT_FORMS_LABEL'               =>  'Mensajes de Contacto',
        'CONTACT_FORM_LABEL'                =>  'Mensaje de contacto',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Mensaje de contacto created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Mensaje de contacto updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Mensaje de contacto deleted successfully',
        'EXISTS_LABEL'                      =>  'Mensaje de contacto exists',

        'PHONE_LABEL'                       =>  'Telefono',
        'WEBSITE_LABEL'                     =>  'Sitio web',
        'MESSAGE_LABEL'                     =>  'Mensaje',
       
    ],
    

];
