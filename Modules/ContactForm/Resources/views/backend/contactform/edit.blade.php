@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">


       <h3  style="margin: 15px" class="box-title">  
          {{ __('contactform::contactform.contactform.CONTACT_FORM_LABEL') }}
          @if (empty($item->id))
            {{ __('general.ADD_LABEL') }}
          @else
           {{ __('general.EDIT_LABEL') }}
          @endif
       </h3>
 
         
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#basic" data-toggle="tab" aria-expanded="true">
                    {{ __('general.BASIC_LABEL') }}
                  </a>
                </li>
              </ul>
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <table>
                  <tbody>
                    <tr>
                      <th>{{ __('general.TITLE_LABEL') }}</th>
                      <td>{{$item->title}}</td>
                    </tr>
                    <tr>
                      <th>{{ __('contactform::contactform.contactform.PHONE_LABEL') }}</th>
                      <td>{{$item->phone}}</td>
                    </tr>
                    <tr>
                      <th>{{ __('Email') }}</th>
                      <td>{{$item->email}}</td>
                    </tr>
                    <tr>
                      <th>{{ __('contactform::contactform.contactform.WEBSITE_LABEL') }}</th>
                      <td>
                        <a href="{{$item->website}}">{{$item->website}}</a>
                      </td>
                    </tr>
                    <tr>
                      <th>{{ __('contactform::contactform.contactform.MESSAGE_LABEL') }}</th>
                      <td>
                        {!!$item->message!!}
                      </td>
                    </tr>


                  </tbody>
                </table>
              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>
            <a href="{{ route('admin_tags_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
            @if (!empty($item->id))
              <a href="{{ route('admin_tags_delete',['id' => $item->id]) }}" class="btn btn-danger">
                {{ __('general.DELETE_LABEL') }}
              </a>
            @endif
        </div>

     
   
    
  </div>
@endsection
@section('footer_scripts')
@endsection
