<?php

namespace  Modules\ContactForm\Http\Controllers\Admin;

use Modules\ContactForm\Entities\ContactForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactFormController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = ContactForm::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('contactform::backend/contactform/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = ContactForm::orderBy('id', 'desc')->where('email', $q )->paginate(15);
        }else
        {
            $items = ContactForm::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('contactform::backend/contactform/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = ContactForm::find( $id);
        return view('contactform::backend/contactform/edit',compact('item'));
    }




 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        ContactForm::find($id)->delete();
        return redirect()->route("admin_contactform_index")
                        ->with('success','contactform::contactform.contactform.DELETED_SUCESSFULLY_LABEL');
    }


}
