<?php

namespace Modules\ContactForm\Entities;

use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model
{
  	protected $table = 'contact_form';
    protected $fillable = [
		'title',
		'website',
		'email',
		'content',
		'phone',
    ];
}
