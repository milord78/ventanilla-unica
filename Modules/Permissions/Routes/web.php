<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('permissions')->group(function() {
    Route::get('/', 'PermissionsController@index');
});


Route::prefix('api')->group(function() 
{
  
  	Route::prefix('permissions')->group(function() 
  	{
		
		Route::prefix('groups')->group(function ()
		{
			Route::post('/list', ['uses' => 'Api\GroupsController@list'])->name('admin_api_permissions_groups_index');
			Route::post('/search', ['uses' => 'Api\GroupsController@list'])->name('admin_api_permissions_groups_search');
		    Route::post('/related', ['uses' => 'Api\GroupsController@get_related'])->name('admin_api_permissions_groups_related');

		    Route::prefix('user')->group(function ()
			{
   				 Route::post('/related', ['uses' => 'Api\UserGroupsPermissionsController@get_related'])->name('admin_api_permissions_user_groups_related');
			});
		});

		
		Route::post('/list', ['uses' => 'Api\PermissionsController@list'])->name('admin_api_permissions_index');
		Route::post('/search', ['uses' => 'Api\PermissionsController@list'])->name('admin_api_permissions_search');
		Route::post('/related', ['uses' => 'Api\PermissionsController@get_related'])->name('admin_api_permissions_related');
		
		
	});
});


Route::prefix('admin')->group(function() 
{
  
  	Route::prefix('permissions')->group(function() 
  	{

		Route::prefix('/groups')->group(function () 
		{
			Route::get('/index', ['uses' => 'Admin\GroupsController@index'])->name('admin_permissions_groups_index');
			Route::get('/search', ['uses' => 'Admin\GroupsController@search'])->name('admin_permissions_groups_search');
			Route::get('/show/{id}', ['uses' => 'Admin\GroupsController@show'])->name('admin_permissions_groups_show');
			Route::get('/new', ['uses' => 'Admin\GroupsController@new'])->name('admin_permissions_groups_new');
			Route::get('/edit/{id}', ['uses' => 'Admin\GroupsController@edit'])->name('admin_permissions_groups_edit');
			Route::post('/store', ['uses' => 'Admin\GroupsController@store'])->name('admin_permissions_groups_store');
			Route::post('/update/{id}', ['uses' => 'Admin\GroupsController@update'])->name('admin_permissions_groups_update');
			Route::get('/delete/{id}', ['uses' => 'Admin\GroupsController@delete'])->name('admin_permissions_groups_delete');
		});
		
		// articles routes
		Route::get('/access_denied', ['uses' => 'Admin\PermissionBlockController@index'])->name('admin_permissions_access_denied');
		
		Route::prefix('/permissions')->group(function () 
		{
			Route::get('/index', ['uses' => 'Admin\PermissionsController@index'])->name('admin_permissions_permissions_index');
			Route::get('/search', ['uses' => 'Admin\PermissionsController@search'])->name('admin_permissions_permissions_search');
			Route::get('/show/{id}', ['uses' => 'Admin\PermissionsController@show'])->name('admin_permissions_permissions_show');
			Route::get('/user/{id}', ['uses' => 'Admin\UserPermissionsController@edit'])->name('admin_user_permissions_permissions_edit');
			Route::post('/user/update/{id}', ['uses' => 'Admin\UserPermissionsController@update'])->name('admin_user_permission_group_update');
			Route::get('/new', ['uses' => 'Admin\PermissionsController@new'])->name('admin_permissions_permissions_new');
			Route::get('/edit/{id}', ['uses' => 'Admin\PermissionsController@edit'])->name('admin_permissions_permissions_edit');
			Route::post('/store', ['uses' => 'Admin\PermissionsController@store'])->name('admin_permissions_permissions_store');
			Route::post('/update/{id}', ['uses' => 'Admin\PermissionsController@update'])->name('admin_permissions_permissions_update');
			Route::get('/delete/{id}', ['uses' => 'Admin\PermissionsController@delete'])->name('admin_permissions_permissions_delete');
		});
		
	});
});
