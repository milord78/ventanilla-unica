<?php

namespace  Modules\Permissions\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class PermissionBlockController extends Controller
{
  
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('admin_check');
       
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
      
        return view('permissions::backend/permissions/access_denied');
    }


}
