<?php

namespace Modules\Permissions\Http\Controllers\Admin;

use Modules\Permissions\Entities\UserGrupsPermission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserPermissionsController extends Controller
{
  
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('admin_check');
        $this->middleware('permission_check:permission,change');
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = UserGrupsPermission::find( $id);
        return view('permissions::backend/permissions/user_group_permission/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = User::find( $id);
   
       
        $route = route('admin_user_permission_group_update',['id' => $item->id]);
        return view('permissions::backend/permissions/user_group_permission/edit',compact('item','route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
            
        ]);
        
            $inder_data = $request->except(['_token','_method','submit']);
  
            $cats = isset($inder_data['groups'])?$inder_data['groups']:[];
            save_user_permissions_groups_related($id,$cats);
            $redirect_url = redirect()->route(
                "admin_user_permissions_permissions_edit",['id'=>$id]
            )->with('success','permissions::permissions.user_permissions_groups.UPDATE_SUCESSFULLY_LABEL');
           
            
            
            return $redirect_url;
       
        
    }

 

}
