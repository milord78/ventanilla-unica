<?php

namespace  Modules\Permissions\Http\Controllers\Admin;

use Modules\Permissions\Entities\GroupsAccess as Postsgroups;
use Modules\Permissions\Entities\UserPermission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class GroupsController extends Controller
{
  
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('admin_check');
        $this->middleware('permission_check:permission,change');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $items = Postsgroups::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('permissions::backend/permissions/groups/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       
        $q = $request->input('title');

         if(!empty($q))
        {
            $items = Postsgroups::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Postsgroups::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('permissions::backend/permissions/groups/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Postsgroups::find( $id);
        return view('permissions::backend/permissions/groups/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = Postsgroups::find( $id);
        $items = UserPermission::all();
        $route = route('admin_permissions_groups_update',['id' => $item->id]);
        return view('permissions::backend/permissions/groups/edit',compact('item','route'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_permissions_groups_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->name = null;
        $items = UserPermission::all();
 
        return view('permissions::backend/permissions/groups/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $inder_data = $request->except([
            '_token','_method','submit'
        ]);
   
        if(Postsgroups::where(
            [
                ['name','=',$inder_data['name']],
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_permissions_groups_edit",['id'=>$id]
            )->with('success','permissions::permissions.groups.UPDATE_SUCESSFULLY_LABEL');
            $cats = isset($inder_data['permissions'])?$inder_data['permissions']:[];
            save_permissions_groups_related($id,$cats);

            Postsgroups::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_permissions_groups_new")
                ->with('error','permissions::permissions.groups.EMAIL_EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Postsgroups::find($id)->delete();
        return redirect()->route("admin_permissions_groups_index")
                        ->with('success','permissions::permissions.groups.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'name' => 'required',
        ]);

        $inder_data = $request->except([
            '_token','_method','submit'
        ]);
       
        if(Postsgroups::where('name', $inder_data['name'])->count() == 0)
        {
        
            $item = Postsgroups::create($inder_data);
            $id = $item->id;
            $cats = isset($inder_data['permissions'])?$inder_data['permissions']:[];
            save_permissions_groups_related($id,$cats);

            return redirect()->route("admin_permissions_groups_edit",['id'=>$item->id])->with('success','permissions::permissions.groups.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_permissions_groups_new")->with('error','permissions::permissions.groups.SLUG_EXISTS_LABEL');  
        }

    }

}
