<?php

namespace Modules\Permissions\Http\Controllers\Admin;

use Modules\Permissions\Entities\UserPermission as PostsCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class PermissionsController extends Controller
{
    private $actions = [
        'access',
        'change',
        'create',
        'update',
        'delete',
    ]; 
    private $modules = [
        'articles',
        'cms',
        'navigation',
        'permission',
        'media',

    ];
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('admin_check');
        $this->middleware('permission_check:permission,change');
       
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $items = PostsCompany::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('permissions::backend/permissions/permission/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       
        $q = $request->input('title');

         if(!empty($q))
        {
            $items = PostsCompany::orderBy('id', 'desc')->where('module', $q )->paginate(15);
        }else
        {
            $items = PostsCompany::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('permissions::backend/permissions/permission/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = PostsCompany::find( $id);
        return view('permissions::backend/permissions/permission/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = PostsCompany::find( $id);
        $actions = $this->actions;
        $modules = $this->modules;
       
        $route = route(
            'admin_permissions_permissions_update',
            ['id' => $item->id]
        );
        return view('permissions::backend/permissions/permission/edit',compact('item','route','actions','modules'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_permissions_permissions_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->module = null;
        $item->action = null;
        $actions = $this->actions;
        $modules = $this->modules;
        
        return view(
            'permissions::backend/permissions/permission/edit',
            compact(
                'route',
                'item',
                'actions',
                'modules'
            )
        );
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
            'module'=>'required',
            'action'=>'required',
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
  
        $redirect_url = redirect()->route(
            "admin_permissions_permissions_edit",['id'=>$id]
        )->with('success','permissions::permissions.permissions.UPDATE_SUCESSFULLY_LABEL');
        
        
      
        PostsCompany::find($id)->update($inder_data);
        return $redirect_url;
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        PostsCompany::find($id)->delete();
        return redirect()->route("admin_permissions_permissions_index")
                        ->with('success','permissions::permissions.permissions.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'module'=>'required',
            'action'=>'required',
        
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
        
        $item = PostsCompany::create($inder_data);
        $id = $item->id;

        
        return redirect()->route("admin_permissions_permissions_edit",
            ['id'=>$id])->with(
                'success',
                'permissions::permissions.permissions.CREATED_SUCCESSFULLY_LABEL'
            );
    }

}
