<?php

namespace Modules\Permissions\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Permissions\Entities\GroupsAccess;
use Modules\Permissions\Entities\GroupsPermissions;

class GroupsController extends Controller
{
    //
    public function __construct()
    {
       $this->middleware('permission_api_check:permission,change');
    }
    public function list(Request $request)
    {
        $pages = $request->input('pages',150);
        $items = GroupsAccess::orderBy('id', 'desc')->paginate($pages);
        return response()->json( $items);
    }


    public function search(Request $request)
    {   

        $q = $request->input('name');
        $exclude = $request->input('exclude');

        if( empty($exclude) && !empty($q))
        {
            $items = GroupsAccess::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }

        if( !empty($exclude) && !empty($q))
        {
            $items = GroupsAccess::where([
                ['name','=', $q],
                ['id','!=',  $exclude ],
            ])->paginate(15);
        }
          

        if( empty($exclude) && empty($q))
        {
            $items = GroupsAccess::paginate(15);
        }

     
        return response()->json( $items);
       
    }
    
    public function get_related(Request $request)
    {
        $id = $request->input('id');
        $items = GroupsPermissions::where([['group_id','=',$id]])->get();
        $items_ids = [];
        foreach ($items as $key => $value) 
        {
            array_push($items_ids , $value->permission_id);
        }
        return response()->json($items_ids);
    }
    
    public function details(Request $request)
    {
        $id = $request->input('id');
        return response()->json( GroupsAccess::find($id));
    }
}
