<?php

namespace Modules\Permissions\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Permissions\Entities\UserPermission;


class PermissionsController extends Controller
{
    //
    public function __construct()
    {
       $this->middleware('permission_api_check:permission,change');
    }
    public function list(Request $request)
    {
        $pages = $request->input('pages',150);
        $items = UserPermission::orderBy('id', 'desc')->paginate($pages);
        return response()->json( $items);
    }


    public function search(Request $request)
    {   

        $q = $request->input('name');
        $exclude = $request->input('exclude');

        if( empty($exclude) && !empty($q))
        {
            $items = UserPermission::orderBy('id', 'desc')->where('module', $q )->paginate(15);
        }

        if( !empty($exclude) && !empty($q))
        {
            $items = UserPermission::where([
                ['module','=', $q],
                ['id','!=',  $exclude ],
            ])->paginate(15);
        }
          

        if( empty($exclude) && empty($q))
        {
            $items = UserPermission::paginate(15);
        }

     
        return response()->json( $items);
       
    }
    
   
    
    public function details(Request $request)
    {
        $id = $request->input('id');
        return response()->json( UserPermission::find($id));
    }
}
