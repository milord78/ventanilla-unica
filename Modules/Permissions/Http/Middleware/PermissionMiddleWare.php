<?php

namespace Modules\Permissions\Http\Middleware;

use Closure;
use Modules\Permissions\Entities\GroupsAccess;
use Modules\Permissions\Entities\GroupsPermissions;
use Modules\Permissions\Entities\UserPermission;
use Modules\Permissions\Entities\UserGroupsPermission;

class PermissionMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$module=null,$action=null)
    {
        if( \Auth::check()==true)
        {
            $user = \Auth::user();
            $id = $user ->id; 
            if($user->role!="admin")
            {  
                if((!empty($module) &&!empty($action))==true )
                {
                    if (count($this->get_navigation_child($module,$action,$id)) == 0)
                    {
                         
                       $redirect_url = redirect()->route("admin_permissions_access_denied");
                        return $redirect_url;
                      
                    }
                }     
            }

            
        }
       

        return $next($request);
    }

     function get_navigation_child($module,$action,$parent_id) 
    {
        return UserGroupsPermission::leftJoin('groups_permissions', 'groups_permissions.group_id', '=', 'user_groups_permissions.group_id')
        -> leftJoin('user_permissions', 'user_permissions.id', '=', 'groups_permissions.permission_id')
       ->select('user_groups_permissions.id')->where([
            ['user_permissions.module','=',$module],
            ['user_permissions.action','=',$action],
            ['user_groups_permissions.user_id','=',$parent_id],
        ])->get();

    } 
}
