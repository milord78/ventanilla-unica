<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;

class UserGroupsPermission extends Model
{
   	protected $table = 'user_groups_permissions';
    protected $fillable = [
		'user_id',
		'group_id',
    ];



}
