<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;

class GroupsAccess extends Model
{
  	protected $table = 'groups_access';
    protected $fillable = [
		'id',
		'name'
    ];
}
