<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;

class GroupsPermissions extends Model
{
  	protected $table = 'groups_permissions';
    protected $fillable = [
		'id',
		'group_id',
		'permission_id',
    ];
}
