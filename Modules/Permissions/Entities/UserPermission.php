<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{

   	protected $table = 'user_permissions';
    protected $fillable = [
		'module',
		'action',
    ];
}
