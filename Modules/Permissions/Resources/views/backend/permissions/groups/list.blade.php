@extends('layouts.backend')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">
      <div class="box-header" style="padding: 15px">
        <h3 class="box-title"> {{__('permissions::permissions.groups.GROUPS_LABEL')}} </h3>
        <div class="box-tools">
          <form method="get" action="{{route('admin_permissions_groups_search')}}">
            <div class="input-group input-group-sm" style="width: 550px;">
              <input type="text" name="name" value="{{$q}}" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }} {{ __('general.NAME_LABEL') }}">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-default">
                  <i class="fa fa-search"></i>
                </button>
                <a href="{{ route('admin_permissions_groups_new') }}" class="btn btn-primary">
                  {{ __('general.ADD_LABEL') }} 
                  {{ __('permissions::permissions.groups.GROUP_LABEL')}}  
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">

        <div class="padding-20">
          <a class="btn btn-primary" href="{{ route('admin_permissions_permissions_index') }}">
            {{  __('permissions::permissions.permissions.PERMISSIONS_LABEL') }}
          </a>
          &nbsp;
          <a class="btn btn-primary" href="{{ route('admin_permissions_groups_index') }}">
            {{  __('permissions::permissions.groups.GROUPS_LABEL') }}
          </a>
          &nbsp;
        </div>
        <table class="table">
          <thead>
            <tr>
              <th class="col2"># </th>
              <th>{{ __('general.NAME_LABEL') }}</th>
              <th  style="width: 60px">{{ __('general.ACTIONS_LABEL') }}</th>
            </tr>
          </thead>
          <tbody>
            @foreach($items  as $index => $item)
            <tr>
              <td>{{$item['id']}}</td>
              <td>{{$item['name']}}</td>
            
              <td>
                <a href="{{ route('admin_permissions_groups_edit',['id'=>$item['id']]) }}" class="btn btn-block btn-success">{{ __('general.EDIT_LABEL') }}</a>
                <a href="{{ route('admin_permissions_groups_delete',['id'=>$item['id']]) }}" class="btn btn-block btn-danger">{{ __('general.DELETE_LABEL') }}</a>
              </td>
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>

@endsection
