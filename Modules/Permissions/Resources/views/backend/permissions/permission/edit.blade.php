@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">


       <h3  style="margin: 15px" class="box-title">  
          {{ __('permissions::permissions.permissions.PERMISSIONS_LABEL') }}
          @if (empty($item->id))
            {{ __('general.ADD_LABEL') }}
          @else
           {{ __('general.EDIT_LABEL') }}
          @endif
       </h3>
 
      <form role="form" method="post" action="{{$route}}">
           @csrf
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#basic" data-toggle="tab" aria-expanded="true">
                    {{ __('general.BASIC_LABEL') }}
                  </a>
                </li>
               
              </ul>
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                    <div style="margin: 15px">
                      
                      <div class="form-group">
                        <label>
                          {{ __('permissions::permissions.permissions.MODULE_LABEL') }}
                        </label>
                        <select name="module" class="form-control">
                          @foreach($modules as $module)
                          <option @if($module == $item->module) selected="selected" @endif value="{{$module}}">{{$module}}</option>
                          @endforeach
                        </select>
                       
                        <br>
                      </div>
                      <div class="form-group">
                        <label>
                          {{ __('permissions::permissions.permissions.ACTION_LABEL') }}
                        </label>
                        <select name="action" class="form-control">
                          @foreach($actions as $action)
                          <option @if($action == $item->action) selected="selected" @endif value="{{$action}}">{{$action}}</option>
                          @endforeach
                        </select>
                     
                        <br>
                      </div>                      
                         
                    </div>
                </div>

              </div>

              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>
            <a href="{{ route('admin_permissions_permissions_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
            @if (!empty($item->id))
              <a href="{{ route('admin_permissions_permissions_delete',['id' => $item->id]) }}" class="btn btn-danger">
                {{ __('general.DELETE_LABEL') }}
              </a>
            @endif
        </div>

      </form>
   
    
  </div>
@endsection
@section('footer_scripts')

@endsection
