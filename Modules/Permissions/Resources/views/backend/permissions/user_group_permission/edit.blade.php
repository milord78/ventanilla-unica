@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">


       <h3  style="margin: 15px" class="box-title">  
          {{ __('permissions::permissions.user_permissions_groups.NAME_LABEL') }}
          @if (empty($item->id))
            {{ __('general.ADD_LABEL') }}
          @else
           {{ __('general.EDIT_LABEL') }}
          @endif
       </h3>
 
      <form role="form" method="post" action="{{$route}}">
           @csrf
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#basic" data-toggle="tab" aria-expanded="true">
                    {{ __('general.BASIC_LABEL') }}
                  </a>
                </li>
               
              </ul>
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                   <div style="margin: 15px">
                       
                        <div class="inner" style="margin: 15px">
                          @include('permissions::backend.permissions.user_group_permission.includes.groups_selectable')
                        </div>
                     

                    </div>  
                </div>

              </div>

              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>
            <a href="{{ route('admin_users_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
          
        </div>

      </form>
   
    
  </div>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/admin/permissions_module_related_groups.js') }}"></script>
<script type="text/javascript"></script>
@endsection
