@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">
    <div class="padding-20">
         <h3 class="box-title">  
          {{ __('permissions::permissions.permissions.ACCESS_DENIED_LABEL') }}
       </h3>

       <p>{{ __('permissions::permissions.permissions.ACCESS_DENIED_DESCRIPTION_LABEL') }}</p>
    </div>

    
 

    
  </div>
@endsection
@section('footer_scripts')

@endsection
