<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 



    'permissions'=>[
            

        'PERMISSIONS_LABEL'                =>  'Permissions',
        'PERMISSION_LABEL'                 =>  'Permission',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Permission created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Permission updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Permission deleted successfully',
        'EXISTS_LABEL'                      =>  'Permission exists',
        'MODULE_LABEL'                 =>  'Module',
        'ACTION_LABEL'                 =>  'Action',
        'ACCESS_DENIED_LABEL' => 'Access Denied',
        'ACCESS_DENIED_DESCRIPTION_LABEL' => 'You dont have permission access to this module please contact to the admin and upgrade yours permission to see this module.',
    ],


    'groups'=>[
            

        'GROUPS_LABEL'                =>  'Groups',
        'GROUP_LABEL'                 =>  'Group',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Group created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Group updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Group deleted successfully',
        'EXISTS_LABEL'                      =>  'Group exists',
      
    ],

    'user_permissions_groups'=>[
            

        'NAME_LABEL'                =>  'User Group Permission',
        'GROUP_LABEL'                 =>  'User Group Permission',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'User Group Permission created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'User Group Permission updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'User Group Permission deleted successfully',
        'EXISTS_LABEL'                      =>  'User Group Permission exists',
      
    ],
    
];
