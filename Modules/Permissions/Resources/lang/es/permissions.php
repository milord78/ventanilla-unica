<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 



    'permissions'=>[
            

        'PERMISSIONS_LABEL'                =>  'Permisos',
        'PERMISSION_LABEL'                 =>  'Permisos',
        'CREATED_SUCCESSFULLY_LABEL'       =>  'Permiso creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Permiso actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Permiso borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Permiso existe',
        'MODULE_LABEL'                 =>  'Modulo',
        'ACTION_LABEL'                 =>  'Acción',
        'ACCESS_DENIED_LABEL'          => 'Acceso denegado',
        'ACCESS_DENIED_DESCRIPTION_LABEL' => 'Lo sentimos pero usted no tiene permisos para acceder a este module por favor contacte al administrador para ver este modulo.',
    ],

    'groups'=>[
            

        'GROUPS_LABEL'                =>  'Grupos',
        'GROUP_LABEL'                 =>  'Grupo',
        'CREATED_SUCCESSFULLY_LABEL'       =>  'Grupo creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Grupo actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Grupo borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Grupo existe',
       
    ],
    

    'user_permissions_groups'=>[
            

        'NAME_LABEL'                         =>  'Permisos de usuario',
        'PERMISSION_LABEL'                 =>  'Permisos de usuario permission',
        'CREATED_SUCCESSFULLY_LABEL'       =>  'Permisos de usuario creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Permisos de usuario actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Permisos de usuario borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Permisos de usuario existe',
       
    ],


    
    

];
