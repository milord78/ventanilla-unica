<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('tags')->group(function() {
    Route::get('/', 'TagsController@index');
});

Route::prefix('api')->group(function() {
	Route::prefix('tags')->group(function ()
	{
	  
	  Route::post('/list', ['uses' => 'Api\TagsController@list'])->name('admin_api_blog_tags_list');
	  
	  Route::post('/search', ['uses' => 'Api\TagsController@search'])->name('admin_api_blog_search_tags');

	  Route::post('/get_related', ['uses' => 'Api\TagsController@get_related'])->name('admin_api_blog_tags_related');


	});
});



Route::prefix('admin')->group(function() {
   	Route::prefix('/tags')->group(function () 
	{
	
		// user routes
		Route::get('/index', ['uses' => 'Admin\TagsController@index'])->name('admin_tags_index');
		Route::get('/search', ['uses' => 'Admin\TagsController@search'])->name('admin_tags_search');
		Route::get('/show/{id}', ['uses' => 'Admin\TagsController@show'])->name('admin_tags_show');
		Route::get('/new', ['uses' => 'Admin\TagsController@new'])->name('admin_tags_new');
		Route::get('/edit/{id}', ['uses' => 'Admin\TagsController@edit'])->name('admin_tags_edit');
		Route::post('/store', ['uses' => 'Admin\TagsController@store'])->name('admin_tags_store');
		Route::post('/update/{id}', ['uses' => 'Admin\TagsController@update'])->name('admin_tags_update');
		Route::get('/delete/{id}', ['uses' => 'Admin\TagsController@delete'])->name('admin_tags_delete');
	
	});
});
	