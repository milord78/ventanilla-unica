<?php

namespace Modules\Tags\Entities;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
  	protected $table = 'tags';
    protected $fillable = [
		'publish',
		'title',
		'slug',
		'meta_description',
		'meta_title',
    ];
}
