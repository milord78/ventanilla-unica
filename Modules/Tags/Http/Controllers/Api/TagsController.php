<?php

namespace Modules\Tags\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Tags\Entities\Tags;
use Modules\Posts\Entities\PostsRelatedTags;

class TagsController extends Controller
{
    //
    public function __construct()
    {
       
    }

    
    public function list(Request $request)
    {
    	
        $items = Tags::orderBy('id', 'desc')->paginate(15);
        return response()->json( $items);

    }


    public function search(Request $request)
    {   

        $q = $request->input('name');
        $exclude = $request->input('exclude');

        if( empty($exclude) && !empty($q))
        {
            $items = Tags::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }

        if( !empty($exclude) && !empty($q))
        {
            $items = Tags::orderBy('id', 'desc')->where([
                ['name','=', $q],
                ['id','!=',  $exclude ],
            ])->paginate(15);
        }
          

        if( empty($exclude) && empty($q))
        {
            $items = Tags::paginate(15);
        }

     
        return response()->json( $items);
       
    }
    

    public function get_related(Request $request)
    {
        $id = $request->input('id');
        $items = PostsRelatedTags::where([['post_id','=',$id]])->get();
        $items_ids = [];
        foreach ($items as $key => $value) 
        {
            array_push($items_ids , $value->tags_id);
        }

       
     
        return response()->json( $items_ids);
    }
    

   
}
