<?php

namespace  Modules\Tags\Http\Controllers\Admin;

use Modules\Tags\Entities\Tags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $items = Tags::orderBy('id', 'desc')->paginate(15);
        $q = "";
        return view('tags::backend/tags/list',compact('items','q'));
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        
        $q = $request->input('name');

         if(!empty($q))
        {
            $items = Tags::orderBy('id', 'desc')->where('name', $q )->paginate(15);
        }else
        {
            $items = Tags::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('tags::backend/tags/list',compact('items','q'));
    }

    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $item = Tags::find( $id);
        return view('tags::backend/tags/edit',compact('item'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item= Tags::find( $id);
       
        $item->publish = (bool)$item->publish ;
        
        $route = route('admin_tags_update',['id' => $item->id]);
        return view('tags::backend/tags/edit',compact('item','route'));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {


        $route = route('admin_tags_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->name = null;
        $item->meta_title = null;
        $item->meta_description = null;
        $item->slug = null;
        $item->publish = true;
 
        return view('tags::backend/tags/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'meta_title' => 'required',

        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   

         $inder_data['publish']= (isset($inder_data['publish']))?true:false;

        if(Tags::where(
            [
                ['slug','=',$inder_data['slug']],
                ['id','!=', $id]
            ])->count() == 0 )
        {
            
  
            $redirect_url = redirect()->route(
                "admin_tags_edit",['id'=>$id]
            )->with('success','tags.UPDATE_SUCESSFULLY_LABEL');
           

            Tags::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_tags_new")
                ->with('error','tags.EXISTS_LABEL');  
        }
        
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        
        Tags::find($id)->delete();
        return redirect()->route("admin_tags_index")
                        ->with('success','tags.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'meta_title' => 'required',
           
          
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
        $inder_data['publish']= (isset($inder_data['publish']))?true:false;
        if(Tags::where('slug', $inder_data['slug'])->count() == 0)
        {
        
            $item = Tags::create($inder_data);

            return redirect()->route("admin_tags_edit",['id'=>$item->id])
                        ->with('success','tags.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_tags_new")
                        ->with('error','tags.SLUG_EXISTS_LABEL');  
        }

    }

}
