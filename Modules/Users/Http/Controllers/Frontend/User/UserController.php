<?php

namespace  Modules\Users\Http\Controllers\Frontend\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\ControlNotas\Entities\Carreras;


class UserController extends Controller
{
    private $roles = ["editor",'user','admin','buyer','seller'];
    private $user_roles = ['buyer','seller'];
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function folio(Request $request)
    {
        $this->validate($request, [
        
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
        $user = \Auth::user();
        return view('users::frontend/user/folio',compact('user'));
      
   
    } 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
        
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   
        $inder_data['generar_cerificado_secundaria'] = (isset($inder_data['generar_cerificado_secundaria']))?'SI':'NO';
        
        
        
       
        
        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = str_replace("public", "images", $path);
        }   

          
        




        if(!empty($request->file('ife_ine')))
        {
            $file_name = $request->file('ife_ine')->getClientOriginalName();
            $path = $request->file('ife_ine')->store('public');
            $inder_data['ife_ine'] = str_replace("public", "images", $path);
        } 
        
        if(!empty($request->file('numero_de_seguro_social')))
        {
            $file_name = $request->file('numero_de_seguro_social')->getClientOriginalName();
            $path = $request->file('numero_de_seguro_social')->store('public');
            $inder_data['numero_de_seguro_social'] = str_replace("public", "images", $path);
        } 
          
        if(!empty($request->file('imss_document')))
        {
            $file_name = $request->file('imss_document')->getClientOriginalName();
            $path = $request->file('imss_document')->store('public');
            $inder_data['imss_document'] = str_replace("public", "images", $path);
        }   



        
          
        if(!empty($request->file('certificado_de_secundaria')))
        {
            $file_name = $request->file('certificado_de_secundaria')->getClientOriginalName();
            $path = $request->file('certificado_de_secundaria')->store('public');
            $inder_data['certificado_de_secundaria'] = str_replace("public", "images", $path);
        }   
          
        if(!empty($request->file('certificado_medico')))
        {
            $file_name = $request->file('certificado_medico')->getClientOriginalName();
            $path = $request->file('certificado_medico')->store('public');
            $inder_data['certificado_medico'] = str_replace("public", "images", $path);
        }   
          
        if(!empty($request->file('acta_de_nacimiento')))
        {
            $file_name = $request->file('acta_de_nacimiento')->getClientOriginalName();
            $path = $request->file('acta_de_nacimiento')->store('public');
            $inder_data['acta_de_nacimiento'] = str_replace("public", "images", $path);
        }   
        if(!empty($request->file('comprobante_domicilio')))
        {
            $file_name = $request->file('comprobante_domicilio')->getClientOriginalName();
            $path = $request->file('comprobante_domicilio')->store('public');
            $inder_data['comprobante_domicilio'] = str_replace("public", "images", $path);
        }   

        $redirect_url = redirect()->route("account_users_me")
        ->with('success','user.UPDATE_SUCESSFULLY_LABEL');
        
        $user = \Auth::user();
        
        if( user_full_filled($user)==true)
        {   
            $redirect_url = redirect()->route(
                "account_users_folio"
            )->with('success','user.UPDATE_SUCESSFULLY_LABEL');

        }else{
            User::find( $user->id)->update($inder_data);
        }
    
        return $redirect_url;
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request,  $id=null)
    {
        $this->validate($request, [
            'password1' => 'required',
            'password2' => 'required',
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
        if($inder_data['password1'] != $inder_data['password2'])
        {
            $redirect_url = redirect()->route("account_users_me")
               ->with('password_error','user.PASSWORD_MICTHMACH');
            return $redirect_url;
        }
        else
        {
            $redirect_url = redirect()->route("account_users_me")
                ->with('success','user.PASSWORD_UPDATE_SUCESSFULLY_LABEL');

            if($id==null)
            {
                $id = \Auth::user()->id;
            }

            if( \Auth::user()->id != $id)
            {   
                $redirect_url = redirect()->route(
                    "admin_users_edit",['id'=>$id]
                )->with('success','user.   PASSWORD_UPDATE_SUCESSFULLY_LABEL');
            }
            
            User::find($id)->update(
                array(
                    'password' => bcrypt($inder_data['password1']) 
                )
            );
            return $redirect_url;
        }
    }

   

    /**
     * Show current user to the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function me(Request $request)
    {
        $q = "";
        $roles = $this->roles;
        $route = route('account_user_me_update');
        $route_update_password = route('account_user_me_update_password');
        $user =  \Auth::user();

        $generos = [
            ['id'=>'M','label'=>"controlnotas::rating.alunnos.SEXO_HOMBRE_LABEL"],
            ['id'=>'F','label'=>"controlnotas::rating.alunnos.SEXO_MUJER_LABEL"]
        ];

     
        $estado_civil = [
            ['id'=>'soltero','label'=>"controlnotas::rating.alunnos.SOLTERO_LABEL"],
            ['id'=>'casado','label'=>"controlnotas::rating.alunnos.CASADO_LABEL"],
            ['id'=>'union-libre','label'=>"controlnotas::rating.alunnos.UNION_LIBRE_LABEL"],
            ['id'=>'divorciado','label'=>"controlnotas::rating.alunnos.DIVORCIADO_LABEL"],
            ['id'=>'viudo','label'=>"controlnotas::rating.alunnos.VIUDO_LABEL"]
        ];

        $tipo_sanguineos = [
            ['id'=>'A+','label'=>'A+'],
            ['id'=>'A-','label'=> 'A-',],
            ['id'=>'B+','label'=> 'B+',],
            ['id'=>'B-','label'=> 'B-',],
            ['id'=>'AB+','label'=> 'AB+',],
            ['id'=>'AB-','label'=> 'AB-',],
            ['id'=>'O+','label'=> 'O+',],
            ['id'=>'O-','label'=> 'O-',],
        ];

        $sino = [
            ['id'=>'SI','label'=>'Si'],
            ['id'=>'NO','label'=> 'No',],
       
        ];

        $sinotalvez = [
            ['id'=>'SO','label'=>'Si'],
            ['id'=>'NO','label'=> 'No',],
            ['id'=>'TALVEZ','label'=> 'Tal vez',],
       
        ];
        $escolaridad = [
            ['id'=>'primaria','label'=>"Primaria"],
            ['id'=>'secundaria','label'=>"Secundaria"],
            ['id'=>'bachillerato','label'=>"Bachillerato"],
            ['id'=>'universitario','label'=>"Universitario"],
        ];

        $tipo_secundaria = [
            ['id'=>'GENERAL','label'=>"GENERAL"],
            ['id'=>'TECNICA','label'=>"TÉCNICA"],
            ['id'=>'ADULTOS','label'=>"ADULTOS"],
            ['id'=>'TELESECUNDARIA','label'=>"TELESECUNDARIA"],
        ];
        $carreras = Carreras::orderBy('id', 'desc')->get();
        return view('users::frontend/user/settings',compact('roles','route','route_update_password','user','q',
            'generos',
            'tipo_sanguineos',
            'escolaridad',
            'carreras',
            'sino',
            'tipo_secundaria',
            'sinotalvez',
            'estado_civil'
        ));
    }

}
