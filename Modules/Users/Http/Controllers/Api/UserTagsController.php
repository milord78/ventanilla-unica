<?php

namespace Modules\Users\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Tags\Entities\Tags;
use Modules\Users\Entities\UserRelatedTags;

class UserTagsController extends Controller
{
    //
    public function __construct()
    {
       
    }


    

    public function get_related(Request $request)
    {
        $id = $request->input('id');
        $items = UserRelatedTags::where([['user_id','=',$id]])->get();
        $items_ids = [];
        foreach ($items as $key => $value) 
        {
            array_push($items_ids , $value->tags_id);
        }

       
     
        return response()->json( $items_ids);
    }
    

   
}
