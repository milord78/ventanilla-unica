<?php

namespace Modules\Users\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;


class UserApiController extends Controller
{
    //
    public function __construct()
    {
       
    }

    
    public function list(Request $request)
    {
    	
        $exclude = $request->input('exclude');
        $items = [];
         if( !empty($exclude))
        {
            $items = User::orderBy('id', 'desc')->where([
                ['id','!=',  $exclude ],
            ])->paginate(15);
            
        }else{
            $items = User::orderBy('id', 'desc')->paginate(15);
        }
        foreach($items as $key => $value)
        {
            $value->link_details = route('admin_users_show',['id'=>$value->id]);
           
        }
        return response()->json( $items);

    }


    public function search(Request $request)
    {   

        $q = $request->input('title');
        $exclude = $request->input('exclude');
        $items = [];
        if( empty($exclude) && !empty($q))
        {
            $items = User::orderBy('id', 'desc')->where('email', $q )->paginate(15);
        }

        if( !empty($exclude) && !empty($q))
        {
            $items = User::orderBy('id', 'desc')->where([
                ['email','=', $q],
                ['id','!=',  $exclude ],
            ])->paginate(15);
        }
          

        if( empty($exclude) && empty($q))
        {
            $items = User::paginate(15);
        }
        foreach($items as $key => $value)
        {
            $value->link_details = route('admin_users_show',['id'=>$value->id]);
           
        }
     
        return response()->json( $items);
       
    }
    

    

 
   
}
