<?php

namespace  Modules\Users\Http\Controllers\Admin\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\ControlNotas\Entities\Carreras;

class UserController extends Controller
{
    private $roles = ["editor",'admin','auxiliar','estudiante','aspriante'];
    private $user_roles = ['buyer','seller'];
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$page=1)
    {
        $this->middleware('admin_check');
        \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $status = $request->input('status','pendiente');        
        $users = User::orderBy('id', 'desc')->paginate(15);
        $pending = false;
        $q = "";
        return view(
            'users::backend/user/list',
            compact('users','q','pending','status')
        );
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $pending = false;
        $status = $request->input('status','pendiente');        
        $q = $request->input('email');

         if(!empty($q))
        {
            $users = User::orderBy('id', 'desc')->where('email', $q )->paginate(15);
        }else
        {
            $users = User::orderBy('id', 'desc')->paginate(15);
        }
       
        return view('users::backend/user/list',compact('users','q','pending','status'));
    }

    public function index_pending(Request $request,$page=1)
    {
        $this->middleware('admin_check');
        \Illuminate\Pagination\Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        $status = $request->input('status','pendiente');
        $users = User::where([['status','=',$status]])->orderBy('id', 'desc')->paginate(15);
        
        $q = "";
        $pending = false;
        return view(
            'users::backend/user/list',
            compact('users','q','pending','status')
        );
    }


    /**
     * Search the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search_pending(Request $request)
    {
        $pending = false;
        $q = $request->input('email');
        $status = $request->input('status','pendiente');

         if(!empty($q))
        {
            $users = User::where([['status','=',$status],['email','=', $q ]])->orderBy('id', 'desc')->paginate(15);
        }else
        {
            $users = User::where([['status','=',$status]])->orderBy('id', 'desc')->paginate(15);
        }
       
        return view('users::backend/user/list',compact('users','q','pending','status'));
    }
    
     /**
     * Show the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::find( $id);
        $roles = $this->roles;
        $generos = [
            ['id'=>'hombre','label'=>"controlnotas::rating.alunnos.SEXO_HOMBRE_LABEL"],
            ['id'=>'mujer','label'=>"controlnotas::rating.alunnos.SEXO_MUJER_LABEL"]
        ];

        $tipo_sanguineos = [
            ['id'=>'A+','label'=>'A+'],
            ['id'=>'A-','label'=> 'A-',],
            ['id'=>'B+','label'=> 'B+',],
            ['id'=>'B-','label'=> 'B-',],
            ['id'=>'AB+','label'=> 'AB+',],
            ['id'=>'AB-','label'=> 'AB-',],
            ['id'=>'O+','label'=> 'O+',],
            ['id'=>'O-','label'=> 'O-',],
        ];


        $escolaridad = [
            ['id'=>'primaria','label'=>"Primaria"],
            ['id'=>'secundaria','label'=>"Secundaria"],
            ['id'=>'bachillerato','label'=>"Bachillerato"],
            ['id'=>'universitario','label'=>"Universitario"],
        ];


        $status = [
            ['id'=>'pendiente','label'=>"Pendiente"],
            ['id'=>'rechazado','label'=>"Rechazado"],
            ['id'=>'aprobado','label'=>"Aprobado"],
            ['id'=>'resubir-documentos','label'=>"Resubir documentos"],
        ];


            $sino = [
                       ['id'=>'Si','label'=>'Si'],
                       ['id'=>'No','label'=> 'No',],
                  
                   ];
            $tipo_secundaria = [
                       ['id'=>'GENERAL','label'=>"GENERAL"],
                       ['id'=>'TECNICA','label'=>"TÉCNICA"],
                       ['id'=>'ADULTOS','label'=>"ADULTOS"],
                       ['id'=>'TELESECUNDARIA','label'=>"TELESECUNDARIA"],
                   ];


        $route = route('admin_users_update_status',['id' => $user->id]);

       
        return view('users::backend/user/show',compact(
            'route',
            'user', 
            'roles',
            'carreras',
            'generos',
            'tipo_sanguineos',
            'escolaridad',
            'status',
            'sino',
            'tipo_secundaria'
        ));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $user = User::find( $id);
        $roles = $this->roles;
        $route_update_password = route('admin_users_update_password',['id' => $user->id]);
        $route = route('admin_users_update',['id' => $user->id]);




        return view('users::backend/user/edit',compact('user','roles','route','route_update_password'));
    }

    public function validar(Request $request, $id)
    {
        $user = User::find( $id);
        $roles = $this->roles;

        $route = route('admin_users_validate_update',['id' => $user->id]);

        $generos = [
            ['id'=>'M','label'=>"controlnotas::rating.alunnos.SEXO_HOMBRE_LABEL"],
            ['id'=>'F','label'=>"controlnotas::rating.alunnos.SEXO_MUJER_LABEL"]
        ];

     
        $estado_civil = [
            ['id'=>'soltero','label'=>"controlnotas::rating.alunnos.SOLTERO_LABEL"],
            ['id'=>'casado','label'=>"controlnotas::rating.alunnos.CASADO_LABEL"],
            ['id'=>'union-libre','label'=>"controlnotas::rating.alunnos.UNION_LIBRE_LABEL"],
            ['id'=>'divorciado','label'=>"controlnotas::rating.alunnos.DIVORCIADO_LABEL"],
            ['id'=>'viudo','label'=>"controlnotas::rating.alunnos.VIUDO_LABEL"]
        ];

        $tipo_sanguineos = [
            ['id'=>'A+','label'=>'A+'],
            ['id'=>'A-','label'=> 'A-',],
            ['id'=>'B+','label'=> 'B+',],
            ['id'=>'B-','label'=> 'B-',],
            ['id'=>'AB+','label'=> 'AB+',],
            ['id'=>'AB-','label'=> 'AB-',],
            ['id'=>'O+','label'=> 'O+',],
            ['id'=>'O-','label'=> 'O-',],
        ];

        $sino = [
            ['id'=>'SI','label'=>'Si'],
            ['id'=>'NO','label'=> 'No',],
       
        ];

        $sinotalvez = [
            ['id'=>'SO','label'=>'Si'],
            ['id'=>'NO','label'=> 'No',],
            ['id'=>'TALVEZ','label'=> 'Tal vez',],
       
        ];
        $escolaridad = [
            ['id'=>'primaria','label'=>"Primaria"],
            ['id'=>'secundaria','label'=>"Secundaria"],
            ['id'=>'bachillerato','label'=>"Bachillerato"],
            ['id'=>'universitario','label'=>"Universitario"],
        ];

        $tipo_secundaria = [
            ['id'=>'GENERAL','label'=>"GENERAL"],
            ['id'=>'TECNICA','label'=>"TÉCNICA"],
            ['id'=>'ADULTOS','label'=>"ADULTOS"],
            ['id'=>'TELESECUNDARIA','label'=>"TELESECUNDARIA"],
        ];
        $carreras = Carreras::orderBy('id', 'desc')->get();
        
        return view('users::backend/user/settings',compact(
            'user',
            'roles',
            'route',
            'generos',
            'tipo_sanguineos',
            'escolaridad',
            'carreras',
            'sino',
            'tipo_secundaria',
            'sinotalvez',
            'estado_civil'
        ));
    }
 
    /**
     * Add new item specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {

        $roles = $this->roles;
        $route = route('admin_users_store');
        
        
        $user = new \stdClass();
        $user->id = null;
        $user->firstname = null;
        $user->midname = null;
        $user->lastname = null;
        $user->email = null;
        $user->address = null;
        $user->password = null;
        $user->role = null;
        $route_update_password = route('admin_users_update_password',['id' =>0]);
 
        return view('users::backend/user/edit',compact('roles','route','route_update_password','user'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
           'firstname',
           'midname',
           'lastname',
           'role',
           'email'
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   

        $tags = isset($inder_data['tags'])?$inder_data['tags']:[];
   

        if(empty($id))
        {
            $id = \Auth::user()->id;
        }
        
        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   


        if(User::where(
            [
                ['email','=',$inder_data['email']],
                ['id','!=', $id]
            ])->count() == 0 )
        {
            $redirect_url = redirect()->route("admin_users_me")
                ->with('success','user.UPDATE_SUCESSFULLY_LABEL');

            if( \Auth::user()->id != $id)
            {   
                $redirect_url = redirect()->route(
                    "admin_users_edit",['id'=>$id]
                )->with('success','user.UPDATE_SUCESSFULLY_LABEL');
            }

            User::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_users_new")
                ->with('error','user.EMAIL_EXISTS_LABEL');  
        }
        
    }

    public function validate_update(Request $request, $id=null)
    {
        $this->validate($request, [
           'firstname',
           'midname',
           'lastname',
           'role',
           'email'
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   



        $inder_data['generar_cerificado_secundaria'] = (isset($inder_data['generar_cerificado_secundaria']))?'SI':'NO';
        
        
        
       
       

        if(empty($id))
        {
            $id = \Auth::user()->id;
        }
        
        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   

        
        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = str_replace("public", "images", $path);
        }   


        if(!empty($request->file('ife_ine')))
        {
            $file_name = $request->file('ife_ine')->getClientOriginalName();
            $path = $request->file('ife_ine')->store('public');
            $inder_data['ife_ine'] = str_replace("public", "images", $path);
        } 
        
        if(!empty($request->file('numero_de_seguro_social')))
        {
            $file_name = $request->file('numero_de_seguro_social')->getClientOriginalName();
            $path = $request->file('numero_de_seguro_social')->store('public');
            $inder_data['numero_de_seguro_social'] = str_replace("public", "images", $path);
        } 
          
        if(!empty($request->file('imss_document')))
        {
            $file_name = $request->file('imss_document')->getClientOriginalName();
            $path = $request->file('imss_document')->store('public');
            $inder_data['imss_document'] = str_replace("public", "images", $path);
        }   



        
          
        if(!empty($request->file('certificado_de_secundaria')))
        {
            $file_name = $request->file('certificado_de_secundaria')->getClientOriginalName();
            $path = $request->file('certificado_de_secundaria')->store('public');
            $inder_data['certificado_de_secundaria'] = str_replace("public", "images", $path);
        }   
          
        if(!empty($request->file('certificado_medico')))
        {
            $file_name = $request->file('certificado_medico')->getClientOriginalName();
            $path = $request->file('certificado_medico')->store('public');
            $inder_data['certificado_medico'] = str_replace("public", "images", $path);
        }   
          
        if(!empty($request->file('acta_de_nacimiento')))
        {
            $file_name = $request->file('acta_de_nacimiento')->getClientOriginalName();
            $path = $request->file('acta_de_nacimiento')->store('public');
            $inder_data['acta_de_nacimiento'] = str_replace("public", "images", $path);
        }   
        if(!empty($request->file('comprobante_domicilio')))
        {
            $file_name = $request->file('comprobante_domicilio')->getClientOriginalName();
            $path = $request->file('comprobante_domicilio')->store('public');
            $inder_data['comprobante_domicilio'] = str_replace("public", "images", $path);
        }   





        if(User::where(
            [
                ['email','=',$inder_data['email']],
                ['id','!=', $id]
            ])->count() == 0 )
        {
            


            $redirect_url = redirect()->route(
                "admin_users_validate",['id'=>$id]
            )->with('success','user.UPDATE_SUCESSFULLY_LABEL');

            User::find($id)->update($inder_data);
            return $redirect_url;
        }
        else
        {
            return redirect()->route("admin_users_new")
                ->with('error','user.EMAIL_EXISTS_LABEL');  
        }
        
    }


 
    public function update_status(Request $request, $id=null)
    {
        $this->validate($request, [
       
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   



        if(empty($id))
        {
            $id = \Auth::user()->id;
        }
      


       
    
                $redirect_url = redirect()->route(
                    "admin_users_show",['id'=>$id]
                )->with('success','user.UPDATE_SUCESSFULLY_LABEL');
           
            User::find($id)->update($inder_data);
            return $redirect_url;
      
        
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request,  $id=null)
    {
        $this->validate($request, [
            'password1' => 'required',
            'password2' => 'required',
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
        if($inder_data['password1'] != $inder_data['password2'])
        {
            $redirect_url = redirect()->route("admin_users_edit")
               ->with('password_error','user.PASSWORD_MICTHMACH');
            return $redirect_url;
        }
        else
        {
            $redirect_url = redirect()->route("admin_users_me")
                ->with('success','user.PASSWORD_UPDATE_SUCESSFULLY_LABEL');

            if($id==null)
            {
                $id = \Auth::user()->id;
            }

            if( \Auth::user()->id != $id)
            {   
                $redirect_url = redirect()->route(
                    "admin_users_edit",['id'=>$id]
                )->with('success','user.   PASSWORD_UPDATE_SUCESSFULLY_LABEL');
            }
            
            User::find($id)->update(
                array(
                    'password' => bcrypt($inder_data['password1']) 
                )
            );
            return $redirect_url;
        }
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
         $this->middleware('admin_check');
        User::find($id)->delete();
        return redirect()->route("admin_users_index")
                        ->with('success','user.DELETED_SUCESSFULLY_LABEL');
    }

   
    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->middleware('admin_check');
        $this->validate($request, [
            'firstname' => 'required',
            'midname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
        $tags = isset($inder_data['tags'])?$inder_data['tags']:[];
   

        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   

        
        if(User::where('email', $inder_data['email'])->count() == 0)
        {
            $inder_data['password'] = bcrypt($inder_data['password']);
            $user = User::create($inder_data);

            save_user_tags_related(
                $user->id,
                $tags
            );
            return redirect()->route("admin_users_edit",['id'=>$user->id])
                        ->with('success','user.USER_CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_users_new")
                        ->with('error','user.EMAIL_EXISTS_LABEL');  
        }

    }

    /**
     * Show current user to the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function me(Request $request)
    {

        $roles = $this->roles;
        $route = route('admin_user_me_update');
        $route_update_password = route('admin_user_me_update_password');
        $user =  \Auth::user();

 
        return view('users::backend/user/edit',compact('roles','route','route_update_password','user'));
    }

}
