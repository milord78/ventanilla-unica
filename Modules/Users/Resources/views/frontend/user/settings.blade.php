@extends('layouts.user_panel')

@section('content')
<div class="app-heading app-heading-bordered app-heading-page">
    <div class="icon icon-lg">
        <span class="icon-list3"></span>
    </div>
    <div class="title">            
        <h1>   {{ __('user.PROFILE_LABEL') }} del aspirante</h1>
    </div>
</div>

    
<div class="app-heading-container app-heading-bordered bottom" >
<div class="pull-right" style="padding:10px">
    Fecha de captura de datos: 
    @if($user->status=="aprobado")
   
    <?php echo date("Y/m/d", strtotime($user->updated_at)) ?> 
   
      @else
     
      <?php echo (new \DateTime())->format("Y/m/d"); ?> 

    @endif
    </div>
    <ul class="breadcrumb">
        <li><a href="#">Inicio</a></li>
        <li class="active">
        {{ __('user.PROFILE_LABEL') }} del aspirante
        </li>
       
    </ul>
   
</div>

<div class="">
  <div class="block"> 
    @if (\Session::has('success'))
    <div class="alert alert-success">
      <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{ __(\Session::get('error')) }}</p>
    </div><br />
  @endif
    <div class="">


    <div>
            @if($user->status=="aprobado")
            <div class="alert alert-success">
              <p>Estatus actual: aprobado</p>
              </div>
            @endif
            @if($user->status=="pendiente")
            <div class="alert alert-info">
              <p>Estatus actual: Actualizar datos pendiente</p>
            </div>
      
            @endif
            @if($user->status=="rechazado")
            <div class="alert alert-danger">
              <p>Estatus actual: lo sentimos pero hemos rechazado tu perfil por favor comunicate con nosostros</p>
            </div>
       
            @endif
            @if($user->status=="resubir-documentos")
            <div class="alert alert-info">
              <p>Estatus actual: Por favor vuelve  a subir documentos</p>
            </div>
            
            @endif

          </div>

      
      <form  style="padding: 15px;" role="form" enctype="multipart/form-data" method="post" action="{{$route}}">
        @csrf
        <div>

            <!-- Nav tabs -->
            <ul id="selectionsy" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">1.  Datos del aspirante </a></li>
              <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">2. Información académica </a></li>
              <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">3. Información médica</a></li>
              <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">4. Datos de los familiares</a></li>
              <li role="presentation"><a href="#settings2" aria-controls="settings2" role="tab" data-toggle="tab">5. Datos del Tutor</a></li>
            
            </ul>

            <!-- Tab panes -->
            <div  id="selectionsy2"  class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="home">     
                
                <div style="padding:10px">

                        <div class="row clearfix">
                          <div class="col-md-2">
                              <div class="avatar" style="background-image:url({{\Auth::user()->logos()}}); background-size: cover;height: 143px;width: 100%; border-radius: 5px;">
                              </div>
                              @if($user->status=="aprobado")
                              @else
                                <div style="padding:15px">
                                      <span  id="thumbnail_label"></span>
                                      <button id="thumbnail" type="button" class="btn btn-primary">
                                        {{__('Cargar Foto') }}
                                      </button>
                                      <input type="file" class="hide" name="thumbnail">
                              
                                </div>
                              @endif
                          </div>
                          <div class="col-md-8">
                            <h4>Recomendaciones para la toma de la fotografía</h4>
                            <ul>
                              <li>Cabello Recogido</li>
                              <li>Frente despejada</li>
                              <li>Sin lentes</li>
                              <li>Sin aretes</li>
                              <li>Fondo blanco o claro</li>
                              <li>Ropa blanca sin diseños</li>
                            </ul>
                          </div>
                        </div>

                        <div class="row clearfix">

                       
                          <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">
                                          {{ __('Nombre') }}:
                                        </label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->firstname}}</div>
                                        @else
                                        <input type="text" name="firstname"  value="{{$user->firstname}}" required class="form-control" >
                                        @endif
                                      
                          </div>

                          <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">
                                          {{ __('Segundo Nombre') }}:
                                        </label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->midname}}</div>
                                        @else
                                        <input type="text" name="midname"  value="{{$user->midname}}" required class="form-control" >
                                        @endif
                                      
                          </div>
                          <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">
                                          {{ __('Apellido Paterno') }}:
                                        </label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->lastname}}</div>
                                        @else
                                        <input type="text" name="lastname"  value="{{$user->lastname}}" required class="form-control" >
                                        @endif
                                      
                          </div> 
                          
                          <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">
                                          {{ __('Apellido Mateno') }}:
                                        </label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->lastname_materno}}</div>
                                        @else
                                        <input type="text" name="lastname_materno"  value="{{$user->lastname_materno}}" required class="form-control" >
                                        @endif
                                      
                          </div> 
                          <div class="form-group col-md-3">
                                      <label>{{ __('controlnotas::rating.alunnos.FECHA_NAC_LABEL') }}:</label>

                                      @if($user->status=="aprobado")
                                      <div>{{$user->fecha_nac}}</div>
                                      @else
                                      <div class="input-group date">
                                        <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </div>
                                        <input  placeholder="2006-12-31" type="fecha_nac" class="form-control pull-right datepicker" id="datepicker" value="{{$user->fecha_nac}}" >
                                      </div>
                                      @endif
                                     
                        </div>
                          <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">
                                          {{ __('Email') }}:
                                        </label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->email}}</div>
                                        @else
                                        <input type="text" name="lastname"  value="{{$user->email}}" required class="form-control" >
                                        @endif
                                      
                          </div> 
                          <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">
                                          {{ __('Teléfono Fijo') }}:
                                        </label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->phone}}</div>
                                        @else
                                        <input type="text" name="phone"  value="{{$user->phone}}" maxlength="10"   class="form-control" >
                                        @endif
                                      
                          </div> 
                          <div class="form-group col-md-3">
                                        <label>Teléfono Celular:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->cell_phone}}</div>
                                        @else
                                        <input type="text" name="cell_phone"  value="{{$user->cell_phone}}" maxlength="10"   class="form-control" >
                                        @endif
                                    
                                    
                                      
                          </div>

                          <div class="form-group col-md-3">
                                        <label>{{ __('controlnotas::rating.alunnos.SEXO_LABEL') }}:</label>
                                        @if($user->status=="aprobado")
                                      <div>
                                          @foreach ($generos as $key => $value)  
                                    
                                          @if($user->genero==$value['id']) {{__($value['label'])}} @endif
                                          @endforeach
                                      </div>

                                      @else
                                        <div>
                                          @foreach ($generos as $key => $value)  
                                            <input  name="genero" type="radio" @if($user->genero==$value['id']) checked="checked" @endif value="{{$value['id']}}" /> <label>{{__($value['label'])}}</label>
                                          @endforeach
                                        </div>
                                      @endif
                                
                        </div>

                        <div class="form-group  col-md-6">
                                        <label>{{ __('controlnotas::rating.alunnos.ESTADO_CIVIL_LABEL') }}:</label>
                                        @if($user->status=="aprobado")
                                      <div>
                                          @foreach ($estado_civil as $key => $value)  
                                    
                                          @if($user->genero==$value['id']) {{__($value['label'])}} @endif
                                          @endforeach
                                      </div>

                                      @else
                                        <div>
                                          @foreach ($estado_civil as $key => $value)  
                                            <input  name="estado_civil" type="radio" @if($user->genero==$value['id']) checked="checked" @endif value="{{$value['id']}}" /> <label>{{__($value['label'])}}</label>
                                          @endforeach
                                        </div>
                                      @endif
                                      
                                      
                        </div>


                        </div>
            
                        
                        <h5 style="font-weight:bold">
                          {{ __('Dirección') }}
                        </h5>
                        <div style="padding:10px " class="row clearfix">
                          <div class="form-group col-md-3">
                                          <label>Calle:</label>
                                          @if($user->status=="aprobado")
                                          <div>{{$user->calle}}</div>
                                          @else
                                          <input type="text" name="calle"  value="{{$user->calle}}"  class="form-control" >
                                          @endif
                                      
                                      
                                          
                          </div>
                          <div class="form-group col-md-3">
                                          <label>Número:</label>
                                          @if($user->status=="aprobado")
                                          <div>{{$user->numero}}</div>
                                          @else
                                          <input type="text" name="numero"  value="{{$user->numero}}"  class="form-control" >
                                          @endif
                                        
                          </div>
                          <div class="form-group col-md-3">
                                        <label>Cruzamientos:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->cruzamientos}}</div>
                                        @else
                                        <input type="text" name="cruzamientos"  value="{{$user->cruzamientos}}"  class="form-control" >
                                        @endif
                                     
                          </div>
                          <div class="form-group col-md-3">
                                        <label>Colonia:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->colonia}}</div>
                                        @else
                                        <input type="text" name="colonia"  value="{{$user->colonia}}"  class="form-control" >
                                        @endif
                                      
                          </div>
                          <div class="form-group col-md-3">
                                        <label>Código postal:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->zip_code}}</div>
                                        @else
                                        <input type="text" name="zip_code"  value="{{$user->zip_code}}"  class="form-control" >
                                        @endif
                                       
                          </div> 
                          <div class="form-group col-md-3">
                                        <label>Municipio:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->municipio}}</div>
                                        @else
                                        <input type="text" name="municipio"  value="{{$user->municipio}}"  class="form-control" >
                                        @endif
                                      
                          </div>
                          <div class="form-group col-md-3">
                                        <label>Entidad federativa:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->entidad_federativa}}</div>
                                        @else
                                        <input type="text" name="entidad_federativa"  value="{{$user->entidad_federativa}}"  class="form-control" >
                                        @endif
                                     
                          </div>   

                          
                          <div class="form-group col-md-3">
                                        <label>Estado donde nació:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->clave_estado_nac}}</div>
                                        @else
                                        <input type="text" name="clave_estado_nac"  value="{{$user->clave_estado_nac}}"  class="form-control" >
                                        @endif
                              
                          </div>   

                          <div class="form-group col-md-3">
                                        <label>Municipio donde nació:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->clave_municipio_nac}}</div>
                                        @else
                                        <input type="text" name="clave_municipio_nac"  value="{{$user->clave_municipio_nac}}"  class="form-control" >
                                        @endif
                                      
                          </div>   
                          <div class="form-group col-md-3">
                                        <label>Municipio donde vive:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->clave_locclave_municipio_vivealidad_vive}}</div>
                                        @else
                                        <input type="text" name="clave_municipio_vive"  value="{{$user->clave_municipio_vive}}"  class="form-control" >
                                        @endif
                                       
                          </div>   

                          <div class="form-group col-md-3">
                                        <label>Localidad donde vive:</label>
                                        @if($user->status=="aprobado")
                                        <div>{{$user->clave_localidad_vive}}</div>
                                        @else
                                        <input type="text" name="clave_localidad_vive"  value="{{$user->clave_localidad_vive}}"  class="form-control" >
                                        @endif
                                      
                          </div>   

                        </div>

                        




                 

                          
                </div>
              
                </div>
                <div role="tabpanel" class="tab-pane" id="settings">
                                  <div class="clearfix" style="width:100%;">
                                      <h3 class="box-title clearfix">
                                        {{ __('Elegir Especialidades por orden de preferencia ') }}
                                      </h3>
                                      <hr>
                                      <div class="row clearfix">
                                      <div class="form-group col-md-3">
                                        @if($user->especialidad_1==null) 
                                       
                                          <label>Especialidad 1:</label>
                                          <select name="especialidad_1" class="form-control">
                                          <option >&nbsp;</option>
                                            @foreach ($carreras as $key => $value)  
                                              <option value="{{$value->carrera}}">{{($value->carrera)}}</option>
                                            @endforeach
                                          </select>
                                         
                                      
                                        @else
                                        <label>{{$user->especialidad_1}}</label>
                                        @endif  
                                        </div>
                                        <div class="form-group  col-md-3">
                                        @if($user->especialidad_2==null) 
                                       
                                          <label>Especialidad 2:</label>
                                          <select name="especialidad_2" class="form-control">
                                          <option >&nbsp;</option>
                                            @foreach ($carreras as $key => $value)  
                                              <option value="{{$value->carrera}}">{{($value->carrera)}}</option>
                                            @endforeach
                                          </select>
                                         
                                        
                                        @else
                                        <label>{{$user->especialidad_2}}</label>
                                        @endif  
                                        </div>
                                        <div class="form-group  col-md-3">
                                        @if($user->especialidad_3==null) 
                                     
                                          <label>Especialidad 3:</label>
                                          <select name="especialidad_3" class="form-control">
                                          <option >&nbsp;</option>
                                            @foreach ($carreras as $key => $value)  
                                              <option value="{{$value->carrera}}">{{($value->carrera)}}</option>
                                            @endforeach
                                          </select>
                                         
                                       
                                        @else
                                        <label>{{$user->especialidad_3}}</label>
                                        @endif  
                                        </div>
                                      </div>
                                     
                              
                                  </div>

                                  <h3 class="box-title">
                                    {{ __('Cargar documentos (extensiones de archivo PDF, JPG o PNG) ') }}
                                  </h3>
                                    
                                  <hr>
                                  <div class="row clearfix">
                                    <div class="form-group col-md-3">
                                      <label>Número IMSS: <span id="imss_document_label"></span></label>

                                      <div>
                                        @if($user->status=="aprobado")
                                        @else
                                        <button id="imss_document" type="button" class="btn btn-primary">
                                        Cargar IMSS
                                        </button>
                                        <input type="file" class="hide" name="imss_document">
                                        @endif

                                        @if($user->imss_document!=null) 
                                        <a href="{{$user->imss_document}}" class="btn btn-primary">
                                          {{__('general.SEE_LABEL') }}&nbsp;
                                          Documento
                                        </a>  
                                        @endif  
                                        <a target="_blank" href="http://www.imss.gob.mx/tramites/imss02008">CONSULTAR NSS</a>
                                      </div>
                                      
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>CAPTURAR NSS: </label>
                                      @if($user->status=="aprobado")
                                      <div>{{$user->nss_number}}</div>
                                      @else
                                      <input type="text" name="nss_number"  value="{{$user->nss_number}}"  class="form-control" >
                                      @endif
                                  
                                  
                                  
                                    </div>

                                  </div>
                         
                                  <div class="box-body">
                                    <label> Acta de nacimiento: <span id="acta_de_nacimiento_label"></span></label>

                                    @if($user->status=="aprobado")
                                    @else
                                    <button id="acta_de_nacimiento" type="button" class="btn btn-primary">
                                    Cargar  Acta de nacimiento
                                    </button>
                                    <input type="file" class="hide" name="acta_de_nacimiento">
                                    @endif
                                    
                                
                                    @if($user->acta_de_nacimiento!=null) 
                                    <a href="{{$user->acta_de_nacimiento}}" class="btn btn-primary">
                                      {{__('general.SEE_LABEL') }}&nbsp;
                                      Documento
                                    </a>  
                                    @endif  
                                  
                                  </div>
                                  <br>

                                  <div class="box-body">
                                    <label> Comprobante de Domicilio: <span id="comprobante_domicilio_label"></span></label>
                                


                                    @if($user->status=="aprobado")
                                    @else
                                    <button id="comprobante_domicilio" type="button" class="btn btn-primary">
                                    Cargar Comprobante de Domicilio
                                    </button>
                                    <input type="file" class="hide" name="comprobante_domicilio">
                                    @endif
                                    
                
                                    @if($user->comprobante_domicilio!=null) 
                                    <a href="{{$user->comprobante_domicilio}}" class="btn btn-primary">
                                      {{__('general.SEE_LABEL') }}&nbsp;
                                    Documento
                                    </a>  
                                    @endif  
                                  
                                  </div>
                                  <br>
                          
                                  <div class="box-body">
                                    <label> Boleta de tercer año o constancia de estudios: <span id="boleta_primaria_secundaria_label"></span></label>
                              
                
                                    @if($user->status=="aprobado")
                                    @else
                                    <button id="boleta_primaria_secundaria" type="button" class="btn btn-primary">
                                    CARGAR BOLETA O CONSTANCIA
                                    </button>
                                    <input type="file" class="hide" name="boleta_primaria_secundaria">
                                    @endif
                                    
                                    @if($user->boleta_primaria_secundaria!=null) 
                                    <a href="{{$user->boleta_primaria_secundaria}}" class="btn btn-primary">
                                      {{__('general.SEE_LABEL') }}&nbsp;
                                      Documento
                                    </a>  
                                    @endif  
                                  
                                  </div>
                                  <br>
                                  <div class="row clearfix">
                                      <div class="box-body col-md-4">
                                        <label> CURP: <span id="curp_label"></span></label>
                                        <div>
                                          @if($user->status=="aprobado")
                                          @else
                                          <button id="curp" type="button" class="btn btn-primary">
                                          Cargar CURP
                                          </button>
                                          <input type="file" class="hide" name="curp">
                                          @endif

                      
                                          @if($user->curp!=null) 
                                          <a href="{{$user->curp}}" class="btn btn-primary">
                                            {{__('general.SEE_LABEL') }}&nbsp;
                                            Documento
                                          </a>  
                                          @endif  
                                          <a  target="_blank" href="https://www.gob.mx/curp/">CONSULTAR CURP</a>
                                        </div>
                                      </div>
                                      
                                      <div class="box-body col-md-3">
                                
                                          <label>Capturar CURP: </label>

                                          @if($user->status=="aprobado")
                                          <div>{{$user->curp_number}}</div>
                                        @else
                                        <input type="text" name="curp_number"  value="{{$user->curp_number}}" required class="form-control" >
                                        @endif

                                        
                                      </div>
                                  
                                  </div>
                                  <br>
                              
                                  <div class="showcenval">
                                    <label> 
                                    Ya cuento con mi folio y resultado de CENEVAL:  
                                      <input type="checkbox" value="SI" @if ($user->tiene_ceneval  == "SI") checked="checked" @endif name="tiene_ceneval">
                                    </label>

                              
                                    <div class="ceneval hide row clearfix">
                                        <div class="form-group col-md-3" >
                                          <label> Resultado CENEVAL:</label>

                                            @if($user->status=="aprobado")
                                              <div>{{$user->resultado_ceneval}}</div>
                                            @else
                                              <input type="text" name="resultado_ceneval"  value="{{$user->resultado_ceneval}}"  class="form-control" >
                                            @endif                  
                                            
                                        </div>

                                        <div class="form-group col-md-3" >
                                          <label> Folio CENEVAL:</label>

                                            @if($user->status=="aprobado")
                                              <div>{{$user->folio_ceneval}}</div>
                                            @else
                                              <input type="text" name="folio_ceneval"  value="{{$user->folio_ceneval}}"  class="form-control" >
                                            @endif                  
                                            
                                        </div>
                                    </div>
                                    
                                  </div>
                                  <br>

                                  <div class="box-body">
                                    <label> Certificado de secundaria: <span id="certificado_de_secundaria_label"></span></label>
                                    @if($user->status=="aprobado")
                                      <label>>No cuento con mi certificado de Secundaria: {{$user->generar_cerificado_secundaria }} </label>
                                    @else
                                    <button id="certificado_de_secundaria" type="button" class="btn btn-primary">
                                    Cargar Certificado de secundaria
                                    </button>
                                    <input type="file" class="hide" name="certificado_de_secundaria">
                                    <input type="checkbox" value="SI" @if ($user->generar_cerificado_secundaria  == "SI") checked="checked" @endif name="generar_cerificado_secundaria">
                                    <label>No cuento con mi certificado de Secundaria</label>
                                    @endif
                                  
                                    @if($user->certificado_de_secundaria!=null) 
                                    <a href="{{$user->certificado_de_secundaria}}" class="btn btn-primary">
                                      {{__('general.SEE_LABEL') }}&nbsp;
                                      Documento
                                    </a> 
                                    @endif  
                                  </div>
                          
                                 <br>
                                <div class="row clearfix">
                                  <div  id="fecha_fin_secundaria" class="form-group col-md-3">
                                    
                                    <label>{{ __('Fecha de finalización Secundaria') }}:</label>

                                      @if($user->status=="aprobado")
                                        <div>
                                        {{$user->fecha_fin_secundaria}}
                                        </div>
                                      @else
                                      <div class="input-group date">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                          </div>
                                          <input   placeholder="2006-12-31" type="fecha_fin_secundaria" class="form-control pull-right datepicker" id="datepicker" value="{{$user->fecha_fin_secundaria}}" >
                                        </div>
                                                
                                      @endif
                               
                                  </div>
                                  <div id="nombre_Secundaria_procedencia" class="form-group col-md-3">
                                    <label>Nombre secundaria procedencia:</label>
                                    @if($user->status=="aprobado")
                                    <div>{{$user->nombre_Secundaria_procedencia}}</div>
                                    @else
                                    <input type="text" name="nombre_Secundaria_procedencia"  value="{{$user->nombre_Secundaria_procedencia}}"  class="form-control" >
                                    @endif
                                  
                                  </div>       
                                 
                                  <div id="tipo_de_secundaria" class="form-group  col-md-3">

                                        @if($user->status=="aprobado")
                                        <div>
                                          <label>Tipo de Secundaria:</label>

                                          @foreach ($tipo_de_secundaria as $key => $value)  
                                            @if($user->tipo_de_secundaria==$value['id']) {{($value['label'])}}@endif 
                                          
                                          @endforeach
                                        
                                        
                                        </div>
                                        @else
                                          <label>Tipo de Secundaria:</label>
                                          <select name="tipo_de_secundaria" class="form-control">
                                          <option>&nbsp;</option>
                                            @foreach ($tipo_secundaria as $key => $value)  
                                              <option @if($user->tipo_de_secundaria==$value['id']) selected="selected" @endif value="{{$value['id']}}">{{($value['label'])}}</option>
                                            @endforeach
                                          </select>
                                        @endif

                                       
                                  </div>
                                
                                  
                                  
                                  <div id="promedio_secundaria" class="form-group  col-md-3">
                                    <label>Promedio Sec. (con decimales):</label>
                                    @if($user->status=="aprobado")
                                    <div>{{$user->promedio_secundaria}}</div>
                                    @else
                                    <input type="number" name="promedio_secundaria" maxlength="2"  value="{{$user->promedio_secundaria}}"  class="form-control" >
                                    @endif
                                    <br>
                                  </div> 
                              </div>
                                  
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
               
                      <h3 class="box-title">
                        {{ __('Datos Médicos del Aspirante') }}:
                      </h3>
                      <hr>
                      <div class="row clearfix">
                      <div class="form-group col-md-3">

                            @if($user->status=="aprobado")
                            <div>
                            <label>Tipo Sanguineo: <span id="tipo_sanguineo_label"></span></label>

                              @foreach ($tipo_sanguineos as $key => $value)  
                              @if($user->tipo_sanguineo==$value['id']) {{($value['label'])}}@endif 
                              
                              @endforeach
                            
                            
                            </div>
                            @else
                            <label>Tipo Sanguineo:</label>
                            <select name="tipo_sanguineo" class="form-control">
                            <option>&nbsp;</option>
                              @foreach ($tipo_sanguineos as $key => $value)  
                                <option @if($user->tipo_sanguineo==$value['id']) selected="selected" @endif value="{{$value['id']}}">{{($value['label'])}}</option>
                              @endforeach
                            </select>
                            
                                    
                            
                            @endif

                          
                      </div>
                      </div>
                      <div class="form-group">
                          
                              @if($user->status=="aprobado")
                              <label>Presenta alguna discapacidad:</label>
                              <div>{{$user->tiene_discapacidad}}</div>
                                @if($user->tiene_discapacidad_tipo!=null)
                                  <label>¿Cuál?</label>
                                  <div>{{$user->tiene_discapacidad_tipo}}</div>
                                @endif
                              @else
                                <div class="">
                                
                                    <label>Presenta alguna discapacidad:</label>
                                    @foreach ($sino as $key => $value)  
                                      <input name="tiene_discapacidad" type="radio" @if($user->tiene_discapacidad==$value['id']) checked="checked" @endif value="{{$value['id']}}" /> <label>{{__($value['label'])}}</label>
                                    @endforeach
                                  
                                    <label>¿Cuál?</label>
                                    <input style="display:inline-block;width:200px;" name="tiene_discapacidad_tipo" value="{{$user->tiene_discapacidad_tipo}}" type="text" class="form-control">
                                    
                                
                                </div>

                              @endif
                         
                      </div>       
                      <div class="form-group">
                            
                            @if($user->status=="aprobado")
                            <label>Tratamiento:</label>
                            <div>{{$user->tratamientos}}</div>
                            @if($user->tratamientos_tipo!=null)
                                <label>¿Cúal?</label>
                                <div>{{$user->tratamientos_tipo}}</div>
                              @endif
                            @else
                                
                                <div class="">
                                
                                  
                                    <label>Tratamiento:</label>
                                    @foreach ($sino as $key => $value)  
                                    <input name="tratamientos" type="radio" @if($user->tratamientos==$value['id']) checked="checked" @endif value="{{$value['id']}}" /> <label>{{__($value['label'])}}</label>
                                  @endforeach
                                  
                                    <label>¿Cúal?</label>
                                    <input style="display:inline-block;width:200px;"  name="tratamientos_tipo" value="{{$user->tratamientos_tipo}}" type="text" class="form-control">
                                  
                                  
                                
                                </div>
                            
                                    
                          
                            @endif
                            
                      </div> 
                      <div class="box-body">
                              <label>Certificado Médico: <span id="certificado_medico_label"></span></label>


                              @if($user->status=="aprobado")
                              @else
                              <button id="certificado_medico" type="button" class="btn btn-primary">
                                Cargar Certificado Médico
                              </button>
                              <input type="file" class="hide" name="certificado_medico">
                              @endif

                              @if($user->certificado_medico!=null) 
                              <a href="{{$user->certificado_medico}}" class="btn btn-primary">
                                {{__('general.SEE_LABEL') }}&nbsp;
                                Documento
                              </a>  
                              @endif  
                              
                      </div>
                      <br>
                      <div class="form-group">
                          
                              @if($user->status=="aprobado")
                              <label>¿Has tenido calentura o fiebre de más de 37.8 °C?:</label>
        
                                @if($user->covid_has_tenido_calentura!=null)
                                  <div>{{$user->covid_has_tenido_calentura}}</div>
                                @endif
                              @else
                                <div class="">
                                    <label>¿Has tenido calentura o fiebre de más de 37.8 °C?:</label>
                                    @foreach ($sinotalvez as $key => $value)  
                                      <input name="covid_has_tenido_calentura" type="radio" @if($user->covid_has_tenido_calentura==$value['id']) checked="checked" @endif value="{{$value['id']}}" /> <label>{{__($value['label'])}}</label>
                                    @endforeach
                                </div>
                              @endif
                         
                      </div>       


                      <div class="form-group">
                          
                          @if($user->status=="aprobado")
                          <label>¿Has perdido el olfato?:</label>
    
                            @if($user->covid_has_perdido_olfato!=null)
                              <div>{{$user->covid_has_perdido_olfato}}</div>
                            @endif
                          @else
                            <div class="">
                                <label>¿Has perdido el olfato?:</label>
                                @foreach ($sinotalvez as $key => $value)  
                                  <input name="covid_has_perdido_olfato" type="radio" @if($user->covid_has_perdido_olfato==$value['id']) checked="checked" @endif value="{{$value['id']}}" /> <label>{{__($value['label'])}}</label>
                                @endforeach
                            </div>
                          @endif
                     
                      </div>       
                      <div class="form-group">
                          
                          @if($user->status=="aprobado")
                          <label>¿Has estado en contacto directo con algún paciente positivo a SARS-COV2 (COVID19)?:</label>
    
                            @if($user->covid_has_tenido_contacto_directo_con_alguien_con_covid19!=null)
                              <div>{{$user->covid_has_tenido_contacto_directo_con_alguien_con_covid19}}</div>
                            @endif
                          @else
                            <div class="">
                                <label>¿Has estado en contacto directo con algún paciente positivo a SARS-COV2 (COVID19)?:</label>
                                @foreach ($sinotalvez as $key => $value)  
                                  <input name="covid_has_tenido_contacto_directo_con_alguien_con_covid19" type="radio" @if($user->covid_has_tenido_contacto_directo_con_alguien_con_covid19==$value['id']) checked="checked" @endif value="{{$value['id']}}" /> <label>{{__($value['label'])}}</label>
                                @endforeach
                            </div>
                          @endif
                     
                      </div>       
                             


                 
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">
                  
                    
            
                        
                        <h3 class="box-title">
                          {{ __('Datos familiares del aspirante') }}
                        </h3>
                        <hr>
                    <div class="row clearfix">
                        <div class="form-group col-md-3">
                          <label>Nombre Completo Padre:</label>
                          @if($user->status=="aprobado")
                          <div>{{$user->nombre_completo_padre}}</div>
                          @else
                          <input type="text" name="nombre_completo_padre"  value="{{$user->nombre_completo_padre}}"  class="form-control" >
                          @endif
                        
                        </div>    
                    
                    <div class="form-group  col-md-3">
                      <label>Ocupacion padre:</label>
                      @if($user->status=="aprobado")
                      <div>{{$user->ocupacion_padre}}</div>
                      @else
                      <input type="text" name="ocupacion_padre"  value="{{$user->ocupacion_padre}}"  class="form-control" >
                      @endif
                    
                    </div>   
                    <div class="form-group  col-md-3">
                      <label>Escolaridad padre:</label>
                      @if($user->status=="aprobado")
                      <div>{{$user->escolaridad_padre}}</div>
                      @else
                      <input type="text" name="escolaridad_padre"  value="{{$user->escolaridad_padre}}"  class="form-control" >
                      @endif
                    
                    </div>  
                    <div class="form-group  col-md-3">
                      <label>Teléfono celular padre:</label>
                      @if($user->status=="aprobado")
                      <div>{{$user->Teléfono_celular_padre}}</div>
                      @else
                      <input type="text" name="Teléfono_celular_padre"  value="{{$user->Teléfono_celular_padre}}"  maxlength="10"  class="form-control" >
                      @endif
                    
                    </div>   

                    </div>
                    <div class="row clearfix">
                    
                    <div class="form-group  col-md-3">
                      <label>Nombre Completo Madre:</label>
                      @if($user->status=="aprobado")
                      <div>{{$user->nombre_completo_madre}}</div>
                      @else
                      <input type="text" name="nombre_completo_madre"  value="{{$user->nombre_completo_madre}}"  class="form-control" >
                      @endif
                     
                    </div>      
         
                    <div class="form-group  col-md-3">
                      <label>Ocupacion madre:</label>
                      @if($user->status=="aprobado")
                      <div>{{$user->ocupacion_madre}}</div>
                      @else
                      <input type="text" name="ocupacion_madre"  value="{{$user->ocupacion_madre}}"  class="form-control" >
                      @endif
                     
                    </div>    
                 
                    <div class="form-group  col-md-3">
                      <label>Escolaridad madre:</label>
                      @if($user->status=="aprobado")
                      <div>{{$user->escolaridad_madre}}</div>
                      @else
                      <input type="text" name="escolaridad_madre"  value="{{$user->escolaridad_madre}}"  class="form-control" >
                      @endif
                     
                    </div>   
                 
                    <div class="form-group  col-md-3">
                      <label>Teléfono celular madre:</label>
                      @if($user->status=="aprobado")
                      <div>{{$user->Teléfono_celular_madre}}</div>
                      @else
                      <input type="text" name="Teléfono_celular_madre"  value="{{$user->Teléfono_celular_madre}}" maxlength="10"   class="form-control" >
                      @endif
                      
                    </div>     
                    
                    </div>
                    <div class="row clearfix">
                      <div class="form-group  col-md-3">
                        <label>¿De quién depende?:</label>
                        @if($user->status=="aprobado")
                        <div>{{$user->de_quien_depende}}</div>
                        @else
                        <input type="text" name="de_quien_depende"  value="{{$user->de_quien_depende}}"  class="form-control" >
                        @endif
                
                      </div>  
                    </div>
                  
                </div>

                <div role="tabpanel" class="tab-pane" id="settings2">
                 
                    <h3 class="box-title">
                      {{ __('Datos del representante o tutor') }}
                    </h3>
                    <hr>
                    <div class="row clearfix">
                    <div class="form-group col-md-3">
                      <label>Nombre de Representante: </label>
                      @if($user->status=="aprobado")
                      <div>{{$user->representante_nombre}}</div>
                      @else
                      <input type="text" name="representante_nombre"  value="{{$user->representante_nombre}}"  class="form-control" >
                      @endif
                    
                    </div>    
                    <div class="form-group  col-md-3">
                      <label>Email: </label>
                      @if($user->status=="aprobado")
                      <div>{{$user->representante_email}}</div>
                      @else
                      <input type="text" name="representante_email"  value="{{$user->representante_email}}"  class="form-control" >
                      @endif
                     
                    </div>      
                    <div class="form-group  col-md-3">
                      <label>Parentesco: </label>
                      @if($user->status=="aprobado")
                      <div>{{$user->representante_parentesco}}</div>
                      @else
                      <input type="text" name="representante_parentesco"  value="{{$user->representante_parentesco}}"  class="form-control" >
                      @endif
                    
                    </div>  

                    </div>
                  
                    <div class="box-body">
                      <label> IFE/INE:  <span id="ife_ine_label"></span></label>

                      @if($user->status=="aprobado")
                        @else
                        <button id="ife_ine" type="button" class="btn btn-primary">
                        Cargar IFE/INE del tutor
                        </button>
                        <input type="file" class="hide" name="ife_ine">
                        @endif
                    

                        @if($user->ife_ine!=null) 
                        <a href="{{$user->ife_ine}}" class="btn btn-primary">
                          {{__('general.SEE_LABEL') }}&nbsp;
                        Documento
                        </a>  
                        @endif  
                      
                    </div>
                
                      
                    <h5 style="font-weight:bold">
                      {{ __('Dirección') }}
                    </h5>

                    <div style="padding:10px" class="row clearfix">
                      <div class="form-group col-md-3">
                        <label>Calle :</label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_calle}}</div>
                        @else
                        <input type="text" name="representante_calle"  value="{{$user->representante_calle}}"  class="form-control" >
                        @endif
                        
                      </div> 
                      <div class="form-group  col-md-3">
                        <label>Número:</label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_numero}}</div>
                        @else
                        <input type="text" name="representante_numero"  value="{{$user->representante_numero}}" maxlength="10"   class="form-control" >
                        @endif
                       
                      </div>       
                      
                      <div class="form-group  col-md-3">
                        <label>Cruzamientos: </label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_cruzamientos}}</div>
                        @else
                        <input type="text" name="representante_cruzamientos"  value="{{$user->representante_cruzamientos}}"  class="form-control" >
                        @endif
                      
                      </div>   

                      <div class="form-group  col-md-3">
                        <label>Colonia: </label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_colonia}}</div>
                        @else
                        <input type="text" name="representante_colonia"  value="{{$user->representante_colonia}}"  class="form-control" >
                        @endif
                       
                      </div>    
                      
                      <div class="form-group  col-md-3">
                        <label>Codigo postal: </label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_zip_code}}</div>
                        @else
                        <input type="text" name="representante_zip_code"  value="{{$user->representante_zip_code}}"  class="form-control" >
                        @endif
                        
                      </div>     
                      <div class="form-group  col-md-3">
                        <label>Municipio: </label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_municipio}}</div>
                        @else
                        <input type="text" name="representante_municipio"  value="{{$user->representante_municipio}}"  class="form-control" >
                        @endif
                       
                      </div>    
                      <div class="form-group  col-md-3">
                        <label>Teléfono fijo: </label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_Teléfono_fijo}}</div>
                        @else
                        <input type="text" name="representante_Teléfono_fijo"  value="{{$user->representante_Teléfono_fijo}}" maxlength="10"   class="form-control" >
                        @endif
                       
                      </div>    
                      <div class="form-group  col-md-3">
                        <label>Teléfono celular: </label>
                        @if($user->status=="aprobado")
                        <div>{{$user->representante_Teléfono_celular}}</div>
                        @else
                        <input type="text" name="representante_Teléfono_celular"  value="{{$user->representante_Teléfono_celular}}"  maxlength="10"  class="form-control" >
                        @endif
                       
                      </div>    
                    </div>
                    <div class="box-footer clearfix">
                    <br>
                    <button type="submit" class="pull-right btn btn-primary">{{ __('Guardar Ficha de Aspirante') }}</button>
                   
                    
                  </div>
                  
                </div>
            </div>
        </div>











          


        <div class="box-footer clearfix">
                    <br>
                    
                    @if(user_full_filled($user)==true)
                      <a href="{{route('account_users_folio')}}" class="btn btn-primary">{{ __('Ver folio') }}</a>
                      
                    @endif
                    
                  </div>


        <!-- /.box-body -->
     
      </form>
    
    
    
    </div>
  </div>
</div>
<button onclick="" id="myBtn" title="Go to top">Continuar con el llenado</button> 
@endsection
@section('footer_scripts')


<script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/timepicker.js') }}"></script>
<script type="text/javascript">
  window.tab = 1;
  var ready = function(e)
  { 
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      language: 'es'
    });
    $("#minmaxTimeExample1").timepicker({
          minTime: "01:00",
          maxTime: "12:00 pm"
        });

      
     
   var myBtnx = function(e)
   {

    e.preventDefault()
   
      $("#selectionsy  li").not(window.tab).removeClass('active');
      $("#selectionsy2 .tab-pane").not(window.tab).removeClass('active');
      $("#selectionsy  li").eq(window.tab).addClass('active');
      $("#selectionsy2 .tab-pane").eq(window.tab).addClass('active');
      window.tab++;
      if(window.tab> $("#selectionsy2 .tab-pane").length-1)
      {
        window.tab =0;
      }else{
        window.tab =  $("#selectionsy2 .tab-pane.active").index() +1
      }
     
    //topFunction()
    }
    $("#myBtn").on('click',myBtnx)
    var aviso_de_privacidad = function(e){
      $("input[name=acta_de_nacimiento]").trigger('click');
    }
    $("#acta_de_nacimiento").on('click',aviso_de_privacidad)
    var acta_de_nacimiento_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#acta_de_nacimiento_label").html(filename)
    }
    $("input[name=acta_de_nacimiento]").on('change',acta_de_nacimiento_change)


    var comprobante_domicilio = function(e){
      $("input[name=comprobante_domicilio]").trigger('click');
    }
    $("#comprobante_domicilio").on('click',comprobante_domicilio)
    var comprobante_domicilio_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#comprobante_domicilio_label").html(filename)
    }
    $("input[name=comprobante_domicilio]").on('change',comprobante_domicilio_change)



    var numero_de_seguro_social = function(e){
      $("input[name=numero_de_seguro_social]").trigger('click');
    }
    $("#numero_de_seguro_social").on('click',numero_de_seguro_social)

    var numero_de_seguro_social_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#numero_de_seguro_social_label").html(filename)
    }
    $("input[name=numero_de_seguro_social]").on('change',numero_de_seguro_social_change)



    var boleta_primaria_secundaria = function(e){
      $("input[name=boleta_primaria_secundaria]").trigger('click');
    }
    $("#boleta_primaria_secundaria").on('click',boleta_primaria_secundaria)

    var boleta_primaria_secundaria_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#boleta_primaria_secundaria_label").html(filename)
    }
    $("input[name=boleta_primaria_secundaria]").on('change',boleta_primaria_secundaria_change)




    var ife_ine = function(e){
      $("input[name=ife_ine]").trigger('click');
    }
    $("#ife_ine").on('click',ife_ine)


    var ife_ine_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#ife_ine_label").html(filename)
    }
    $("input[name=ife_ine]").on('change',ife_ine_change)



    var certificado_de_secundaria = function(e){
      $("input[name=certificado_de_secundaria]").trigger('click');
    }
    $("#certificado_de_secundaria").on('click',certificado_de_secundaria)


    var certificado_de_secundaria_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#certificado_de_secundaria_label").html(filename)
    }
    $("input[name=certificado_de_secundaria]").on('change',certificado_de_secundaria_change)





    var thumbnail = function(e){
      $("input[name=thumbnail]").trigger('click');
    }
    $("#thumbnail").on('click',thumbnail)

    var thumbnail_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#thumbnail_label").html(filename)
    }
    $("input[name=thumbnail]").on('change',thumbnail_change)


    var imss_document = function(e){
      $("input[name=imss_document]").trigger('click');
    }
    $("#imss_document").on('click',imss_document)

    var imss_document_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#imss_document_label").html(filename)
    }
    $("input[name=imss_document]").on('change',imss_document_change)

    var generar_cerificado_secundaria = function(e)
    {
        if($(this).prop('checked')==true)
        {
          $('#fecha_fin_secundaria,#nombre_Secundaria_procedencia,#tipo_de_secundaria,#promedio_secundaria').css('display','none')
        }else{
          $('#fecha_fin_secundaria,#nombre_Secundaria_procedencia,#tipo_de_secundaria,#promedio_secundaria').css('display','block')
        }
    }

    $("input[name=generar_cerificado_secundaria]").on('click',generar_cerificado_secundaria)


    var tiene_ceneval = function(e)
    {
        if($(this).prop('checked')==true)
        {
          $('.ceneval').removeClass('hide')
        }else{
          $('.ceneval').addClass('hide')
        }
    }

    $("input[name=tiene_ceneval]").on('click',tiene_ceneval)
    
      if( $("input[name=tiene_ceneval]").prop('checked')==true)
        {
          $('.ceneval').removeClass('hide')
        }else{
          $('.ceneval').addClass('hide')
        }
    
    
    var certificado_medico = function(e){
      $("input[name=certificado_medico]").trigger('click');
    }
    $("#certificado_medico").on('click',certificado_medico)

    var certificado_medico_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#certificado_medico_label").html(filename)
    }
    $("input[name=certificado_medico]").on('change',certificado_medico_change)


    var curp = function(e){
      $("input[name=curp]").trigger('click');
    }
    $("#curp").on('click',curp)

    var curp_change = function(e)
    {
      var filename = e.target.files.length ? e.target.files[0].name : "";
      $("#curp_label").html(filename)
    }
    $("input[name=curp]").on('change',curp_change)

    
  } 
  $(window).on('load',ready);

  mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 35 || document.documentElement.scrollTop > 35) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
} 
</script>
@endsection
@section('custom_styles')

<link rel="stylesheet" href="{{ asset('assets/admin/bower_components/timepicker.css') }}">

@endsection

