@extends('layouts.user_panel')

@section('content')
<div class="app-heading app-heading-bordered app-heading-page">
    <div class="icon icon-lg">
        <span class="icon-list3"></span>
    </div>
    <div class="title">            
        <h1>   {{ __('user.PROFILE_LABEL') }} del aspirante</h1>
    </div>
</div>

    
<div class="app-heading-container app-heading-bordered bottom" >
<div class="pull-right" style="padding:10px">
    Fecha de captura de datos: 
    @if($user->status=="aprobado")
   
    <?php echo date("Y/m/d", strtotime($user->updated_at)) ?> 
   
      @else
     
      <?php echo (new \DateTime())->format("Y/m/d"); ?> 

    @endif
    </div>
    <ul class="breadcrumb">
        <li><a href="#">Inicio</a></li>
        <li class="active">
            Folio
        </li>
       
    </ul>
   
</div>

<div class="">
  <div class="block"> 
    @if (\Session::has('success'))
    <div class="alert alert-success">
      <p>{{ __(\Session::get('success')) }}</p>
    </div>
    <br />
    @endif
    @if (\Session::has('error'))
        <div class="alert alert-danger">
        <p>{{ __(\Session::get('error')) }}</p>
        </div><br />
    @endif
    <div class="">


        <div>
            @if($user->status=="aprobado")
                <div class="alert alert-success">
                    <p>Estatus actual: aprobado</p>
                </div>
            @endif
            @if($user->status=="pendiente")
                <div class="alert alert-info">
                    <p>Estatus actual: Actualizar datos pendiente</p>
                </div>
            @endif
            @if($user->status=="rechazado")
                <div class="alert alert-danger">
                    <p>Estatus actual: lo sentimos pero hemos rechazado tu perfil por favor comunicate con nosostros</p>
                </div>
        
            @endif
            @if($user->status=="resubir-documentos")
                <div class="alert alert-info">
                    <p>Estatus actual: Por favor vuelve  a subir documentos</p>
                </div>
            @endif

        </div>
        <div class="box-body clearfix">
            <h3>
                Felicidades, has concluido la captura y carga de documetos El siguiente número es tu folio de aspirante 
            </h3>
            <p>
                Favor de descarga los siguientes archivos y presentarse en las oficinas de control escolar con la siguiente documentación original
            </p>
            <div class="form-group">
                <label>Numero de Folio:</label>
                <?php print(sprintf("%06d", $user['id']));  ?>
            </div>
            <?php $logo = get_settings(); ?>
            <div class="form-group">
                <label>Reglamento del plantel:</label>
                <a download href="{{$logo->reglamento_del_plantel}}" class="btn btn-primary">
                    {{__('general.SEE_LABEL') }}&nbsp;
                    Documento
                </a>  
            </div>
            <div class="form-group">
                <label>Firma del reglamento:</label>
                <a download href="{{$logo->firma_de_reglamento}}" class="btn btn-primary">
                    {{__('general.SEE_LABEL') }}&nbsp;
                    Documento
                </a>  
            </div>
            <div class="form-group">
                <label>Firma de autorización:</label>
                <a download href="{{$logo->firma_de_authorizacion}}" class="btn btn-primary">
                    {{__('general.SEE_LABEL') }}&nbsp;
                    Documento
                </a>  
            </div>
            <div class="form-group">
                <label>Firma de veracidad:</label>
                <a download href="{{$logo->firma_de_veracidad}}" class="btn btn-primary">
                    {{__('general.SEE_LABEL') }}&nbsp;
                    Documento
                </a>  
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
            <br>
            <a href="{{route('account_users_me')}}" class="btn btn-primary">{{ __('Ver Perfil del aspirante ') }}</a>
        </div>

    </div>
  </div>
</div>
@endsection
@section('footer_scripts')
@endsection
@section('custom_styles')
@endsection

