@extends('layouts.auth')

@section('content')

<div class="login-box">
  
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">{{ __('Ingresar a ventanilla única') }} </p>

    <form method="POST" action="{{ route('admin_login') }}">
        @csrf
        <div class="form-group has-feedback">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ __('Dirección de Correo') }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group has-feedback">
            <input id="password" type="password" class="form-control{{ $errors->has('admin_password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Contraseña') }}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('contraseña') }}</strong>
                </span>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Recordar Sesión') }}
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('ingresar a ventanilla única') }}</button>
            </div>
            <!-- /.col -->
        </div>
    </form>


    <!-- /.social-auth-links -->

    <a  href="{{ route('admin_password.request') }}">
        {{ __(' ¿ Olvidó contraseña ?') }}
    </a><br>
   
  </div>
  <!-- /.login-box-body -->
</div>

@endsection
@section('scripts')


@endsection