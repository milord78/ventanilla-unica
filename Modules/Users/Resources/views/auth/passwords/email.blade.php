@extends('layouts.auth')
@section('content')

<div class="login-box">

  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">{{ __('Recuperar contraseña') }}</p>
    @if (session('status'))
        <div class="login-box-msg">
            {{ session('status') }}
        </div>
    @endif
    <form method="POST" action="{{ route('password.request') }}">
        @csrf
        <div class="form-group has-feedback">
           
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="{{ __('Dirección de correo') }}">

            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          
        </div>

        <div class="row">
            <!-- /.col -->
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">  {{ __('Enviar link de restabelicimiento de contraseña') }}</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
    <br>

    <a   class="btn btn-primary btn-block btn-flat" href="{{ route('admin_login') }}">
        {{ __('ingresar a ventanilla única') }}
    </a><br>


  </div>
  <!-- /.login-box-body -->
</div>

@endsection
@section('scripts')
@endsection
