@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">
    

    <h3 class="box-title">Perfil de usuario</h3>
    <form role="form" method="post" action="{{$route}}" enctype='multipart/form-data'>
       @csrf
      <div class="box-body">
        <div class="nav-tabs-custom" >
          <!-- Tabs within a box -->
       
          <div class="tab-content no-padding">
            <!-- Morris chart - Sales -->
            <div id="basic" class="tab-pane active">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nombre</label>
                      <input type="text" name="firstname"  value="{{$user->firstname}}" required class="form-control" >
                    </div>
                    
                    <div class="form-group">
                      <label for="exampleInputEmail1">{{ __('user.LAST_NAME_LABEL') }}</label>
                      <input type="text" name="lastname"  value="{{$user->lastname}}" required class="form-control" >
                    </div> 
                    @if (empty($user->id))
                      <div class="form-group">
                      <label for="exampleInputEmail1">{{ __('user.PASSWORD_LABEL') }}</label>
                      <input type="password" name="password"  required class="form-control" >
                      </div>  
                    @endif
                    <div class="form-group">
                      <label for="exampleInputEmail1">{{ __('Email') }}</label>
                      <input type="text" name="email"  value="{{$user->email}}" required class="form-control" >
                    </div>        
                    @if (\Auth::user()->id != $user->id and \Auth::user()->role != "user")

                      <div class="form-group">
                        <label for="exampleInputEmail1">{{ __('user.PERMISSION_LABEL') }}</label>
                        
                        <select class="form-control" name="role" required>
                          @foreach($roles  as $index => $role)
                            <option <?php echo ($user->role == $role)?"selected":""; ?>  value="{{$role}}">{{$role}}</option>
                          @endforeach
                        </select>
                      </div>
                    
                    @endif
                       
                    <div class="box-body">
                      <label for="exampleInputEmail1">{{__('user.IMAGE_LABEL') }}</label>
                      <input type="file" name="thumbnail">
                    </div>

            </div>
           
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }} Datos</button>

        @if (route('admin_user_me_update') != $route)

          
          <a href="{{ route('admin_users_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
          @if (!empty($user->id))
            <a href="{{ route('admin_users_delete',['id' => $user->id]) }}" class="btn btn-danger">{{ __('general.DELETE_LABEL') }}</a>
          @endif
        @endif
      </div>
    </form>
    @if (!empty($user->id))
    <h3 class="box-title">Cambiar Contraseña</h3>
     @if (\Session::has('password_sucess'))
        <div class="alert alert-success">
          <p>{{  __(\Session::get('password_sucess')) }}</p>
        </div><br />
      @endif
      @if (\Session::has('password_error'))
        <div class="alert alert-error">
          <p>{{  __(\Session::get('password_error')) }}</p>
        </div><br />
      @endif

    <form role="form" method="post" action="{{ $route_update_password }}">
       @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('user.PASSWORD_LABEL') }}</label>
          <input type="password" name="password1" required class="form-control" >
        </div> 
        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('user.REPEAT_PASSWORD_LABEL') }}</label>
          <input type="password" name="password2" required class="form-control" >
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ __('general.CHANGE_LABEL') }}</button>
      </div>
    </form>
    @endif
  </div>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/admin/ecommerce_module_user_related_tags.js') }}"></script>
@endsection