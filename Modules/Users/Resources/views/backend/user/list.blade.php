@extends('layouts.backend')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">

      <div class="box-header" style="padding: 15px">
        <h3 class="box-title">{{ __('Aspirantes') }}</h3>
        <div class="box-tools">

          @if($pending == true)
          <form method="get" action="{{route('admin_users_search_pending',['status'=>$status])}}">
          @else  
          <form method="get" action="{{route('admin_users_search')}}">
          @endif
            <div class="input-group input-group-sm" style="width: 550px;">
               <input type="text" name="email" value="{{$q}}" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }} {{__('Aspirante') }}">
            
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
      <div style="padding:10px">
      <h3>Filtrar aspirantes:</h3>
        <a href="{{ route('admin_users_index_pending',['status'=>'pendiente']) }}" class="btn btn-primary">Pendiente</a>
        <a href="{{ route('admin_users_index_pending',['status'=>'aprobado']) }}" class="btn btn-primary">Aprobado</a>
        <a href="{{ route('admin_users_index_pending',['status'=>'rechazado']) }}" class="btn btn-primary">Rechazado</a>
        <a href="{{ route('admin_users_index_pending',['status'=>'resubir-documento']) }}" class="btn btn-primary">Resubir documento</a>
        

        <a href="{{ route('admin_users_index') }}" class="btn btn-primary">Todos</a>
      </div>

        <table class="table">
          <thead>
            <tr>
              <th class="col2"># </th>
              <th>{{ __('user.FIRST_NAME_LABEL') }}</th>
              <th>{{ __('user.MIDDLE_NAME_LABEL') }}</th>
              <th>{{ __('user.LAST_NAME_LABEL') }}</th>
              <th>{{ __('Email') }}</th>
              <th>{{ __('user.PERMISSION_LABEL') }}</th>
              <th  style="width: 60px">{{ __('general.ACTIONS_LABEL') }}</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users  as $index => $user)
            <tr>
              <td><?php  print(sprintf("%04d", $user['id']));  ?></td>
              <td>{{$user['firstname']}}</td>
              <td>{{$user['midname']}}</td>
              <td>{{$user['lastname']}}</td>
              <td>{{$user['email']}}</td>
              <td><span class="label label-primary">{{$user['role']}} </span></td>
              <td>
              <a href="{{ route('admin_users_show',['id'=>$user['id']]) }}" class="btn btn-block btn-warning">{{ __('Ver ficha') }}</a>
     

              
                @if ((\Auth::user()->role == "admin" ) or \Auth::user()->role == "editor" && \Auth::user()->id == $user->id)
                <a href="{{ route('admin_users_edit',['id'=>$user['id']]) }}" class="btn btn-block btn-success">{{ __('general.EDIT_LABEL') }}</a>
                @endif
                @if (\Auth::user()->id != $user->id and \Auth::user()->role == "admin")
                  <a href="{{ route('admin_users_delete',['id'=>$user['id']]) }}" class="btn btn-block btn-danger">{{ __('general.DELETE_LABEL') }}</a>
                  <a href="{{ route('admin_user_permissions_permissions_edit',['id'=>$user['id']]) }}" class="btn btn-block btn-primary">{{ __('permissions::permissions.permissions.PERMISSIONS_LABEL') }}</a>
                @endif
              

              </td>
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

 
 @if ($users->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($users->onFirstPage())
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li>
                    <a href="{{ pagination_previousPageUrl($users) }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif
            <?php $slide = pagination_slide($users); ?> 

            @foreach ($slide as $element)
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true">
                      <span>{{ $element }}</span>
                    </li>
                @endif
                @if (is_array($element))
                  @foreach ($element as $page => $url)
                      @if ($page == $users->currentPage())
                          <li class="active" aria-current="page">
                            <span>{{ $page }}</span>
                          </li>
                      @else
                        <!--{{route('admin_users_index_page',['page'=>$page ])}}-->
                          <li>
                            <a href="{{$url}}">
                              {{ $page }}
                            </a>
                          </li>
                      @endif
                  @endforeach
                @endif
             @endforeach
            {{-- Pagination Elements --}}
            @for($page=1;$page<=$users->total();$page++)

                {{-- Pagination Elements --}}
           
                {{-- Array Of Links --}}
       
              
            @endfor
            {{-- Next Page Link --}}
            @if ($users->hasMorePages())
                <li>
                  

                    <a href="{{ pagination_nextPageUrl($users) }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
    </nav>
@endif

      
      </div>
    </div>

@endsection
