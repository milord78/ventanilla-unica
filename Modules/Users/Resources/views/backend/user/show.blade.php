@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
 <div class="box">
    

     <h3 class="box-title">Ficha de Alumno</h3>

    <div class="box-body">
        <div class="nav-tabs-custom" >
          <!-- Tabs within a box -->
      
            <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                    <div class="row clearfix">
                        <div class="col-md-3">
                            <div class="avatar" style="background-image:url({{\Auth::user()->logos()}}); background-size: cover;height: 200px;width: 100%; border-radius: 5px;">
                            </div>
                        </div>
                        <div class="col-md-8">
                        <div class="box clearfix" style="border-radius: 5px;margin-top: 25px;">
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('user.FIRST_NAME_LABEL') }}</label>
                            {{$user->firstname}}
                        </div>
                        <div class="form-group">
                        <label for="exampleInputEmail1">{{ __('user.MIDDLE_NAME_LABEL') }}</label>
                        {{$user->midname}}
                        </div>
                        <div class="form-group">
                        <label for="exampleInputEmail1">{{ __('user.LAST_NAME_LABEL') }}</label>
                        {{$user->lastname}}
                        </div> 
                    
                        <div class="form-group">
                        <label for="exampleInputEmail1">{{ __('Email') }}</label>
                        {{$user->email}}
                        
                        </div>        
                        @if (\Auth::user()->id != $user->id and \Auth::user()->role != "user")

                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('user.PERMISSION_LABEL') }}</label>
                            
                            @foreach($roles  as $index => $role)
                                <?php echo ($user->role == $role)?$role:""; ?>
                            @endforeach
                        
                        </div>
                        
                        @endif
                        

                        <div class="form-group">
                            <label for="exampleInputEmail1">
                            {{ __('Telefono') }}
                            </label>
                            {{$user->phone}}
                        </div> 
                            <div class="form-group">
                            <label>Telefono Celular</label>
                            {{$user->cell_phone}}
                            <br>
                            </div>

                    
                        
                            <div class="form-group">
                                <label>{{ __('controlnotas::rating.alunnos.SEXO_LABEL') }}</label>
            
                            
                                @foreach ($generos as $key => $value)  
                                <?php echo ($user->genero==$value['id'])?$value['label']:""; ?>
                                @endforeach
                            
                                <br>
                            </div>



        
                            <div class="form-group">
                                <label>{{ __('controlnotas::rating.alunnos.FECHA_NAC_LABEL') }}</label>
                                <i class="fa fa-calendar"></i>
                                {{$user->fecha_nac}}
                                <br>
                            </div>
                            <h5 style="font-weight:bold">
                          {{ __('Dirección') }}
                        </h5>
                      <div style="padding:10px">
                          <div class="form-group">
                            <label>Calle</label>
                   
                            <div>{{$user->calle}}</div>
                         
                        
                        
                            <br>
                          </div>
                       
                          <div class="form-group">
                            <label>Número</label>
                         
                            <div>{{$user->numero}}</div>
                            
                            <br>
                          </div>
                        <div class="form-group">
                          <label>Cruzamientos</label>
                        
                          <div>{{$user->cruzamientos}}</div>
                          
                          <br>
                        </div>
                        <div class="form-group">
                          <label>Colonia</label>
                          
                          <div>{{$user->colonia}}</div>
                          
                          <br>
                        </div>
                        
                        <div class="form-group">
                          <label>Código postal</label>
                         
                          <div>{{$user->zip_code}}</div>
                          
                          <br>
                        </div>
                        
                        <div class="form-group">
                          <label>Municipio</label>
                        
                          <div>{{$user->municipio}}</div>
                          
                          <br>
                        </div>
                        <div class="form-group">
                          <label>Entidad federativa</label>
                         
                          <div>{{$user->entidad_federativa}}</div>
                          
                          <br>
                        </div>   
                      </div>

                    <h3 class="box-title">
                    {{ __('Especialidades') }}
                    </h3>


                    <div class="form-group">
                        <label>Especialidad 1</label>
                        @if($user->especialidad_1==null) 
                        <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                        @else
                        <h6>{{$user->especialidad_1}}</h6>
                    @endif  
                
                        
                    </div>
                    <div class="form-group">
                    <label>Especialidad 2</label>
                    @if($user->especialidad_2==null) 
                    <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                    @else
                    <h6>{{$user->especialidad_2}}</h6>
                    @endif  
                
                        
                    </div>
                    <div class="form-group">
                    <label>Especialidad 3</label>
                    @if($user->especialidad_3==null) 
                    <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                    @else
                    <h6>{{$user->especialidad_3}}</h6>
                    @endif  
                
                        
                    </div>
                    
                

                <h3 class="box-title">
                {{ __('Cargar documentos ') }}
                </h3>
                
                <hr>



                <div class="row">
                    <div class="form-group col-md-3">
                    <label>CAPTURAR NSS </label>
                    {{$user->nss_number}}
                    <br>
                    </div>
                </div>


                <div class="row">
                    <div class="form-group col-md-3">
                    <label>CONSULTAR NSS</label>
                    <a href="{{$user->imss_link}}"> VER NSS</a>
                  
                    <br>
                    </div>
                </div>
                   


                <div class="box-body">
                        <h6> IMSS</h6>

                        @if($user->imss_document!=null) 
                        <a href="{{$user->imss_document}}" class="btn btn-primary">
                        {{__('general.SEE_LABEL') }}&nbsp;
                        Documento
                        </a>  
                        @else
                        <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                        @endif  
                        
                    </div>
                       



                <div class="box-body">
                    <h6> Acta de nacimiento</h6>
                    

                    @if($user->acta_de_nacimiento!=null) 
                    <a href="{{$user->acta_de_nacimiento}}" class="btn btn-primary">
                    {{__('general.SEE_LABEL') }}&nbsp;
                    Documento
                    </a>
                    @else
                    <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                    @endif  
                    
                </div>
                <div class="box-body">
                <h6> Comprobante de Domicilio</h6>
                    

                    @if($user->comprobante_domicilio!=null) 
                    <a href="{{$user->comprobante_domicilio}}" class="btn btn-primary">
                    {{__('general.SEE_LABEL') }}&nbsp;
                    Documento
                    </a>  
                    @else
                    <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                    @endif  
                    
                </div>
      

                <div class="box-body">
                    <h6> Boletas Tercer año</h6>

                    @if($user->boleta_primaria_secundaria!=null) 
                    <a href="{{$user->boleta_primaria_secundaria}}" class="btn btn-primary">
                    {{__('general.SEE_LABEL') }}&nbsp;
                    Documento
                    </a>  
                    @else
                    <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                    @endif  
                
                </div>

                   
                    


                    <div class="box-body">
                        <h6> Certificado de secundaria</h6>
                        

                        @if($user->certificado_de_secundaria!=null) 
                        <a href="{{$user->certificado_de_secundaria}}" class="btn btn-primary">
                        {{__('general.SEE_LABEL') }}&nbsp;
                        Documento
                        </a>  
                        @else
                        <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                        @endif  
                        <label>Generar certificado de Secundaria: {{$user->generar_cerificado_secundaria }} </label>
                    </div>



                    <div  class="form-group">
                        <br>
                        <label>{{ __('Fecha de finalización Secundaria') }}</label>
                     
                           
                            <i class="fa fa-calendar"></i>
                          
                            {{$user->fecha_fin_secundaria}}
                       
                        
                        <br>
                    </div>
                    


  

                    <div class="box-body">
                        <h6> CURP</h6>

                        @if($user->curp!=null) 
                        <a href="{{$user->curp}}" class="btn btn-primary">
                        {{__('general.SEE_LABEL') }}&nbsp;
                        Documento
                        </a>  
                        @else
                        <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                        @endif  
                        
                    </div>
                    <div class="form-group">
                        <br>
                        <label>NÚMERO CURP</label>
                            {{$user->curp_number}}
                        <br>
                    </div>

                    <div class="box-body">
                        <h6> CENEVAL</h6>

                        @if($user->tiene_ceneval!=null) 
                        <div class="form-group">
                              <br>
                              <label>Resultado Ceneval</label>
                              @if($user->resultado_ceneval!=null) 
                                {{$user->resultado_ceneval}}
                              @else
                               <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                              @endif 
                              <br>
                        </div>
                        <div class="form-group">
                              <br>
                              <label>Folio Ceneval</label>
                              @if($user->folio_ceneval!=null) 
                                {{$user->folio_ceneval}}
                              @else
                                <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                              @endif 
                              <br>
                        </div>
                        @else
                          <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                        @endif  
                        
                    </div>
                    
    

                    </div>
                            <div>
                                <label>Nombre secundaria procedencia</label>
                                <div>{{$user->nombre_Secundaria_procedencia}}</div>
                            </div>
                            <div>
                                <label>Tipo de Secundaria</label>
                                @foreach ($tipo_secundaria as $key => $value)  
                                  @if($user->tipo_de_secundaria==$value['id']) {{($value['label'])}}@endif 
                                @endforeach
                            </div>        

                            <div class="form-group">
                                <label>Promedio Secundaria</label>
                                <div>{{$user->promedio_secundaria}}</div>
                                <br>
                            </div> 
                            <h3>
                                 Datos Médicos del Aspirante 
                            </h3>
                            <div class="form-group ">
                                    <label>Tipo Sanguineo</label>
                                    @foreach ($tipo_sanguineos as $key => $value)  
                                        <?php echo ($user->tipo_sanguineo==$value['id'])?$value['label']:""; ?>
                                    @endforeach
                                    <br>
                            </div>
                            <div class="form-group">
                                <label>Presenta alguna discapacidad:</label>
                                <div>{{$user->tiene_discapacidad}}</div>
                                <label>¿Cual?</label>
                                <div>{{$user->tiene_discapacidad_tipo}}</div>
                            </div>
                            

                            <div class="form-group">
                                <label>Tratamiento</label>
                                <div>{{$user->tratamientos}}</div>
                                <label>¿Cual?</label>
                                <div>{{$user->tratamientos_tipo}}</div>
                                <br>
                            </div>
                            <div class="box-body">
                        <h6>Certificado Medico</h6>
                        
                        @if($user->certificado_medico!=null) 
                        <a href="{{$user->certificado_medico}}" class="btn btn-primary">
                        {{__('general.SEE_LABEL') }}&nbsp;
                        Documento
                        </a>  
                        @else
                        <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                        @endif  

                    </div>
                    <div>
                            

                          <h3 class="box-title">
                            {{ __('Datos familiares del aspirante') }}
                          </h3>
               
                        <div class="form-group">
                          <label>Nombre Completo Padre</label>
                         
                          <div>{{$user->nombre_completo_padre}}</div>
                        
                          <br>
                        </div>    
                        <div class="form-group">
                          <label>Nombre Completo Madre</label>
                          
                          <div>{{$user->nombre_completo_madre}}</div>
                        
                          <br>
                        </div>      
                        <div class="form-group">
                          <label>Ocupacion padre</label>
                         
                          <div>{{$user->ocupacion_padre}}</div>
                          
                          <br>
                        </div>   
                        <div class="form-group">
                          <label>Ocupacion madre</label>
                        
                          <div>{{$user->ocupacion_madre}}</div>
                          
                          <br>
                        </div>    
                        
                        <div class="form-group">
                          <label>Escolaridad padre</label>
                       
                          <div>{{$user->escolaridad_padre}}</div>
                         
                          <br>
                        </div>   
                        <div class="form-group">
                          <label>Escolaridad madre</label>
                       
                          <div>{{$user->escolaridad_madre}}</div>
                        
                          <br>
                        </div>  
                        <div class="form-group">
                          <label>Telefono celular padre</label>
                         
                          <div>{{$user->telefono_celular_padre}}</div>
                         
                          <br>
                        </div>   
                        <div class="form-group">
                          <label>Telefono celular madre</label>
                        
                          <div>{{$user->telefono_celular_madre}}</div>
                        
                          <br>
                        </div>     
                        <div class="form-group">
                          <label>De quien depende</label>
                         
                          <div>{{$user->de_quien_depende}}</div>
                          
                          <br>
                        </div>  
                        <h3 class="box-title">
                        {{ __('Datos del representante o tutor') }}
                        </h3>

                   
                        <div class="form-group">
                          <label>Nombre de Representante </label>
                        
                          <div>{{$user->representante_nombre}}</div>
                        
                          <br>
                        </div>    
                        <div class="form-group">
                          <label>Email </label>
                         
                          <div>{{$user->representante_email}}</div>
                          
                          <br>
                        </div>      
                        <div class="form-group">
                          <label>Parentesco </label>
                        
                          <div>{{$user->representante_parentesco}}</div>
                         
                          <br>
                        </div>  

                        <div class="box-body">
                        <h6> IFE/INE</h6>
                    
                        @if($user->ife_ine!=null) 
                        <a href="{{$user->ife_ine}}" class="btn btn-primary">
                        {{__('general.SEE_LABEL') }}&nbsp;
                        Documento
                        </a>  
                        @else
                        <h6>NO SE HA RELLENADO ESTA INFORMACION</h6>
                        @endif  

                    </div>
                          
                        <h5 style="font-weight:bold">
                          {{ __('Dirección') }}
                        </h5>

                        <div style="padding:10px">
                          <div class="form-group">
                            <label>calle </label>
                           
                            <div>{{$user->representante_calle}}</div>
                          
                            <br>
                          </div> 
                          <div class="form-group">
                            <label>Número </label>
                          
                            <div>{{$user->representante_numero}}</div>
                           
                          </div>       
                          
                          <div class="form-group">
                            <label>Cruzamientos </label>
                          
                            <div>{{$user->representante_cruzamientos}}</div>
                           
                            <br>
                          </div>   

                          <div class="form-group">
                            <label>Colonia </label>
                        
                            <div>{{$user->representante_colonia}}</div>
                            
                            <br>
                          </div>    
                          
                          <div class="form-group">
                            <label>Código postal </label>
                           
                            <div>{{$user->representante_zip_code}}</div>
                           
                            <br>
                          </div>     
                          <div class="form-group">
                            <label>Municipio </label>
                          
                            <div>{{$user->representante_municipio}}</div>
                           
                            <br>
                          </div>    
                          <div class="form-group">
                            <label>Telefono fijo </label>
                         
                            <div>{{$user->representante_telefono_fijo}}</div>
                          
                            <br>
                          </div>    
                          <div class="form-group">
                            <label>Telefono celular </label>
                           
                            <div>{{$user->representante_telefono_celular}}</div>
                           
                            <br>
                          </div>    
                        </div>
                          

                            
                    </div>


                    <form role="form" method="post" action="{{$route}}" enctype='multipart/form-data'>
                    @csrf
                    <div class="form-group">
                          <label>{{ __('Actualizar estatus') }}</label>
                          <select name="status" class="form-control">
                            @foreach ($status as $key => $value)  
                              <option @if($user->status==$value['id']) selected="selected" @endif value="{{$value['id']}}">{{__($value['label'])}}</option>
                            @endforeach
                          </select>
                          <br>
                       </div>
                       <div class="box-footer">
                            <button type="submit" class="btn btn-primary">{{ __('general.CHANGE_LABEL') }}</button>
                            <a href="{{route('admin_users_validate',['id'=>$user->id])}}" class="btn btn-primary">{{ __('Validar usuario') }}</a>
                        </div>        
                    </form>
                </div>
            
            </div>
                        </div>
                    
                    </div>

                       


           
        </div>
    </div>
</div>  

   
@endsection
@section('footer_scripts')

@endsection