<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class UserRelatedTags extends Model
{
   
  	protected $table = 'user_related_tags';

    protected $fillable = [

		'tag_id',
		'user_id',
    ];     
}
