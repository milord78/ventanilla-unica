<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    
Route::prefix('api')->group(function() 
{
  
    Route::prefix('user')->group(function() 
    {
       
        Route::post('/list', ['uses' => 'Api\UserApiController@list'])->name('admin_api_user_list');
        
        Route::post('/search', ['uses' => 'Api\UserApiController@search'])->name('admin_api_user_pages_search');

        Route::post('/related_tag', ['uses' => 'Api\UserTagsController@get_related'])->name('admin_api_user_get_related');
       
    });

});



    Route::prefix('account')->group(function() 
    {
        Route::get('/me', ['uses' => 'Frontend\User\UserController@me'])->name('account_users_me');
        Route::get('/folio', ['uses' => 'Frontend\User\UserController@folio'])->name('account_users_folio');
        Route::post('/me/update', ['uses' => 'Frontend\User\UserController@update'])->name('account_user_me_update');
        Route::post('/me/update-password', ['uses' => 'Frontend\User\UserController@update_password'])->name('account_user_me_update_password');
    });

    Route::prefix('users')->group(function() 
    {
        Route::get('/', 'UsersController@index');
      
    });


Route::prefix('admin')->group(function() {
    
    Route::prefix('account')->group(function() 
    {

        Route::get('/login', ['uses' => 'Auth\AdminLoginController@showLoginForm'])->name('admin_login');
        Route::post('/login', ['uses' => 'Auth\AdminLoginController@login']);
        Route::post('/logout', ['uses' => 'Auth\AdminLoginController@logout'])->name('admin_logout');
        Route::get('/password/confirm', ['uses' => 'Auth\AdminConfirmPasswordController@showConfirmForm'])->name('admin_password.confirm');
        Route::post('/password/confirm', ['uses' => 'Auth\AdminConfirmPasswordController@confirm']);
        Route::post('/password/email', ['uses' => 'Auth\AdminForgotPasswordController@sendResetLinkEmail'])->name('admin_password.email');
        Route::post('/password/reset', ['uses' => 'Auth\AdminResetPasswordController@reset'])->name('admin_password.update');
        Route::get('/password/reset', ['uses' => 'Auth\AdminForgotPasswordController@showLinkRequestForm'])->name('admin_password.request');
        Route::post('/password/reset/{token}', ['uses' => 'Auth\AdminResetPasswordController@showResetForm'])->name('admin_password.reset');
        
       # Route::post('/register', ['uses' => 'Auth\AdminRegisterController@register']);
        #Route::get('/register', ['uses' => 'Auth\AdminRegisterController@showRegistrationForm'])->name('admin_register');
       
        Route::get('/me', ['uses' => 'Admin\User\UserController@me'])->name('admin_users_me');
      
        Route::get('/validate/{id}', ['uses' => 'Admin\User\UserController@validar'])->name('admin_users_validate');
        Route::get('/validate/{id}/update', ['uses' => 'Admin\User\UserController@validate_update'])->name('admin_users_validate_update');
        




        Route::post('/me/update', ['uses' => 'Admin\User\UserController@update'])->name('admin_user_me_update');
        Route::post('/me/update-password', ['uses' => 'Admin\User\UserController@update_password'])->name('admin_user_me_update_password');
    });



    Route::prefix('/users')->group(function () 
    {
    
        // user routes
        Route::group(['middleware' => 'admin_check'], function(){

            Route::prefix('/pending')->group(function () 
            {
                Route::get('/index', ['uses' => 'Admin\User\UserController@index_pending'])->name('admin_users_index_pending');
                Route::get('/index/page/{page}', ['uses' => 'Admin\User\UserController@index_pending'])->name('admin_users_index_page_pending');
                Route::get('/search', ['uses' => 'Admin\User\UserController@search_pending'])->name('admin_users_search_pending');
            });
            
            
                    
            Route::get('/index', ['uses' => 'Admin\User\UserController@index'])->name('admin_users_index');
            Route::get('/index/page/{page}', ['uses' => 'Admin\User\UserController@index'])->name('admin_users_index_page');
            Route::get('/search', ['uses' => 'Admin\User\UserController@search'])->name('admin_users_search');
            Route::get('/show/{id}', ['uses' => 'Admin\User\UserController@show'])->name('admin_users_show');
            Route::get('/new', ['uses' => 'Admin\User\UserController@new'])->name('admin_users_new');
            Route::get('/edit/{id}', ['uses' => 'Admin\User\UserController@edit'])->name('admin_users_edit');
            Route::post('/update/status/{id}', ['uses' => 'Admin\User\UserController@update_status'])->name('admin_users_update_status');

            
            Route::post('/store', ['uses' => 'Admin\User\UserController@store'])->name('admin_users_store');
            Route::post('/update/{id}', ['uses' => 'Admin\User\UserController@update'])->name('admin_users_update');
            Route::post('/update-password/{id}', ['uses' => 'Admin\User\UserController@update_password'])->name('admin_users_update_password');
            Route::get('/delete/{id}', ['uses' => 'Admin\User\UserController@delete'])->name('admin_users_delete');
        });


     
    });


});



