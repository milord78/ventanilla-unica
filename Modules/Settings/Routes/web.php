<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->group(function() {
    Route::prefix('settings')->group(function() {
     Route::get('/', ['uses' => 'Admin\SettingsController@me'])->name('admin_settings');
     Route::post('/update', ['uses' => 'Admin\SettingsController@update'])->name('admin_settings_update');
    });
});

