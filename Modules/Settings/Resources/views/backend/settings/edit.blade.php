@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">
    

    <h3 class="box-title">{{ __('Configuración') }}</h3>
    <form role="form" method="post" action="{{$route}}" enctype='multipart/form-data'>
       @csrf
      <div class="box-body">
        <div class="nav-tabs-custom" >
          <!-- Tabs within a box -->
         
          <div class="tab-content no-padding">
            <!-- Morris chart - Sales -->
            <div id="basic" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="avatar" style="background-image:url({{$item->logos()}}); background-size: cover;height: 200px;width: 100%; border-radius: 5px;">
                            </div>
                            <br>
                        </div>
                        <div class="col-md-8">
                            
                          <label for="exampleInputEmail1">{{__('Logo') }}</label>
                          <input type="file" name="thumbnail">
                        
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">{{ __('Nombre de la institución') }}</label>
                      <input type="text" name="name" value="{{$item->name}}"   class="form-control" >
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">{{ __('Descripción') }}</label>
                      <textarea  name="description"  class="form-control" >{{$item->description}}</textarea>
                    </div> 
                  
                  
                   
                        
                   

            </div>
           
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>

      </div>
    </form>

  </div>
@endsection
