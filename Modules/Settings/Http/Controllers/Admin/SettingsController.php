<?php

namespace  Modules\Settings\Http\Controllers\Admin;


use Modules\Settings\Entities\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }


 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
         
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
   

     

        
        if(!empty($request->file('thumbnail')))
        {
            $file_name = $request->file('thumbnail')->getClientOriginalName();
            $path = $request->file('thumbnail')->store('public');
            $inder_data['logo'] = $path;
            //$inder_data['thumbnail_name'] = $file_name;
        }   

     
            $redirect_url = redirect()->route("admin_settings")
                ->with('success','settings::settings.settings.UPDATE_SUCESSFULLY_LABEL');
                $item = Settings::get();
                if(count($item)> 0)
                {
                    $item = $item[0];
                }else{
                    $item = Settings::create(['name'=>null]);
                }
          
                Settings::find($item->id)->update($inder_data);
                return $redirect_url;
        
    }
 
  

    public function me(Request $request)
    {

     
        $route = route('admin_settings_update');
        $item = Settings::get();
        if(count($item)> 0)
        {
            $item = $item[0];
        }else{
            $item = Settings::create(['name'=>null]);
        }
        

 
        return view('settings::backend/settings/edit',compact('item','route'));
    }

}
