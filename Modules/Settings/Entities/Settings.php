<?php

namespace Modules\Settings\Entities;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
  	protected $table = 'settings';
    protected $fillable = [
        'logo',
        'name',
        'description',
    ];
    public function logos()
    {
        if($this->logo==null)
        {
            return asset('assets/img/profileicon-1487703856.png');

        }else{
            
            return "/".str_replace("public","images",$this->logo);
        }
    }

}
