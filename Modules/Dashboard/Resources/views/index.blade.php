@extends('layouts.backend')
@section('content')
  	<br>
    <div id="app" class=" clearfix">
    	<div class="col-md-6">

		    <div class="box">
		      	<div class="box-header" style="padding: 15px">
		        	<h3 class="box-title">{{  __('Aspirantes inscritos') }}</h3>
					<div class="box-body no-padding">
						<div id="chartOne" v-el:chartOne></div>
					</div>
				</div>
			</div>
    		
    	</div>
    	<div class="col-md-6">

		    <div class="box">
		      	<div class="box-header" style="padding: 15px">
		        	<h3 class="box-title">{{  __('Aspirantes certificados de secundaria') }}</h3>
					<div class="box-body no-padding">
						<div id="chartTwo" v-el:chartTwo></div>
					</div>
				</div>
			</div>
    		
    	</div>
    	<div class="col-md-6">

		    <div class="box">
		      	<div class="box-header" style="padding: 15px">
		        	<h3 class="box-title">{{  __('Hombres inscritos') }}</h3>
					<div class="box-body no-padding">
						<div id="chartTree" v-el:chartTree></div>
					</div>
				</div>
			</div>
    		
    	</div>
		<div class="col-md-6">

			<div class="box">
				<div class="box-header" style="padding: 15px">
					<h3 class="box-title">{{  __('Mujeres inscritos') }}</h3>
					<div class="box-body no-padding">
						<div id="chartTree" v-el:chartTree></div>
					</div>
				</div>
			</div>

			</div>
    	
    </div>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/admin/charts.js') }}"></script>
@endsection