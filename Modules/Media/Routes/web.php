<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('media')->group(function() {
    Route::get('/', 'MediaController@index');
});


	Route::prefix('api')->group(function () 
	{

		Route::prefix('media')->group(function ()
		{
		  	Route::prefix('list')->group(function ()
			{
				Route::post('/files', ['uses' => 'Api\MediaController@list_files'])->name('admin_api_files_list');
		  		Route::post('/dir', ['uses' => 'Api\MediaController@list_dir'])->name('admin_api_dir_list');
		  	});
		  
		  	Route::prefix('file')->group(function ()
			{
				Route::post('/upload', ['uses' => 'Api\MediaController@upload_file'])->name('admin_api_file_upload');
				Route::post('/delete', ['uses' => 'Api\MediaController@delete_file'])->name('admin_api_file_delete');
				Route::post('/rename', ['uses' => 'Api\MediaController@rename'])->name('admin_api_file_rename');
			});

		  	Route::prefix('dir')->group(function ()
			{
				Route::post('/delete', ['uses' => 'Api\MediaController@delete_directory'])->name('admin_api_dir_delete');
				Route::post('/make', ['uses' => 'Api\MediaController@make_directory'])->name('admin_api_dir_make');
			});

		});
	});


Route::prefix('admin')->group(function() 
{
	Route::prefix('media')->group(function () 
	{
		Route::get('/', ['uses' => 'Admin\MediaController@index'])->name('admin_media');
	
	});
});