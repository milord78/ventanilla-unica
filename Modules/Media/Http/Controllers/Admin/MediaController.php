<?php

namespace Modules\Media\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }

    /**
     *  List the resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
       
        return view('media::backend/media/list');
    }


   
}
