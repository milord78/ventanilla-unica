<?php

namespace Modules\Media\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class MediaController extends Controller
{
    //
    public function __construct()
    {
       
    }

    
    public function list_files(Request $request)
    {
        $path = "public".$request->input('path');
        $files = [];
        foreach (Storage::files($path) as $key => $file) 
        {
            //$file = str_replace(  $path , "", $file);
            // $file = 
            $file2 = str_replace($path, "", $file);
            if($file2!=".gitignore")
            {   
                array_push($files, [
                    'file' =>$file2,
                    'path' =>$file,
                ]);
            }
            
        }
        return response()->json($files );
    }

    
    public function list_dir(Request $request)
    {
        
        $path = "public".$request->input('path');
        $files = [];
        foreach (Storage::directories($path) as $key => $file) 
        {
            $file = str_replace($path, "", $file);
            if($file!=".gitignore")
            {   
                array_push($files, [
                    'path' => $path,
                    'file' =>$file,
                ]);
            }
            
        }
        return response()->json($files );
   

    }


    public function upload_file(Request $request)
    {
        
        $path = $request->input('path');
        if(!empty($request->file('file')))
        {
            $file = $request->file('file')->store('public/'.$path);
            return response()->json(array('success' => true ));
        }   
        
        return response()->json(array('success' => false ));
    }
    public function delete_file(Request $request)
    {   
        $path = $request->input('path');

        if(Storage::disk('public')->exists($path))
        {
            Storage::disk('public')->delete($path);
             return response()->json(array('success' => true ));
        }
     
        return response()->json(array('success' => false ));
       
    }

    public function delete_directory(Request $request)
    {   
        $path = 'public/'.$request->input('path');
        Storage::deleteDirectory($path);
        return response()->json(array('success' => true ));
    }

      public function make_directory(Request $request)
    {   

       
        $path = $request->input('path');
        Storage::makeDirectory('public/'.$path );

     
        return response()->json(array('success' => true ));
       
    }

    public function rename(Request $request)
    {   

        $path = 'public/'.$request->input('path');
        $from = $request->input('from');
        $to = $request->input('to');
        Storage::move($path. $from , $path .$to);
        return response()->json(array('success' => true ));
       
    }
 
 
    

   
}
