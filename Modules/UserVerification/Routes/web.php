<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('userverification')->group(function() {
    Route::get('/', 'UserVerificationController@index');
});

Route::prefix('account')->group(function() 
{
  	Route::prefix('verification')->group(function() 
  	{
  		Route::get('/status', ['uses' => 'Frontend\UserVerificationController@status'])->name('frontend_user_verification_status');
  		Route::get('/sent', ['uses' => 'Frontend\UserVerificationController@sent'])->name('frontend_user_verification_sent');


  		Route::get('/new', ['uses' => 'Frontend\UserVerificationController@new'])->name('frontend_user_verification_new');
  		Route::post('/store', ['uses' => 'Frontend\UserVerificationController@store'])->name('frontend_user_verification_store');
  		 Route::post('/update', ['uses' => 'Frontend\UserVerificationController@update'])->name('frontend_user_verification_update');
  	});
  	
});

Route::prefix('admin')->group(function() 
{
  	Route::prefix('verification')->group(function() 
  	{
		Route::prefix('/user')->group(function () 
		{
			Route::get('/index', ['uses' => 'Admin\UserVerificationController@index'])->name('admin_user_verification_index');
			Route::get('/search', ['uses' => 'Admin\UserVerificationController@search'])->name('admin_user_verification_search');

			Route::get('/index/{status}', ['uses' => 'Admin\UserVerificationController@index_status'])->name('admin_user_verification_index_status');
			Route::get('/search/{status}', ['uses' => 'Admin\UserVerificationController@search_status'])->name('admin_user_verification_search_status');

			Route::get('/show/{id}', ['uses' => 'Admin\UserVerificationController@show'])->name('admin_user_verification_show');
			Route::get('/new', ['uses' => 'Admin\UserVerificationController@new'])->name('admin_user_verification_new');
			Route::get('/edit/{id}', ['uses' => 'Admin\UserVerificationController@edit'])->name('admin_user_verification_edit');
			Route::post('/store', ['uses' => 'Admin\UserVerificationController@store'])->name('admin_user_verification_store');
			Route::post('/update/{id}', ['uses' => 'Admin\UserVerificationController@update'])->name('admin_user_verification_update');
			Route::get('/delete/{id}', ['uses' => 'Admin\UserVerificationController@delete'])->name('admin_user_verification_delete');
		});

	});
});

