<?php

namespace Modules\UserVerification\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\UserVerification\Entities\UserProfileVerification;
use Illuminate\Support\Facades\Config;

class UserVerificationController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin_check');
    }
    
    public function index(Request $request)
    {
       
        $items = UserProfileVerification::orderby('id','desc')->paginate(15);
        $q = "";
        $status = "all";
        $route = route('admin_user_verification_search');
        return view('userverification::backend/userverification/list',compact('items','q','route','status'));
    }

    public function search(Request $request)
    {
        $q = $request->input('name');
        $status = "all";
        $route = route('admin_user_verification_search');
        if(!empty($q))
        { 
            $items = UserProfileVerification::where('name',$q)->orderby('id','desc')->paginate(15);
        }else
        {
            $items = UserProfileVerification::orderby('id','desc')->paginate(15);
        }
        
        return view('userverification::backend/userverification/list',compact('items','q','route','status'));
    }


     public function index_status(Request $request,$status)
    {
        $route = route(
            'admin_user_verification_search_status',
            ['status'   =>  $status]
        );
        $items = UserProfileVerification::where([
                ['statuses','=',$status]
            ])->orderby('id','desc')->paginate(15);
        $q = "";
        return view('userverification::backend/userverification/list',compact('items','q','route','status'));
    }

    public function search_status(Request $request,$status='pending')
    {
        $q = $request->input('name');
        $route = route(
            'admin_user_verification_search_status',
            ['status'=>$status]
        );
        if(!empty($q))
        { 
            $items = UserProfileVerification::where([['name','=',$q],['statuses','=',$status]])->orderby('id','desc')->paginate(15);
        }else
        {
            $items = UserProfileVerification::where([
                ['statuses','=',$status]
            ])->orderby('id','desc')->paginate(15);
        }
        
        return view('userverification::backend/userverification/list',compact('items','q','route','status'));
    }

    
    public function show(Request $request, $id)
    {
        $price = UserProfileVerification::find( $id);
        return view('userverification::backend/userverification/edit',compact('price'));
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $item = UserProfileVerification::find( $id);
     
        $route = route('admin_user_verification_update',['id' => $item->id]);
        return view('userverification::backend/userverification/edit',compact('item','route'));
    }
 
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {

        $route = route('admin_user_verification_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->statuses = null;
        $item->document_rebsumit_type_reason = null;


        return view('userverification::backend/userverification/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id=null)
    {
        $this->validate($request, [
            'statuses' => 'required'
        ]);
        
        $inder_data = $request->except(['_token','_method','submit']);
       
        $redirect_url = redirect()->route("admin_user_verification_edit",['id'=>$id])->with('success','userverification::userverification.userverification.UPDATE_SUCESSFULLY_LABEL');
        
        UserProfileVerification::find($id)->update($inder_data);
        return $redirect_url;
       
        
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request,$id)
    {
        UserProfileVerification::find($id)->delete();
        return redirect()->route("admin_user_verification_index")
                        ->with('success','userverification::userverification.userverification.DELETED_SUCESSFULLY_LABEL');
    }

   
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
        ]);

        $inder_data = $request->except(['_token','_method','submit']);
        if(UserProfileVerification::where('name', $inder_data['name'])->count() == 0)
        {
            $user = UserProfileVerification::create($inder_data);

            return redirect()->route("admin_user_verification_edit",['id'=>$user->id])
                        ->with('success','userverification::userverification.userverification.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return redirect()->route("admin_user_verification_new")
                        ->with('error','userverification::userverification.userverification.EXISTS_LABEL');  
        }

    }



}
