<?php

namespace Modules\UserVerification\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\UserVerification\Entities\UserProfileVerification;
use Illuminate\Support\Facades\Config;

class UserVerificationController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }
    

    
    public function status(Request $request)
    {

        if(user_profile_verificatio_count()==false)
        {
            $redirect_url = redirect()->route("frontend_user_verification_store")->with('success','userverification::userverification.userverification.PLEASE_SUBMIT_A_DOCUMENT_LABEL');
            return $redirect_url;
        }
        $id = user_profile_verificatio_get()->id;
        $item = UserProfileVerification::find( $id);
        $route = route('frontend_user_verification_update',['id' => $item->id]);
        return view('userverification::frontend/userverification/status',compact('item','route'));
    }

    public function sent(Request $request)
    {
       
        return view('userverification::frontend/userverification/sent');
    }

 
        
    
       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {   


        if(user_profile_verificatio_count()==false)
        {
            $redirect_url = redirect()->route("frontend_user_verification_store")->with('success','userverification::userverification.userverification.PLEASE_SUBMIT_A_DOCUMENT_LABEL');
            return $redirect_url;
        }
        $id = user_profile_verificatio_get()->id;
        $item = UserProfileVerification::find( $id);
     
        $route = route('frontend_user_verification_update',['id' => $item->id]);
        return view('userverification::frontend/userverification/edit',compact('item','route'));
    }
 
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new(Request $request)
    {

        
        
        $item = new \stdClass();
        $item->id = null;
        $item->statuses = null;
        $item->document_rebsumit_type_reason = null;
        $item->document_type = null;
        $item->document_photo = null;
        $item->user_photo_with_document = null;
        $item->document_number = null;     
        $item->code_generated = generateRandomString(7); 
        $request->session()->put('code_generated',$item->code_generated );
        $item->address_document_type = null;   
        $item->address_document_photo = null;          
        $item->document_rebsumit_type_reason = null;          

        $route = route('frontend_user_verification_store');
        return view('userverification::frontend/userverification/edit',compact('route','item'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $this->validate($request, [ ]);
        
        $inder_data = $request->except([
            '_token','_method','submit'
        ]);
       
        if(user_profile_verificatio_count()==false)
        {
            $redirect_url = redirect()->route("frontend_user_verification_store")->with('success','userverification::userverification.userverification.PLEASE_SUBMIT_A_DOCUMENT_LABEL');
            return $redirect_url;
        }
        $redirect_url = redirect()->route("frontend_user_verification_sent")->with('success','userverification::userverification.userverification.UPDATE_SUCESSFULLY_LABEL');
        $id = user_profile_verificatio_get()->id;
        $item = UserProfileVerification::find($id);


        if($item->autor_id == \Auth::user()->id)
        {


            $inder_data['statuses'] = "pending";
            if(!empty($request->file('document_photo')))
            {
                $file_name = $request->file('document_photo')->getClientOriginalName();
                $path = $request->file('document_photo')->store('public/verification');
                $inder_data['document_photo'] = str_replace("public/verification", "images/verification", $path);
            } 
            if(!empty($request->file('user_photo_with_document')))
            {
                $file_name = $request->file('user_photo_with_document')->getClientOriginalName();
                $path = $request->file('user_photo_with_document')->store('public/verification');
                $inder_data['user_photo_with_document'] = str_replace("public/verification", "images/verification", $path);
            } 
            if(!empty($request->file('address_document_photo')))
            {
                $file_name = $request->file('address_document_photo')->getClientOriginalName();
                $path = $request->file('address_document_photo')->store('public/verification');
                $inder_data['address_document_photo'] = str_replace("public/verification", "images/verification", $path);
            } 

            $item->update($inder_data);
        }
        
        return $redirect_url;
       
        
    }
 

   
    public function store(Request $request)
    {
        $this->validate($request, []);

        $inder_data = $request->except([
            '_token','_method','submit'
        ]);
      
        if(user_profile_verificatio_count()==false)
        {

            $inder_data['statuses'] = "pending";
            $inder_data['code_generated'] =  $request->session()->get('code_generated');
            if(!empty($request->file('document_photo')))
            {
                $file_name = $request->file('document_photo')->getClientOriginalName();
                $path = $request->file('document_photo')->store('public/verification');
                $inder_data['document_photo'] = str_replace("public/verification", "images/verification", $path);
            } 
            if(!empty($request->file('user_photo_with_document')))
            {
                $file_name = $request->file('user_photo_with_document')->getClientOriginalName();
                $path = $request->file('user_photo_with_document')->store('public/verification');
                $inder_data['user_photo_with_document'] = str_replace("public/verification", "images/verification", $path);
            } 
            if(!empty($request->file('address_document_photo')))
            {
                $file_name = $request->file('address_document_photo')->getClientOriginalName();
                $path = $request->file('address_document_photo')->store('public/verification');
                $inder_data['address_document_photo'] = str_replace("public/verification", "images/verification", $path);
            } 

            $inder_data['autor_id'] = \Auth::user()->id;
            $user = UserProfileVerification::create($inder_data);
        }
        return redirect()->route("frontend_user_verification_sent")
                        ->with('success','userverification::userverification.userverification.CREATED_SUCCESSFULLY_LABEL');
      

    }



}
