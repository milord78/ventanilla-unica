<?php

namespace Modules\UserVerification\Entities;

use Illuminate\Database\Eloquent\Model;

class UserProfileVerification extends Model
{
	protected $table = 'user_profile_verification';
	
    protected $fillable = [
		'autor_id',
		'document_type',
		'statuses',
		'document_photo',
		'user_photo_with_document',
		'document_number',
		'code_generated',
		'address_document_type',
		'address_document_photo',
		'document_rebsumit_type_reason',
    ];

    public function user()
    {
        return $this->belongsTo('App\User','autor_id');
    }    
}
