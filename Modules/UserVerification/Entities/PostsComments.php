<?php

namespace Modules\ArticlesComments\Entities;

use Illuminate\Database\Eloquent\Model;

class PostsComments extends Model
{
	protected $table = 'articles_comment';

    protected $fillable = [
    	'post_id',
		'content',
		'autor_id',
		'publish',
    ];        
}
