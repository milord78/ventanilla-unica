@extends('layouts.backend')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">


      <div class="box-header" style="padding: 15px">
        <h3 class="box-title">{{  __('articlescomments::articlescomments.articlescomments.COMMENTS_LABEL') }}</h3>
        <div class="box-tools">
          <form method="get" action="{{route('admin_articles_search')}}">
            <div class="input-group input-group-sm" style="width: 550px;">
               <input type="text" name="title" value="{{$q}}" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }} {{ __('Email') }}">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
        <table class="table">
          <thead>
            <tr>
              <th class="col2"># </th>
              <th>{{ __('user.USER_LABEL') }}</th>
              <th>{{ __('articlescomments::articlescomments.articlescomments.COMMENT_LABEL') }}</th>
              <th  style="width: 60px">{{ __('general.ACTIONS_LABEL') }}</th>
            </tr>
          </thead>
          <tbody>
            @foreach($items  as $index => $item)
            <tr>
              <td>{{$item['id']}}</td>

              <td>
                <div>
                   @if ($item->logo != null)
                    <img src="/images/{{$item->logo}}" class="img-circle" alt="User Image">
                  @else
                    <img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                  @endif
                </div>
                <strong>{{$item['firstname']}}</strong>
                <strong>{{$item['lastname']}}</strong> -
                <strong>{{$item['email']}}</strong>
              </td>
              <td>{{$item['content']}}</td>
              <td>
                <a href="{{ route('admin_article_comment_delete',['id'=>$item['id']]) }}" class="btn btn-block btn-danger">
                  {{ __('general.DELETE_LABEL') }}
                </a>
              </td>
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>

@endsection
