@extends('layouts.backend')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">
      <div class="box-header" style="padding: 15px">
        <h3 class="box-title">
          {{ __('userverification::userverification.userverification.USER_VERIFICATIONS_LABEL') }}
        </h3>
        <div class="box-tools">
          <form method="get" action="{{$route}}">
            <div class="input-group input-group-sm" style="width: 550px;">
               <input type="text" name="name" value="{{$q}}" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }} {{ __('Email') }}">
              <div class="input-group-btn">
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">

        <div class="nav-tabs-custom" >
          <!-- Tabs within a box -->
          <ul class="nav nav-tabs">
            <li @if ($status == 'all' ) class="active" @endif>
              <a href="{{route('admin_user_verification_index')}}" >
                {{ __('general.All_LABEL') }}
              </a>
            </li>
            <li @if ($status == 'pending' ) class="active" @endif><a href="{{route('admin_user_verification_index_status',['status'=>'pending'])}}" aria-expanded="false">{{get_verification_statutses('pending')}}</a></li>
            <li  @if ($status == 'resubmit' ) class="active" @endif><a href="{{route('admin_user_verification_index_status',['status'=>'resubmit'])}}"  aria-expanded="false">{{get_verification_statutses('resubmit')}}</a></li>
            <li  @if ($status == 'cancelled' ) class="active" @endif><a href="{{route('admin_user_verification_index_status',['status'=>'cancelled'])}}" aria-expanded="false">{{get_verification_statutses('cancelled')}}</a></li>
            <li  @if ($status == 'refused' ) class="active" @endif><a href="{{route('admin_user_verification_index_status',['status'=>'refused'])}}" aria-expanded="false">{{get_verification_statutses('refused')}}</a></li>
            <li @if ($status == 'verified' ) class="active" @endif><a href="{{route('admin_user_verification_index_status',['status'=>'verified'])}}" aria-expanded="false">{{get_verification_statutses('verified')}}</a></li>
          </ul>

          
        </div>

    

        <table class="table">
          <thead>
            <tr>
              <th class="col2"># </th>
              <th>{{ __('Email') }}</th>
              <th>{{ __('general.STATUS_LABEL') }}</th>
              <th>{{ __('userverification::userverification.userverification.DOCUMENT_TYPE_LABEL') }}</th>
              <th>{{ __('userverification::userverification.userverification.ADDRESS_DOCUMENT_TYPE_LABEL') }}</th>
              <th  style="width: 60px">{{ __('general.ACTIONS_LABEL') }}</th>
            </tr>
          </thead>
          <tbody>
            @foreach($items  as $index => $item)
            <tr>
              <td>{{$item['id']}}</td>
              <td>{{$item->user->email}}</td>
              <td>
                <label class="label @if($item['statuses']=='pending') label-danger @endif @if($item['statuses']=='resubmit') label-info @endif @if($item['statuses']=='cancelled') label-danger @endif @if($item['statuses']=='refused') label-danger @endif @if($item['statuses']=='verified') label-primary @endif">
                  {{get_verification_statutses($item['statuses'])}}
                </label>
            </td>
            
              <td>
                <label class="label label-primary"> {{get_verification_document_type($item['document_type'])}}
                </label>
              </td>
              <td>
                <label class="label label-primary"> {{get_address_verification_document_type($item['address_document_type'])}}
                </label>
              </td>
            
              <td>
                <a href="{{ route('admin_user_verification_edit',['id'=>$item['id']]) }}" class="btn btn-block btn-success">
                  {{ __('general.VIEW_LABEL') }}
                </a>
              </td>
            </tr>
            @endforeach    
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>

@endsection
