@extends('layouts.backend')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">




       <h3  style="margin: 15px" class="box-title">  
          {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }}
         
           {{ __('general.VIEW_LABEL') }}
         
       </h3>
 
      <form role="form" method="post" action="{{$route}}">
           @csrf
          <div class="box-body">
            <div class="nav-tabs-custom" >
              <!-- Tabs within a box -->
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#basic" data-toggle="tab" aria-expanded="true">
                    {{ __('general.BASIC_LABEL') }}
                  </a>
                </li>
              </ul>
              <div class="tab-content no-padding">
                <!-- Morris chart - Sales -->
                <div id="basic" class="tab-pane active">
                    <div style="margin: 15px">
                      <table class="table">
                        <tbody>
                          <tr>
                            <th>{{ __('general.STATUS_LABEL') }}:</th>
                            <td width="85%">
                              <label class="label @if($item->statuses=='pending') label-danger @endif @if($item->statuses=='resubmit') label-info @endif @if($item->statuses=='cancelled') label-danger @endif @if($item->statuses=='refused') label-danger @endif @if($item->statuses=='verified') label-primary @endif">{{$item->statuses}}</label>
                               <a style="margin: 15px;display:inline-block;" id="change_status_btn" class="btn btn-primary" >{{ __('general.CHANGE_STATUS_LABEL') }}</a>
                               <div style="margin: 15px;display:inline-block;" id="change_status" class="hide">
                                <select name="statuses" class="form-control">
                                  @foreach (Config('userverification.statuses') as $key => $value)  
                                    <option @if($item->statuses==$value['name']) selected="selected" @endif value="{{$value['name']}}">{{$value['value']}}</option>
                                  @endforeach
                                </select>
                               </div>
                               <div style="margin: 15px;display:inline-block;" id="change_status_reason" class="hide">
                                <select name="document_rebsumit_type_reason" class="form-control">
                                  @foreach (Config('userverification.document_rebsumit_type_reason') as $key => $value)  
                                    <option @if($item->document_rebsumit_type_reason==$value['name']) selected="selected" @endif  value="{{$value['name']}}">{{$value['value']}}</option>
                                  @endforeach
                                </select>
                               </div>
                            </td>
                          </tr>
                          <tr>
                            <th><label>{{ __('userverification::userverification.userverification.CODE_GENERATED_LABEL') }}:</label></th>
                            <td>
                              <label class="label label-primary"> {{$item->code_generated}}
                              </label>
                            </td>
                          </tr>
                          <tr>
                            <th><label>{{ __('userverification::userverification.userverification.DOCUMENT_TYPE_LABEL') }}:</label></th>
                            <td>
                              <label class="label label-primary"> {{get_verification_document_type($item->document_type)}}
                              </label>
                            </td>
                          </tr>
                           <tr>
                            <th> <label>{{ __('userverification::userverification.userverification.DOCUMENT_PHOTO_LABEL') }}:</label></th>
                            <td>
                              <img src="{{$item->document_photo}}">
                            </td>
                          </tr>
                          <tr>
                            <th>  <label>{{ __('userverification::userverification.userverification.USER_PHOTO_WITH_DOCUMENT_LABEL') }}:</label></th>
                            <td>
                               <img src="{{$item->user_photo_with_document}}">
                            </td>
                          </tr>
                          <tr>
                            <th> <label>{{ __('userverification::userverification.userverification.ADDRESS_DOCUMENT_TYPE_LABEL') }}:</label></th>
                            <td>
                              <label class="label label-primary"> {{get_address_verification_document_type($item->address_document_type)}}
                              </label>
                            </td>
                          </tr>
                          <tr>
                            <th><label>{{ __('userverification::userverification.userverification.ADDRESS_DOCUMENT_PHOTO_LABEL') }}:</label></th>
                            <td>
                              <a class="btn btn-primary" target="_blank" href="{{$item->address_document_photo}}">
                            {{ __('userverification::userverification.userverification.VIEW_DOCUMENT_LABEL') }}
                          </a>
                            </td>
                          </tr>                         
                        </tbody>
                      </table>
       


                       
                      
                       
                    </div>
                </div>

              </div>

              </div>
          </div>
         
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{ __('general.SAVE_LABEL') }}</button>
            <a href="{{ route('admin_user_verification_index') }}" class="btn btn-success">{{ __('general.BACK_LABEL') }}</a>
            
        </div>

      </form>
   
    
  </div>
@endsection
@section('footer_scripts')
<script type="text/javascript">

  string_to_slug = function (str) 
  {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;~#";
    var to   = "aaaaeeeeiiiioooouuuunc--------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
  }
  var ready = function(e)
  { 

    var change_status_btn = function(e)
    {
      $(this).addClass('hide');
      $("#change_status").removeClass('hide');
      if($("#change_status select").val()=='resubmit')
      {
        $("#change_status_reason").removeClass('hide');
      }
    }

    var change_status = function(e)
    {
      selected = $(this).find('select').val();
      if(selected == 'resubmit')
      {
        $("#change_status_reason").removeClass('hide');

      }else{
        $(this).addClass('hide');
        $("#change_status_reason").addClass('hide')
        $("#change_status_btn").removeClass('hide');
      }
    }

    var change_status_reason = function(e)
    {
      $(this).addClass('hide');
      $("#change_status").addClass('hide');
      $("#change_status_btn").removeClass('hide');

    }
    $("#change_status_btn").on('click',change_status_btn)
    $("#change_status ").on('change',change_status)
    $("#change_status_reason select").on('change',change_status_reason)


    copy_title = function(e)
    {
      value = $(this).val()
      
      $("input[name=slug]").val(string_to_slug(value));
      $("input[name=meta_title]").val(value);

      
    }
    $("input[name=name]").on('keyup',copy_title);
  } 
  $(window).on('load',ready);

</script>
@endsection
