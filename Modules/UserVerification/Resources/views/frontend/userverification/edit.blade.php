
@extends('layouts.user_panel')

@section('content')
<div class="app-heading app-heading-bordered app-heading-page">
    <div class="icon icon-lg">
        <span class="icon-list3"></span>
    </div>
    <div class="title">            
        <h1>  {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }} </h1>
    </div>
</div>

    
<div class="app-heading-container app-heading-bordered bottom" >
    <ul class="breadcrumb">
        <li><a href="#">Dashboard</a></li>
        <li class="active">
        {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }} 
        </li>
    </ul>
</div>
<div class="container">
  <div class="block"> 
  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div   id="verification_form" class="box">





 
      <form   enctype="multipart/form-data"   method="post" action="{{$route}}">
           @csrf
          <h3  style="margin: 15px" class="box-title">  
            {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }} <span id="step" >Step 1</span> 
          </h3>
          <div class="box-body">
            <div id="step1" style="padding: 15px;">
              <h6>{{ __('userverification::userverification.userverification.DOCUMENT_TYPE_LABEL') }} </h6>
              <img style="width: 250px; display: block;margin: 0 auto; margin-top: 10px; margin-bottom: 10px;" src="{{ asset('assets/img/indentity.svg') }}" class="img-responsive">
              
              <select name="document_type" class="form-control">
               
                @foreach (Config('userverification.document_type') as $key => $value)  
                  <option    value="{{$value['name']}}">{{$value['value']}}</option>
                @endforeach
                
              </select>
              
              
              <div style="padding: 10px;">
                <h6>{{ __('userverification::userverification.userverification.DOCUMENT_NUMBER_LABEL') }} </h6>
                <input type="text" class="form-control" placeholder="{{ __('userverification::userverification.userverification.WRITE_DOCUMENT_NUMBER_LABEL') }}" name="document_number">
                 
              </div>
     
              <div style="padding: 10px;">
                
                <input type="file" name="document_photo">
                 
              </div>
             
              
            </div>
            <div id="step2"  class="hide"  style="padding: 15px;">
              <h6>{{ __('userverification::userverification.userverification.USER_PHOTO_WITH_DOCUMENT_LABEL') }} </h6>
              <img style="width: 250px; display: block;margin: 0 auto; margin-top: 10px; margin-bottom: 10px;" src="{{ asset('assets/img/photo_picture.svg') }}" class="img-responsive">
              <div><strong>{{ __('userverification::userverification.userverification.CODE_GENERATED_LABEL') }}:&nbsp;</strong>{{$item->code_generated}}</div>
              <p>{{ __('userverification::userverification.userverification.WRITE_CODE_IN_A_TEXT_LABEL') }}</p>
        
              <div style="padding: 10px;">
                <input type="file" name="user_photo_with_document">
              </div>
              
            </div>

            <div id="step3" class="hide" style="padding: 15px;">
              <h6>{{ __('userverification::userverification.userverification.ADDRESS_DOCUMENT_PHOTO_LABEL') }} </h6>
              <img style="width: 250px; display: block;margin: 0 auto; margin-top: 10px; margin-bottom: 10px;" src="{{ asset('assets/img/photo_document_upload.svg') }}" class="img-responsive">
              
              <p>{{ __('userverification::userverification.userverification.UPLOAD_ADDRESS_DOCUMENT_LABEL') }}</p>

              <select name="address_document_type" class="form-control">
             
                @foreach (Config('userverification.document_type_addresses') as $key => $value)  
                  <option    value="{{$value['name']}}">{{$value['value']}}</option>
                @endforeach
             
              </select>
            
              <div style="padding: 10px;">
                <input type="file" name="address_document_photo">
              </div>
            
            </div>
             <div id="step4"  class="hide"  style="padding: 15px;">
               <p>{{ __('userverification::userverification.userverification.PROCESSING_PLEASE_WAIT_LABEL') }}</p>
             </div>

            <div  class="box-footer">
              <button id="back"  type="button"  style="margin: 15px" class="btn btn-primary hide">{{ __('general.PREV_LABEL') }}</button>
              <button  id="next" type="button"  style="margin: 15px" class="btn btn-primary ">{{ __('general.NEXT_LABEL') }}</button>
              <button  id="submit"   style="margin: 15px;" type="submit" class="btn btn-primary hide">{{ __('general.SUBMIT_LABEL') }}</button>
            </div>
          </div>
         
        <!-- /.box-body -->
        

      </form>
   
    
    </div>
  </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">
  var ready = function(e)
  {
    var step = 1;
    
    var move_next = function(e)
    {
      step++;
      if(step>2)
      {

        $("#next").addClass("hide")
        $("#submit").removeClass("hide")
      }
      if(step>1 & step<4)
      {
        $("#back").removeClass("hide")
      }
      if(step>3)
      {

        $("#back").addClass("hide")
        $("#submit").addClass("hide")
      }


      if(step==2)
      {
        $("#step1").addClass("hide")
        $("#step2").removeClass("hide")
      }
      if(step==3)
      {
        $("#step2").addClass("hide")
        $("#step3").removeClass("hide")
      }
      if(step==4)
      {
        $("#step3").addClass("hide");
        $("#step4").removeClass("hide");
        $("#verification_form form").submit();
        
      }
             


      
    }
      var submitx = function(e)
    {
      move_next(e)
    }
     var back = function(e)
    {
      step--;
      if(step<=1 )
      {
        step = 1;
        $("#next").removeClass("hide"); 
        $("#step1").removeClass("hide"); 
          
        $("#back").addClass("hide");        
      }

       if(step > 1 && step<4 )
      {      
        $("#next").removeClass("hide");
        $("#submit").addClass("hide"); 

      }

      if(step==2)
      {
        $("#step1").addClass("hide")
        $("#step2").removeClass("hide")
      }
      if(step==3)
      {
        $("#step2").addClass("hide")
        $("#step3").removeClass("hide")
      }
      if(step==4)
      {
        $("#step3").addClass("hide")
        $("#step4").removeClass("hide")
      }

    }
    $("#back").on('click',back);
    $("#next").on('click',move_next);
    $("#submit").on('click',submitx);
    
  }
  $(window).on('load',ready);

</script>
@endsection
