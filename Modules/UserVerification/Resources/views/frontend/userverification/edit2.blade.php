
@extends('layouts.app2')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div  id="verification_form"  class="box">





 
      <form enctype="multipart/form-data"   role="form" method="post" action="{{$route}}">
           @csrf
          <h3  style="margin: 15px" class="box-title">  
            {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }} <span v-if="step <= 3 " >Step  @verbatim {{step}} @endverbatim</span> 
          </h3>
          <div class="box-body">
            <div v-if="step == 1 " style="padding: 15px;">
              <h6>{{ __('userverification::userverification.userverification.DOCUMENT_TYPE_LABEL') }} </h6>
              <img style="width: 250px; display: block;margin: 0 auto; margin-top: 10px; margin-bottom: 10px;" src="{{ asset('assets/img/indentity.svg') }}" class="img-responsive">
              @verbatim
              <select name="document_type" class="form-control">
                @endverbatim
                @foreach (Config('userverification.document_type') as $key => $value)  
                  <option    value="{{$value['name']}}">{{$value['value']}}</option>
                @endforeach
                @verbatim
              </select>
                @endverbatim
              
              <div style="padding: 10px;">
                <h6>{{ __('userverification::userverification.userverification.DOCUMENT_NUMBER_LABEL') }} </h6>
                <input type="text" class="form-control" placeholder="{{ __('userverification::userverification.userverification.WRITE_DOCUMENT_NUMBER_LABEL') }}" name="document_number">
                 
              </div>
              @verbatim
              <div style="padding: 10px;">
                
                <input type="file" name="document_photo">
                 
              </div>
              @endverbatim
              
            </div>
            <div v-if="step == 2" style="padding: 15px;">
              <h6>{{ __('userverification::userverification.userverification.USER_PHOTO_WITH_DOCUMENT_LABEL') }} </h6>
              <img style="width: 250px; display: block;margin: 0 auto; margin-top: 10px; margin-bottom: 10px;" src="{{ asset('assets/img/photo_picture.svg') }}" class="img-responsive">
              <div><strong>{{ __('userverification::userverification.userverification.CODE_GENERATED_LABEL') }}:&nbsp;</strong>{{$item->code_generated}}</div>
              <p>{{ __('userverification::userverification.userverification.WRITE_CODE_IN_A_TEXT_LABEL') }}</p>
              @verbatim
              <div style="padding: 10px;">
                <input type="file" name="user_photo_with_document">
              </div>
               @endverbatim
            </div>

            <div v-if="step == 3" style="padding: 15px;">
              <h6>{{ __('userverification::userverification.userverification.ADDRESS_DOCUMENT_PHOTO_LABEL') }} </h6>
              <img style="width: 250px; display: block;margin: 0 auto; margin-top: 10px; margin-bottom: 10px;" src="{{ asset('assets/img/photo_document_upload.svg') }}" class="img-responsive">
              
              <p>{{ __('userverification::userverification.userverification.UPLOAD_ADDRESS_DOCUMENT_LABEL') }}</p>

              @verbatim
              <select name="address_document_type" class="form-control">
              @endverbatim
                @foreach (Config('userverification.document_type_addresses') as $key => $value)  
                  <option    value="{{$value['name']}}">{{$value['value']}}</option>
                @endforeach
              @verbatim
              </select>
               @endverbatim
              @verbatim
              <div style="padding: 10px;">
                <input type="file" name="address_document_photo">
              </div>
               @endverbatim
            </div>
             <div v-if="step == 4" style="padding: 15px;">
               <p>{{ __('userverification::userverification.userverification.PROCESSING_PLEASE_WAIT_LABEL') }}</p>
             </div>

            <div  class="box-footer">
              <button v-if="step > 1 && step <= 3" v-on:click="back()" type="button"  style="margin: 15px" class="btn btn-primary">{{ __('general.PREV_LABEL') }}</button>
              <button v-if="step < 3 " v-on:click="move_next()" type="button"  style="margin: 15px" class="btn btn-primary">{{ __('general.NEXT_LABEL') }}</button>
              <button v-if="step == 3 "  v-on:click="move_next()" style="margin: 15px;" type="submit" class="btn btn-primary">{{ __('general.SUBMIT_LABEL') }}</button>
            </div>
          </div>
         
        <!-- /.box-body -->
        

      </form>
   
    
  </div>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/verification_form.js') }}"></script>
@endsection
