
@extends('layouts.app2')

@section('content')
<div class="app-heading app-heading-bordered app-heading-page">
    <div class="icon icon-lg">
        <span class="icon-list3"></span>
    </div>
    <div class="title">            
        <h1>  {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }} </h1>
    </div>
</div>

    
<div class="app-heading-container app-heading-bordered bottom" >
    <ul class="breadcrumb">
        <li><a href="#">Dashboard</a></li>
        <li class="active">
        {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }} 
        </li>
    </ul>
</div>
<div class="container">
  <div class="block"> 
  @if (\Session::has('success'))
    <div class="alert alert-success">
       <p>{{ __(\Session::get('success')) }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{  __(\Session::get('error')) }}</p>
    </div><br />
  @endif
  <div class="box">



    <div style="padding: 15px">
      <h3  style="margin: 15px" class="box-title">  
        {{ __('userverification::userverification.userverification.USER_VERIFICATION_LABEL') }}
      </h3>
      <h4>{{ __('userverification::userverification.userverification.DOCUMENT_SENT_LABEL') }}</h4>
      <p> {{ __('userverification::userverification.userverification.DOCUMENT_SENT_MESSAGE_LABEL') }}</p>
      <a class="btn btn-primary" href="{{route('homepage')}}">
        {{ __('userverification::userverification.userverification.GO_HOME_PAGE_LABEL') }}
      </a>
    </div>



   
    
    </div>
  </div>
</div>
@endsection
@section('footer_scripts')
<script type="text/javascript">

  string_to_slug = function (str) 
  {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();
    
    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;~#";
    var to   = "aaaaeeeeiiiioooouuuunc--------";
    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
  }
  var ready = function(e)
  { 

    var change_status_btn = function(e)
    {
      $(this).addClass('hide');
      $("#change_status").removeClass('hide');
      if($("#change_status select").val()=='resubmit')
      {
        $("#change_status_reason").removeClass('hide');
      }
    }

    var change_status = function(e)
    {
      selected = $(this).find('select').val();
      if(selected == 'resubmit')
      {
        $("#change_status_reason").removeClass('hide');

      }else{
        $(this).addClass('hide');
        $("#change_status_reason").addClass('hide')
        $("#change_status_btn").removeClass('hide');
      }
    }

    var change_status_reason = function(e)
    {
      $(this).addClass('hide');
      $("#change_status").addClass('hide');
      $("#change_status_btn").removeClass('hide');

    }
    $("#change_status_btn").on('click',change_status_btn)
    $("#change_status ").on('change',change_status)
    $("#change_status_reason select").on('change',change_status_reason)


    copy_title = function(e)
    {
      value = $(this).val()
      
      $("input[name=slug]").val(string_to_slug(value));
      $("input[name=meta_title]").val(value);

      
    }
    $("input[name=name]").on('keyup',copy_title);
  } 
  $(window).on('load',ready);

</script>
@endsection
