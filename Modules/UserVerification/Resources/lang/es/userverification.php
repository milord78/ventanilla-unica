<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 


    'userverification'=>[
            

        'USER_VERIFICATIONS_LABEL'          =>  'User Verifications',
        'USER_VERIFICATION_LABEL'           =>  'User Verification',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'User Verification created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'User Verification updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'User Verification deleted successfully',
        'EXISTS_LABEL'                      =>  'User Verification exists',
        'DOCUMENT_TYPE_LABEL'               =>  'Tipo de documento',
        'ADDRESS_DOCUMENT_TYPE_LABEL'       =>  'Tipo Documento de Direccion',
        'VERIFICATION_NUMBER_LABEL'         =>  'Numero de Verification',
        'CODE_GENERATED_LABEL'              =>  'Codigo generado',
        'DOCUMENT_PHOTO_LABEL'              =>  'Foto del documento',
        'USER_PHOTO_WITH_DOCUMENT_LABEL'    =>  'Foto del usuario con el documento',
        'ADDRESS_DOCUMENT_PHOTO_LABEL'      =>  'Foto del documento de direccion',
        'RESUMIT_REASON_LABEL'              =>  'Motivo de rechazo',
        'VIEW_DOCUMENT_LABEL'               =>  'Ver documento',
        'VERIFICATION_NEEDED_LABEL'         =>  'Verificacion necesaria, por favor verifica tu cuenta ahora.',
        'VERIFY_NOW_LABEL'                  =>  'Verificar ahora',
         "PLEASE_SUBMIT_A_DOCUMENT_LABEL"    =>  'Por favor envia tus documentos',
        "DOCUMENT_SENT_LABEL"               =>  'Documento enviado',
        "DOCUMENT_SENT_MESSAGE_LABEL"               =>  'Documento enviado. existosamente, nuestro proceso de verificacion toma al rededor 48 horas por favor tenga paciencia.',
        "GO_HOME_PAGE_LABEL"               =>  'ir a la pagina principal',
        "GO_USER_PANEL_LABEL"               =>  'Ir al panel usuario',
        "WRITE_CODE_IN_A_TEXT_LABEL"       => "Escribe el codigo que ves a continuacion en un papel y tomate una foto junto a tu documento de identidad.",
        "UPLOAD_ADDRESS_DOCUMENT_LABEL"    =>  'Subir documento de direccion',
      
         "PROCESSING_PLEASE_WAIT_LABEL"    => "Procesando por favor espere...",
        "DOCUMENT_NUMBER_LABEL"           => "Numero de documento",
        "WRITE_DOCUMENT_NUMBER_LABEL"     => "Por favor escriba su Numero de documento"

    ],
    

];
