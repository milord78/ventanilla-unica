<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'articlescomments'=>[
        'COMMENTS_LABEL'                    =>  'Commentarios',
        'COMMENT_LABEL'                     =>  'Commentario',
     
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Commentario creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Commentario actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Commentario borrado exitosamente',
        'USER_CREATED_SUCCESSFULLY_LABEL'   =>  'Commentario creado exitosamente',
        'EXISTS_LABEL'                      =>  'error Commentario existe',
       
      
    ]
];
