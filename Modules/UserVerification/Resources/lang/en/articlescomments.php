<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'articlescomments'=>[
        'COMMENTS_LABEL'                    =>  'Comments',
        'COMMENT_LABEL'                     =>  'Comment',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Comment created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Comment updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Comment deleted successfully',
        'USER_CREATED_SUCCESSFULLY_LABEL'   =>  'Comment created successfully',
        'EXISTS_LABEL'                      =>  'Comment exists',
        'COMMENT_LABEL'                     =>  "Comment",
       

    ]
  
    
];
