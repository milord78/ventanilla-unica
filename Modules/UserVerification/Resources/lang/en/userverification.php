<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 



    'userverification'=>[
            

        'USER_VERIFICATIONS_LABEL'          =>  'User Verifications',
        'USER_VERIFICATION_LABEL'           =>  'User Verification',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'User Verification created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'User Verification updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'User Verification deleted successfully',
        'EXISTS_LABEL'                      =>  'User Verification exists',
        'DOCUMENT_TYPE_LABEL'               =>  'Document Type',
        'ADDRESS_DOCUMENT_TYPE_LABEL'       =>  'Address Document Type',
        'VERIFICATION_NUMBER_LABEL'         =>  'Verification number',
        'CODE_GENERATED_LABEL'              =>  'Code generated',
        'DOCUMENT_PHOTO_LABEL'              =>  'Document Photo',
        'USER_PHOTO_WITH_DOCUMENT_LABEL'    =>  'User Document Photo',
        'ADDRESS_DOCUMENT_PHOTO_LABEL'      =>  'Address Document Photo',
        'VIEW_DOCUMENT_LABEL'               =>  'View document',
        'RESUMIT_REASON_LABEL'               =>  'Resumit Reason',
        'VERIFICATION_NEEDED_LABEL'         =>  'Verification needed, please verify your account now.',
        'VERIFY_NOW_LABEL'                  =>  'Verify now',
        "PLEASE_SUBMIT_A_DOCUMENT_LABEL"    =>  'Please submit a document',
        "DOCUMENT_SENT_LABEL"               =>  'Document sent.',
        "DOCUMENT_SENT_MESSAGE_LABEL"       =>  'Document had been sended, please wait our process verification take 48 hours.',
        "GO_HOME_PAGE_LABEL"               =>  'Go home page',
        "GO_USER_PANEL_LABEL"              =>  'Go USER PANEL',
        "WRITE_CODE_IN_A_TEXT_LABEL"       => "Write code in a paper and took a photo with your identity document.",
        "UPLOAD_ADDRESS_DOCUMENT_LABEL"    =>  'Upload address document',
        "PROCESSING_PLEASE_WAIT_LABEL"    => "Processing please wait...",
        "DOCUMENT_NUMBER_LABEL"           => "Document number",
        "WRITE_DOCUMENT_NUMBER_LABEL"     => "Please write your Document number"
     
    ],


];
