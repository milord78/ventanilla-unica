<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Config;


class CreateUserProfileVerificationTable extends Migration
{
    private $document_types = [];
    private $address_document_types = [];
    private $document_rebsumit_type_reason = [];
    private $statuses = [];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_verification', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('autor_id');
            $table->foreign('autor_id')->references('id')->on('users')->onDelete('cascade');
            
            foreach (Config('userverification.document_type') as $key => $value) 
            {
                array_push($this->document_types, $value['name']);
            };
            $table->enum(
                'document_type',
                $this->document_types
            );


            foreach (Config('userverification.statuses') as $key => $value) 
            {
                array_push($this->statuses, $value['name']);
            };
            $table->enum(
                'statuses',
                $this->statuses
            )->default('pending');


            foreach (Config('userverification.document_rebsumit_type_reason') as $key => $value) 
            {
                array_push($this->document_rebsumit_type_reason, $value['name']);
            };
            $table->enum(
                'document_rebsumit_type_reason',
                $this->document_rebsumit_type_reason
            )->default('pending');

            $table->text('document_photo');
            $table->text('user_photo_with_document');
            $table->string('document_number');
            $table->string('code_generated');
            
            foreach (Config('userverification.document_type_addresses') as $key => $value) 
            {
                array_push($this->address_document_types, $value['name']);
            };
            $table->enum(
                'address_document_type',
                $this->address_document_types
            );
            $table->string('address_document_photo');

          

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_verification');
    }
}
