<?php

namespace Modules\UserVerification\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserVerificationDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();

        DB::table('user_profile_verification')->insert([
            'autor_id' => 1,
            'document_type'  => "id_number",
            'document_photo'  => "",
            'user_photo_with_document'  => "",
            'document_number'  => "55555555",
            'code_generated'  => "551525",
            'statuses' => 'pending',
            'address_document_type' => 'council_document',
            'address_document_photo' => '',
        ]);


        DB::table('user_profile_verification')->insert([
            'autor_id' => 1,
            'document_type'  => "driver_license",
            'document_photo'  => "",
            'user_photo_with_document'  => "",
            'document_number'  => "55555555",
            'code_generated'  => "551525",
            'statuses' => 'pending',
            'address_document_type' => 'council_document',
            'address_document_photo' => '',
        ]);








        // $this->call("OthersTableSeeder");
    }
}
