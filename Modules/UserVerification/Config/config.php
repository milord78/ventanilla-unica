<?php






return [
    'name' => 'UserVerification',
    "document_type" => [
        ['name'=>'id_number','value'=>'Id Number'],
        ['name'=>'passport','value'=>'Passport'],
        ['name'=>'driver_license','value'=>'Driver License'],
    ],
    "document_rebsumit_type_reason" => [
        ['name'=>'pending','value'=>'pending'],
        ['name'=>'id_number_invalid','value'=>'Id Number Invalid'],
        ['name'=>'passport_invalid','value'=>'Passport Invalid'],
        ['name'=>'driver_license_invalid','value'=>'Driver License Invalid'],
        ['name'=>'tax_billing_document_invalid','value'=>'Tax billing Invalid'],
        ['name'=>'council_document_invalid','value'=>'Council document Invalid'],
        ['name'=>'court_document_invalid','value'=>'Court document Invalid'],
        ['name'=>'bank_statement_invalid','value'=>'Bank Statement Invalid'],
    ],
   
    "statuses" => [
        ['name'=>'pending','value'=>'Pending'],
        ['name'=>'resubmit','value'=>'Resubmit'],
        ['name'=>'cancelled','value'=>'Cancelled'],
        ['name'=>'refused','value'=>'Refused'],
        ['name'=>'verified','value'=>'Verified'],
    ],
    "document_type_addresses" => [
        ['name'=>'tax_billing_document','value'=>'Tax billing'],
        ['name'=>'council_document','value'=>'Council document'],
        ['name'=>'court_document','value'=>'Court document'],
        ['name'=>'bank_statement','value'=>'Bank Statement'],
    ],
];
