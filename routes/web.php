<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use League \ Flysystem \ FileExistsException;
Auth::routes();
/*
Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@index')->name('homepage');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register-success', 'HomeController@registersuccess')->name('registersuccess');
//Route::get('/contacto', 'ContactController@index')->name('contact_page');

Route::get('/plan-de-precios', 'PageDetailsController@price_table')->name('price_table');
//Route::get('/contacto/enviar', 'ContactController@contact')->name('contact_form');

Route::prefix('account')->group(function() 
{
	//two factor
	Route::get('/2fa', ['uses' => 'TwoFactorController@new'])->name('frontend_account_two_factor_new');
	Route::post('/2fa/verify', ['uses' => 'TwoFactorController@verifyToken'])->name('frontend_account_two_factor_store');

   
});


Route::fallback(function () {
    /** This will check for the 404 view page unders /resources/views/errors/404 route */
    return response()->view('errors_page.404_frontend', [])->setStatusCode(404);
});





