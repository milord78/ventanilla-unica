
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#verification_form',
 
  data: {
    step:1,
  },
  computed: {
    orderedUsers: function () {
      return _.orderBy(this.cats, 'selected','desc')
    }
  },
  methods: {
    move_next: function()
    {
       if(this.step == 3)
      {
        $("#verification_form form").submit();
      }
      this.step++;

    },
    back: function()
    {
      this.step--;
      if(this.step<0)
      {
        this.step = 0;
      }
    },
    list_categories : function(page)
    {
    },


    
  },
 
  created: function() 
  {
    this.list_categories();
  }
})
