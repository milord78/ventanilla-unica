
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#notifications',
 
  data: {
    path:"http://localhost/PHP/laravel_run_opinion_clone/public/",
    current_path:"/",
    carrer_id:0,
    tab:"opinions",
    next_page_cats:null,
    notifications:[],

  },
  methods: {
   

  
    list_notifications : function()
    {
      this.notifications = [];
 
      var url = this.path + "account/api/user/notifications/list";
      
      this.$http.post(url, {}, {}).then(data => 
      {
          
          this.notifications = data.body.data;
          this.next_page_cats = data.body.next_page_url
         
      }, response => {
        // error callback
      });

    },
   

    load_more : function(page)
    {


      this.$http.post(this.next_page_cats, { }, {}).then(data => 
      {

        this.notifications.push(...data.body.data);

        
        this.next_page_cats = data.body.next_page_url;

      }, response => {
      // error callback
      });
    },
    mark_as_read: function (item)
    {  
          var url = this.path + "account/api/user/notifications/readed";
          this.$http.post(url, {
            
            'id':item.id
          }, {}).then(data => 
          {
             
            this.notifications = this.notifications.filter(user => {
              return user.id != item.id;
            });
          }, response => {
            // error callback
          });
           
   
      
    },
     redirect: function (item)
    {  
          var url = this.path + "account/api/user/notifications/readed";
          this.$http.post(url, {
            
            'id':item.id
          }, {}).then(data => 
          {
             window.location.href = item.url
            
          }, response => {
            // error callback
          });
           
   
      
    },
  },
  created: function() 
  {

    this.list_notifications();

  }

})
