
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#questions',
 
  data: {
    path:window.api_path,
    current_path:"/",
    comments:[],
    contacts:[],
    comment:"",

  },
  methods: {
    report_item:function(item)
    {

          var url = this.path+"api/ecommerce/articles/questions/report";
      
          this.$http.post(url, {
            post_id:window.object_id,
            id:item.id,
          }, {}).then(data => 
          {
         
             
          
          }, response => {
            // error callback
          });
        
    },
    list_users_new_answers: function(item)
    {
      
        var url = this.path+"api/ecommerce/articles/questions/answer/get_related/public";
      
        this.$http.post(url,  {id:item.id}, {}).then(data => 
        {
          data.body.forEach(function(value,key )
          {
            value.selected = false;
          });
          item.answers = data.body;
     
        }, response => {
          // error callback
        });
    },
    create_text_message:function()
    {

     
        if(this.comment.length>0)
        {
          var url = this.path+"api/ecommerce/articles/questions/create";
      
          this.$http.post(url, {
            post_id:window.object_id,
            message:this.comment,
          }, {}).then(data => 
          {
            if (this.comments.length > 0)
            {
              //this.comments.push(data.body);
            }else{
             //this.comments = [data.body];
            }
            this.comment = "";
             
            this.list_users_new_messages();
          }, response => {
            // error callback
          });
        }
    },

    list_users_new_messages : function()
    {
      
 
      var url = this.path+"api/ecommerce/articles/questions/get_related";
      var that = this;
      this.$http.post(url,  {id:window.object_id}, {}).then(data => 
      {
        data.body.forEach(function(value,key )
        {
          value.selected = false;
          value.answers = [];
          that.list_users_new_answers(value);
        });
        this.comments = data.body;
         
      }, response => {
        // error callback
      });
    },
  
 
  },
  created: function() 
  {

    
    this.list_users_new_messages();

  }

})
