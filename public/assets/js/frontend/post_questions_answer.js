
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#questions_answer',
 
  data: {
    path:window.api_path,
    next_page_cats:"",
    current_path:"/",
    comments:[],
    contacts:[],
    comment:"",
    cats_query:""

  },
  methods: {
    report_item:function(item)
    {

          var url = this.path+"api/ecommerce/articles/questions/report";
      
          this.$http.post(url, {
            post_id:window.object_id,
            id:item.id,
          }, {}).then(data => 
          {
         
             
          
          }, response => {
            // error callback
          });
        
    },
    create_text_message:function(item)
    {

        var that = this;
    
        if(item.message.length>0)
        {
          var url = this.path+"api/ecommerce/articles/questions/answer/create";
            
          this.$http.post(url, {
            question_id:item.id,
            message:item.message,
            private:item.answers.length > 0,
          }, {}).then(data => 
          {
            if (item.answers.length > 0)
            {
              //this.comments.push(data.body);
            }else{
             //this.comments = [data.body];
            }
            item.message = "";
             
            that.list_users_new_answers(item);

          }, response => {
            // error callback
          });
        }
    },

    list_users_new_answers: function(item)
    {
      
        var url = this.path+"api/ecommerce/articles/questions/answer/get_related";
      
        this.$http.post(url,  {id:item.id}, {}).then(data => 
        {
          data.body.forEach(function(value,key )
          {
            value.selected = false;
          });
          item.answers = data.body;
     
        }, response => {
          // error callback
        });
    },
    list_users_new_messages : function()
    {
      
 
      var url = this.path+"api/ecommerce/articles/questions/list";
        var that = this;
      this.$http.post(url,  {}, {}).then(data => 
      {
        data.body.data.forEach(function(value,key )
        {
            value.selected = false;
            value.answers = [];
            value.message = "";
            that.list_users_new_answers(value);
        })
        this.comments = data.body.data;
        this.next_page_cats = data.body.next_page_url
      }, response => {
        // error callback
      });
    },


    search_question : function()
    {

      if(this.cats_query.length > 0)
      {

        var url = this.path+"api/ecommerce/articles/questions/search";

        this.$http.post(url, {'name':this.cats_query}, {}).then(data => 
        {

           // get body data
            data.body.data.forEach(function(value,key )
            {
              value.selected = false;
              value.message = "";
              value.answers = [];
            })

    

          this.comments = data.body.data

          
          this.next_page_cats = data.body.next_page_url

        }, response => {
          // error callback
        });

      }else{

         this.list_users_new_messages();
      }

    },
    load_more : function(page)
    {
      this.$http.post(this.next_page_cats, {}, {}).then(data => {

         // get body data
     
            data.body.data.forEach(function(value,key )
            {
                value.selected = false;
                value.message = "";
                value.answers = [];
            });

            this.comments.push(...data.body.data);
            this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
  
 
  },
  created: function() 
  {

    
    this.list_users_new_messages();

  }

})
