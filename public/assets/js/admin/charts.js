Vue.use(VueResource);
Vue.http.interceptors.push(function(request) {
 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#app',
 
  data: {
    path:window.api_path,
    cats_query:"",
    related_ids:[],
    selected:false,
    cats:[],
  },
  methods: {

    list_categories : function(page)
    {
        var url = this.path+"api/statistics/users_by_month";
    
        this.$http.post(url, {}, {}).then(data => {

            // get body data
            new Morris.Line({
              // ID of the element in which to draw the chart.
              element: "chartOne",
              // Chart data records -- each entry in this array corresponds to a point on
              // the chart.
              data: data.body.data,
              // The name of the data record attribute that contains x-values.
              xkey: 'month',
              // A list of names of data record attributes that contain y-values.
              ykeys: ['role'],
              // Labels for the ykeys -- will be displayed when you hover over the
              // chart.
              labels: ['Role']
            });
       
          });
    },
    list_user_by_year : function(page)
    {
        var url = this.path+"api/statistics/users_by_year";
    
        this.$http.post(url, {}, {}).then(data => {

            // get body data
            new Morris.Line({
              // ID of the element in which to draw the chart.
              element: "chartOne",
              // Chart data records -- each entry in this array corresponds to a point on
              // the chart.
              data: data.body.data,
              // The name of the data record attribute that contains x-values.
              xkey: 'year',
              // A list of names of data record attributes that contain y-values.
              ykeys: ['role'],
              // Labels for the ykeys -- will be displayed when you hover over the
              // chart.
              labels: ['Role']
            });
       
          });
    },

    list_suscriptions_by_month : function(page)
    {
        var url = this.path+"api/statistics/suscriptions_by_month";
    
        this.$http.post(url, {}, {}).then(data => {

            // get body data
            new Morris.Line({
              // ID of the element in which to draw the chart.
              element: "chartTwo",
              // Chart data records -- each entry in this array corresponds to a point on
              // the chart.
              data: data.body.data,
              // The name of the data record attribute that contains x-values.
              xkey: 'month',
              // A list of names of data record attributes that contain y-values.
              ykeys: ['role'],
              // Labels for the ykeys -- will be displayed when you hover over the
              // chart.
              labels: ['Role']
            });
       
          });
    },
    list_suscriptions_by_year : function(page)
    {

        var url = this.path+"api/statistics/suscriptions_by_year";
    
        this.$http.post(url, {}, {}).then(data => {

            // get body data
            new Morris.Line({
              // ID of the element in which to draw the chart.
              element: "chartTree",
              // Chart data records -- each entry in this array corresponds to a point on
              // the chart.
              data: data.body.data,
              // The name of the data record attribute that contains x-values.
              xkey: 'year',
              // A list of names of data record attributes that contain y-values.
              ykeys: ['role'],
              // Labels for the ykeys -- will be displayed when you hover over the
              // chart.
              labels: ['Role']
            });
       
          });
    },
        


  },
 
  created: function() 
  {
    this.list_categories();
         
    this.list_user_by_year();
    this.list_suscriptions_by_month();
    this.list_suscriptions_by_year();  
  }
})
