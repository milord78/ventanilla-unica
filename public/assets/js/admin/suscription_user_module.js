
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#suscription_user_selectlable',
 
  data: {
    path:window.api_path,
    cats_query:"",
    selected:false,
    related_query:null,
    next_page_cats:null,
    cats:[],
  },
  computed: {
    orderedUsers: function () {
      return _.orderBy(this.cats, 'selected','desc')
    }
  },
  methods: {
  
    list_categories : function(page)
    {


      var url = this.path+"api/user/list";
    
      this.$http.post(url, {}, {}).then(data => {

         // get body data
            data.body.data.forEach(function(value,key )
            {
              if(window.user_id == value.id)
                {
                  value.selected = true;
                }else{
                  value.selected = false;
                }
            })
          
        this.cats = data.body.data

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },

    load_more : function(page)
    {
      this.$http.post(this.next_page_cats, {}, {}).then(data => {

         // get body data
          data.body.data.forEach(function(value,key )
          {
             if(window.user_id == value.id)
              {
                value.selected = true;
              }else{
                value.selected = false;
              }
            
          })

        this.cats.push(...data.body.data)

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    set_select_item : function(item)
    {
      if(item.selected == false)
       {
          this.cats.forEach(function(item,key )
          {
            item.selected = false ;
          });

          item.selected = true;

       }else{
          item.selected = false;
       }
       
    },





    search_category : function()
    {

      if(this.cats_query.length > 0)
      {

        var url = this.path + "api/user/search";

        this.$http.post(url, {'name':this.cats_query}, {}).then(data => 
        {

          // get body data
          data.body.data.forEach(function(value,key )
          {
            if(window.user_id == value.id)
            {
              value.selected = true;
            }else{
              value.selected = false;
            }
          })

          this.cats = data.body.data

          
          this.next_page_cats = null

        }, response => {
          // error callback
        });

      }else{
        this.list_categories();
      }

    },

    
  },
 
  created: function() 
  {
    this.list_categories();
  }
})
