
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#media_module',
 
  data: {
    path:window.api_path ,
    directories:[],
    search_pressed:false,
    current_path:"/",
    dir_query:"",
    files:[],
    dirs:[],
    progresss:0,
  },
  methods: {
    string_to_slug: function (str) 
    {
      str = str.replace(/^\s+|\s+$/g, ''); // trim
      str = str.toLowerCase();
      
      // remove accents, swap ñ for n, etc
      var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;~#";
      var to   = "aaaaeeeeiiiioooouuuunc--------";
      for (var i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes

      return str;
    },
    list_files : function(path)
    {

      
      var url = this.path+"api/media/list/files";
      
      this.$http.post(url, {path:path}, {}).then(data => {
           data.body.forEach(function(value,key )
            {
               value.dir = false;
               value.old_name = value.file;
              value.editable = false;
            })
         //if(this.files.length > 0)
         {
           // this.files.push(...data.body) ;
         // }else{
            this.files = data.body;
          }
         
      }, response => {
        // error callback
      });

    },

    list_dir : function(path)
    {

      var url = this.path+"api/media/list/dir";
    
      this.$http.post(url,  {path:path}, {}).then(data => {
        data.body.forEach(function(value,key )
            {
               value.dir = true;
                 value.old_name = value.file;
              value.editable = false;
            })
          if(data.body.length > 0)
          {
            this.dirs = data.body ;
          }
          this.list_files(this.current_path);
        
      }, response => {
        // error callback
      });

    },
    make_editable:function(item)
    {
      if(item.editable == false)
      {
        item.editable = true;
      }else{
         item.editable = false;
      }
    },
    navigate:function(item)
    {
      this.current_path += item+'/';
      this.files = [];
      this.dirs = [];
      this.list_dir(this.current_path);
      
    },
    navigate_query:function()
    {
      if(this.current_path.length > 0 )
      {
        this.list_dir(this.current_path);
      
      }
    },
    move_to_parent:function()
    {
      if(this.current_path.length > 0 && this.current_path!='/')
      {
        var s = this.current_path.split('/');
        var h = "";
        for(i=0;i<=s.length-3;i++)
        {
          h+=s[i]+"/";
        }
        this.dirs = [];
        this.files = [];
        this.current_path = h;
        this.list_dir(this.current_path);
     
      }
        
    },
    save_name:function(item)
    {

        item.editable = false;
       
      var url = this.path+"api/media/file/rename";
      this.$http.post(url,  {path:this.current_path,from:item.old_name,to:this.string_to_slug(item.file)}, {}).then(data => {
        item.old_name =  this.string_to_slug(item.file);
        item.file = this.string_to_slug(item.file);
      }, response => {
        // error callback
      });
    },

     delete_file:function(item)
    {


      if(item.dir == false)
      {
        var url = this.path+"api/media/file/delete";
        this.$http.post(url,  {path:this.current_path+item.file}, {}).then(data => {
             this.files = this.files.filter(user => {
            return user.file != item.file;
          });
     
        }, response => {
        // error callback
        });
      }else{
        var url = this.path+"api/media/dir/delete";
        this.$http.post(url,  {path:this.current_path+item.file}, {}).then(data => {
        
          this.dirs = this.dirs.filter(user => {
            return user.file != item.file;
          });
     
        }, response => {
          // error callback
        });
      }
      
    },
    set_image:function(item)
    {
     
    },
    image_src:function(item)
    {
      return "/"+item.path;
    },
    create_dir : function()
    {


      var url = this.path+"api/media/dir/make";
      
      this.$http.post(url, {path:this.current_path + this.string_to_slug(this.dir_query)}, {}).then(data => {
         this.files =[];
         this.dirs =[];
        this.list_dir(this.current_path);
       
        
      }, response => {
        // error callback
      });

    },


     open_uploader: function ($e)
      {  

        const elem = this.$refs.id_files
        elem.click()
        
      },


      drop:function(e)
      {
          e.preventDefault()
          if (e.dataTransfer)
          {

              if (e.dataTransfer.files.length > 0)
               {
                this.upload(e.dataTransfer.files);
              }
          }
      },
      change:function($e)
      {

        var files = $e.target.files;
        this.upload(files);
      },
      upload: function(files) 
      {
        
        var fd = new FormData();
        fd.append('path', this.current_path);
        for(i=0;i<=files.length-1;i++)
        {
           fd.append('file', files[i]);
        }
       
        var url = this.path+"api/media/file/upload";
        var app = this;
        this.$http.post(url, fd, 
        {
          progress(e) 
          {
            if (e.lengthComputable) 
            {
              app.progresss = (e.loaded / e.total * 100);

              if(app.progresss >= 100)
              {   
                var cancel = function()
                {
                  app.progresss = 0;

                }
                setTimeout(cancel, 1000); 
                app.list_dir(app.current_path);
                                  
              }
            }
          }
        });


      },
   
    progress_status:function()
    {
      return {
        width:this.progresss+"%"
      }
    }
      
  },
  created: function() 
  {
  
   
    this.list_dir(this.current_path);
    
     
         
  }

})
