
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#product_especifications',
  data: {
    path:window.api_path ,
    espefications:[],
    espefication:{name:""},
   
    meta:{meta_key:"",meta_value:""},
  },
  methods: {

    list_especifications : function()
    {



      var url = this.path+"api/ecommerce/especifications/list";
    
      this.$http.post(url, { post_id:window.object_id,}, {}).then(data => {

         // get body data
         var that = this;
        data.body.data.forEach(function(value,key )
        {
          value.selected = false;
          value.editable = false;
          value.children = [];
           that.list_meta_key(value,1)
        })
          
        this.espefications = data.body.data

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    load_more : function(page)
    {
      this.$http.post(this.next_page_cats, {}, {}).then(data => {

         // get body data
         var that = this;
         data.body.data.forEach(function(value,key )
            {
              value.selected = false;
              value.editable = false;
              value.children = [];
              that.list_meta_key(value,1)
            })

        
            this.espefications .forEach(function(value0,key )
            {
             
                data.body.forEach(function(value,key )
                {
                    if(value0 == value.id)
                    {
                      value.selected = true;
                    }
                  
                })

            })



        this.espefications.push(...data.body)

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    set_select_item : function(item)
    {
      if(item.selected == false)
       {
          item.selected = true;

       }else{
          item.selected = false;
       }
       
    },
    set_editable_item : function(item)
    {
      if(item.editable == false)
       {
          item.editable = true;

       }else{
          item.editable = false;
       }
       
    },
    store_meta_key : function(espeification)
    {


      var url = this.path+"api/ecommerce/meta/store";
    
      this.$http.post(url, {
        especification_id:espeification.id,
        meta_key:this.meta.meta_key,
        meta_value:this.meta.meta_value
      }, {}).then(data => {

         // get body data
      
            value = data.body;
            value.selected = false;
            value.editable = false;
          
          
        espeification.children.push(value);

        
        //this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    update_meta_value : function(item)
    {

      var url = this.path+"api/ecommerce/meta/update";
    
      this.$http.post(url, {
        meta_key:item.meta_key,
        meta_value:item.meta_value,
        id:item.id
      }, {}).then(data => {
        // get body data
        //this.next_page_cats = data.body.next_page_url
        item.editable = false;
      }, response => {
        // error callback
      });

    },
    delete_meta_value : function(item,espefications)
    {

      var url = this.path+"api/ecommerce/meta/delete";
    
      this.$http.post(url, {
        id:item.id
      }, {}).then(data => {

        // get body data
        //this.next_page_cats = data.body.next_page_url

        espefications.children = espefications.children.filter(user => {
          return user.id != item.id;
        });
      }, response => {
        // error callback
      });

    },
    store_especifications : function()
    {
      var url = this.path+"api/ecommerce/especifications/store";
    
      this.$http.post(url, {
        post_id:window.object_id,
        name:this.espefication.name
      }, {}).then(data => {

          // get body data
           var value = data.body;
            value.selected = false;
            value.editable = false;
            value.children = []
          
        this.espefications.push(value);

        
        //this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    update_especifications : function(item)
    {

      var url = this.path+"api/ecommerce/especifications/update";
    
      this.$http.post(url, {
        name:item.name,
        id:item.id
      }, {}).then(data => {

        // get body data
        //this.next_page_cats = data.body.next_page_url
         item.editable = false;
      }, response => {
        // error callback
      });

    },
    delete_especifications : function(item)
    {

      var url = this.path+"api/ecommerce/especifications/delete";
    
      this.$http.post(url, {
        id:item.id
      }, {}).then(data => {

        // get body data
        //this.next_page_cats = data.body.next_page_url

        this.espefications = this.espefications.filter(user => {
          return user.id != item.id;
        });
      }, response => {
        // error callback
      });

    },
    list_meta_key : function(espeification)
    {


      var url = this.path+"api/ecommerce/meta/list";
    
      this.$http.post(url, {especification_id:espeification.id}, {}).then(data => {

        // get body data
        data.body.data.forEach(function(value,key )
        {
          value.selected = false;
           value.editable = false;
        })

        espeification.children = data.body.data;
        //this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    search_category : function()
    {

      if(this.cats_query.length > 0)
      {

        var url = this.path + "api/tags/search";

        this.$http.post(url, {'name':this.cats_query}, {}).then(data => 
        {

           // get body data
            data.body.data.forEach(function(value,key )
            {
              value.selected = false;
            })

        
            this.related_ids .forEach(function(value0,key )
            {
             
                data.body.data.forEach(function(value,key )
                {
                    if(value0 == value.id)
                    {
                      value.selected = true;
                    }
                  
                })

            })

          this.cats = data.body.data

          
          this.next_page_cats = null

        }, response => {
          // error callback
        });

      }else{

         this.list_especifications();
      }

    },
  },
 
  created: function() 
  {
    this.list_especifications();


       
  }
})
