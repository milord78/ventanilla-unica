
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#navigations_modal_module',
 
  data: {
    path:window.api_path,
    current_path:"/",
    items:[],
    modules:['articles','car'],
    actions:['list','details'],
    module:"",
    action:"",
    cats_query:"",
    modal:false,
    next_page_tag:null,
    next_page_cats:null,
    next_page_related:null,
    selected:{title:"",id:0},
    refe:"",
  },
  methods: {
    get_active_class:function()
    {
      if(this.modal == true)
      {
        return 'modal display in showmodal'
      }else{
        return 'hide'
      }
      
    },
    initializE:function()
    {
      this.module=window.modulex;
      this.action=window.action;
    },
    close_modal:function()
    {
      this.modal = false;
    },
    open_modal:function(current)
    {
      this.refe = current
      this.modal = true;
    },
    list_categories : function(path)
    {
      var url = this.path+"api/cms/pages/list";
      var app = this;
      this.$http.post(url, {path:path}, {}).then(data => {
          data.body.data.forEach(function(value,key )
          {
            if(window.item_id == value.id)
            {
              value.selected = true;
              app.selected = value;
            }else{
              value.selected = false;
            }
          });

          this.items = data.body.data;
        
        }, response => {});

    },
    search_category : function()
    {

      if(this.cats_query.length > 0)
      {

        var url = this.path + "api/cms/pages/search";
        var app = this;
        this.$http.post(url, {'name':this.cats_query}, {}).then(data => 
        {

          // get body data
          data.body.data.forEach(function(value,key )
          {
            if(window.item_id == value.id)
            {
              value.selected = true;
              app.selected = value;
            }else{
              value.selected = false;
            }
          })

          this.items = data.body.data

          
          this.next_page_cats = null

        }, response => {
          // error callback
        });

      }else{
        this.list_categories();
      }

    },
    set_select_item:function(item)
    {
      if(item.selected == false)
      {
        this.items.forEach(function(value,key ){
          value.selected = false;
        });
        item.selected = true;
        this.selected = item;
      }else{
        item.selected = false;
      }
    }
  },
  created: function() 
  {
    this.initializE();
    this.list_categories();
  }

})
