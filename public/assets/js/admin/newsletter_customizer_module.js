
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#newsletter-customizer',
 
  data: {
    path:window.api_path,
    action:0,

    items:[]
  },
  methods: {

    addItemText()
    {
      var id = "item" + new Date().getTime()
      this.items.push( Object.assign({}, {action:1,link:"",content:"",id:id,editor: Object.assign({}, window.tinymce)}))
      var that = this;
      function varitem(){
        var item = that.items.filter(itemx => {
           return itemx.id == id
          });
        item[0].editor.init({
          selector: '#'+id,
          height: 600
        })
       
      }
      setTimeout(varitem,500)
     
    },
     addItemLink()
    {
      var id = "item" + new Date().getTime()
      this.items.push( Object.assign({},{action:2,link:"",content:"",id:id}))

    
     
    },
     addItemImage()
    {
      var id = "item" + new Date().getTime()
      this.items.push( Object.assign({},{action:4,link:"",content:"",id:id}))

    
     
    },
   addItemAttachment()
    {
      var id = "item" + new Date().getTime()
      this.items.push( Object.assign({},{action:3,attachment:"",id:id}))

    
     
    },
    
    saveItemText(item)
    {
      item.content = item.editor.activeEditor.getContent()
      
    },
    deleteItem(item)
    {
      this.items = this.items.filter(itemx => {
        return itemx.id != item.id
      });
    }
    
    
  },
 
  created: function() 
  {
    
       
  }
})
