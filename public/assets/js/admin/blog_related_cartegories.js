
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#categories_selectlable',
 
  data: {
    path:window.api_path,
    cats_query:"",
    related_ids:[],
    selected:false,
    related_query:null,
    next_page_tag:null,
    next_page_cats:null,
    next_page_related:null,
    cats:[],


  },
  methods: {

    list_categories : function(page)
    {


      var url = this.path+"api/blog/categories/list";
    
      this.$http.post(url, {}, {}).then(data => {

         // get body data
             data.body.data.forEach(function(value,key )
            {
              value.selected = false;
            })

        
            this.related_ids .forEach(function(value0,key )
            {
             
                data.body.data.forEach(function(value,key )
                {
                    if(value0 == value.id)
                    {
                      value.selected = true;
                    }
                  
                })

            })

       
        
          
        this.cats = data.body.data

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },

    load_more : function(page)
    {
      this.$http.post(this.next_page_cats, {}, {}).then(data => {

         // get body data
         data.body.data.forEach(function(value,key )
            {
              value.selected = false;
            })

        
            this.related_ids .forEach(function(value0,key )
            {
             
                data.body.data.forEach(function(value,key )
                {
                    if(value0 == value.id)
                    {
                      value.selected = true;
                    }
                  
                })

            })



        this.cats.push(...data.body.data)

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    set_select_item : function(item)
    {
      if(item.selected == false)
       {
          item.selected = true;

       }else{
          item.selected = false;
       }
       
    },

    set_select_all: function()
    {

        if(this.selected==true)
        {
          this.selected = false;
           this.cats.forEach(function(item,key )
        {
            item.selected = false ;
          
          
        })
        }else{
          this.selected = true;
           this.cats.forEach(function(item,key )
        {
            item.selected = true ;
        })
        }
       
    },


    get_related : function(callback)
    {


        var url = this.path + "api/blog/categories/get_related";
        id = window.location.href.split('/')
        id = id[id.length-1]
        if(isNaN(id))
        {
          id = 0;
        }
        this.$http.post(url, {id:id}, {}).then(data => {

          this.related_ids = data.body;
          callback();
         }, response => { });
        

    },

    search_category : function()
    {

      if(this.cats_query.length > 0)
      {

        var url = this.path + "api/blog/categories/search";

        this.$http.post(url, {'name':this.cats_query}, {}).then(data => 
        {

           // get body data
            data.body.data.forEach(function(value,key )
            {
              value.selected = false;
            })

        
            this.related_ids .forEach(function(value0,key )
            {
             
                data.body.data.forEach(function(value,key )
                {
                    if(value0 == value.id)
                    {
                      value.selected = true;
                    }
                  
                })

            })

          this.cats = data.body.data

          
          this.next_page_cats = null

        }, response => {
          // error callback
        });

      }else{

         this.list_categories();
      }

    },

    
  },
 
  created: function() 
  {
    this.get_related(this.list_categories);
   ;

       
  }
})
