
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#navigations_modal_module_parnet_child',
 
  data: {
    path:window.api_path,
    current_path:"/",
    items:[],
    cats_query:"",
    modal:false,
    next_page_tag:null,
    next_page_cats:null,
    next_page_related:null,
    selected:{title:"",id:0},
    refe:"",
  },
  methods: {
    get_active_class:function()
    {
      if(this.modal == true)
      {
        return 'modal display in showmodal'
      }else{
        return 'hide'
      }
      
    },
    close_modal:function()
    {
      this.modal = false;
    },
    open_modal:function(current)
    {
      this.refe = current
      this.modal = true;
    },
    list_categories : function()
    {
      var url = this.path+"api/cms/navigations/list";
      
      this.$http.post(url, {exclude:window.object_id}, {}).then(data => {
          var app = this;
          data.body.data.forEach(function(value,key )
          {
            if(window.parent_id == value.id)
            {
              value.selected = true;
              value.child = [];
              app.selected = value;
            }else{
              value.selected = false;
            }
          });

          this.items = data.body.data.filter(user => {
            return user.id != window.object_id;
          });
        
        }, response => {});

    },

    list_child : function(item)
    {

      var url = this.path+"api/cms/navigations/child";
      
      this.$http.post(url, {parent_id:item.id}, {}).then(data => {
          var app = this;
          data.body.data.forEach(function(value,key )
          {
            if(window.parent_id == value.id)
            {
              value.selected = true;
              value.child = [];
              app.selected = value;
            }else{
              value.selected = false;
            }
          });

          item.child = data.body.data;
        
        }, response => {});

    },


    search_category : function()
    {

      if(this.cats_query.length > 0)
      {

        var url = this.path + "api/cms/navigations/search";

        this.$http.post(url, {'name':this.cats_query,exclude:window.object_id,}, {}).then(data => 
        {

          // get body data
          var app = this;
          data.body.data.forEach(function(value,key )
          {
            if(window.parent_id == value.id)
            {
              value.selected = true;
              value.child = [];
              app.selected = value;
            }else{
              value.selected = false;
            }
          })

          this.items = data.body.data.filter(user => {
            return user.id != window.object_id;
          });

          
          this.next_page_cats = null

        }, response => {
          // error callback
        });

      }else{
        this.list_categories();
      }

    },
    set_select_item:function(item)
    {
      if(item.selected == false)
      {
        this.items.forEach(function(value,key )
        {
          value.selected = false;
        });
        item.selected = true;
        this.selected = item;
      }else{
        item.selected = false;
      }
    }
  },
  created: function() 
  {
    this.list_categories();
  }

})
