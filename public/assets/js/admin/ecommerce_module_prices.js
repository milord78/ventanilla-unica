
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#product_prices',
  data: {
    path:window.api_path ,
    prices:[],
    companies:[],
     values:{
      payment_methods:[
        'Visa',
        'MasterCard',
        'Paypal',
      ],
      countries:[
        {name:"All",value:"Todas"},
        {name:"ES",value:"España"},
        {name:"AR",value:"Argentina"},
        {name:"BO",value:"Bolivia"},
        {name:"BR",value:"Brasil"},
        {name:"CH",value:"Chile"},
        {name:"CO",value:"Colombia"},
        {name:"CR",value:"Costa Rica"},
        {name:"CU",value:"Cuba"},
        {name:"EC",value:"Ecuador"},
        {name:"SA",value:"El salvador"},
        {name:"GT",value:"Guatemala"},
        {name:"HN",value:"Honduras"},
        {name:"MX",value:"México"},
        {name:"NI",value:"Nicaragua"},
        {name:"PA",value:"Panamá"},
        {name:"PY",value:"Paraguay"},
        {name:"PE",value:"Perú"},
        {name:"DO",value:"República Dominicana"},
        {name:"UY",value:"Uruguay"},
        {name:"VE",value:"Venezuela"}
         
      ],
    },
    price:{
      title:"",
      post_id:window.object_id,
      company_id:0,
      price:0.0,
      delivery:"",
      payments_methods:"",
      country_code:"",
      affiliate_link:"",
      free_shipping:""

    },
    meta:{meta_key:"",meta_value:""},
    actions:{add:false,update:false},
  },
  methods: {
    cancel_store: function()
    {
      this.actions.add = false;
    },
    new_item: function()
    {
      this.actions.add = true;
    },
    cancel_update: function()
    {
      this.actions.update = false;
    },
    new_update: function()
    {
      this.actions.update = true;
    },
    list_prices : function(page)
    {
      var url = this.path+"api/ecommerce/prices/list";
    
      this.$http.post(url, { post_id:window.object_id,}, {}).then(data => {

         // get body data
        data.body.data.forEach(function(value,key )
        {
          value.selected = false;
          value.editable = false;
          value.payments_methods = value.payments_methods.split(',')
          value.country_code = value.country_code.split(',')
        })
          
        this.prices = data.body.data

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },

    list_companies : function()
    {



      var url = this.path+"api/ecommerce/company/list";
    
      this.$http.post(url, {}, {}).then(data => {

         // get body data
        data.body.data.forEach(function(value,key )
        {
          value.selected = false;
          value.editable = false;

        })
          
        this.companies = data.body.data

        
        //this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    load_more : function(page)
    {
      this.$http.post(this.next_page_cats, {}, {}).then(data => {

         // get body data
         data.body.data.forEach(function(value,key )
            {
              value.selected = false;
              value.editable = false;
              value.children = [];
            })

        
            this.espefications .forEach(function(value0,key )
            {
             
                data.body.data.forEach(function(value,key )
                {
                    if(value0 == value.id)
                    {
                      value.selected = true;
                    }
                  
                })

            })



        this.prices.push(...data.body.data)

        
        this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    set_select_item : function(item)
    {
      if(item.selected == false)
       {
          item.selected = true;

       }else{
          item.selected = false;
       }
       
    },
    set_editable_item : function(item)
    {
      if(item.editable == false)
       {
          item.editable = true;
          this.actions.update = true;

       }else{
          item.editable = false;
          this.actions.update = false;
       }
       
    },
   
    store_price : function()
    {
      var url = this.path+"api/ecommerce/prices/store";
      this.price.payments_methods = this.price.payments_methods.join(',')
      this.price.country_code =  this.price.country_code.join(',')
      this.$http.post(url, this.price, {}).then(data => {

          // get body data
         var value =  data.body
        
            value.selected = false;
            value.editable = false;
            
          var value = data.body;
          value.selected = false;
          value.editable = false;
          value.payments_methods = value.payments_methods.split(',')
          value.country_code = value.country_code.split(',')
        this.prices.push(value);

        
        //this.next_page_cats = data.body.next_page_url

      }, response => {
        // error callback
      });

    },
    update_price : function(item)
    {

      var url = this.path+"api/ecommerce/prices/update";
    
      this.$http.post(url, {
        id:item.id,
        title:item.title,
        company_id:item.company_id,
        price:item.price,
        delivery:item.delivery,
        payments_methods:item.payments_methods.join(','),
        country_code:item.country_code.join(','),
        affiliate_link:item.affiliate_link,
        free_shipping:item.free_shipping
      }, {}).then(data => {

        // get body data
        //this.next_page_cats = data.body.next_page_url
         item.editable = false;
         this.actions.update = false;
      }, response => {
        // error callback
      });

    },
    delete_price : function(item)
    {

      var url = this.path+"api/ecommerce/prices/delete";
    
      this.$http.post(url, {
        id:item.id
      }, {}).then(data => {

        // get body data
        //this.next_page_cats = data.body.next_page_url

        this.prices = this.prices.filter(user => {
          return user.id != item.id;
        });
         this.actions.update = false;
      }, response => {
        // error callback
      });

    },
  
    search_category : function()
    {

      if(this.cats_query.length > 0)
      {

        var url = this.path + "api/tags/search";

        this.$http.post(url, {'name':this.cats_query}, {}).then(data => 
        {

           // get body data
            data.body.data.forEach(function(value,key )
            {
              value.selected = false;
            })

        
            this.related_ids .forEach(function(value0,key )
            {
             
                data.body.data.forEach(function(value,key )
                {
                    if(value0 == value.id)
                    {
                      value.selected = true;
                    }
                  
                })

            })

          this.cats = data.body.data

          
          this.next_page_cats = null

        }, response => {
          // error callback
        });

      }else{

         this.list_prices();
      }

    },
  },
 
  created: function() 
  {
    this.list_companies();
   this.list_prices(1);
       
  }
})
