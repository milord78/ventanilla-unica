(function($) 
{
  	$(function()
  	{
    	var winload = function()
		{
			var show_social_nav = function (e)
			{		
				e.preventDefault();
				var root = $(this).parents().find(".social");
				var children = $(this).find('i');

				if (children.hasClass("fa-caret-right"))
				{
					children.removeClass("fa-caret-right");
					children.addClass("fa-caret-down");
					root.find(".list").css("display","block");
				}
				else
				{
					if (children.hasClass("fa-caret-down"))
					{
						children.removeClass("fa-caret-down");
						children.addClass("fa-caret-right");
						root.find(".list").css("display","none");
					}
				}
			}

			$('.social .inner .arrow.Down').on('click', show_social_nav);
		} 

		$(window).on('load',winload);
  	});
})(jQuery);
