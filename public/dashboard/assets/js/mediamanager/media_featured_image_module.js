
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#media_featured_module',
 
  data: {
    path:window.api_path ,
    directories:[],
    search_pressed:false,
    current_path:"/",
    dir_query:"",
    featured_image:"/"+window.featured_image,
    files:[],
    dirs:[],
    editable:true,
    progresss:0,
  },
  methods: {
    string_to_slug: function (str) 
    {
      str = str.replace(/^\s+|\s+$/g, ''); // trim
      str = str.toLowerCase();
      
      // remove accents, swap ñ for n, etc
      var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;~#";
      var to   = "aaaaeeeeiiiioooouuuunc--------";
      for (var i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes

      return str;
    },
    list_files : function()
    {

      var url = this.path+"api/media/list/files";
      
      this.$http.post(url, {path:path}, {}).then(data => {
           data.body.forEach(function(value,key )
            {
               value.dir = false;
               value.old_name = value.file;
              value.editable = false;
            })
         //if(this.files.length > 0)
         {
           // this.files.push(...data.body) ;
         // }else{
            this.files = data.body;
          }
         
      }, response => {
        // error callback
      });

    },


    make_editable:function(status)
    {
      this.editable = status
    },



    image_src:function(item)
    {
      return "/"+item.file;
    },



     open_uploader: function ($e)
      {  

        const elem = this.$refs.id_files
        elem.click()
        
      },


      drop:function(e)
      {
          e.preventDefault()
          if (e.dataTransfer)
          {

              if (e.dataTransfer.files.length > 0)
               {
                this.upload(e.dataTransfer.files);
              }
          }
      },
      change:function($e)
      {

        var files = $e.target.files;
        this.upload(files);
      },
      upload: function(files) 
      {
        
        var fd = new FormData();
        fd.append('id', window.object_id);
        for(i=0;i<=files.length-1;i++)
        {
           fd.append('attacthment', files[i]);
        }

        
       
        var url = this.path+"api/ecommerce/articles/images/upload/featured";
        var app = this;
        this.$http.post(url, fd, 
        {
          progress(e) 
          {
            if (e.lengthComputable) 
            {
              app.progresss = (e.loaded / e.total * 100);

              if(app.progresss >= 100)
              {   
                var cancel = function()
                {
                  app.progresss = 0;
                  app.editable = false;


                }
                setTimeout(cancel, 1000); 
               
                                  
              }
            }
          }
        }).then(data => {

          app.featured_image = "/"+data.body.featured_image;
        }, response => {
          // error callback
        });
   


      },
   
   
    progress_status:function()
    {
      return {
        width:this.progresss+"%"
      }
    }
      
  },
  created: function() 
  {
  
   
     //this.list_dir(this.current_path);
    
     if(this.featured_image.length>0)
     {
       this.editable = false;
     }
         
         
  }

})
