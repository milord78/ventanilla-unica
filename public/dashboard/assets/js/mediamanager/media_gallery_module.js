
Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

 request.headers.set('X-XSRF-TOKEN', $.cookie('XSRF-TOKEN'));
});
var app = new Vue({
  el: '#media_gallery_module',
 
  data: {
    path:window.api_path ,
    directories:[],
    search_pressed:false,
    current_path:"/",
    dir_query:"",
    files:[],
    dirs:[],
    progresss:0,
  },
  methods: {
    string_to_slug: function (str) 
    {
      str = str.replace(/^\s+|\s+$/g, ''); // trim
      str = str.toLowerCase();
      
      // remove accents, swap ñ for n, etc
      var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;~#";
      var to   = "aaaaeeeeiiiioooouuuunc--------";
      for (var i=0, l=from.length ; i<l ; i++) {
          str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
      }

      str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
          .replace(/\s+/g, '-') // collapse whitespace and replace by -
          .replace(/-+/g, '-'); // collapse dashes

      return str;
    },
    list_files : function()
    {

      var url = this.path+"api/ecommerce/articles/images/list";
      
      this.$http.post(url, {id:window.object_id}, {}).then(data => {
           data.body.forEach(function(value,key )
            {
               value.dir = false;
               value.old_name = value.image;
              value.editable = false;
            })
      
            this.files = data.body;
         
      }, response => {
        // error callback
      });

    },


    make_editable:function(item)
    {
      if(item.editable == false)
      {
        item.editable = true;
      }else{
         item.editable = false;
      }
    },

    itemStyle:function(item)
    {
      return "margin:0 auto;margin-bottom:10px; height:70px;width:70px;background:url(/"+item.image+");background-size:cover;background-color:#333;"
    },


     delete_file:function(item)
    {


        var url = this.path+"api/ecommerce/articles/images/delete";
        this.$http.post(url,  {id:item.id,post_id: window.object_id}, {}).then(data => {
             this.files = this.files.filter(user => {
            return user.id != item.id;
          });
     
        }, response => {
        // error callback
        });
      
      
    },


     open_uploader: function ($e)
      {  

        const elem = this.$refs.id_files
        elem.click()
        
      },


      drop:function(e)
      {
          e.preventDefault()
          if (e.dataTransfer)
          {

              if (e.dataTransfer.files.length > 0)
               {
                this.upload(e.dataTransfer.files);
              }
          }
      },
      change:function($e)
      {

        var files = $e.target.files;
        this.upload(files);
      },
      upload: function(files) 
      {
        
        var fd = new FormData();
        fd.append('id', window.object_id);
        var a = 0;
        for(i=0;i<=files.length-1;i++)
        {
           fd.append('attacthment['+i+']', files[i]);
           a++;
        }
       
        var url = this.path+"api/ecommerce/articles/images/upload/image";
        var app = this;
        this.$http.post(url, fd, 
        {
          progress(e) 
          {
            if (e.lengthComputable) 
            {
              app.progresss = (e.loaded / e.total * 100);

              if(app.progresss >= 100)
              {   
                var cancel = function()
                {
                  app.progresss = 0;
                  app.list_files();
                }
                setTimeout(cancel, 1000); 
               
                                  
              }
            }
          }
        });


      },
   
    progress_status:function()
    {
      return {
        width:this.progresss+"%"
      }
    }
      
  },
  created: function() 
  {
  
    this.list_files();
     
         
  }

})
