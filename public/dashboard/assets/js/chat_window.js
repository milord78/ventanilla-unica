Vue.use(VueResource);

Vue.http.interceptors.push(function(request) {

  // modify method
  //request.method = 'POST';

  // modify headers

  request.headers.set('X-CSRFToken', $.cookie('csrftoken'));


});
var vueapp = new Vue({
  el: '#chat_window',
  data: {
    email:'',
    message:'',
    messages:[],
    threads:[],
    window:[],
    cart_item:{},
    closed:false,
    email_setted:false,
    agent_setted:true,
    thread_id:0,
    article_id:0,
    invalid_email_setted:false,
    minimized:false,
    interval:{},
    api_url: window.api_path,
    thread_url:'api/messages/thread',
    message_url:'api/messages/send/text',
    message_last_url:'api/messages/last',
    message_list:'api/messages/list',
    message_sales:'api/messages/sales'
  },
  methods: {
    validateEmail($email) 
    {
     var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
     if (!emailReg.test($email)) {
          return 0;
     } else {
        return 1;
     }
    },
    is_me(message) 
    {
     if(message.is_me == true)
     {
       return 'message left appeared';
     }else{

       return 'message right appeared';
     }
    },




    reverseMessage: function () {
      this.messages = this.messages.split('').reverse().join('')
    },
    createMessage:function()
    {

      this.$http.post(
        this.api_url+this.message_url, 
        {
          'message':this.message,
          'thread_id':this.thread_id,
        }, 
        {}
      ).then(data => {
        this.message = null;
      }, response => {/* error callback*/ });
    },
    setClosed: function () {
      this.closed = true;
    },
    setClosed: function () {
      this.minimized = true;
    },
    setThreadId: function (item) 
    {
      
      this.threads.forEach(function(value,key )
      {
        value.active = false;
      })
      item.active = true;
      this.thread_id = item.id;
      this.messages_list();
    },
    setEmailSetted: function () 
    {

      if(this.validateEmail(this.email)==true)
      {
        this.email_setted = true;
        this.invalid_email_setted = false;
        this.createThread();
      }else
      {
        this.invalid_email_setted = true;
      }
     
    },
    PushMEssage:function()
    {
      this.createMessage();
    },
    sales_list :function()
    {

        var that = this;
        this.$http.post(this.api_url+this.message_sales, {}, {}).then(data => {

          // get body data
          data.body.forEach(function(value,key )
          {
            var item2 = Object.assign({}, value);
            item2.active = false;
            var exist_message = that.threads.filter(item => {
              return item.id ==value. id;
            });
            if(exist_message.length==0)
            {
              that.threads.push(item2);
            }
            if(key==0)
            {
              that.setThreadId(that.threads[0]);
            }
          });

          if(typeof that.interval ==='function')
          {
            clearInterval(that.interval );
          }
          
          //that.interval = setInterval(that.sales_list, 3000);
        }, 
        response => {});
   
      
    },
  
    messages_list :function()
    {
      if(this.thread_id!=0)
      {

        var that = this;
        this.messages = [];
        this.$http.post(this.api_url+this.message_list, {'thread_id':this.thread_id}, {}).then(data => {

          if(typeof that.interval ==='function')
          {
            clearInterval(that.interval );
          }
          // get body data
          if( data.body.result!=false)
          {
            this.messages = data.body;
            that.interval = setInterval(that.messages_last,3000);
          }
         
         
        // 
          
        }, 
        response => {
          // error callback
        });
      }

      
    },
    messages_last :function()
    {
      var that = this;
      if(this.thread_id!=0)
      {

        
        this.$http.post(this.api_url+this.message_last_url, {'thread_id':this.thread_id}, {}).then(data => {

          // get body data
          var id = data.body.id
          if( data.body.result!=false)
          {
            var exist_message = that.messages.filter(item => {
              return item.id == id;
            });
           // console.log(exist_message.length)
            if( exist_message.length == 0 )
            {
            
            
              that.messages.push(data.body);
             
              
            }
            
          }
         
          
        }, 
        response => {
          // error callback
        });
      }

      
    },
  },
    created: function() 
    {

      
      this.sales_list();
    
    	
     	
    }

 
})
