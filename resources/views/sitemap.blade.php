<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    <url>
        <loc>{{ url('/') }}</loc>
        <changefreq>Daily</changefreq>
        <priority>1</priority>
    </url>
    @foreach($categories  as $category => $cat)
    <url>
        <loc>{{ url('/') }}{{route('article_category',['slug'=>$cat['slug']])}}</loc>
        <lastmod>{{date('Y-m-d', strtotime($cat['created_at']))}}</lastmod>        
        <changefreq>weekly</changefreq>
        <priority>0.4</priority>
    </url>
    @endforeach  

      
    @foreach($posts  as $post => $item)
     
    <url>
        <loc>{{ url('/') }}/{{route('article_details',['slug'=>$item['slug']])}}</loc>
        <lastmod>{{date('Y-m-d', strtotime($item['created_at']))}}</lastmod>       
        <changefreq>Monthly</changefreq>
        <priority>0.2</priority>
    </url>
   
    @endforeach
    @foreach($pages  as $page => $item)
    <url>
        <loc>{{ current_domain }}/{{route('page_details',['slug'=>$item['slug']])}}</loc>
        <lastmod>{{date('Y-m-d', strtotime($item['created_at']))}}</lastmod>       
        <changefreq>Monthly</changefreq>
        <priority>0.2</priority>
    </url>
   
    @endforeach
</urlset>