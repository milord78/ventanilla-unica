@extends('layouts.app')

@section('content')

<div style="width:700px;margin:0 auto;padding-top:45px"  class="white login-forn text-left">
    <div class="login padding-45 ">
        <div class="row  clearfix">
            
            <div class="col-md-12">
                <div class="nav-tabs-custom registration" >
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs">
                    <li  class="active" >
                        <a class="active" href="#basic" data-toggle="tab" aria-expanded="true">
                            COMENZAR REGISTRO
                        </a>
                      </li>
                      <li>
                        <a href="#seo" data-toggle="tab" aria-expanded="false">
                            CONTINUAR REGISTRO
                        </a>
                      </li>
                    
                    </ul>
                    <div class="tab-content no-padding" >
                        <div id="basic" class="tab-pane active">
                            <div class="text-center padding-25"  style="margin: 15px">
                        
                                <br>
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-group has-feedback">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="{{ __('Nombre') }}">
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus placeholder="{{ __('Apellido') }}">
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        @if ($errors->has('lastname'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('lastname') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input id="phone"  maxlength="10"  type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required autofocus placeholder="{{ __('Teléfono') }}">
                                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>

                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ __('Dirección de correo') }}">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Contraseña') }}">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="{{ __('Confirmar contraseña') }}">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-block" >
                                         Registrárme como Aspirante 
                                     </button>

                                </form>
                       
                               
                            </div>
                        </div>
                        <div id="seo" class="tab-pane ">
                            <div class="text-center padding-25"  style="margin: 15px">
                  
                                <br>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group has-feedback">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ __('Dirección de correo') }}">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Contraseña') }}">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                               
                                    <div class="form-group text-left">
                                    
                                            <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Recordar') }}
                                            </label>
                                    
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block" >
                                    CONTINUAR CON MI REGISTRO
                                    </button>
                                </form>
                           
                            </div>
                        </div>
                    </div>
                </div>
                <?php $settigns = get_settings(); ?>
                <p class="padding-10">Registrandote en este sitio, usted acepta nuestros términos de uso
                    y <a class="bold" target="blank" href="{{$settigns->aviso_de_privacidad}}"> Aviso de privacidad</a></p>
            </div>
           
        </div>

    </div>

</div>


@endsection
@section('scripts')


@endsection