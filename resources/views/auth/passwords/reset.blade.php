@extends('layouts.app')

@section('content')
<div class="container background">
    <div class="login-box" style="padding: 15px;">

     
      <!-- /.login-logo -->
      <div class="login-box-body">
        <h3 class="login-box-msg">{{ __('Resetear Password') }}</h3>

        <form method="POST" action="{{ route('password.request') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group has-feedback">
                <label for="email" class="col-form-label text-md-right">{{ __('Dirección de correo') }}</label>
              
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              
            </div>

            <div class="form-group has-feedback">
                    
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Contraseña') }}">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
               
            </div>

            <div class="form-group has-feedback"">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="{{ __('Confirmar Contraseña') }}">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('Resetear Contraceña') }}</button>
                    <br>
                    <a  class="btn btn-primary btn-block btn-flat" href="{{ route('login') }}">
                        {{ __('Login') }}
                    </a><br>
                    <a  class="btn btn-primary btn-block btn-flat" href="{{ route('register') }}" class="text-center">{{ __('REGISTRARSE') }}</a>
                </div>
                <!-- /.col -->
            </div>
        </form>


      

      </div>
      <!-- /.login-box-body -->
    </div>
</div>
@endsection
@section('scripts')
@endsection

