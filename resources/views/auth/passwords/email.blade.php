@extends('layouts.app')

@section('content')
<div class="container background">
    <div class="login-box" style="padding: 15px;">

      <!-- /.login-logo -->
      <div class="login-box-body">
        <h3 class="login-box-msg">
            {{ __('Resetear Contraseña') }}
        </h3>
        @if (session('status'))
            <div class="login-box-msg">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.request') }}">
            @csrf
            <div class="form-group has-feedback">
               
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="{{ __('Dirección de email') }}">

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              
            </div>

            <div class="row">
                <!-- /.col -->
                <div class="col-md-12">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">  {{ __('Enviar confirmacion por correo') }}</button>
                    <br>
                    <a  class="btn btn-primary btn-block btn-flat" href="{{ route('login') }}">
                        {{ __('Login') }}
                    </a><br>
                    <a  class="btn btn-primary btn-block btn-flat" href="{{ route('register') }}" class="text-center">{{ __('REGITRARSE') }}</a>
                </div>

                <!-- /.col -->
            </div>
        </form>


       

      </div>
      <!-- /.login-box-body -->
    </div>
</div>
@endsection
@section('scripts')
@endsection
