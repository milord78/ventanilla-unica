@extends('layouts.backend')

@section('content')


<div class="container post">

    <h3>
        {{ __('general.404_ERROR_TITLE_LABEL') }}
    </h3>
    <p>
      {{ __('general.404_ERROR_MESSAGE_LABEL') }}
    </p>
</div>
@endsection
