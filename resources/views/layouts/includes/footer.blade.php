<div class="footer-clean">
    <footer>
        <div class="container">
        <?php $settigns = get_settings(); ?>
            {{$settigns->direccion_postal}} - {{$settigns->telefono}} - {{$settigns->telefono_movil}} - 
            <a href="{{$settigns->website}}">Visitar Sitio web</a> - 
            {{$settigns->email}} -
            @if($settigns->fan_page_facebook)
            <a href="{{$settigns->fan_page_facebook}}">Facebook</a>
            @endif
        </div>
    </footer>
</div>