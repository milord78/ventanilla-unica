<div class="col-md-3 ">
    <article class="card">
    	<header class="panel">
            <h4 class="title"> {{ __('blog.categories.CATEGORIES_LABEL') }}</h4>
        </header>
    	
        <div class="margin-15">

        	 @foreach(list_blog_categories()  as $index => $item)

        	 <div class="margin-2">
                <a href="{{route('blog_category_details_slug',['slug'=>$item['slug']])}}" class="btn btn-primary small" type="button">
                    {{$item['name']}}
                </a>
            </div>
			 @endforeach    
           
        </div>
    </article>
</div>