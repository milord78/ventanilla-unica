<div class="col-md-12 ">
    <article class="card white">
        <header class="panel">
            <h4 class="title">  
                Panel del aspirante 
                <i class="fa fa-cog"></i>
            </h4>
        </header>

        <div class="margin-15">

            <div class="margin-2">
                <div class="box">
                        
                        <ul>
                            <li>
                                <a href="{{ route('account_users_me') }}"  type="button">
                                    {{ __('general.EDIT_PROFILE') }}    del aspirante 
                                </a>
                            </li>
                            @if(count(count_inscripciones( \Auth::user()->id))>0)
                            <li>
                                <a href="{{ route('frontend_alunnos_index') }}"  type="button">
                                    Inscripciones
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('frontend_plantel_index') }}"  type="button">
                                    Informacion Planteles
                                </a>
                            </li>
                            @endif
                        
                            
                         
                        </ul>
                    </div>


                
                
                
            </div>
    
        </div>
    </article>
</div>