<div class="app-header">
    <ul class="app-header-buttons">
        <li class="visible-mobile">
          <a href="#" class="btn btn-link btn-icon" data-sidebar-toggle=".app-sidebar.dir-left">
            <span class="icon-menu"></span>
          </a>
        </li>
        <li class="hidden-mobile">
          <a href="#" class="btn btn-link btn-icon" data-sidebar-minimize=".app-sidebar.dir-left">
            <span class="icon-list4"></span>
          </a>
        </li>
    </ul>

    <ul class="app-header-buttons pull-right">
        <li>
            <div class="contact contact-rounded contact-bordered contact-lg contact-ps-controls">
              
         
                <img src="{{Auth::user()->logos()}}"  style="height: 40px;" class="img-responsive">
              
                <div class="contact-container">
                  <div>&nbsp;</div>
                 
                 
                </div>
                <div class="contact-controls">
                    <div class="dropdown">
                        <button type="button" class="btn btn-default btn-icon" data-toggle="dropdown">
                          <span class="icon-cog"></span>
                        </button>                        
                        <ul class="dropdown-menu dropdown-left">
                            <li>
                              <a href="{{ route('account_users_me') }}" >
                                <span class="icon-user"></span> 
                                {{ __('general.SETTINGS_LABEL') }}
                              </a>
                            </li> 
                            <li>
                             
                              <a class="dropdown-item"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <span class="fa fa-sign-out" aria-hidden="true"></span>
                                        {{ __('Cerrar Session') }}
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </a>
                            </li> 
                        </ul>
                    </div>                    
                </div>
            </div>
        </li> 
        <li>
          <div class="dropdown">        
              <?php 
                  $system_messages  = [];
                  $system_messages = notification_all(Auth::user()->id);
              ?>                                    
              <button class="btn btn-default btn-icon btn-informer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                  <span class="icon-alarm"></span>
               
                <span class="informer informer-danger informer-sm informer-square">
                {{count($system_messages)}}
                </span>
                

              </button>
              <ul class="dropdown-menu dropdown-form dropdown-left dropdown-form-wide">
                  <li class="padding-0">                        
                  
                      <div class="app-heading title-only app-heading-bordered-bottom">
                          <div class="icon">
                              <span class="icon-text-align-left"></span>
                          </div>
                          <div class="title">
                              <h2>{{ __('notifications::notifications.notification.MESSAGE_COUNT_LABEL') }} </h2>                            
                          </div>
                         
                      </div>
                      
                      <div class="app-timeline scroll app-timeline-simple text-sm" style="height: 240px;">
                        
                          @foreach($system_messages  as $index => $item)
                          <div class="app-timeline-item">
                              <div class="dot dot-primary"></div>
                              <div class="content">                                    
                                  <div class="title margin-bottom-0">
                                    <a href="{{ route('frontend_notifications_view',['id'=>$item['id']]) }}">
                                      {{__($item['message'])}}
                                    </a> 
                                  </div>
                              </div>                                                
                          </div>
                          @endforeach   
                      
                          
                      </div>
                      
                  </li>          
              </ul>
          </div>
        </li>
                            
    </ul>
</div>