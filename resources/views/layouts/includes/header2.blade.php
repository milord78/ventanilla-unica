<div class="header-navbar navbar">
    <div class="container padding-15">
        <a  href="{{route('homepage')}}">
            <?php $logo = get_settings(); ?>
            <img style="width:25px" src="{{$logo->logos()}}">
        </a>
        <ul class="nav nav-tabs pull-right">
                @guest
                <li class="dropdown bordered">
                    <a class="dropdown-toggle clearfix" data-toggle="dropdown" aria-expanded="false" href="#">
                        <span class="flag" style="background-image:url({{ asset('assets/img/profileicon-1487703856.png')}});"></span> 
                        <span class="country">Cuenta </span>
                        <span class="caret pull-right"></span>
                    </a>
                    <ul
                        class="dropdown-menu" role="menu">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Comenzar registro') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Continuar con mi registro') }}</a>
                        </li>
                    </ul>
                </li>
                @else
                <li class="dropdown bordered">
                    <a class="dropdown-toggle clearfix" data-toggle="dropdown" aria-expanded="false" href="#">
                        <span class="flag" style="background-image:url({{\Auth::user()->logos()}});"></span> 
                                &nbsp;
                        <span class="country">
                            Intranet alumnos
                        </span>
                        <span class="caret pull-right"></span>
                    </a>

                    <ul
                        class="dropdown-menu" role="menu">
                        <li role="presentation">
                            <a role="presentation" href="{{ route('account_users_me') }}" class="dropdown-item">    {{ __('Ficha de inscripcón') }}
                            </a>
                        </li>
                        @if(count(count_inscripciones( \Auth::user()->id))>0)
                            <li>
                                <a href="{{ route('frontend_alunnos_index') }}"  type="button">
                                    Inscripciones
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('frontend_plantel_index') }}"  type="button">
                                    Informacion Planteles
                                </a>
                            </li>
                            @endif
                        <li role="presentation">
                            
                            <a class="dropdown-item" 
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar session') }}
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </a>
                        </li>
                    </ul>
                  
                </li>
                @endguest
           
            
         
    </div>
</div>
