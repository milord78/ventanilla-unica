<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('extra_meta')  
    <title>{{ config('app.name', 'Followando') }} Admin</title>
    
    <!-- Scripts -->
  
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/skins/_all-skins.min.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/morris.js/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">


    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/custom.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">-->
    @yield('custom_styles')
</head>
<body  class="hold-transition skin-blue sidebar-mini">
  <div>
    <div class="wrapper">

        <header class="main-header">
          <!-- Logo -->
          <a href="{{ route('dashboard_index') }}" class="logo">
              <!-- mini logo for sidebar mini 50x50 pixels -->
              <span class="logo-mini"><b>A</b>{{ config('app.name', 'AlondraEcommerce') }}</span>
              <!-- logo for regular state and mobile devices -->
              <span class="logo-lg"><b>{{ config('app.name', 'AlondraEcommerce') }}</b> V1.0</span>
          </a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top">
              <!-- Sidebar toggle button-->
              <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
              </a>

              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                           <?php $system_messages  = [];
                    $system_messages = system_notification_all() ?>
                  <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-bell-o"></i>
                      <span class="label label-warning">{{count($system_messages)}}</span>
                    </a>
                    <ul class="dropdown-menu">
                      <li class="header">{{ __('notifications::notifications.notification.MESSAGE_COUNT_LABEL') }} {{count($system_messages)}} </li>
                      <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                          @foreach($system_messages  as $index => $item)

  
                          <li>
                            <a href="{{ route('admin_system_notifications_view',['id'=>$item['id']]) }}">
                              <i class="fa fa-users text-aqua"></i> {{__($item['message'])}}
                            </a>
                          </li>
                           @endforeach   
                          
                        </ul>
                      </li>
                      <li class="footer"><a href="{{route('admin_system_notifications_index')}}">{{__('general.VIEW_ALL_LABEL')}}</a></li>
                    </ul>
                  </li>
  

                  <!-- User Account: style can be found in dropdown.less -->
                  <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     <i class="fa fa-gears"></i>
                     
                    </a>
                    <ul class="dropdown-menu">
                      <!-- Menu Footer-->
                      <li class="user-footer">
                         <div class="pull-left">
                          <a class="btn btn-default btn-flat" href="{{ route('admin_users_me') }}" >
                              {{ __('user.PROFILE_LABEL') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                        </div>
                        <div class="pull-right">
                        @if (\Auth::user()->role == "admin")

                          <a class="btn btn-default btn-flat" href="{{ route('admin_settings') }}"
                             >
                             <i class="fa fa-cog"></i>
                          </a>
                          @endif

                        
                          <a class="btn btn-default btn-flat" 
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Cerrar sesión') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                        </div>
                      </li>
                    </ul>
                  </li>
                 
              </div>
          </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
          <!-- sidebar: style can be found in sidebar.less -->
          <section class="sidebar">
            <div class="user-panel">
              <div class="pull-left image">
                @if (Auth::user()->logo != null)
                  <img src="/images/{{Auth::user()->logo }}" class="img-circle" alt="User Image">
                @else
                   <img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                @endif
               
              </div>
              <div class="pull-left info">
                <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
              </div>
            </div>
              <!-- sidebar menu: : style can be found in sidebar.less -->
              <ul class="sidebar-menu" data-widget="tree">
                  <li class="header">Menú</li>
                
         
                  <li>
                    <a href="{{ route('dashboard_index') }}">
                      <i class="fa fa-circle-o text-blue"></i> 
                      <span>
                      Escritorio
                      </span>
                    </a>
                  </li>
                  <li class="treeview  ">
                      <a>
                        <i class="fa fa-circle-o text-red"></i> 
                        <span>Aspirantes</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
           

                        @if (Auth::user()->role != "user")
                        <li>
                          <a href="{{ route('admin_users_index') }}">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>CENEVAL</span>
                          </a>
                        </li>
                        
                        @endif
                        <li>
                          <a href="#">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>COSDAC</span>
                          </a>
                        </li>
                        <li>
                          <a href="{{ route('admin_alunnos_index') }}">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Inscribir Aspirantes </span>
                          </a>
                        </li>
                      
                      </ul>
                    </li>   
            

                    

                                 
                    <li class="treeview  ">
                      <a>
                        <i class="fa fa-circle-o text-red"></i> 
                        <span>Configuración</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        
                       
                        <li>
                          <a href="{{ route('admin_carreras_index') }}">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>{{ __('controlnotas::rating.carreras.CARRERAS_LABEL') }}</span>
                          </a>
                        </li>
                   
                        <li>
                          @if(count_plantel()==0)
                          <a href="{{ route('admin_plantel_new') }}">
                          @else
                          <a href="{{ route('admin_plantel_edit',['id'=>1]) }}">
                          @endif
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>{{ __('controlnotas::rating.plantel.PLANTEL_LABEL') }}</span>
                          </a>
                        </li>

                        


                        <li>
                          <a href="{{ route('admin_grupos_index') }}">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Grupos</span>
                          </a>
                        </li>
                        <li>
                          <a href="{{ route('admin_ciclos_index') }}">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Ciclos</span>
                          </a>
                        </li>
                       
                  
                       
                      </ul>
                    </li>   
           
                    <li class="treeview  ">
                      <a>
                        <i class="fa fa-circle-o text-red"></i> 
                        <span>Trámites</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        
                        <li>
                          <a href="#">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Certificados</span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Constancias</span>
                          </a>
                        </li>
                         
                        <li>
                          <a href="#">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Becas</span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Titulación</span>
                          </a>
                        </li>
                        
                      </ul>
                    </li>

                    <li class="treeview  ">
                      <a>
                        <i class="fa fa-circle-o text-red"></i> 
                        <span>Exámenes</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        
                        <li>
                          <a href="#">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Extraordinario</span>
                          </a>
                        </li>
                         
                        <li>
                          <a href="#">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>Intersemetrales</span>
                          </a>
                        </li>
                          
                      
                      </ul>
                    </li>

                    <li class="treeview  ">
                      <a>
                        <i class="fa fa-circle-o text-red"></i> 
                        <span>Reportes</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        
                       
                      

                      </ul>
                    </li>   

                  <li class="treeview  ">
                      <a>
                        <i class="fa fa-circle-o text-red"></i> <span>Sistema</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                                 

                      @if (Auth::user()->role != "user")
                      <li>
                          <a href="{{ route('admin_users_new') }}">
                            <i class="fa fa-user text-blue"></i> 
                            <span>{{ __('general.ADD_LABEL') }} {{ __('user.USER_LABEL') }}</span>
                          </a>
                        </li>


                        <li>
                          <a href="{{ route('admin_permissions_permissions_index') }}">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>{{ __('permissions::permissions.permissions.PERMISSIONS_LABEL') }}</span>
                          </a>
                        </li>
                        @endif
                      
                      

                        <li>
                          <a href="{{ route('admin_media') }}">
                            <i class="fa fa-circle-o text-blue"></i> 
                            <span>{{ __('media.MEDIA_LABEL') }}</span>
                          </a>
                        </li>
                        
                      </ul>
                  </li>

                                 
                 
           
              </ul>
          </section>
          <!-- /.sidebar -->
        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
       
    </div>
  </div>  
    
    <!-- jQuery 3 -->
    <script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('assets/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="{{ asset('assets/admin/bower_components/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/admin/bower_components/morris.js/morris.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('assets/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ asset('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('assets/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('assets/admin/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/admin/bower_components/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('assets/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('assets/admin/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('assets/admin/dist/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('assets/admin/dist/js/demo.js') }}"></script>
    
    <script src="{{ asset('assets/admin/dist/js/demo.js') }}"></script>
    

    <script src="{{ asset('assets/js/jquery.cookie.js') }}"></script>

    <script src="{{ asset('assets/js/vue.js') }}"></script>

    <script src="{{ asset('assets/js/vue-resource.js') }}"></script>
    <script src="{{ asset('assets/js/lodash.js') }}"></script>
    <script type="text/javascript">
      window.home_path = "{{ url('/') }}/admin/";
      window.api_path = "{{ url('/') }}/";
    </script>
    @yield('footer_scripts')
   
</body>
</html>
