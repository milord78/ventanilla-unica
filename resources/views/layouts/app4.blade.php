<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('extra_meta')
    <title>{{ config('app.name', 'Followando - Comprar Followers redes sociales') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap-reboot.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css') }}">
    <link href="{{ asset('assets/css/custom2.css') }}" rel="stylesheet">
    @yield('custom_styles')
    
</head>
<body class="no-style">

   

    @yield('content')
   

    <script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/popper.min.js')}}" ></script>
    <script src="{{ asset('frontend/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('assets/js/vue.js') }}"></script>
    <script src="{{ asset('assets/js/vue-resource.js') }}"></script>
    <script src="{{ asset('frontend/assets/js/custom.js') }}"></script>
    <script type="text/javascript">
      window.api_path = "{{ url('/') }}/";
    </script>
    @yield('footer_scripts')
</body>
</html>
