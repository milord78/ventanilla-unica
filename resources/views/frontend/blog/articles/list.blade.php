@extends('layouts.blog')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">
      <div class="box-header" style="padding: 15px">
        <div class="box-tools">
          <form method="get" action="{{route('account_users_runnable_carrers_search')}}">
            <div class="input-group input-group-sm" style="width: 550px;">
              <input type="text" name="title" value="{{$q}}" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }} {{ __('general.TITLE_LABEL') }}">
              <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
        <div class="row">
          @foreach($items  as $index => $item)
          <div class="col-md-3">
            <div class="thumb no-radius"></div>
            <article>
                <header>
                  <div>
                    <p class="date">
                      <i class="fa fa-calendar-o"></i> 
                      {{time_elapsed_string($item['created_at'])}}
                    </p>
                  </div>
                  <a href="{{route('blog_details_slug',['slug'=>$item['slug']])}}" class="title">
                    {{$item['title']}}
                  </a>
                </header>
            </article>
          </div>
          @endforeach    

        </div>

      </div>
      <!-- /.box-body -->

      <div class="box-footer clearfix">

        {{ $items->links() }}
      </div>
    </div>

@endsection
