@extends('layouts.blog')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div class="box">
    	<h1>{{$item['title']}}</h1>
    	<div class="date" style="font-size: 11px;">
    		<i class="fa fa-calendar-o"></i>
    		{{time_elapsed_string($item['created_at'])}}
    	</div>
    	<div style="font-size: 12px;">
    		{{$item['excerpt']}}
    	</div>
    	<div style="margin: 15px;" >
    		{{$item['content']}}
    	</div>
        @foreach($tags  as $index => $item)
           <a href="{{route('blog_tag_details_slug',['slug'=>$item['slug']])}}" class="tag font-white"> {{$item['name']}}</a>
        @endforeach    
    </div>

@endsection
