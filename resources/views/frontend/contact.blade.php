@extends('layouts.app')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
    @endif

    <div class="clean-form">


      <form method="POST" action="{{ route('contact_form') }}">
          @csrf
          <h1>Contactanos </h1>
          <div class="form-group">
            <label class="control-label">Sitio web</label>
            <input class="form-control" type="text" name="website">
          </div>
          <div class="form-group">
            <label class="control-label">Telefono</label>
            <input class="form-control" type="text" name="phone" >
          </div>
          <div class="form-group">
              <label class="control-label">{{ __('Email') }}*</label>
              <input class="form-control" type="email" name="email" required autocomplete="off" autofocus>
          </div>
          <div class="form-group">
              <label class="control-label">{{ __('Mensaje') }}*</label>
              <textarea class="form-control" type="text" name="message" required autocomplete="off" autofocus></textarea>
          </div>
          <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Enviar </button>

            <button class="btn btn-info" type="reset">
              Limpiar 
            </button>
            <div class="row contact padding-top-20">
                      <div class="col-md-4 col-sm-4 col-xs-12 item clearfix"><img class="img-responsive cover" src="assets/img/phone.svg" width="40">
                          <div class="text"><strong>Telefonos </strong>
                              <p>+57 552255333</p>
                              <p>+57 552255333</p>
                          </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 item clearfix"><img class="img-responsive cover place" src="assets/img/place.svg" width="30">
                          <div class="text"><strong>Direccion </strong>
                              <p>Cartagena colombia calle 3, codigo postal A2F20. colombia</p>
                          </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 item clearfix"><img class="img-responsive cover" src="assets/img/calendar.svg" width="40">
                          <div class="text"><strong>Horarios </strong>
                              <p>8 am a 5pm</p>
                              <p>Lunes a sabado</p>
                          </div>
                      </div>
            </div>
          </div>
      </form>

@endsection
