@extends('layouts.app')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
    @endif
    <div id="payment_form" class="container padding-top-20">   <h1>
          Estas a un paso de disfrutar de nuestros servicios
        </h1>
        <p>
          Por favor selecciona la opcion que mas te convenga.
        </p>
        <div style="padding: 10px;">
        	<a v-bind:href="url" class="btn btn-block btn-primary">
        		{{ __('Realizar pago') }}
        	</a>
        </div>
        <div style="padding: 10px;">
          <a href="{{route('homepage')}}" class="btn btn-block btn-primary">
          {{ __('Cancelar') }}
        </a>
        </div>
      	
    </div>
@endsection

@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/payment_form.js') }}"></script>
@endsection