@extends('layouts.app3')
@section('content')
<div class="padding-45">
<div class="container">
@if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{ \Session::get('error') }}</p>
    </div><br />
  @endif
<form action="{{route('frontend_account_two_factor_store')}}" method="post">
     @csrf
     <div class="form-group">
         <label for="token">Codigo de seguridad</label>
         <input type="text" name="token" placeholder="Ingresar codigo de Autorizacion" class="form-control{{ $errors->has('token') ? ' is-invalid' : '' }}" id="token"> 
           @if($errors->has('token'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('token') }}</strong>
              </span> 
           @endif
        </div>
        <p>Revise su correo, incluyendo su carpeta de spam le hemos enviado un codigo de autorización</p>
        <p>
            <?php
                $date1 = new DateTime(\Auth::user()->two_factor_token_expiry);
                $date2 =  \Carbon\Carbon::now();
                $date3 = $date1->diff($date2)->format("%h:%i:%s");
            ?>
            @if($date3 !="00:00:00")
            Su codigo experia dentro de: {{$date3}}
            @else
            Codigo nuevo enviado a su correo por favor
            @endif 
    </p>
        <button class="btn btn-primary btn-block">Verficiar</button>
</form>

</div>
</div>
@endsection
@section('footer_scripts')
@endsection
@section('custom_styles')

@endsection