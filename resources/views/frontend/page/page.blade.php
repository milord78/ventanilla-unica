@extends('layouts.app')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <div>
        <div class="container background" style="margin-bottom: 15px;">
            <div class="row">
                @foreach($prices as $index => $product)

                    @if($index<=3)
                        
                        <div class="col-md-3 related text-center">
                            <div class="image-item" style="background-image: url({{$item['thumbnail']}});background-size: 100%;background-repeat: no-repeat;background-position: center;@if(!empty($item['thumbnail']))background-color: #fff;@endif">
                            </div>
                            <span class="link">
                                {{$product['title']}}
                            </span>
                            

                                                        
                                <?php $company = $product->company ?> 
                                 <a style="display: block;" href="{{route('company_details',['slug'=>$company['slug']])}}">
                                     {{$company->title}}
                                </a>
                                <?php $rating = get_user_rating_members($product['id']); 
                                ?>

                                @if($rating >0 )
                                <div class="ratting">
                                <?php for ($i=1;$i<5;$i++): ?>
                                    @if($rating >=$i)
                                        <i class="glyphicon glyphicon-star"></i>
                                    @endif
                                <?php endfor; ?>
                                </div>
                                @endif
                                <p class="price">
                                    {{currency_exchange($product['price'])}}
                                    
                                </p>
                                <div>
                                @if($product['free_shipping']==true)
                                <p class="label label-primary">
                                    Envio gratiuto 
                                </p>
                                @endif
                                </div>
                                <a href="{{$product['affiliate_link']}}" class="btn btn-primary">
                                    VER OFERTA
                                </a>
                           </div>
                    @endif
                @endforeach
           
        </div>
        <div class="container">
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        @foreach($prices  as $index => $cat)
                        @if($index >3)
                        <tr>
                            <td char="text-center">
                                <span class="title">
                                    {{$cat['title']}}
                                </span>
                            </td>
                            <td>
                                <div>
                                    <span class="price">
                                        {{currency_exchange($cat['price'])}} 
                                    </span>
                                    <span class="delivery">
                                        {{$cat['price_delivery']}}
                                        @if($cat['free_shipping']==true)
                                         envio incl.
                                        @endif
                                    </span>
                                </div>
                            </td>
                            <td>
                                <div>
                                    @foreach(explode(
                                    ',',
                                    $cat['payments_methods'])  as $payments_methods => $payments_method)
                                    @if($payments_method =="Visa")
                                    <div class="payment_method">
                                        <img src="{{ asset('assets/img/visa(1).svg') }}" width="25">
                                    </div>
                                    @endif
                                    @if($payments_method =="MasterCard")

                                    <div class="payment_method">
                                        <img src="{{ asset('assets/img/mastercard.svg') }}" width="25">
                                    </div>
                                    @endif
                                    @if($payments_method =="Paypal")
                                    <div class="payment_method">
                                        <img src="{{ asset('assets/img/paypal.svg') }}" width="25">
                                    </div>
                                    @endif
                                    <span class="delivery"> </span>
                                    @endforeach  
                                </div>
                            </td>
                            <td>
                                <div class="list_devivery">
                                   {!!$cat['delivery']!!}
                                </div>
                            </td>
                            <td>
                                <?php $company = $cat->company; ?> 
                                <div style="text-align: center;">
                                    <a style="display: inline-block;" href="{{route('company_details',['slug'=>$company['slug']])}}">
                                     <div  style="width: 107px;height: 33px; background-image: url({{$company['logo']}});background-size: cover;background-position: center;box-shadow: 1px 1px 5px rgba(0,0,0,.1);"></div>   
                                    </a>
                                </div>
                                <div class="ratting-img">
                                    <?php $rating = get_user_rating_members($company['id']); ?>
                                    <?php for ($i=1;$i<5;$i++): ?>
                                        @if($rating >=$i)
                                            <i class="glyphicon glyphicon-star"></i>
                                        @endif
                                    <?php endfor; ?>
                                    
                                </div>
                                <div class="text-center">
                                    <p>
                                        {{get_user_rating_count($company['id'])}} opiones
                                    </p>
                                </div>
                            </td>
                            <td>
                                <a  href="{{$cat['affiliate_link']}}" class="btn btn-primary" type="button">
                                    Ver oferta
                                </a>
                            </td>
                        </tr>
                        @endif
                        @endforeach  
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="title2">
                        Especificaciones {{$item['title']}}
                    </h1>
                </div>
                <div class="col-md-4">
                    <div class="image-item" style="background-image: url({{$item['featured_image']}});background-size: 75%;background-repeat: no-repeat;background-position: center;@if(!empty($item['featured_image']))background-color: #fff;@endif"></div>
                    <?php $rating = get_post_rating($item['id']); ?>
                    <?php for ($i=1;$i<5;$i++): ?>
                        @if($rating >=$i)
                            <i class="glyphicon glyphicon-star"></i>
                        @endif
                    <?php endfor; ?>
                    <div style="padding: 5px">
                        <a href="{{route('account_product_rating_new',['id'=>$item['id']])}}" class="btn btn-default">
                            Calificar Producto
                        </a>
                        <a href="{{route('product_report_new',['id'=>$item['id']])}}" class="btn btn-default">
                           Reportar fallo
                        </a>    
                    </div>

                </div>
            </div>
            <div class="row background">
                <div class="col-md-12">
                    <h3 class="text-center">{{$item['title']}}</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                 @foreach($especifications  as $category => $cat)
                                
                                <tr>
                                    <td>{{$cat['name']}} </td>
                                    <?php $a = 0; ?>
                                    @foreach($cat['children'] as $category => $children)
                                        @if($a==0)
                                        <td>
                                        @endif 
                                        <div>
                                            <strong>
                                                {{
                                                    $children['meta_key']
                                                }} : 
                                            </strong>
                                            <span>
                                                {{
                                                    $children['meta_value']
                                                }} 
                                            </span>
                                        </div>
                                    
                                        @if(($a==4) or ($a>=$category))
                                            <?php $a = 0; ?>
                                        </td>
                                        @else
                                        <?php $a++; ?>
                                        @endif 
                                    
                                    @endforeach 
                                    
                                </tr>
                                @endforeach  
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>Descripcion breve del producto</h3>
                    {!!$item['content']!!}
                </div>
            </div>
        </div>
    </div>
@endsection
