@extends('layouts.user_panel')

@section('content')

    @if (\Session::has('success'))
    <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{ \Session::get('error') }}</p>
    </div><br />
  @endif
  <div class="box">
    <h3 class="box-title">
      {{ __('Editar Comentario Producto') }}
    </h3>
    <form role="form" method="post" action="{{$route}}">
       @csrf
      <div class="box-body">

        <div class="form-group">
          <label for="exampleInputEmail1">
            {{ __('Comentario') }}
          </label>
          <textarea required class="form-control"  name="comment"  value="{{$item->comment}}"></textarea>
         
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">
            {{ __('Rating') }}
          </label>
          <div id="stars-existing" style="margin-top: 5px; margin-bottom: 5px; background: #fff; border-radius: 5px; padding: 5px; display: inline-block;" class="starrr" data-rating='5'></div>
          <input id="id_rating" type="hidden" name="rating">
        </div>                       
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a  href="{{route('account_company_rating_delete',['id'=>$item['id']])}}" class="btn btn-danger" > 
                    <i class="fa fa-trash-o"></i> 
                </a>
      </div>
    </form>
  
  </div>
@endsection
@section('footer_scripts')
  <script src="{{ asset('assets/js/starrr.js') }}"></script>
@endsection


