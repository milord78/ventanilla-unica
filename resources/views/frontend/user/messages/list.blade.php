@extends('layouts.app2')
@section('custom_styles')
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endsection
@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif


<div class="container" id="content">
    <div id="messages" class="row">
        <div class="col-md-3 user-list white padding-35">
            @verbatim
            <div v-if="contacts.length > 0" v-for="item in contacts" v-on:click="select_user(item)"  class="user-item">
                <div class="left">
                    <img :src=item.logo class="photo" />
                </div>
                <div class="right">
                    <h6 class="title">{{item.nick}} </h6><span :class=online_offline_class(item)> </span>
                    <p v-if="item.has_new_messages == true" class="tag message">
                        nuevo 
                        <i class="fa fa-exclamation-circle"></i>
                    </p>
                </div>
            </div>
            @endverbatim
            
        </div>
        <div class="col-md-9">
            <div class="row padding-15 panel">
                <div class="col-md-12 text-left">
                    <div class="dropdown ">
                        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true" type="button"> 
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <div role="menu" class="dropdown-menu ">
                            <a v-on:click="set_status_online()" role="presentation"  class="dropdown-item">
                                <span class="status online"> </span>
                                <span class="margin-5">online </span>
                            </a>
                            <a v-on:click="set_status_offline()" role="presentation" class="dropdown-item">
                                <span class="status offline"> </span>
                                <span class="margin-5">offline </span>
                            </a>
                            <a role="presentation" v-on:click="set_block_user()" class="dropdown-item">Block user</a>
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="row padding-15 panel">
                <div class="col-md-12 chat white">
                    <div ref="messages">
                        @verbatim
                        <div class="message" v-if="comments.length > 0" v-for="item in comments">
                            <div v-if="item.clasification == 'image' ">
                                <img :src="item.attachment" class="img-fluid" />
                            </div>

                            <div v-if="item.clasification == 'video' ">
                                <video width="560" height="315" :src="item.attachment" controls class="full-widh"></video>
                            </div>

                            <div v-if="item.clasification == 'attachment' ">
                                <a :href="item.attachment" >Ver archivo</a>
                            </div>

                            
                            <div>{{item.message}}</div>
                            <p class="date">{{item.time_elapsed}}</p>
                            <img :src=item.logo class="photo pull-right" />
                        </div>
                        @endverbatim
                    </div>
                    
                </div>
            </div>
            <div class="row padding-15 panel">
                <div class="col-md-12">
                     <div id="holder" v-if="action != 'text' " v-on:click="open_uploader($event)"  @dragover.prevent @drop.stop.prevent="drop($event)" class="dropzone dz-clickable">
                        <div class="dz-default dz-message">
                            <div class="dropzone-preupload-title">
                              {{ __('media.DROP_ARCHIVES_LABEL') }}
                            </div> 
                            <p>
                              {{ __('media.UNLOADED_ARCHIVES_LABEL') }}
                            </p>
                        </div>
                        <input type="file" v-on:change="change($event)" ref="id_files"   id="id_files" class="hide" >

                    </div>
                    <br>
                </div>
                <div class="col-md-12 text-right">
                    <form v-if="action == 'text'" >
                        <div class="form-group">
                            <textarea v-model="comment" class="form-control"></textarea>
                        </div>
                    </form>

                    <button  v-on:click="set_action('attachment')" class="btn btn-secondary small" type="button">
                        <i class="fa fa-file-text"></i> 
                        archivo 
                    </button>
                    <button v-on:click="set_action('image')" class="btn btn-info small" type="button">
                        <i class="fa fa-picture-o"></i> imagen 
                    </button>
                    <button v-on:click="set_action('video')" class="btn btn-info small"
                    type="button">
                        <i class="fa fa-film"></i> video 
                    </button>
                    <button v-if="action != 'text'" v-on:click="set_action('text')" class="btn btn-info small"
                    type="button">
                         texto 
                    </button>
                    <button v-on:click="create_comment()" class="btn btn-primary" type="button">
                        <i class="fa fa-check"></i> enviar 
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/messages.js') }}"></script>
@endsection