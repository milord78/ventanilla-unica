@extends('layouts.app2')

@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif
<div class="row  padding-15  ">
    <div class="col-md-3 ">
        <article class="card">
            <header class="panel">
                <h4 class="title"> {{ __('general.OPTIONS_LABEL') }} <i class="fa fa-cog"></i></h4>
            </header>
            <form action="{{ route('users_on_the_site_search') }}" method="get">
                <input type="hidden" name="province" value="{{$province_slug}}">
                <input type="hidden" name="profession" value="{{$profession_slug}}">
            <div class="margin-15">
                <div>
                    
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle province" data-toggle="dropdown" aria-expanded="false" type="button">
                            {{ __($province_label) }} 
                        </button>
                        <div class="dropdown-menu" role="menu">
         
                             <a class="dropdown-item provinces" role="presentation" data-name="{{__("user.ALL_PROVINCES_LABEL") }}" data-slug="0" class="btn btn-primary small" type="button">
                                   {{ __("user.ALL_PROVINCES_LABEL") }} 
                                </a>

                            @foreach(get_runnable_all_provinces()  as $index => $item)
                                <a class="dropdown-item provinces" role="presentation" data-name="{{$item->name}}" data-slug="{{$item['slug']}}" class="btn btn-primary small" type="button">
                                   {{$item->name}}
                                </a>
                            @endforeach  
                        </div>

                    </div>
                </div>
                
                <div>
                    <div class="dropdown">
                        <button class="profession btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button">
                            {{ __($profession_label) }} 
                        </button>
                        <div class="dropdown-menu" role="menu">
                             <a class="dropdown-item professions" role="presentation" data-name="{{__("user.ALL_PROFESSIONS_LABEL") }}" data-slug="0" class="btn btn-primary small" type="button">
                                   {{ __("user.ALL_PROFESSIONS_LABEL") }} 
                                </a>
                            @foreach(get_runnable_all_professions()  as $index => $item)
                            
                                <a class="dropdown-item professions" role="presentation" data-name="{{$item->name}}"  data-slug="{{$item['slug']}}" class="btn btn-primary small" type="button">
                                   {{$item->name}}
                                </a>
                            @endforeach  
                        </div>
                    </div>
                </div>
                <div>
                    <button  class="btn btn-primary" type="submit">
                        {{ __('general.filter') }} 
                    </button>
                </div>
            </form>
            </div>
        </article>
    </div>
    <div class="col-md-9">
        <div class="box-header" style="padding: 15px">
            <div class="box-tools">
              <form method="get" action="{{route('users_on_the_site_search')}}">
                <div class="input-group input-group-sm" style="width: 550px;">
                  <input type="text" name="title" value="{{$q}}" class="form-control pull-right" placeholder="{{__('general.SEARCH_LABEL') }}">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
        </div>
        <div class="row  padding-15  table-list">
            <div class="col-md-12">
                <table class="table">
                   
                    <tbody>

                        @foreach($items  as $index => $item)
          
                        <tr>
                            <td>
                                <div class="user-photo big" style="background:url({{ $item->logos()}}); background-size: cover;"></div>
                            </td>
                            <td>
                                {{$item['nick']}}
                            </td>
                            <td><span class="info"> {{time_elapsed_string($item->created_at)}} </span></td>
                            <td>
                                <div><span class="info"> {{$item['phone']}}</span></div>
                            </td>
                            <td class="text-center">
                             
                                <div class="padding-5">
                                    <strong>({{get_count_opinions($item->id)}}) </strong>
                                    <span>opiniones </span>
                                </div>
                                <a href="{{route('account_public_profile',['id'=>$item->id])}}" class="btn btn-primary" type="button">
                                    {{ __('user.VIEW_PROFILE_LABEL') }} 
                                </a>

                            </td>
                        </tr>
                        @endforeach  
                     
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row padding-15 panel">
            {{ $items->links() }}
        </div>
    </div>


</div>






@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/user_search.js') }}"></script>
<script type="text/javascript">


  var ready = function(e)
  { 


    select_province = function(e)
    {
      slug = $(this).attr('data-slug');
      name = $(this).attr('data-name');
      if(slug == 0)
        {
            slug = null
        }
      $("input[name=province]").val(slug);
      $(".province").html(name);
    }
    $(".provinces").on('click',select_province);

    select_profession = function(e)
    {
      slug = $(this).attr('data-slug');
      name = $(this).attr('data-name');
      if(slug == 0)
        {
            slug = null
        }
      $("input[name=profession]").val(slug);
      $(".profession").html(name);
    }
    $(".professions").on('click',select_profession);
  } 
  $(window).on('load',ready);

</script>
@endsection