@extends('layouts.user_panel')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{ \Session::get('error') }}</p>
    </div><br />
  @endif
  <div class="">
    <h3 class="box-title">
      {{ __('Comentario enviado exitosamente') }}
    </h3>
    <p>{{ __('Muchas gracias por enviar tu comentario y muchas gracias por confiar en nosotros, trabajamos dia a dia por ofrecerte una mejor plataforma para afrontar tus nuevos retos') }}.</p>

  </div>
@endsection

