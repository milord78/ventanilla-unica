@extends('layouts.user_panel')
@section('custom_styles')
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endsection
@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

    <section class="content">
    <!-- COLOR PALETTE -->
      <div class="box box-default color-palette-box">
  
        <div class="box-header with-border">
            <h3 class="box-title"> 
                {{ __('media.MY_VIDEOS') }}  
            </h3>
        </div>      

        <div id="media_module" class="box-body">

        
    
                <div style="padding: 20px;" class="clearfix">
                    <div>
                        <div id="holder"  v-on:click="open_uploader($event)"  @dragover.prevent @drop.stop.prevent="drop($event)" class="dropzone dz-clickable">
                            <div class="dz-default dz-message">
                                <div class="dropzone-preupload-title">
                                  {{ __('media.DROP_FILES_LABEL') }}
                                </div> 
                                <p>
                                  {{ __('media.UNLOADED_FILES_LABEL') }}
                                </p>
                            </div>
                            <input type="file" v-on:change="change($event)"  ref="id_files" id="id_files" class="hide" >
                        </div>
                        <br>

                        @verbatim
                       
                        <div v-if="progresss > 0" style="margin-top:25px;" class="progress">
                         <div class="progress-bar" role="progressbar" :aria-valuenow=progresss
                          aria-valuemin="0" aria-valuemax="100" :style="progress_status()" >
                            <span class="sr-only">{{progresss}} Complete</span>
                          </div>
                        </div>
                        @endverbatim
                    </div>

                </div>                          
                    
                     
                        
           
                <div style="padding: 20px;" class="clearfix card">      

                    <table class="table">

                        <tbody>
                         
                 

                            <tr v-if="files.length > 0" v-for="item in files">
                                    @verbatim
                                <td class="app-timeline-item" width="20px;">
                                    <div class=" padding-top-20" style="height: 102px;">
                                   
                                        paste video embed
                                      
                                    </div>
                                </td>
                                <td width="75%" >
                                    
                                    <strong v-if="item.editable == false" >{{item.name}}</strong>
                                     @endverbatim
                                    <div  v-if="item.editable == true" class="form-group ">
                                        <input type="text" v-model=item.name  class=" form-control">
                                    </div>
                                </td>

                             
                                <td>
                                   
                                    <a  v-if="item.editable == true"  v-on:click="save_name(item)"  class="btn btn-primary small">
                                      {{ __('general.SAVE_LABEL') }}
                                     
                                    </a>
                                    &nbsp;
                                    <a  v-if="item.editable == false" style="padding: 10px;font-size: 12px;" v-on:click="make_editable(item)" class="btn btn-primary small">
                                        <i class="fa fa-pencil-square-o"></i>
                                        {{ __('general.EDIT_LABEL') }}
                                    
                                    </a>
                                    &nbsp;
                                    <a v-on:click="delete_file(item)" style="padding: 10px;font-size: 12px;"  class="btn btn-danger small" >
                                        <i class="fa fa-trash"></i>           
                                    </a>

                                </td>
                            </tr>
                           
                        </tbody>
                    </table>

                </div>
                       
                <div >
                    <button v-if="next_page_cats!=null"  v-on:click="load_more()"  class="btn btn-block btn-primary" type="button">
                        {{__('general.LOAD_MORE_LABEL') }} 
                    </button>
                </div>
                                  
                    
                    


      
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
    </div>

@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/media_video.js') }}"></script>
@endsection