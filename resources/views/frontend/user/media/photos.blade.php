@extends('layouts.user_panel')

@section('custom_styles')
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endsection
@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif

  <section class="content">
    <!-- COLOR PALETTE -->
      <div class="box box-default color-palette-box">
  
  



        <div id="media_module" class="box-body">
            <div v-if="show_big_image == false"  class="box-header with-border">
                <h3 class="box-title"> 
                  {{ __('media.MY_ALBUMS') }}
                </h3>
            </div>    

             <div v-if="show_big_image == true"  class="box-header with-border">
                <h3 class="box-title"> 
                  {{ __('media.MY_PICTURES') }}
                </h3>
            </div>    
            <div v-if="hidden_main_buttons == false">
              <div v-if="album_new==false" class="row">
                  <div class="col">
                      <button v-on:click="toggle_new_album()" class="btn btn-primary" type="button">
                      <i class="fa fa-plus"></i>&nbsp;
                      {{ __('media.NEW_ALBUM') }}
                      </button>
                  </div>
              </div>

              <div v-if="album_new==true" class="form-group">
                  <input class="form-control" placeholder="{{ __('general.NAME_LABEL') }}" type="text" v-model="album_name" required autocomplete="off" autofocus>
                  <button v-on:click="new_album()" class="btn btn-primary" type="button">
                    {{ __('general.SAVE_LABEL') }}
                  </button>
                  <button v-on:click="cancel_album()" class="btn btn-primary" type="button">
                    {{ __('general.CANCEL_LABEL') }}
                  </button>
              </div>
              

              <div class="row">
                  <div  v-for="item in albums" class="col-md-3 margin-bottom-10">
                      <div v-on:click="move_to_album(item)" class="thumb">
                        @verbatim
                        <span class="title">{{item.name.substr(0,5)}}...</span>
                        @endverbatim
                      </div>
                  </div>
                
              </div>                     
            </div>
            <div v-if="hidden_main_buttons == true ">
               <div v-if="show_big_image ==false" class="form-group">

                  <button v-on:click="move_to_parent()" class="btn btn-primary" type="button">
                    {{ __('general.BACK_LABEL') }}
                  </button>
                   <button v-on:click="edit_album()" class="btn btn-primary" type="button">
                    {{ __('general.EDIT_LABEL') }}
                    {{ __('general.NAME_LABEL') }}
                  </button>

                    <button v-on:click="delete_album(instance)" class="btn btn-danger" type="button">
                    {{ __('general.DELETE_LABEL') }}
                 
                  </button>
              </div>

              <div v-if="album_edit==true && show_big_image ==false" class="form-group">
                  <input class="form-control" placeholder="{{ __('general.NAME_LABEL') }}" type="text" v-model="instance.name" required autocomplete="off" autofocus>
                  <button v-on:click="update_album(instance)" class="btn btn-primary" type="button">
                    {{ __('general.SAVE_LABEL') }}
                  </button>
                  <button v-on:click="cancel_album()" class="btn btn-primary" type="button">
                    {{ __('general.CANCEL_LABEL') }}
                  </button>
              </div>
              <div  v-if="album_edit==false && show_big_image ==false" class="box-header with-border">
                  <h3 class="box-title"> 
                    {{ __('media.ALBUM_LABEL') }}:&nbsp;
                    @verbatim
                      {{instance.name}}
                    @endverbatim
                  </h3>

              </div>
              <div  v-if="album_edit==false && show_big_image ==false" style="padding: 20px;" class="clearfix">
                    <div>
                        <div id="holder"  v-on:click="open_uploader($event)"  @dragover.prevent @drop.stop.prevent="drop($event)" class="dropzone dz-clickable">
                            <div class="dz-default dz-message">
                                <div class="dropzone-preupload-title">
                                  {{ __('media.DROP_FILES_LABEL') }}
                                </div> 
                                <p>
                                  {{ __('media.UNLOADED_FILES_LABEL') }}
                                </p>
                            </div>
                            <input type="file" v-on:change="change($event)"  ref="id_files" id="id_files" class="hide" >
                        </div>
                        <br>

                        @verbatim
                       
                        <div v-if="progresss > 0" style="margin-top:25px;" class="progress">
                         <div class="progress-bar" role="progressbar" :aria-valuenow=progresss
                          aria-valuemin="0" aria-valuemax="100" :style="progress_status()" >
                            <span class="sr-only">{{progresss}} Complete</span>
                          </div>
                        </div>
                        @endverbatim
                    </div>

                </div>                          
                
                <div class="row">
                  <div  v-if="show_big_image == false" v-for="item in files" class="col-md-3 margin-bottom-10">
                      <div v-on:click="show_big_image_click(item)"  class="thumb" :style="show_background_image(item)">
                        @verbatim
                        <span class="title">{{item.name.substr(0,5)}}...</span>
                        @endverbatim
                      </div>
                  </div>

                   <div  v-if="show_big_image == true" class="col-md-12 margin-bottom-10">
                      <div  class="thumb margin-bottom-10" :style="show_background_image(instance_image)">
                        @verbatim
                        <span class="title">{{instance_image.name.substr(0,5)}}...</span>
                        @endverbatim
                      </div>
                      <div v-if="photo_edit==false"  class="form-group">

                          <button v-on:click="move_to_album_image_parent()" class="btn btn-primary" type="button">
                            {{ __('general.BACK_LABEL') }}
                          </button>


                           <button v-on:click="edit_picture_click()" class="btn btn-primary" type="button">
                            {{ __('general.EDIT_LABEL') }}
                            {{ __('general.NAME_LABEL') }}
                          </button>

                          <button v-on:click="delete_file(instance_image)" class="btn btn-danger" type="button">
                            {{ __('general.DELETE_LABEL') }}
                         
                          </button>
                      </div>
                       <div v-if="photo_edit==true" class="form-group">
                          <input class="form-control" placeholder="{{ __('general.NAME_LABEL') }}" type="text" v-model="instance_image.name" required autocomplete="off" autofocus>
                          <button v-on:click="save_name(instance_image)" class="btn btn-primary" type="button">
                            {{ __('general.SAVE_LABEL') }}
                          </button>
                          <button v-on:click="cancel_picture_click()" class="btn btn-primary" type="button">
                            {{ __('general.CANCEL_LABEL') }}
                          </button>
                      </div>
                  </div>
                </div>    
            </div>

                         
                    
                    


      
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
    </div>
  </section>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/media_photos.js') }}"></script>
@endsection