@extends('layouts.app2')

@section('custom_styles')
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endsection
@section('content')

<div class="container" id="content">
    <div class="row">
        <div class="col-md-12">
            <header class="profile-dash">
                <img src="{{ $current_user->logos() }}"  style="height: auto !important"  class="photo" />
                <ul class="list-inline text-center pull-right padding-15">
                    <li><span class="block title"><i class="fa fa-thumbs-o-up"></i> Seguidores </span><span class="asblock">0 </span></li>
                    <li><span class="block title"><i class="fa fa-star"></i>Opiniones </span><span class="asblock">0 </span></li>
                    <li><span class="block title"><i class="fa fa-users"></i> Clubs</span><span class="asblock">0 </span></li>
                </ul>
            </header>
        </div>
        
          

          @if (\Session::has('success'))
          <div class="col-md-12">
            <div class="alert alert-success">
              <p>{{ __(\Session::get('success')) }}</p>
            </div><br />
          </div>
          @endif
        
    </div>
    <div class="row">
      


        @include('layouts.includes.public_user_panel_sidebar')
        <div id="wall" class="col-md-9 ">
          <section class="content padding-15">
          <!-- COLOR PALETTE -->
            <div class="box box-default color-palette-box">
        
        



              <div id="media_module" class="box-body">
                  

                   <div v-if="show_big_image == true"  class="box-header with-border">
                      <h3 class="box-title"> 
                        {{ __('media.PICTURES') }}
                      </h3>
                  </div>    


                  <div v-if="hidden_main_buttons == false">
             

                    <div class="row">
                        <div  v-for="item in albums" class="col-md-3 margin-bottom-10">
                            <div v-on:click="move_to_album(item)" class="thumb">
                              @verbatim
                              <span class="title">{{item.name.substr(0,5)}}...</span>
                              @endverbatim
                            </div>
                        </div>
                      
                    </div>                     
                  </div>
                  <div v-if="hidden_main_buttons == true ">
                      <div v-if="show_big_image == false" class="form-group">

                        <button v-on:click="move_to_parent()" class="btn btn-primary" type="button">
                          {{ __('general.BACK_LABEL') }}
                        </button>
                    </div>
                    <div  v-if="album_edit==false && show_big_image ==false" class="box-header with-border">
                        <h3 class="box-title"> 
                          {{ __('media.ALBUM_LABEL') }}:&nbsp;
                          @verbatim
                            {{instance.name}}
                          @endverbatim
                        </h3>

                    </div>
                 
                                   
                      
                      <div class="row">
                        <div  v-if="show_big_image == false" v-for="item in files" class="col-md-3 margin-bottom-10">
                            <div v-on:click="show_big_image_click(item)"  class="thumb"  :style="show_background_image(item)">
                              @verbatim
                              <span class="title">{{item.name.substr(0,5)}}...</span>
                              @endverbatim
                            </div>
                        </div>

                         <div  v-if="show_big_image == true" class="col-md-12 margin-bottom-10">
                            <div  class="thumb margin-bottom-10" :style="show_background_image(instance_image)">
                              @verbatim
                              <span class="title">{{instance_image.name.substr(0,5)}}...</span>
                              @endverbatim
                            </div>
                            <div v-if="photo_edit==false"  class="form-group">

                                <button v-on:click="move_to_album_image_parent()" class="btn btn-primary" type="button">
                                  {{ __('general.BACK_LABEL') }}
                                </button>


                            </div>
                             
                        </div>
                      </div>    
                  </div>

                               
                          
                          


            
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            </div>
          </section>
        </div>
      </div>
  </div>
@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/media_photos.js') }}"></script>
<script src="{{ asset('assets/js/frontend/user_follow.js') }}"></script>
@endsection