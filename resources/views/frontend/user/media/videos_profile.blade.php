@extends('layouts.app2')
@section('custom_styles')
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endsection
@section('content')


<div class="container" id="content">
    <div class="row">
        <div class="col-md-12">
            <header class="profile-dash">
                <img src="{{ $current_user->logos() }}"  style="height: auto !important"  class="photo" />
                <ul class="list-inline text-center pull-right padding-15">
                    <li><span class="block title"><i class="fa fa-thumbs-o-up"></i> Seguidores </span><span class="asblock">0 </span></li>
                    <li><span class="block title"><i class="fa fa-star"></i>Opiniones </span><span class="asblock">0 </span></li>
                    <li><span class="block title"><i class="fa fa-users"></i> Clubs</span><span class="asblock">0 </span></li>
                </ul>
            </header>
        </div>
        
          

          @if (\Session::has('success'))
            <div class="col-md-12">
            <div class="alert alert-success">
              <p>{{ __(\Session::get('success')) }}</p>
            </div><br />
            </div>
          @endif
        
    </div>
    <div class="row">



        @include('layouts.includes.public_user_panel_sidebar')
        <div id="wall" class="col-md-9">
            <section class="content padding-15">
            <!-- COLOR PALETTE -->
              <div class="box box-default color-palette-box">
          
                <div class="box-header with-border">
                    <h3 class="box-title"> 
                        {{ __('media.VIDEOS') }}  
                    </h3>
                </div>      

                <div id="media_module" class="box-body">

                
            
                             
                                
                   
                        <div style="padding: 20px;" class="clearfix row">      

                            <div v-for="item in files" class="col-md-6 margin-bottom-10">
                              <div v-on:click="show_big_image_click(item)"  class="thumb">
                                @verbatim
                                <span class="title">{{item.name.substr(0,5)}}...</span>
                                @endverbatim
                              </div>
                            </div>

                        </div>
                               
                        <div >
                            <button v-if="next_page_cats!=null"  v-on:click="load_more()"  class="btn btn-block btn-primary" type="button">
                                {{__('general.LOAD_MORE_LABEL') }} 
                            </button>
                        </div>
                                          
                            
                            


              
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


@endsection
@section('footer_scripts')
<script src="{{ asset('assets/js/frontend/media_video.js') }}"></script>
<script src="{{ asset('assets/js/frontend/user_follow.js') }}"></script>
@endsection