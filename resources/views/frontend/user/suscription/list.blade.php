@extends('layouts.user_panel')
@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ __(\Session::get('success')) }}</p>
      </div><br />
    @endif
    @if (\Session::has('error'))
      <div class="alert alert-danger">
        <p>{{ __(\Session::get('error')) }}</p>
      </div><br />
    @endif
    <section class="content">
      <!-- COLOR PALETTE -->
      <div class="box box-default color-palette-box">
  
        <h3>{{ __('pricetable.suscription.SUSCRIPTIONS_LABEL') }}</h3>
       
        @if(user_have_active_suscriptions($current_user,$current_date) == false)
          <p>{{ __('pricetable.suscription.SELECT_SUSCRIPTIONS_LABEL') }}</p>
          <table class="table">
            @foreach($items  as $index => $item)
            <tr>
              <td>{{$item['name']}}<td>
              <td>{{$item['price']}} usd<td>
              <td>
                <button data-id="{{$item['id']}}" class="btn select_suscription small btn-primary">
                  {{ __('pricetable.suscription.SELECT_LABEL') }}
                </button>
              </td>
            </tr>
            @endforeach  
          </table>
        @else
          <table class="table">
             @foreach(user_last_active_suscriptions($current_user)  as $index => $item)
              <tr>
                <td>{{$item->suscription->name}}<td>
                <td>{{$item->suscription->price}} usd<td>
                <td>Expira: {{$item['expire']}}<td>
                <td>
                  <a href="{{ route('account_users_suscription_cancel')}}" class="btn small btn-primary">
                    {{ __('general.CANCEL_LABEL') }}
                  </a>
                </td>
              </tr>
            @endforeach  
          </table>

        @endif
      <!-- /.box-body -->
      </div>
    </section>
@endsection
@section('footer_scripts')
<script type="text/javascript">
  var load = function(e)
  {
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
      }
    
    var error = false;
    
   

    var procesar = function(e)
    {
        if( error == false)
        {
          var xsrfToken = decodeURIComponent(getCookie('XSRF-TOKEN'));
         
          var id  = $(this).attr('data-id');
 
          
        
          $.ajaxSetup({
              headers: {
                  'Accept': 'application/json',
                  'X-XSRF-TOKEN': xsrfToken
              }
          });
         
          $.post( "{{route('payments_paypal_process')}}", { 
         
            id:id,
          }, function( data ) 
          {
            if(data.success == true)
            {
              window.location = data.approval_url;
            }
          });  
        }
        
    }
    $(".select_suscription").on('click',procesar)
  }

  $(window).on('load',load);

</script>
@endsection