@extends('layouts.user_panel')

@section('content')

  @if (\Session::has('success'))
    <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{ \Session::get('error') }}</p>
    </div><br />
  @endif
  <div style="padding: 15px;">
    <h3 class="box-title">
      {{ __('Editar Comentario Producto') }}
    </h3>
    <form role="form" method="post" action="{{$route}}">
       @csrf
      <div class="box-body">
        <div class="form-group">              
          <label for="exampleInputEmail1">{{__('user.IMAGE_LABEL') }}</label>
          <input type="file" name="logo">
        </div>
        
        <div class="form-group">
          <label for="exampleInputEmail1">
            {{ __('testimonials.COMPANY_LABEL') }}
          </label>
          <input type="text" name="company" value="{{$item->company}}" required class="form-control" >
        </div>

        <div  class="form-group">
          <label>
            {{ __('marketplace.articles.CONTENT_LABEL') }}
          </label>
          <textarea name="content" id="mytextarea"  class=" form-control" >{{$item->content}}</textarea>
          <br>
        </div> 
        <div class="form-group">
          <label for="exampleInputEmail1">
            {{ __('Rating') }}
          </label>
          <div id="stars-existing" style="margin-top: 5px; margin-bottom: 5px; background: #fff; border-radius: 5px; padding: 5px; display: inline-block;" class="starrr" data-rating='5'></div>
          <input id="id_rating" type="hidden" name="rating" />
        </div>                       
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">
          {{ __('ENVIAR TESTIMONIO') }}
        </button>
      </div>
    </form>
  
  </div>
@endsection
@section('footer_scripts')
  <script src="{{ asset('assets/js/starrr.js') }}"></script>
@endsection
@section('custom_styles')
  <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
@endsection


