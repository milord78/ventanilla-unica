@extends('layouts.user_panel')

@section('content')

    @if (\Session::has('success'))
    <div class="alert alert-success">
      <p>{{ \Session::get('success') }}</p>
    </div><br />
  @endif
  @if (\Session::has('error'))
    <div class="alert alert-danger">
      <p>{{ \Session::get('error') }}</p>
    </div><br />
  @endif
  <div class="box">
    <h3 class="box-title">{{ __('Informacion Basica') }}</h3>
    <form role="form" method="post" action="{{$route}}">
       @csrf
      <div class="box-body">
         <div class="box-body">
          <label for="exampleInputEmail1">{{__('Foto') }}</label>
          <input type="file" name="thumbnail">
          <br>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Mombre') }}</label>
          <input type="text" name="firstname"  value="{{$user->firstname}}" required class="form-control" >
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Segundo Nombre') }}</label>
          <input type="text" name="midname" value="{{$user->midname}}"  required class="form-control" >
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Apellido') }}</label>
          <input type="text" name="lastname"  value="{{$user->lastname}}" required class="form-control" >
        </div> 

         <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Nick') }}</label>
          <input type="text" name="nick"  value="{{$user->nick}}" required class="form-control" >
        </div> 
    
         <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Email') }}</label>
          <input type="text" name="email"  value="{{$user->email}}" required class="form-control" >
        </div> 

        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Direccion') }}</label>
          <input type="text" name="address"  value="{{$user->address}}" required class="form-control" >
        </div> 

         <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Telefono') }}</label>
          <input type="text" name="phone"  value="{{$user->phone}}" required class="form-control" >
        </div>         


        <div class="box-body">
          <label for="exampleInputEmail1">{{__('runnable.professionals_categories.PROFESSION_LABEL') }}</label>
           <select class="form-control" name="profession_id" required>
            @foreach($professions as $index => $website)
               <option <?php echo ($user->profession_id == $website['id'])?"selected":""; ?>  value="{{$website['id']}}">{{__($website['name'])}}</option>
             @endforeach
          </select>
          
          <br>
        </div>


        <div class="box-body">
          <label for="exampleInputEmail1">{{__('runnable.province.PROVINCE_LABEL') }}</label>
           <select class="form-control" name="province_id" required>
            @foreach($provinces as $index => $website)
               <option <?php echo ($user->province_id == $website['id'])?"selected":""; ?>  value="{{$website['id']}}">
                  {{__($website['name'])}}
               </option>
             @endforeach
          </select>
          
          <br>
        </div>


                                 
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
      </div>
    </form>
  
    <h3 class="box-title">{{ __('Cambiar contraceña') }}</h3>
     @if (\Session::has('password_sucess'))
        <div class="alert alert-success">
          <p>{{  __(\Session::get('password_sucess')) }}</p>
        </div><br />
      @endif
      @if (\Session::has('password_error'))
        <div class="alert alert-error">
          <p>{{  __(\Session::get('password_error')) }}</p>
        </div><br />
      @endif

    <form role="form" method="post" action="{{ $route_update_password }}">
       @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Password') }}</label>
          <input type="password" name="password1" required class="form-control" >
        </div> 
        <div class="form-group">
          <label for="exampleInputEmail1">{{ __('Repeat Password') }}</label>
          <input type="password" name="password2" required class="form-control" >
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ __('Cambiar Contraceña') }}</button>
      </div>
    </form>
   
  </div>
@endsection


