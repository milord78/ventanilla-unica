@extends('layouts.app3')
@section('content')
<div style="padding:15px;">
    <?php $settigns = get_settings(); ?>
    <h3>
    Felicidades has comenzado con el primer paso para registrarte como aspirate a 
    {{$settigns->nombre}}
    </h3>
    <p>El siguiente paso es llenar el formulario con tus datos personales 
    Recuerda que para el correcto llenado debes contar con toda la información. </p>
    <p>Si en este momento no cuentas con los datos completos puedes continuar con tu registro posteriormente usando el correo y contraseña que proporcionaste anteriormente </p>
    <div class="text-center">
        <a href="{{route('account_users_me')}}" class="btn btn-success">
        YA CUENTO CON TODA MI DOCUMENTACIÓN 
        </a>
    </div>
</div>

@endsection
@section('custom_styles')
@endsection