<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 
    'articles'=>[
        'ARTICLES_LABEL'                    =>  'Articles',
        'ARTICLE_LABEL'                     =>  'Article',
        'EXCERPT_LABEL'                     =>  'Excerpt',
        'CONTENT_LABEL'                     =>  'Content',
        'PUBLISH_DATE_LABEL'                =>  'Publish date',
        'PUBLISH_DAYS_LABEL'                =>  'Publish Days',
        'CATEGORY_LABEL'                    =>  'Category',
        'VIDEO_CODE_LABEL'                  =>  'Video Embed Code',
        'IS_ON_FEED_LABEL'                  =>  'is on feed',
        'IS_FEATURED_LABEL'                 =>  'is featured',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Article created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Article updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Article deleted successfully',
        'EXISTS_LABEL'                      =>  'Article exists',
        'VIEW_DETAILS_LABEL'                =>  'Read more',
        'DISCOUNTS_LABEL'                   =>  'Discounts',
        'GIFTCARD_LABEL'                    =>  'GiftCard',
        'TAXES_LABEL'                       =>  'Taxes',
        'IN_OFFER_LABEL'                    => "In offer ?",
        'IN_OFFER_START_DATE_LABEL'         => "In offer Start Date",
        'IN_OFFER_END_DATE_LABEL'           => "In offer End Date",
        'AUTOR_ERROR_LABEL'                 => "You must be the autor from this post",
        'PUBLISH_LABEL'                     =>  'Publish',
        'FEATURED_PRODUCT_LABEL'            =>  'Featured Product',
        'RECENTLY_VIEWED_LABEL'             =>  'Recently Viewed',
        'RELATED_PRODUCT_LABEL'             =>  'Related Product',
        'QUESTION_AND_ASWER_LABEL'          =>  'Question and Answer',
        'LASTEST_QUESTIONS_LABEL'           =>  'Latest Questions',
        'RESULT_FOR_LABEL'                  =>  'Laests Results For:',
        'PUBLICATIONS_LABEL'                =>  'Publications',
        'CALIFICATIONS_LABEL'               =>  'Califications',
        'RECENT_CALIFICATIONS_LABEL'        =>  'Recent Califications',
        'PRICES_LABEL'                      =>  'Prices',
        'STATUS_LABEL'                      =>  'Status',
        'PUSLISH_DATE'                      =>  'Publish Date',
        'MIN_PRICES_LABEL'                  =>  'Min price',
        'MAX_PRICES_LABEL'                  =>  'Max price',
        'USED_LABEL'                        =>  'Used',
        'NEW_LABEL'                         =>  'New',
        'TYPE_ADDRESS_SEARCH_LABEL'         =>  'Type an Address ',
        'INFINITE_LABEL'                    =>  'Infinite',
        'CONDITION_LABEL'                   =>  'Condition'
        
        
    ],
    'categories'=>[
    
        'CATEGORIES_LABEL'                  => 'Categories',
        'CATEGORY_LABEL'                    => 'Category',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Category created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Category updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Category deleted successfully',
        'EXISTS_LABEL'                      =>  'Category exists'
    ],
    'currency'=>[
    
        'CURRENCIES_LABEL'                  =>  'Currencies',
        'CURRENCY_LABEL'                    =>  'Currency',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Currency created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Currency updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Currency deleted successfully',
        'EXISTS_LABEL'                      =>  'Currency exists',
        "AMOUNT_LABEL"                      =>  'Amount',
        "CODE_LABEL"                        =>  'Code',
    ],

    'question'=>[
        'REPORTS_LABEL'                     =>  'Question Reported',
        'REPORT_LABEL'                      =>  'Question Reported',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Question Report created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Question Report updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Question Report deleted successfully',
        'EXISTS_LABEL'                      =>  'Question Report exists',
        'COMMENT_LABEL'                     =>  'Question',
        'ARTICLE_LABEL'                     =>  'Article',
        'STRIKE_CREATED_SUCESSFULLY_LABEL'  =>  'You had been received an strike ',
        'QUESTIONS_LABEL'                   =>  'Questions',
        'WRITE_ANSWER_LABEL'                =>  'Write Answer',
        'WRITE_QUESTION_LABEL'              =>  'Write Question',
        'ANWSERS_LABEL'                     =>  'Answers',
        'NO_ANWSERS_LABEL'                  =>  'No Answers',

    ],
    'company'=>[
    
        'COMPANIES_LABEL'                   =>  'Companies',
        'COMPANY_LABEL'                     =>  'Company',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Company created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Company updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Company deleted successfully',
        'EXISTS_LABEL'                      =>  'Company exists',

        'CATEGORIES_LABEL'                  => 'Company Categories',
        'CATEGORY_LABEL'                    => 'Company Category'
    ],

];
