<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 

    'suscription'=>[
    
        'SUSCRIPTIONS_LABEL'                  => 'Suscriptions',
        'SUSCRIPTION_LABEL'                    => 'Suscription',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Suscription created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Suscription updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Suscription deleted successfully',
        'EXISTS_LABEL'                      =>  'Suscription exists',
        'START_DATE_LABEL'                  =>  'Start Date',
        'END_DATE_LABEL'                    =>  'End Date',
        'ACTIVE_LABEL'                      =>  'Active',
        'INACTIVE_LABEL'                    =>  'Inactive',



    ],
    
];
