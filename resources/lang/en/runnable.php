<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    
    'wall'=>[
        'WALLS_LABEL'                       =>  'Walls',
        'WALL_LABEL'                        =>  'Wall',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Wall created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Wall updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Wall deleted successfully',
        'EXISTS_LABEL'                      =>  'Wall exists',
        'MARK_AS_INAPROPIATE_CONTENT_LABEL' =>  'inapropiate content'
    ],
    'categories'=>[
        'CATEGORIES_LABEL'                  =>  'Categories',
        'CATEGORY_LABEL'                    =>  'Category',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Category created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Category updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Category deleted successfully',
        'EXISTS_LABEL'                      =>  'Category exists',
        'ALL_CATEGORIES_LABEL'              =>  'All Categories',
    ],


    'services'=>[
        'SERVICES_LABEL'                    =>  'Services',
        'SERVICE_LABEL'                     =>  'Service',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Service created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Service updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Service deleted successfully',
        'EXISTS_LABEL'                      =>  'Service exists'
    ],

    'club'=>[
        'CLUB_LABEL'                        =>  'Club',
        'CLUBS_LABEL'                       =>  'Clubs',
        'MY_CLUBS_LABEL'                    =>  'My Clubs',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Club created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Club updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Club deleted successfully',
        'EXISTS_LABEL'                      =>  'Club exists',
        'PUBLIC_LABEL'                      =>  'Public',
        'PRIVATE_LABEL'                     =>  'Private',
        'MEMBERS_LABEL'                     =>  'Members',
        'USER_INVITATION_LABEL'             =>  'User invitation?',
        'VISIBILITY_LABEL'                  =>  'Visibility',
        'VIEW_CLUB_LABEL'                   =>  'View Club',
        'JOIN_CLUB_LABEL'                   =>  'Join to Club',
        'CREATED_BY_LABEL'                  =>  'Created by',
        'MANAGE_INVITATIONS_LABEL'          =>  'Manage invitations',
        'INVATION_CREATED_SUCCESSFULLY_LABEL' => 'Invitation sended successfully,please await to be acepted',
        'INVATION_ACEPTED_LABEL'            => 'Invitation acepted successfully',
        'ACEPT_INVITATION_LABEL'            => 'Acept invitation',
        'THE_FOLLOWING_USER_WANTS_TO_JOIN_TO_CLUB_LABEL'            => 'The following user wants to join to club',
        'YOUR_ALERDAY_INVITATION_CREATED_SUCCESSFULLY_LABEL'=>'Hi there you already sent invitation this club',
    ],

    'province'=>[
        'PROVINCE_LABEL'                    =>  'Province',
        'PROVINCES_LABEL'                   =>  'Provinces',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Province created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Province updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Province deleted successfully',
        'EXISTS_LABEL'                      =>  'Province exists'
    ],

    'distances'=>[
        'DISTANCE_LABEL'                    =>  'Distance',
        'DISTANCES_LABEL'                   =>  'Distances',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Distance created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Distance updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Distance deleted successfully',
        'EXISTS_LABEL'                      =>  'Distance exists',
        'ALL_DISTANCES_LABEL'               => 'All distances',
    ],
    'modalities'=>[
        'MODALITY_LABEL'                    =>  'Modality',
        'MODALITIES_LABEL'                  =>  'Modalities',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Modality created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Modality updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Modality deleted successfully',
        'EXISTS_LABEL'                      =>  'Modality exists',
        'ALL_MODALITIES_LABEL'              => 'All modalities',
    ],


    'professionals_categories'=>[
        'CATEGORIES_LABEL'                  =>  'Professional Categories',
        'CATEGORY_LABEL'                    =>  'Professional Category',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Category created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Category updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Category deleted successfully',
        'EXISTS_LABEL'                      =>  'Category exists',
        'PROFESSION_LABEL'                  =>  'Profession',

    ],



    'carrers'=>[
        'MY_CARRERS'                        =>  'My Carrers',
        'VIEW_EVENTS'                       =>  'View Events',
        'OPINIONS_LABEL'                    =>  'User Opinions',
        'MY_SUSCRIPTION_LABEL'              =>  'My suscriptions',
        'START_DATE_LABEL'                  =>  'Start inscriptions',
        'END_DATE_LABEL'                    =>  'End inscriptions',
        'EVENT_DATE_LABEL'                  =>  'Event Date',
        'TYPE_LABEL'                        =>  'Carrer type',
        'BENEFITS_TO_SOLIDARY_CAUSES'       =>  'Benefits to solidary Causes',
        'INSCRIPTION_PRICES_LABEL'          =>  'Inscription Price',
        'PERIOD_PRICE_LABEL'                =>  'Price outside period',
        'DISCOUNT_PRICE_LABEL'              =>  'Discount Price',
        'WEBSITE_LABEL'                     =>  'Website web',
        'RATING_LABEL'                      =>  'Rating',
        'COMMENTS_LABEL'                    =>  'Comments',

        'INSCRIPTIONS_AMOUNT_LABEL'         =>  'Max inscriptions Amount',
        'AVITUALLAMIENTOS_LIQUIDOS_LABEL'   =>  'Avituallamientos liquidos',
        'AVITUALLAMIENTOS_SOLIDOS_LABEL'    =>  'Avituallamientos solidos',
        'HEIGHT_LABEL'                      =>  'Height',
        'DECSENCE_LABEL'                    =>  'Descense',
        'MAX_TIME_LABEL'                    =>  'Max time',
        'PERCENTAJE_ALPHAT_LABEL'           =>  'Porcent asphalt',
        'ASCENCE_LABEL'                     =>  'Ascense',
        'START_PLACE_LABEL'                 =>  'Star place',
        'END_PLACE_LABEL'                   =>  'END place',
        'TRAVEL_LABEL'                      =>  'Travel',
        'OPINIONS_LABEL'                    =>  'Opinions',
        'INFORMATION_LABEL'                 =>  'Information',
        'LET_OPINION_LABEL'                 =>  'Let opinion',
        'WRITE_COMMENT'                     =>  'Write Comment',
        'HOW_YOU_VALORATE_THIS_CARRER'      =>  'How you valorate this carrer',
        'CARRER'                            =>  'Carrer',
        'PRICE_QUALITY'                     =>  'Precio Calidad',
        'AMBIENT'                           =>  'Ambiente',
        'ORGANIZATION'                      =>  'Organization',
        'GENERAL_CLASIFICATION'             =>  'General Clasification',
        'RESUME'                            =>  'Resume',
        'VOTES'                             =>  'Vote',
        'VERY_GOOD'                         =>  'Very good',
        'BAD'                               =>  'Bad',
        'VERY_BAD'                          =>  'Very bad',
        'THE_MOST_VOTE'                     =>  'The most vote', 
        'RUTE'                              =>  'Rute', 
       
        'SHARE'                             =>   'Share this carrer',
        'FOLLOW'                            =>   'Follow',
        'SUSCRIBE'                          =>   'Suscribre',
        'UNUSUCRIBE'                        =>   'Unsuscribe',
        'PARTICIPANTS'                      =>   'Participants',
    ],

   
      
];
