<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 



    'pricetable'=>[
            

        'PRICE_TABLES_LABEL'                =>  'Price tables',
        'PRICE_TABLE_LABEL'                 =>  'Price Tables',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Price tables created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Price tables updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Price tables deleted successfully',
        'EXISTS_LABEL'                      =>  'Price tables exists',
        'SELECT_SUSCRIPTIONS_LABEL'         =>  'Select Price tables',
        'SELECT_LABEL'                      =>  'Select',
        'TASA_IVA_LABEL'                    =>  'Percentage Iva',
        'PUBLISH_DAYS_LABEL'                =>  'Publish Days'
    ],
    
];
