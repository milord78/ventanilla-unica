<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'TAGS_LABEL'                    =>  'Tags',
    'TAG_LABEL'                    =>  'Tag',
    
      'CREATED_SUCCESSFULLY_LABEL' =>  'Tag created successfully',
    'UPDATE_SUCESSFULLY_LABEL'          =>  'Tag updated successfully',
    'DELETED_SUCESSFULLY_LABEL'         =>  'Tag deleted successfully',
    'USER_CREATED_SUCCESSFULLY_LABEL'   =>  'Tag created successfully',
    'EXISTS_LABEL'                      =>  'Tag exists'

    
];
