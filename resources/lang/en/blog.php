<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'pages'=>[
        'PAGES_LABEL'                    =>  'Pages',
        'PAGE_LABEL'                     =>  'Page',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Page created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Page updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Page deleted successfully',
        'EXISTS_LABEL'                      =>  'Page exists',
        'VIEW_DETAILS_LABEL'                =>  'Read more'
    ],
     'comment'=>[
        'COMMENTS_LABEL'                    =>  'Comments',
        'COMMENTS_LABEL'                     =>  'Comment',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Comment created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Comment updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Comment deleted successfully',
        'EXISTS_LABEL'                      =>  'Comment exists',
        'VIEW_DETAILS_LABEL'                =>  'Read more'
    ],
     'post'=>[
        'POSTS_LABEL'                    =>  'News',
        'POST_LABEL'                     =>  'News',
        'EXCERPT_LABEL'                     =>  'Excerpt',
        'CONTENT_LABEL'                     =>  'Content',
        'IS_ON_FEED_LABEL'                  =>  'is on feed',
        'IS_FEATURED_LABEL'                 =>  'is featured',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'News created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'News updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'News deleted successfully',
        'EXISTS_LABEL'                      =>  'News exists',
        'VIEW_DETAILS_LABEL'                =>  'Read more'
    ],
    'articles'=>[
        'ARTICLES_LABEL'                    =>  'Articles',
        'ARTICLE_LABEL'                     =>  'Article',
        'EXCERPT_LABEL'                     =>  'Excerpt',
        'CONTENT_LABEL'                     =>  'Content',
        'IS_ON_FEED_LABEL'                  =>  'is on feed',
        'IS_FEATURED_LABEL'                 =>  'is featured',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Article created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Article updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Article deleted successfully',
        'EXISTS_LABEL'                      =>  'Article exists',
        'VIEW_DETAILS_LABEL'                =>  'Read more'
    ],
    'categories'=>[
    
        'CATEGORIES_LABEL'                  => 'Categories',
        'CATEGORY_LABEL'                    => 'Category',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Category created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Category updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Category deleted successfully',
        'EXISTS_LABEL'                      =>  'Category exists'
    ],
    'especifications' => [
    
        'ESPECIFICATION_LABEL'                  => 'Especification',
        'ESPECIFICATIONS_LABEL'                    => 'Especifications',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Especification created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Especification updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Especification deleted successfully',
        'EXISTS_LABEL'                      =>  'Especification exists',
        'meta' => [
    
        'NAME_LABEL'                  => 'Title',
        'VALUE_LABEL'                    => 'Value',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Especification Meta created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Especification Meta updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Especification Meta deleted successfully',
        'EXISTS_LABEL'                      =>  'Especification Meta exists'
        ],
    ],
    'price' => [
        "NAME_LABEL"                        =>  'Name',
        "PRICE_LABEL"                       =>  'Price',
        "PAYMENT_METHOD_LABEL"              =>  'Payment method',
        "COUNTRY_METHOD_LABEL"              =>  'Country',
        "DELIVERY_LABEL"                    =>  'Delivery',
        "LINK_LABEL"                        =>  'Link',
        "COMPANY_LABEL"                     =>  'Company',
        "FREE_SHIPPING_LABEL"               =>  'Free shipping',
        'PRICE_LABEL'                       =>  'Price',
        'PRICES_LABEL'                      =>  'Prices',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Price created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Price updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Price deleted successfully',
        'EXISTS_LABEL'                      =>  'Price exists',
        'meta' => [
    
            'NAME_LABEL'                        => 'Title',
            'VALUE_LABEL'                       => 'Value',
            'CREATED_SUCCESSFULLY_LABEL'        =>  'Price Meta created successfully',
            'UPDATE_SUCESSFULLY_LABEL'          =>  'Price Meta updated successfully',
            'DELETED_SUCESSFULLY_LABEL'         =>  'Price Meta deleted successfully',
            'EXISTS_LABEL'                      =>  'Price Meta exists'
        ],

    ],

];
