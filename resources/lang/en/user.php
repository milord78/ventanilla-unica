<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'USERS_LABEL'                       => 'Users',
    'PROFILE_LABEL'                     => 'Profile',
    'lOG_OUT_LABEL'                     => 'log out',
    'IMAGE_LABEL'                       => 'Picture',   
    'FIRST_NAME_LABEL'                  => 'Firstname',
    'LAST_NAME_LABEL'                   => 'Lastname',
    'MIDDLE_NAME_LABEL'                 => 'Midname',
    'PERMISSION_LABEL'                  => 'Permission',
    'PHOTO_LABEL'                       => 'Photo',
    'WALLET_ADDRESS_LABEL'              => 'Wallet Address',
    'PASSWORD_LABEL'                    => 'Password',
    'REPEAT_PASSWORD_LABEL'             => 'Repeat password',
    'CHANGE_PASSWORD_LABEL'             => 'Change password',
    'BASIC_INFORMATION_LABEL'           => 'Basic information',
    'USER_LABEL'                        => 'User',
    'VIEW_MY_PROFILE'                   => 'View my Profile',
    'UPDATE_SUCESSFULLY_LABEL'          =>  'User updated successfully',
    'DELETED_SUCESSFULLY_LABEL'         =>  'User deleted successfully',
    'PASSWORD_MICTHMACH'                =>  'Error Password michtmach',
    'PASSWORD_UPDATE_SUCESSFULLY_LABEL' =>  'Password updated successfully',
    'USER_CREATED_SUCCESSFULLY_LABEL'   =>  'Users created successfully',
    'EMAIL_EXISTS_LABEL'                =>  'Oops email exists',
    'PHONE_LABEL'                       =>  'Phone',
    'FOLLOW_LABEL'                      =>  'Follow',
    'UNFOLLOW_LABEL'                    =>  'unFollow',
    'VIEW_PROFILE_LABEL'                =>  'View profile',
    'ALL_PROVINCES_LABEL'               => 'All provinces',
    'ALL_PROFESSIONS_LABEL'             => 'All professions',
    'MY_ACCOUNT_LABEL'                  =>  'My Account',       
];
