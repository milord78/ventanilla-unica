<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 

     'MEDIA_LABEL'                       =>  'Media',
     'SELECT_MEDIA_LABEL'                =>  'Select Media',
     'MEDIA_DIRECTORY_LABEL'             =>  'Media Directory',
     'FILE_PLURAL_LABEL'                 =>  'File',
     'PARENT_LABEL'                      =>  'Parent',
     'ITEM_DUPLICATE_LABEL'              =>  'Item Duplicate',
     'DROP_FILES_LABEL'                  =>  'Drop files',
     'UNLOADED_FILES_LABEL'              =>  'Upload your favorite Files',
     'DROP_FILES_VIDEOS_LABEL'           =>  'Drop Videos',
     'UNLOADED_FILES_VIDEOS_LABEL'       =>  'Upload your favorite Videos',
     'MY_PICTURES'                       =>  'My Pictures',
     'MY_VIDEOS'                         =>  'My Videos',
     'VIEW_ATTACTMENT_LABEL'                   =>  'View Attachment',
     'NEW_ALBUM'                         =>  'New Album',
     'MY_ALBUMS'                         =>  'My Album',
     'ALBUM_LABEL'                       =>  'Album',
     'GALLERY_LABEL'                     =>  'Gallery',
     'CARTEL_LABEL'                      =>  'Cartel (250x250)',
     'HEADER_LABEL'                      =>  'Header (1920x350)',
     'HEADER_LANDING_LABEL'              =>  'Header Landing (1920x350)',
     'LOGO_LABEL'                        =>  'Logo (225X225)',
     'DROP_ARCHIVES_LABEL'              =>  'Upload your favorite Files',
     'UNLOADED_ARCHIVES_LABEL'       =>  'Upload your favorite Files',
     


];  
