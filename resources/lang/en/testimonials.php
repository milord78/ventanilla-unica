<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 
 
   
    'TESTIMONIALS_LABEL'    =>  'Testimonials',
    'TESTIMONIAL_LABEL'     =>  'Testimonial',
    'SUBMIT_LABEL'          =>  'Submit Testimonial',
    'COMPANY_LABEL'         =>  'Company',
    'RATING_LABEL'          =>  'Rating',

];
