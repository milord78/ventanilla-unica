<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */


    
    'NAVIGATIONS_LABEL'                     =>  'Navigations',
    'NAVIGATION_LABEL'                      =>  'Navigation',
    'POSITION_LABEL'                        =>  'Position',
    'NAVIGATIONS_POSITIONS_LABEL'            =>  'Navigation Positions',
    'NAVIGATION_POSITION_LABEL'             =>  'Navigation Position',    
    'CREATED_SUCCESSFULLY_LABEL'            =>  'Navigation created successfully',
    'UPDATE_SUCESSFULLY_LABEL'              =>  'Navigation updated successfully',
    'DELETED_SUCESSFULLY_LABEL'             =>  'Navigation deleted successfully',
    'USER_CREATED_SUCCESSFULLY_LABEL'       =>  'Navigation created successfully',
    'EXISTS_LABEL'                          =>  'Navigation exists',
    'SELECT_NAVIGATIONS_LABEL'              =>  'Select Navigation',
    'SELECT_ITEM'                           =>  'Select Item',
    'SELECT_PARENT_ITEM'                    =>  'Select Parent Item',
    'SELECT_NAVIGATIONS_PARENT_LABEL'                =>  'Select Navigation Parent Item',
    'LIST_CHILD_LABEL'                      =>  'List Child',
    'SELECT_MODULE_LABEL'                   =>  'Select Module',
    'SELECT_ACTION_LABEL'                   =>  'Select Action'
        
];
