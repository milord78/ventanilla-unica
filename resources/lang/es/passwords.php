<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Su contraceña ha sido cambiada!',
    'sent' => 'Nosotros hemos enviado un email con el link de cambio de contraceña!',
    'token' => 'El password reset token es invalido.',
    'user' => "No podemos encontrar el usuario con la direccion de correo.",

];
