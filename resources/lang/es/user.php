<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'USERS_LABEL'                       => 'Usuario',
    'PROFILE_LABEL'                     => 'Perfil',
    'lOG_OUT_LABEL'                     => 'cerrar session',
    'PHOTO_LABEL'                       => 'Fecha',      
    'FIRST_NAME_LABEL'                  => 'Primer nombre',
    'LAST_NAME_LABEL'                   => 'Apellido',
    'MIDDLE_NAME_LABEL'                 => 'Segundo Nombre',
    'PERMISSION_LABEL'                  => 'Permisos',
    'PHOTO_LABEL'                       => 'Fotos',
    'IMAGE_LABEL'                       => 'Foto',
    'WALLET_ADDRESS_LABEL'              => 'Wallet Address',
    'PASSWORD_LABEL'                    => 'Contraseña',
    'REPEAT_PASSWORD_LABEL'             => 'Repetir Contraseña',
    'CHANGE_PASSWORD_LABEL'             => 'Cambiar Contraseña',
    'BASIC_INFORMATION_LABEL'           => 'Informacion basica',
    'USER_LABEL'                        => 'Usuario',
    'VIEW_MY_PROFILE'                   => 'Ver mi Perfil',
    'UPDATE_SUCESSFULLY_LABEL'          =>  'Usuario actualizada exitosamente',
    'DELETED_SUCESSFULLY_LABEL'         =>  'Usuario borrada exitosamente',
    'PASSWORD_MICTHMACH'                =>  'Error Contraseña no concuerda',
    'PASSWORD_UPDATE_SUCESSFULLY_LABEL' =>  'Contraseña actualizada exitosamente',
    'USER_CREATED_SUCCESSFULLY_LABEL'   =>  'Usuario creado existosamente',
    'EMAIL_EXISTS_LABEL'                =>  'Oops email existe',
    'PHONE_LABEL'                       => 'Telefono',
    'FOLLOW_LABEL'                      =>  'Seguir',
    'UNFOLLOW_LABEL'                    =>  'Dejar de Seguir',
    'VIEW_PROFILE_LABEL'                =>  'Ver perfil',
    'ALL_PROVINCES_LABEL'               => 'Todas las provincias',
    'ALL_PROFESSIONS_LABEL'             => 'Todas las profesiones',
    'MY_ACCOUNT_LABEL'                  =>  'Mi Cuenta',        

    

];
