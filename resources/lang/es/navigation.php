<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */



    'NAVIGATIONS_LABEL'                 =>  'Navegacion',
    'NAVIGATION_LABEL'                  =>  'Navegacion',
    'POSITION_LABEL'                        =>  'Posicion',
    'NAVIGATIONS_POSITIONS_LABEL'       =>  'Posiciones de Navegacion',
    'NAVIGATION_POSITION_LABEL'         =>  'Posicion Navegacion',
    'CREATED_SUCCESSFULLY_LABEL'        =>  'Navegacion creado exitosamente',
    'UPDATE_SUCESSFULLY_LABEL'          =>  'Navegacion actualizado exitosamente',
    'DELETED_SUCESSFULLY_LABEL'         =>  'Navegacion borrado exitosamente',
    'EXISTS_LABEL'                      =>  'Navegacion existe',
    'SELECT_NAVIGATIONS_LABEL'          =>  'Seleccionar Navegacion',
    'SELECT_ITEM'                       =>  'Seleccionar Item',
    'SELECT_PARENT_ITEM'                =>  'Seleccionar Pariente',
    'SELECT_NAVIGATIONS_PARENT_LABEL'                =>  'Seleccionar Pariente',
    'LIST_CHILD_LABEL'                  =>  'Listar Hijo',

    'SELECT_MODULE_LABEL'                   =>  'Seleccionar Modulo',
    'SELECT_ACTION_LABEL'                   =>  'Seleccionar Accion'
];
