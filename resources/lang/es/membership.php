<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 

    'suscription'=>[
    
        'SUSCRIPTIONS_LABEL'                  => 'Suscripciónes',
        'SUSCRIPTION_LABEL'                    => 'Suscripción',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Suscripción creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Suscripción actualizada exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Suscripción borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Suscripción existe',
        'START_DATE_LABEL'                  =>  'Fecha de inicio',
        'END_DATE_LABEL'                    =>  'Fecha de fin',
        'ACTIVE_LABEL'                      =>  'Activo',
        'INACTIVE_LABEL'                    =>  'Inactivo',
    ],
    
];
