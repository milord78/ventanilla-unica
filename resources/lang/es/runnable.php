<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'wall'=>[
        'WALLS_LABEL'                       =>  'Muros',
        'WALL_LABEL'                        =>  'Muro',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Muro creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Muro actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Muro borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Muro existe',
        'MARK_AS_INAPROPIATE_CONTENT_LABEL' =>  'contenido inapropiado',

    ],

    'categories'=>[
        'CATEGORIES_LABEL'                  =>  'Categorias',
        'CATEGORY_LABEL'                    =>  'Categoria',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Categoria creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Categoria actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Categoria borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Categoria existe',
        'ALL_CATEGORIES_LABEL'              =>  'Todas las Categorias',
    ],


    'services'=>[
        'SERVICES_LABEL'                    =>  'Servicios',
        'SERVICE_LABEL'                     =>  'Servicio',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Servicio creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Servicio actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Servicio borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Servicio existe'
    ],

    'club'=>[
        'CLUB_LABEL'                        =>  'Club',
        'CLUBS_LABEL'                       =>  'Clubs',
        'MY_CLUBS_LABEL'                    =>  'Mis Clubs',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Club creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Club actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Club borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Club existe',
        'INVATION_CREATED_SUCCESSFULLY_LABEL' => 'Invitación enviada existosamente,por favor espera a ser aceptado',
        'INVATION_ACEPTED_LABEL'            => 'Invitacion creada exitosamente',
        'INVATION_ACEPTED_SUCCESSFULLY_LABEL' => 'Invitacion aceptada exitosamente',
        'THE_FOLLOWING_USER_WANTS_TO_JOIN_TO_CLUB_LABEL'            => 'El siguiente usuario se quiere unir al club',
        'YOUR_ALERDAY_INVITATION_CREATED_SUCCESSFULLY_LABEL'=>'Hola ya enviaste una invitación a este club',
        'MANAGE_INVITATIONS_LABEL'          =>  'Administrar invitaciones',
        'PUBLIC_LABEL'                      =>  'Publico',
        'PRIVATE_LABEL'                     =>  'Privado',
        'MEMBERS_LABEL'                     =>  'Miembros',
        'USER_INVITATION_LABEL'             =>  'Los usuarios pueden invitar?',
        'VISIBILITY_LABEL'                  =>  'Visibilidad',
        'VIEW_CLUB_LABEL'                   =>  'Ver Club',
        'JOIN_CLUB_LABEL'                   =>  'Unirme al Club',
        'CREATED_BY_LABEL'                  =>  'Creado por ',
        'ACEPT_INVITATION_LABEL'            => 'Aceptar invitación',
    ],
    'province'=>[
        'PROVINCE_LABEL'                    =>  'Provincia',
        'PROVINCES_LABEL'                   =>  'Provincias',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Provincia creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Provincia actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Provincia borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Provincia existe'
    ],
    'modalities'=>[
        'MODALITY_LABEL'                    =>  'Modalidad',
        'MODALITIES_LABEL'                  =>  'Modalidades',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Modalidad creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Modalidad actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Modalidad borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Modalidad existe',
        'ALL_MODALITIES_LABEL'              =>  'Todas las Modalidades',
    ],
    'distances'=>[
        'DISTANCE_LABEL'                    =>  'Distancia',
        'DISTANCES_LABEL'                   =>  'Distancias',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Distancia creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Distancia actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Distancia borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Distancia existe',
        'ALL_DISTANCES_LABEL'               =>  'Todas las Distancias',
    ],


    'professionals_categories'=>[
        'CATEGORIES_LABEL'                  =>  'Categorias Profesionales',
        'CATEGORY_LABEL'                    =>  'Categoria Profesionales',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Categoria creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Categoria actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Categoria borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Categoria existe',
        'PROFESSION_LABEL'                  =>  'Profesion',
    ],

    'carrers'=>[
        'CARRER_LABEL'                      =>  'Carrera',
        'MY_CARRERS'                        =>  'Mis Carreras',
        'VIEW_EVENTS'                       =>  'Ver Eventos',
        'OPINIONS_LABEL'                    =>  'Opiniones',
        'MY_SUSCRIPTION_LABEL'              =>  'Mis suscripciones',
        'START_DATE_LABEL'                  =>  'Inicio de inscripciones',
        'END_DATE_LABEL'                    =>  'Fin de inscripciones',
        'EVENT_DATE_LABEL'                  =>  'Fecha',
        'TYPE_LABEL'                        =>  'Tipo',
        'BENEFITS_TO_SOLIDARY_CAUSES'       =>  'Beneficios a causas solidarias',
        'INSCRIPTION_PRICES_LABEL'          =>  'Precio de inscripcion',
        'PERIOD_PRICE_LABEL'                =>  'Precio fuera de periodo',
        'DISCOUNT_PRICE_LABEL'              =>  'Precio de descuento',
        'WEBSITE_LABEL'                     =>  'Sitio web',
        'RATING_LABEL'                      =>  'Ranking',
        'COMMENTS_LABEL'                    =>  'Comentarios',
        'INSCRIPTIONS_AMOUNT_LABEL'         =>  'maximo de inscripciones',
        'AVITUALLAMIENTOS_LIQUIDOS_LABEL'   =>  'Avituallamientos liquidos',
        'AVITUALLAMIENTOS_SOLIDOS_LABEL'    =>  'Avituallamientos solidos',
        'HEIGHT_LABEL'                      =>  'Altura maxima',
        'DECSENCE_LABEL'                    =>  'Descenso',
        'MAX_TIME_LABEL'                    =>  'Tiempo maximo',
        'PERCENTAJE_ALPHAT_LABEL'           =>  'Porcentaje asfalto',
        'ASCENCE_LABEL'                     =>  'Ascenso',
        'START_PLACE_LABEL'                 =>  'Lugar de salida',
        'END_PLACE_LABEL'                   =>  'Lugar de llegada',
        'TRAVEL_LABEL'                      =>  'Recorrido',
        'OPINIONS_LABEL'                    =>  'opiniones',
        'INFORMATION_LABEL'                 =>  'Información',
        'DESCRIPTION_LABEL'                 =>  'Description',
        'LET_OPINION_LABEL'                 =>  'Dejar opinion',
        'WRITE_COMMENT'                     =>  'Escribir un comentario',
        'HOW_YOU_VALORATE_THIS_CARRER'      =>  'Como valoras esta carrera',
        'CARRER'                            =>  'Recorrido',
        'PRICE_QUALITY'                     =>  'Precio Calidad',
        'AMBIENT'                           =>  'Ambiente',
        'ORGANIZATION'                      =>  'Organización',
        'EXELENT'                           =>  'Exelente',
        'GENERAL_CLASIFICATION'             =>  'Clasificación General',
        'RESUME'                            =>  'Resumen',
        'VOTES'                             =>  'Voto',
        'VERY_GOOD'                         =>  'Muy bueno',
        'BAD'                               =>  'Malo',
        'VERY_BAD'                          =>  'Pesimo',
        'THE_MOST_VOTE'                     =>  'lo mas votado', 
        'RUTE'                              =>  'Hoja de Ruta', 
        'SHARE'                             =>'Compartir esta carrera',
        'FOLLOW'                            =>'Seguir',
        'SUSCRIBE'                          =>'Inscribirse',
        'UNUSUCRIBE'                        =>'Inscrito',
        'PARTICIPANTS'                      =>'Participantes',
    ],   
];
