<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

  
    'pricetable'=>[
        
        'PRICE_TABLES_LABEL'                =>  'Tabla de Precio',
        'PRICE_TABLE_LABEL'                 =>  'Tabla de Precios',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Tabla de Precios created successfully',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Tabla de Precios updated successfully',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Tabla de Precios deleted successfully',
        'EXISTS_LABEL'                      =>  'Tabla de Precios exists',
        'SELECT_SUSCRIPTIONS_LABEL'         =>  'Seleccionar Tabla de Precio',
        'SELECT_LABEL'                      =>  'Seleccionar',
        'TASA_IVA_LABEL'                    =>  'Tasa Iva',
        'PUBLISH_DAYS_LABEL'                =>  'Dias'
    ],
    

    
];
