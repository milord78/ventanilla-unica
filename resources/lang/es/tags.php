<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'TAGS_LABEL'                    =>  'Tags',
    'TAG_LABEL'                    =>  'Tag',
 
    'CREATED_SUCCESSFULLY_LABEL' =>  'Tag creado exitosamente',
    'UPDATE_SUCESSFULLY_LABEL'          =>  'Tag actualizado exitosamente',
    'DELETED_SUCESSFULLY_LABEL'         =>  'Tag borrado exitosamente',
    'USER_CREATED_SUCCESSFULLY_LABEL'   =>  'Tag creado exitosamente',
    'EXISTS_LABEL'                      =>  'error Tag existe'
];
