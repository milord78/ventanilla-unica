<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 
    'articles'=>[


        'ARTICLES_LABEL'                    =>  'Articulos',
        'ARTICLE_LABEL'                     =>  'Articulo',
        'EXCERPT_LABEL'                     =>  'Extracto',
        'CONTENT_LABEL'                     =>  'Contenido',
        'PUBLISH_DATE_LABEL'                =>  'Fecha de publicación',
        'PUBLISH_DAYS_LABEL'                =>  'Dias de publicacion',
        'CATEGORY_LABEL'                    =>  'Categoria',
        'VIDEO_CODE_LABEL'                  =>  'Video Embed Code',
        'IS_ON_FEED_LABEL'                  =>  'mostrar en Feed?',
        'IS_FEATURED_LABEL'                 =>  'es destacado?',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Articulo creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Articulo actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Articulo borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Articulo existe',
        'VIEW_DETAILS_LABEL'                =>  'Leer mas',
        'DISCOUNTS_LABEL'                   =>  'Descuentos',
        'GIFTCARD_LABEL'                    =>  'GiftCard',
        'TAXES_LABEL'                       =>  'Impuestos',
        'IN_OFFER_LABEL'                    => "En Oferta ?",
        'IN_OFFER_START_DATE_LABEL'         => "En Oferta Fecha inicio",
        'IN_OFFER_END_DATE_LABEL'           => "En Oferta Fecha fin",
        'AUTOR_ERROR_LABEL'                 => "Tienes que ser el autor de este articulo",
        'PUBLISH_LABEL'                     =>  'Comenzar a publicar',
        'FEATURED_PRODUCT_LABEL'            =>  'Productos Destacados',
        'RECENTLY_VIEWED_LABEL'             =>  'Vistos recientemente',
        'RELATED_PRODUCT_LABEL'             =>  'Productos relacionados',
        'QUESTION_AND_ASWER_LABEL'          =>  'Preguntas y respuestas',
        'LASTEST_QUESTIONS_LABEL'           =>  'Ultimas preguntas',
        'RESULT_FOR_LABEL'                  =>  'Resultados Para:',
        'PUBLICATIONS_LABEL'                =>  'Publicaciones',
        'CALIFICATIONS_LABEL'               =>  'Calificaciones',
        'RECENT_CALIFICATIONS_LABEL'        =>  'Calificaciones Recientes',
        'PRICES_LABEL'                      =>  'Precios',
        'STATUS_LABEL'                      =>  'Estado',
        'PUSLISH_DATE'                      =>  'Publciado hace',
        'MIN_PRICES_LABEL'                  =>  'Precio minimo',
        'MAX_PRICES_LABEL'                  =>  'Precio maximo',
        'USED_LABEL'                        =>  'Used',
        'NEW_LABEL'                         =>  'New',
        'TYPE_ADDRESS_SEARCH_LABEL'         =>  'Escribe una dirección',
        'INFINITE_LABEL'                    =>  'Infinito',
        'CONDITION_LABEL'                   =>  'Condicion'
    ],
    'categories'=>[
        'CATEGORIES_LABEL'                  =>  'Categorias',
        'CATEGORY_LABEL'                    =>  'Categoria',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Categoria creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Categoria updated exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Categoria borrado exitosamente',
        'EXISTS_LABEL'                      =>  'error Categoria existe'
    ],

    
    'question'=>[
    
        'REPORTS_LABEL'                     =>  'Preguntas Reportadas',
        'REPORT_LABEL'                      =>  'Preguntas Reportada',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Preguntas Reportada creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Preguntas Reportada updated exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Preguntas Reportada borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Preguntas Reportada existe',
        'COMMENT_LABEL'                     =>  'Pregunta',
        'ARTICLE_LABEL'                     =>  'Articulos',
        'STRIKE_CREATED_SUCESSFULLY_LABEL'  =>  'Has recibido un strike por favor lee las reglas antes de usar la plataforma',
        'QUESTIONS_LABEL'                   =>  'Preguntas',
        'WRITE_ANSWER_LABEL'                =>  'Escribir respuesta',
        'WRITE_QUESTION_LABEL'              =>  'Escribir Pregunta',
        'ANWSERS_LABEL'                     =>  'Respuestas',
        'NO_ANWSERS_LABEL'                  =>  'No hay Respuestas en este monento',
    ],
    
    'company'=>[
    
        'COMPANIES_LABEL'                   =>  'Tiendas',
        'COMPANY_LABEL'                     =>  'Tienda',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Tienda creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Tienda updated exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Tienda borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Tienda existe',

        'CATEGORIES_LABEL'                  => 'Categorias Tienda',
        'CATEGORY_LABEL'                    => 'Categoria Tienda'
    ],

    'currency'=>[
        'CURRENCIES_LABEL'                  =>  'Monedas',
        'CURRENCY_LABEL'                    =>  'Moneda',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Moneda creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Moneda actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Moneda borrado exitosamente',
        "EXISTS_LABEL"                      =>  'Moneda existe',
        "AMOUNT_LABEL"                      =>  'Monto',
        "CODE_LABEL"                        =>  'Codigo',
    ], 
];
