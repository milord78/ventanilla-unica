<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

  
    'FEE_LABEL'                     => 'Fee',
    'RATE_SELL_LABEL'               => 'Precio de venta ',
    'RATE_BUY_LABEL'                => 'Precio de compra',
    'FEE_SELL_LABEL'                => 'Fee venta',
    'FEE_BUY_LABEL'                 => 'Fee compra',
    'TOTAL_SELL_LABEL'              => 'Total a recibir',
    'TOTAL_BUY_LABEL'               => 'Total a pagar',
    'UTILITY_LABEL'                 => 'Utilidad',
    'YOUR_CLIENT_RECEIVE_LABEL'     => 'Su amigo recive',
    'PAYMENT_DETAILS_LABEL'         => 'Detalles Depago',
    'LIST_LABEL'                    => 'Precios Exchange',
    'SINGLE_LABEL'                  => 'Precio Exchange',
    'UPDATE_SUCESSFULLY_LABEL'      => 'Precio Exchange actiualizado exitosamente',
    'DELETED_SUCESSFULLY_LABEL'     => 'Precio Exchange borrado exitosamente',
    'CREATED_SUCCESSFULLY_LABEL'    => 'Precio Exchange creado exitosamente',
    'EXISTS_LABEL'                  => 'Oops Precio Exchange existe',

    
];
