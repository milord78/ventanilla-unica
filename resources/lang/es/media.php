<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 
     'MEDIA_LABEL'                       =>  'Media',
     'SELECT_MEDIA_LABEL'                =>  'Seleccionar Media',
     'MEDIA_DIRECTORY_LABEL'             =>  'Media Directory',
     'FILE_PLURAL_LABEL'                 =>  'Archivo',
     'PARENT_LABEL'                      =>  'Pariente',
     'ITEM_DUPLICATE_LABEL'              =>  'Item duplicado',
     'DROP_FILES_LABEL'                  =>  'Arrastrar Fotos',
     'UNLOADED_FILES_LABEL'              =>  'Sube tus Fotos Favoritas',
     'DROP_FILES_VIDEOS_LABEL'           =>  'Arrastrar Videos',
     'UNLOADED_FILES_VIDEOS_LABEL'       =>  'Sube tus Videos Favoritas',
     'MY_PICTURES'                       =>  'Mis Fotos',
     'MY_VIDEOS'                         =>  'Mis Videos',
     'PICTURES'                          =>  'Fotos',
     'VIDEOS'                            =>  'Videos',
     'NEW_ALBUM'                         =>  'Nuevo Album',
     'MY_ALBUMS'                         =>  'Mis Albums de fotos',
     'ALBUM_LABEL'                       =>  'Album',
     'GALLERY_LABEL'                     =>  'Galeria',
     'CARTEL_LABEL'                      =>  'Cartel (250x250)',
     'HEADER_LABEL'                      =>  'Cabezera (1920x350)',
     'HEADER_LANDING_LABEL'              =>  'Cabezera Landing (1920x350)',
     'LOGO_LABEL'                        =>  'Logo (225X225)',

          'DROP_ARCHIVES_LABEL'              =>  'Sube tus Archivos Favoritos',
     'UNLOADED_ARCHIVES_LABEL'       =>   'Sube tus Archivos Favoritos',
     'VIEW_ATTACTMENT_LABEL'                   =>  'Ver Archivo',
];
