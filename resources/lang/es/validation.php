<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute debe ser aceptado.',
    'active_url'           => 'The :attribute no es una url valida.',
    'after'                => 'The :attribute debe ser una fecha despues de :date.',
    'after_or_equal'       => 'The :attribute debe ser una fecha despues de  o igual a :date.',
    'alpha'                => 'The :attribute solamente debe contener letras.',
    'alpha_dash'           => 'The :attribute solamente debe contener letras, numeros, y dashes.',
    'alpha_num'            => 'The :attribute solamente debe contener letras y numeros.',
    'array'                => 'The :attribute debe ser un array.',
    'before'               => 'The :attribute debe ser una fecha antes :date.',
    'before_or_equal'      => 'The :attribute debe ser una fecha antes o igual a :date.',
    'between'              => [
        'numeric' => 'The :attribute debe estar entre :min y :max.',
        'file'    => 'The :attribute debe estar entre :min y :max kilobytes.',
        'string'  => 'The :attribute debe estar entre :min y :max characters.',
        'array'   => 'The :attribute debe estar entr :min y :max items.',
    ],
    'boolean'              => 'el :attribute el archivo debe ser verdadero o falso.',
    'confirmed'            => 'el :attribute la confirmacion no se encuntra.',
    'date'                 => 'el :attribute no es fecha valida.',
    'date_format'          => 'el :attribute no se encuentra el formato :format.',
    'different'            => 'el :attribute y :other deben ser diferentes.',
    'digits'               => 'el :attribute deben ser :digits digitos.',
    'digits_between'       => 'el :attribute deben ser between :min and :max digitos.',
    'dimensions'           => 'el :attribute tiene dimenciones invalidas.',
    'distinct'             => 'el :attribute campo tiene valores duplicados.',
    'email'                => 'el :attribute debe ser una direccion de email valida.',
    'exists'               => 'el seleccionado :attribute es invalido.',
    'file'                 => 'el :attribute debe ser un archivo.',
    'filled'               => 'el :attribute debe tener valor.',
    'gt'                   => [
        'numeric' => 'The :attribute debe ser superior a :value.',
        'file'    => 'The :attribute debe ser superior a :value kilobytes.',
        'string'  => 'The :attribute debe ser superior a :value caracteres.',
        'array'   => 'The :attribute debe tener mas :value items.',
    ],
    'gte'                  => [
        'numeric' => 'The :attribute debe ser mayor o igual :value.',
        'file'    => 'The :attribute debe ser mayor o igual :value kilobytes.',
        'string'  => 'The :attribute debe ser mayor o igual :value caracteres.',
        'array'   => 'The :attribute debe tener el valor :value o mas.',
    ],
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute debe ser una direccion valida IP .',
    'ipv4'                 => 'The :attribute debe ser una direccion valida IPv4 .',
    'ipv6'                 => 'The :attribute debe ser una direccion valida IPv6 .',
    'json'                 => 'The :attribute debe ser una direccion cadena valida JSON .',
    'lt'                   => [
        'numeric' => 'The :attribute debe ser menor que :value.',
        'file'    => 'The :attribute debe ser menor que :value kilobytes.',
        'string'  => 'The :attribute debe ser menor que :value characters.',
        'array'   => 'The :attribute debe ser que estos  :value valores.',
    ],
    'lte'                  => [
        'numeric' => 'The :attribute debe ser menor que o igual :value.',
        'file'    => 'The :attribute debe ser menor que o igual :value kilobytes.',
        'string'  => 'The :attribute debe ser menor que o igual :value characters.',
        'array'   => 'The :attribute no debe ser mas que estos :value valores.',
    ],
    'max'                  => [
        'numeric' => 'The :attribute no debe ser mayor que :max.',
        'file'    => 'The :attribute no debe ser mayor que :max kilobytes.',
        'string'  => 'The :attribute no debe ser mayor que :max characters.',
        'array'   => 'The :attribute no debe tener mas que estos :max valores.',
    ],
    'mimes'                => 'The :attribute debe ser un archivo del tipo: :values.',
    'mimetypes'            => 'The :attribute debe ser un archivo del tipo: :values.',
    'min'                  => [
        'numeric' => 'The :attribute debe ser de al menos :min.',
        'file'    => 'The :attribute debe ser de al menos :min kilobytes.',
        'string'  => 'The :attribute debe ser de al menos :min characters.',
        'array'   => 'The :attribute debe tener al menos estos :min valores.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'not_regex'            => 'The :attribute el formato es invalido.',
    'numeric'              => 'The :attribute debe ser numerico.',
    'present'              => 'The :attribute debe ser presente.',
    'regex'                => 'The :attribute el formato es invalido.',
    'required'             => 'The :attribute este campo es requerido.',
    'required_if'          => 'The :attribute este campo es requerido cuando :other es :value.',
    'required_unless'      => 'The :attribute este campo es requerido al menos :other cuando esta en :values.',
    'required_with'        => 'The :attribute este campo es requerido cuando :values esta presente.',
    'required_with_all'    => 'The :attribute este campo es requerido cuando :values esta presente.',
    'required_without'     => 'The :attribute este campo es requerido cuando :values no esta presente.',
    'required_without_all' => 'The :attribute este campo es requerido cuando ningundo de los :values esta presente.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute debe ser :size.',
        'file'    => 'The :attribute debe ser :size kilobytes.',
        'string'  => 'The :attribute debe ser :size characters.',
        'array'   => 'The :attribute debe contener :size valores.',
    ],
    'string'               => 'The :attribute debe ser una cadena.',
    'timezone'             => 'The :attribute debe ser una zona valida.',
    'unique'               => 'The :attribute al menos ha sido tomada.',
    'uploaded'             => 'The :attribute fallado para subir.',
    'url'                  => 'The :attribute formato es invalido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
