<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

 
 
   
    'TESTIMONIALS_LABEL'    =>  'Testimonios',
    'TESTIMONIAL_LABEL'     =>  'Testimonio',
    'SUBMIT_LABEL'          =>  'Enviar Testimonio',
    'COMPANY_LABEL'         =>  'Compañia',
    'RATING_LABEL'          =>  'Ranking',

];
