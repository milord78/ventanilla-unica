<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'pages'=>[
        'PAGES_LABEL'                    =>  'Paginas',
        'PAGE_LABEL'                     =>  'Pagina',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Pagina creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Pagina actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Pagina borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Pagina existe',
        'VIEW_DETAILS_LABEL'                =>  'Leer mas'
    ],
     'comment'=>[
        'COMMENTS_LABEL'                    =>  'Comentarios',
        'COMMENTS_LABEL'                     =>  'Commentario',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Commentario creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Commentario actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Commentario borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Commentario existe',
        'VIEW_DETAILS_LABEL'                =>  'Leer mas'
    ],

    'post'=>[
        'POSTS_LABEL'                    =>  'Noticia',
        'POST_LABEL'                     =>  'Noticia',
        'EXCERPT_LABEL'                     =>  'Extracto',
        'CONTENT_LABEL'                     =>  'Contendo',
        'IS_ON_FEED_LABEL'                  =>  'Mostrar en el feed',
        'IS_FEATURED_LABEL'                 =>  'es destacado',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Noticia creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Noticia actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Noticia borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Noticia existe',
        'VIEW_DETAILS_LABEL'                =>  'Read more'
    ],
 
    'articles'=>[
        'ARTICLES_LABEL'                    =>  'Articulos',
        'ARTICLE_LABEL'                     =>  'Articulo',
        'EXCERPT_LABEL'                     =>  'Extracto',
        'CONTENT_LABEL'                     =>  'Contendo',
        'IS_ON_FEED_LABEL'                  =>  'Mostrar en el feed',
        'IS_FEATURED_LABEL'                 =>  'es destacado',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Articulo creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Articulo actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Articulo borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Articulo existe',
        'VIEW_DETAILS_LABEL'                =>  'Read more'
    ],
    'categories'=>[
    
        'CATEGORIES_LABEL'                  => 'Categorias',
        'CATEGORY_LABEL'                    => 'Categoria',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Categoria creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Categoria actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Categoria borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Categoria existe'
    ],
    'especifications' => [
    
        'ESPECIFICATION_LABEL'              =>  'Especificaciones',
        'ESPECIFICATIONS_LABEL'             =>  'Especificación',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Especificación creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Especificación actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Especificación borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Especificación existe',
        'meta' => [
    
        'NAME_LABEL'                  => 'Titulo',
        'VALUE_LABEL'                    => 'Valor',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Especificación Meta creada exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Especificación Meta actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Especificación Meta borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Especificación Meta existe'
        ],
    ],
    'price' => [
        "NAME_LABEL"                        =>  'Nombre',
        "PRICE_LABEL"                       =>  'Precio',
        "PAYMENT_METHOD_LABEL"              =>  'Metodo de pago',
        "COUNTRY_METHOD_LABEL"              =>  'Pais',
        "DELIVERY_LABEL"                    =>  'Envio',
        "LINK_LABEL"                        =>  'Link',
        "COMPANY_LABEL"                     =>  'Tienda',
        "FREE_SHIPPING_LABEL"               =>  'Envio gratuito',
        'PRICE_LABEL'                       =>  'Precio',
        'PRICES_LABEL'                      =>  'Precios',
        'CREATED_SUCCESSFULLY_LABEL'        =>  'Precio creado exitosamente',
        'UPDATE_SUCESSFULLY_LABEL'          =>  'Precio actualizado exitosamente',
        'DELETED_SUCESSFULLY_LABEL'         =>  'Precio borrado exitosamente',
        'EXISTS_LABEL'                      =>  'Precio existe',
        'meta' => [
    
            'NAME_LABEL'                        => 'Titulo',
            'VALUE_LABEL'                       => 'Valor',
            'CREATED_SUCCESSFULLY_LABEL'        =>  'Precio Meta creado exitosamente',
            'UPDATE_SUCESSFULLY_LABEL'          =>  'Precio Meta actualizado exitosamente',
            'DELETED_SUCESSFULLY_LABEL'         =>  'Precio Meta borrado exitosamente',
            'EXISTS_LABEL'                      =>  'Precio Meta existe'
        ],

    ],

];
