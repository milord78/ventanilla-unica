<?php

/**
 * 將新增的分頁方法註冊到查詢構建器中，以便在模型例項上使用
 * 註冊方式：
 * 在 AppServiceProvider 的 boot 方法中註冊：AcademyPaginator::rejectIntoBuilder();
 * 使用方式：
 * 將之前程式碼中在模型例項上呼叫 paginate 方法改為呼叫 seoPaginate 方法即可：
 * Article::where('status', 1)->seoPaginate(15, ['*'], 'page', page);
 */
namespace App\Providers;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Container\Container;
use App\PaginationFactory;

class RiakServiceProvider extends ServiceProvider
{
   

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
       

    }
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
       $this->app['paginator'] = $this->app->share(function($app) {
            $paginator = new PaginationFactory($app['request'], $app['view'],
                $app['translator']);

            $paginator->setViewName($app['config']['view.pagination']);

            $app->refresh('request', $paginator, 'setRequest');

            return $paginator;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('paginator');
    }

}
