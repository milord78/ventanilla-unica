<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Posts\Entities\CurrencyExchange;
use Library\Networking\HttpClient\HttpClient;

class WordOfTheDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:wordoftheday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $currencies = [
            "EUR",
            "ARS",
            "BOB",
            "BRL",
            "CLP",
            "COP",
            "CRC",
            "CUP",
            "USD",
            "SVC",
            "GTQ",
            "HNL",
            "MXN",
            "NIO",
            "PAB",
            "PYG",
            "PEN",
            "DOP",
            "UYI",
            "VEF"
        ];
        $rand_keys = array_rand($currencies, 1);
        $this->create_currency($currencies[$rand_keys[0]]);
    }

    function create_currency($code)
    {
        $item = new HttpClient();
        $url = "https://prepaid.currconv.com/api/v7/convert";
        $data = array(
            'q'         => "USD_{$code},{$code}_USD",
            'compact'   => "ultra",
            'apiKey'    => "pr_d0fb4ea4ac904a3b949876729d0e87fd",
        );
        $item->post($url, $data);
        $obj = json_decode($item->html, true);
        $amount = floatval($obj["USD_{$code}"]);
        $currency = CurrencyExchange::where('currency',$code)->get();
        if(count($currency)>0)
        {
            CurrencyExchange::find($currency[0]->id)->update(["amount"=>$amount
            ]);
        }else
        {
            CurrencyExchange::create([
                "amount"=>$amount,
                "currency"=>$code
            ]);
        }
    }
}
