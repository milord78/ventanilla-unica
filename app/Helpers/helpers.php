<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\Testimonial\Entities\Testimonials;
use Modules\Navigations\Entities\Navigations;
use Modules\Navigations\Entities\NavigationPosition;
use Modules\Settings\Entities\Settings;
use Modules\Permissions\Entities\UserGroupsPermission;
use Modules\Permissions\Entities\GroupsPermissions;
use Modules\UserVerification\Entities\UserProfileVerification;
use Modules\Notifications\Entities\Notification;
use Modules\Notifications\Entities\SystemNotification;
use Modules\ControlNotas\Entities\Plantel;
use Modules\ControlNotas\Entities\Alunnos;


if (! function_exists('slugify')) 
{
function slugify($str) {
    // Convert to lowercase and remove whitespace
    $str = strtolower(trim($str));  
  
    // Replace high ascii characters
    $chars = array("ä", "ö", "ü", "ß");
    $replacements = array("ae", "oe", "ue", "ss");
    $str = str_replace($chars, $replacements, $str);
    $pattern = array("/(é|è|ë|ê)/", "/(ó|ò|ö|ô)/", "/(ú|ù|ü|û)/");
    $replacements = array("e", "o", "u");
    $str = preg_replace($pattern, $replacements, $str);
  
    // Remove puncuation
    $pattern = array(":", "!", "?", ".", "/", "'");
    $str = str_replace($pattern, "", $str);
  
    // Hyphenate any non alphanumeric characters
    $pattern = array("/[^a-z0-9-]/", "/-+/");
    $str = preg_replace($pattern, "-", $str);
  
    return $str;
  }
}


if (! function_exists('time_elapsed_string')) 
{

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'año',
        'm' => 'mes',
        'w' => 'semana',
        'd' => 'dia',
        'h' => 'hora',
        'i' => 'minuto',
        's' => 'segundo',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ?"Hace ". implode(', ', $string): 'justo ahora';
    }
}




if (! function_exists('show_currency_format')) 
{
    function show_currency_format($number, $fractional=false) 
    {
        if ($fractional) 
        {
            $number = sprintf('%.2f', $number);
        }
        
        while (true) 
        {
            $replaced = preg_replace(
                '/(-?\d+)(\d\d\d)/', 
                '$1,$2', 
                $number
            );
            if ($replaced != $number) 
            {
                $number = $replaced;
            } else 
            {
                break;
            }
        }
        return $number;
    } 
}


if (! function_exists('date_format')) 
{
    function date_format($created_at) 
    {
        return \Carbon\Carbon::parse($created_at)->format('d/m/Y');
    } 
}




if (! function_exists('save_permissions_groups_related')) 
{
    function save_permissions_groups_related($id,$ids) 
    {
        GroupsPermissions::where('group_id',$id)->delete();
        foreach ($ids as  $key => $value) 
        {
            $items = GroupsPermissions::create([
                'group_id'=>$id,
                'permission_id'=>$value
            ]);
        }
    }
}


if (! function_exists('save_user_permissions_groups_related')) 
{
    function save_user_permissions_groups_related($id,$ids) 
    {
        

        UserGroupsPermission::where('user_id',$id)->delete();
        foreach ($ids as  $key => $value) 
        {
            $items = UserGroupsPermission::create([
                'user_id'=>$id,
                'group_id'=>$value
            ]);
        }
    }
}


if (! function_exists('list_tags')) 
{
    function list_tags($id,$ids) 
    {
        return Tags::all();
    }
}



if (! function_exists('get_navigation')) 
{
    function get_navigation($postion_name) 
    {
        return Navigations::leftJoin('app_navigation_position', 'app_navigation_position.id', '=', 'app_navigation.position_id')->
        leftJoin('pages', 'pages.id', '=', 'app_navigation.item_id')
       ->select('pages.*')->where([
            ['app_navigation_position.name','=',$postion_name],
            ['app_navigation.parent_id','=',0],
            ['app_navigation.publish','=',true]
        ])->get();

    } 
}

if (! function_exists('get_navigation_child')) 
{
    function get_navigation_child($postion_name,$parent_id) 
    {

        return Navigations::leftJoin('app_navigation_position', 'app_navigation_position.id', '=', 'app_navigation.position_id')
        -> leftJoin('pages', 'pages.id', '=', 'app_navigation.item_id')
       ->select('pages.*')->where([
            ['app_navigation_position.name','=',$postion_name],
            ['app_navigation.parent_id','=',$parent_id],
            ['app_navigation.publish','=',true]
        ])->get();

    } 
}




    function pagination_get_url($page,$articles)
    {
        if ($page <= 0) 
        {
            $page = 1;
        }

        // If we have any extra query string key / value pairs that need to be added
        // onto the URL, we will put them in query string form and then attach it
        // to the URL. This allows for extra information like sortings storage.
      
        $parameters = [];
        if (count(request()->query) > 0) {
            $parameters = [request()->query];
        }
        $path = request()->url();
        $pattern = '/\/page\/(\d+)/';
        $replacement = '';

        $url = preg_replace($pattern, $replacement, $path)
                        ."/page/".$page
                        .Arr::query($parameters);

        return  $url;
    }
    
    function pagination_previousPageUrl($articles)
    {
        if ($articles->currentPage() > 1) {
            return pagination_get_url($articles->currentPage() - 1,$articles);
        }
    }

    function pagination_nextPageUrl($articles)
    {
        if ($articles->lastPage() > $articles->currentPage()) {
            return pagination_get_url($articles->currentPage() + 1,$articles);
        }
    }

    function pagination_getUrlRange($start, $end,$articles)
    {
        return collect(range($start, $end))->mapWithKeys(function ($page) use ($articles) {
            return [$page => pagination_get_url($page,$articles)];
        })->all();
    }

    if (! function_exists('is_pagination_finish')) 
    {
        function is_pagination_finish($articles) 
        {
            return pagination_getUrlRange(
                $articles->lastPage() - 1,
                $articles->lastPage(),
                $articles
            );
        }
    }

   if (! function_exists('paginator_getStart')) 
    {
        function  paginator_getStart($articles)
        {
            return pagination_getUrlRange(1, 2, $articles);
        }
    }

    if (! function_exists('paginator_getSmallSlider')) 
    {
        function paginator_getSmallSlider($articles)
        {
            return [
                'first'  => pagination_getUrlRange(1, $articles->lastPage(), $articles),
                'slider' => null,
                'last'   => null,
            ];
        }
    }


    if (! function_exists('pagination_getFinish')) 
    {
        function pagination_getFinish($articles)
        {
            return pagination_getUrlRange(
                $articles->lastPage() - 1,
                $articles->lastPage(),
                $articles
            );
        }
    }  

    if (! function_exists('paginator_getFullSlider')) 
    {
        function paginator_getFullSlider($onEachSide,$articles)
        {
            return [
                'first'  => paginator_getStart($articles),
                'slider' => paginator_getAdjacentUrlRange($onEachSide, $articles),
                'last'   => pagination_getFinish($articles),
            ];
        }
    }

    if (! function_exists('paginator_getAdjacentUrlRange')) 
    {

        function paginator_getAdjacentUrlRange($onEachSide,$articles)
        {
            return pagination_getUrlRange(
                $articles->currentPage() - $onEachSide,
                $articles->currentPage() + $onEachSide,
                $articles
            );
        }
    }

    if (! function_exists('paginator_getUrlSlider')) 
    {

        function paginator_getUrlSlider($onEachSide,$articles)
        {
            $window = $onEachSide * 2;

            if (! $articles->hasPages()) {
                return ['first' => null, 'slider' => null, 'last' => null];
            }

            // If the current page is very close to the beginning of the page range, we will
            // just render the beginning of the page range, followed by the last 2 of the
            // links in this list, since we will not have room to create a full slider.
            if ($articles->currentPage() <= $window) {
                return paginator_getSliderTooCloseToBeginning($window,$articles);
            }

            // If the current page is close to the ending of the page range we will just get
            // this first couple pages, followed by a larger window of these ending pages
            // since we're too close to the end of the list to create a full on slider.
            elseif ($articles->currentPage() > ($articles->lastPage() - $window)) {
                return paginator_getSliderTooCloseToEnding($window,$articles);
            }

            // If we have enough room on both sides of the current page to build a slider we
            // will surround it with both the beginning and ending caps, with this window
            // of pages in the middle providing a Google style sliding paginator setup.
            return paginator_getFullSlider($onEachSide,$articles);
        }
    }

 




    if (! function_exists('paginator_getSliderTooCloseToEnding')) 
    {

        function paginator_getSliderTooCloseToEnding($window,$articles)
        {
            $last = pagination_getUrlRange(
                $articles->lastPage() - ($window + 2),
                $articles->lastPage(),
                $articles
            );

            return [
                'first' => paginator_getStart($articles),
                'slider' => null,
                'last' => $last,
            ];
        }

    }

    if (! function_exists('paginator_getSliderTooCloseToBeginning')) 
    {
        function paginator_getSliderTooCloseToBeginning($window,$articles)
        {
            return [
                'first' => pagination_getUrlRange(1, $window + 2),
                'slider' => null,
                'last' => is_pagination_finish($articles),
            ];
        }
    }

    if (! function_exists('pagination_slide')) 
    {
        function pagination_slide($articles) 
        {
           $onEachSide = $articles->onEachSide;
           if ($articles->lastPage() < ($onEachSide * 2) + 6) {

                return paginator_getSmallSlider( $articles);
            }

            return paginator_getUrlSlider($onEachSide,$articles);
        }
    }



if (! function_exists('generateRandomString')) 
{
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}


if (! function_exists('push_notification')) 
{
        function push_notification($autor_id,$message,$link,$readed=False){
        
            Notification::create([
                "autor_id"=>$autor_id,
                "message"=>$message,
                "link"=>$link,
                "readed"=>$readed
            ]);
        }
}


if (! function_exists('push_system_notification')) 
{
    function push_system_notification($message,$link,$readed=False)
    {
        SystemNotification::create([
            "message"=>$message,
            "link"=>$link,
            "readed"=>$readed
        ]);
    }
}

if (! function_exists('system_notification_all')) 
{
    function system_notification_all()
    {
        return SystemNotification::where([
            ['readed','=',false],
        ])->get();
    }
}


if (! function_exists('notification_all')) 
{
    function notification_all($autor_id)
    {
        return Notification::where([
            ['readed','=',false],
            ['autor_id','=',$autor_id]
        ])->get();
    }
}



if (! function_exists('count_inscripciones')) 
{
    function count_inscripciones($autor_id) 
    {
         return Alunnos::where([
      
        ['user_id','=',$autor_id]
     ])->get();
    }
}

if (! function_exists('get_settings')) 
{
    function get_settings() 
    {
        $item = Plantel::get();
        if(count($item)> 0)
        {
            $item = $item[0];
        }else{
            $item = Plantel::create(['nombre'=>null]);
        }
        return $item;
    }
}

if (! function_exists('count_plantel')) 
{
    function count_plantel() 
    {
        $item = Plantel::get();

        return count($item);
    }
}


if (! function_exists('user_full_filled')) 
{
    function user_full_filled($item) 
    {
        if((
            $item->firstname != null && 
            $item->midname != null &&
            $item->lastname != null &&
            $item->email != null &&
            $item->cell_phone!=null &&
            $item->phone != null &&
            $item->curp_number != null &&
            $item->nss_number != null && 
            $item->imss_link != null &&
            $item->imss_document != null &&
            $item->escolaridad != null && 
            $item->tipo_sanguineo != null && 
            $item->estado_civil != null &&
            $item->fecha_nac != null &&
            $item->info != null &&
            $item->logo != null &&
            $item->payment_info != null && 
            $item->especialidad_1 != null &&
            $item->especialidad_2 != null &&
            $item->especialidad_3 != null &&
            $item->status != null &&
            $item->generar_cerificado_secundaria != null &&
            $item->nombre_Secundaria_procedencia != null &&
            $item->tipo_de_secundaria  != null &&
            $item->promedio_secundaria != null &&
            $item->posee_beca  != null &&
            $item->tiene_discapacidad  != null &&
            $item->tratamientos  != null &&
            $item->nombre_completo_padre  != null &&
            $item->nombre_completo_madre  != null &&
            $item->ocupacion_padre  != null &&
            $item->ocupacion_madre  != null &&
            $item->telefono_celular_padre != null &&
            $item->telefono_celular_madre  != null &&
            $item->de_quien_depende  != null &&
            $item->representante_nombre  != null &&
            $item->representante_parentesco  != null &&
            $item->representante_calle != null &&
            $item->representante_numero  != null &&
            $item->representante_cruzamientos  != null &&
            $item->representante_colonia  != null &&
            $item->representante_zip_code  != null &&
            $item->representante_municipio  != null &&
            $item->representante_telefono_fijo  != null &&
            $item->representante_telefono_celular  != null &&
            $item->representante_email != null &&
            $item->tiene_discapacidad_tipo  != null &&
            $item->tratamientos_tipo != null &&
            $item->escolaridad_padre != null &&
            $item->escolaridad_madre != null &&
            $item->clave_entidad_federativa  != null &&
            $item->clave_estado_nac != null &&
            $item->clave_localidad_vive  != null &&
            $item->clave_municipio_nac != null &&
            $item->clave_municipio_vive  != null &&
            $item->resultado_ceneval != null &&
            $item->folio_ceneval != null &&
            $item->tiene_ceneval  != null ))
        {
            return true;
        }
        else
        {
            return false;
        }

        
    }
}




