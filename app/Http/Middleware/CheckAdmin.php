<?php

namespace App\Http\Middleware;

use Closure;


class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( \Auth::check()==true)
        {
            $roles = ['user','aspirante','estudiante'];

            if (in_array(\Auth::user()->role, $roles) == true)
            {
                $redirect_url = redirect()->route("homepage");
                return $redirect_url;
              
            }
        }
       

        return $next($request);
    }
}
