<?php

namespace App\Http\Middleware;

use Closure;
use App\Mail\TwoFactorAuthMail;
class TwoFactorVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
        if( \Auth::check()==true)
        {
            $user = \Auth::user();
            if ($user->two_factor_expiry > \Carbon\Carbon::now()) {
                return $next($request);
            }



           
            $check_route =in_array(
                \Request::route()->getName(),
                [
                    'frontend_account_two_factor_new',
                    'logout',
                    'register',
                    'verification.notice',
                    'verification.resend',
                    'verification.verify',
                    'frontend_account_two_factor_store'
                ]
            );
            if( $check_route==true)
            {
                return $next($request);
            }else{
                
                if ($user->two_factor_token_expiry < \Carbon\Carbon::now()) {
                    $user->two_factor_token_expiry = \Carbon\Carbon::now()->addMinutes(38);
                    $user->two_factor_token = $this->random_str(10);
                    $user->save();
                    \Mail::to($user->email)->send(new TwoFactorAuthMail($user->two_factor_token));
                    
                }
              
                
                    return redirect()->route('frontend_account_two_factor_new');
               
                

              
            }
       
            
        }
        return $next($request);
      
    }
    private function random_str(
        $length=10,
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
            ) {
                $str = '';
                $max = mb_strlen($keyspace, '8bit') - 1;
                if ($max < 1) {
                    throw new Exception('$keyspace must be at least two characters long');
                }
                for ($i = 0; $i < $length; ++$i) {
                    $str .= $keyspace[random_int(0, $max)];
                }
                return $str;
            }
}
