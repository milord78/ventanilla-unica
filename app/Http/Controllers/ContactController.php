<?php

namespace App\Http\Controllers;
use  App\Http\Controllers\Controller;
use App\User;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Modules\ContactForm\Entities\ContactForm;

class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Contact page .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('frontend/contact');
    }
        public function sent(Request $request)
    {
        return view('frontend/contact_sent');
    }
    /**
     * Contact page send mail message .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request)
    {

        $inder_data = $request->except(['_token','_method','submit']);

        ContactForm::create([

                'title'=>$inder_data['title'],
                'website'=>$inder_data['website'],
                'email'=>$inder_data['email'],
                'content'=>$inder_data['message'],
                'phone'=>$inder_data['phone'],
        ]);

        push_system_notification('Tienes un nuevo mensaje de contacto',route('admin_contact_form_index'),false);
    
            try {
                
                Mail::send('email/contact', [
                    'data' => $inder_data
                ], function($message) use ($inder_data )
                {
                    $message->to(
                        $inder_data['email'], 
                        $inder_data['firstname']
                    )->subject( Config::get('app.name').' Mensaje de Contacto');

                    $message->from(
                        Config::get('mail.from.address'),
                        Config::get('mail.from.name')
                    );
                });
           
            } catch (Exception $e) {
                
            }
                
        return redirect()->route("contact_message_sent_succesfully")
            ->with('success','Mensaje enviado existosamente ');  
       
        
    }
  
    

}
