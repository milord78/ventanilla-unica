<?php

namespace App\Http\Controllers;
use Modules\Pages\Entities\Pages ;
use Modules\Posts\Entities\Posts as PostsItems;
use Modules\Posts\Entities\PostsCompany;
use Modules\Posts\Entities\PostsCategories as PostsCategories;
use Illuminate\Http\Request;

class SitempController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = PostsItems::orderBy('id', 'desc')->where([
                ['publish','=',  1 ],
            ])->get();
        $pages = Pages::orderBy('id', 'desc')->where([
                ['publish','=',  1],
            ])->get();
        $categories = PostsCategories::orderBy('id', 'desc')->get();
    

           
        return response()->view('sitemap',compact('posts','pages','categories'),200)->header('Content-Type', "text/xml");
    }

}
