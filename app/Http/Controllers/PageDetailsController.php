<?php

namespace App\Http\Controllers;
use Modules\Pages\Entities\Pages as PostsItems;
use Illuminate\Http\Request;

class PageDetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$slug)
    {
        $item = PostsItems::where([
            ['slug','=', $slug],
            ['publish','=',true]
        ])->get();

        if(count($item )>0)
        {
            $item = $item[0];
        }else
        {
            return  response()->view('errors_page.404_frontend', [])->setStatusCode(404);
        }

        return view(
            'frontend/pages/page',
            compact(
                'item'
            )
        );
    }
    public function price_table(Request $request)
    {
        return view(
            'frontend/suscription/price_table'
        );
    }
}
