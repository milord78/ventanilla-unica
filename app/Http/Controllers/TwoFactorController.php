<?php

namespace App\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Routing\ControllerDispatcher;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class TwoFactorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function new(Request $request)
    {
       
     
       
        return view(
            'frontend/2fa'
        );
    }



    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verifyToken(Request $request)
    {
        
        $request->validate([
            'token' => 'required',
        ]);
        if($request->input('token') == \Auth::user()->two_factor_token){            
            $user = \Auth::user();
            $user->two_factor_expiry = \Carbon\Carbon::now()->addMinutes(config('session.lifetime'));
            $user->save();
            return redirect()->route('homepage');
        } else {
            return redirect()->route('frontend_account_two_factor_new')->with('error', 'Incorrect code.');
        }

    }

}
