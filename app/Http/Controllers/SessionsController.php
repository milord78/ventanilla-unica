<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class SessionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    public function set_country(Request $request,$code)
    {

        session()->put('country_code', $code);
        $redirect_url = redirect()->route("homepage");
        return $redirect_url;
    }



}
