<?php
namespace App\Http\Controllers;


use Illuminate\Routing\Controller;
use Illuminate\Routing\ControllerDispatcher;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;

class DynamicRouteController extends Controller
{
    /**
     * This method handles dynamic routes when route can begin with a category or a user profile name.
     * /women/t-shirts vs /user-slug/product/something
     *
     * @param $slug1
     * @param null $slug2
     * @return mixed
     */
    public function handle(
        Request $request, 
        $slug1, 
        $slug2 = null
    )
    {
        //$controller = DefaultController::class;
        $action = 'index';
        $controller = HomeController::class;

        if ($slug1 == 'holas') {
            $action = 'index2';
        }

        if ($slug1 == 'hola2') {
            $action = 'index';
        }

        $container = app();
        $route = $container->make(Route::class);
        $controllerInstance = $container->make($controller );

        return (new ControllerDispatcher($container))->dispatch($route, $controllerInstance, $action);
    }
}