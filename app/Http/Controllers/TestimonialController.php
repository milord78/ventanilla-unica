<?php

namespace App\Http\Controllers;
use Modules\Testimonial\Entities\Testimonials;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function new(Request $request)
    {
       
        if(can_i_post_testimonial()==true)
        {
            return redirect()->route("fronted_testimonials_success");
        }
        $route = route('fronted_testimonials_store');
        
        $item = new \stdClass();
        $item->id = null;
        $item->name = null;
        $item->company = null;
        $item->content = null;
        $item->publish = false;
        $item->rating = 0;
       
        return view(
            'frontend/user/testimonial_form',
            compact('item','route')
        );
    }

    public function success(Request $request)
    {
       
 
        return view(
            'frontend/user/testimonial_form_success'
        );
    }


    /**
     * Save new the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'company' => 'required',
        ]);



        $inder_data = $request->except([
            '_token',
            '_method',
            'submit'
        ]);

        if(Testimonials::where('company', $inder_data['company'])->count() == 0)
        {
            $user_id = \Auth::user()->id;
            $inder_data['user_id'] = $user_id;

            if(!empty($request->file('logo')))
            {
                $file_name = $request->file('logo')->getClientOriginalName();
                $path = $request->file('logo')->store('public');
                $inder_data['logo'] = str_replace("public", "images", $path);
            }   

            $item = Testimonials::create($inder_data);

            return redirect()->route("fronted_testimonials_success")
                        ->with('success','testimonials.CREATED_SUCCESSFULLY_LABEL');  
        }else{
            return  redirect()->route("fronted_testimonials_success")
                        ->with('error','testimonials.SLUG_EXISTS_LABEL');  
        }

    }

}
