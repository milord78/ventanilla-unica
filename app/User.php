<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'midname',
        'lastname',

        'email',
        'password',
        'cell_phone',
        'phone',
        'curp_number',
        'nss_number',
        'imss_link',
        'imss_document',
        'escolaridad',
        'tipo_sanguineo',
        'estado_civil',
        'fecha_nac',
        'info',
        'email_verified_at',
        'password',
        'two_factor_expiry',
        'two_factor_token_expiry',
        'two_factor_token',
        'logo',
        'payment_info',
        'strikes',
        'role',
        'provider',
        'provider_id',
        'access_token',
        'status',

        'generar_cerificado_secundaria',
        'nombre_Secundaria_procedencia',
        'tipo_de_secundaria',
        'promedio_secundaria',
        'posee_beca',
        'tiene_discapacidad',
        'tratamientos',
        'promedio_secundaria',
        'nombre_completo_padre',
        'nombre_completo_madre',
        'ocupacion_padre',
        'ocupacion_madre',
        'telefono_celular_padre',
        'telefono_celular_madre',
        'de_quien_depende',
        'representante_nombre',
        'representante_parentesco',
        'representante_calle',
        'representante_numero',
        'representante_cruzamientos',
        'representante_colonia',
        'representante_zip_code',
        'representante_municipio',
        'representante_telefono_fijo',
        'representante_telefono_celular',
        'representante_email',
        'tiene_discapacidad_tipo',
        'tratamientos_tipo',
        
        'escolaridad_padre',
        'escolaridad_madre',


        'clave_entidad_federativa',
        'clave_estado_nac',
        'clave_localidad_vive',
        'clave_municipio_nac',
        'clave_municipio_vive',

        'resultado_ceneval',
        'folio_ceneval',
        'tiene_ceneval',

        'confirmed',


        'covid_has_tenido_calentura',
        'covid_has_perdido_olfato',
        'covid_has_tenido_contacto_directo_con_alguien_con_covid19',

        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ]; 
    
    public function logos()
    {
        if($this->logo==null)
        {
            return asset('assets/img/profileicon-1487703856.png');

        }else{
            return "/".str_replace("public","images",$this->logo);
        }
    }


}
