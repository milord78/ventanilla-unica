<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname')->nullable();
            $table->string('midname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('website')->nullable();
            $table->string('email')->unique();
            $table->string('cell_phone')->nullable();
            $table->string('phone')->nullable();
            $table->string('curp_number')->nullable();
            $table->string('nss_number')->nullable();
            $table->text('imss_document')->nullable();
            $table->string('imss_link')->nullable();
            $table->string('ife_ine')->nullable();
            $table->string('certificado_de_secundaria')->nullable();
            $table->enum(
                'generar_cerificado_secundaria',
                ['SI','NO']
            )->defalt('NO');
            $table->date('fecha_fin_secundaria')->nullable();
            $table->string('certificado_medico')->nullable();
            $table->string('acta_de_nacimiento')->nullable();
            $table->string('comprobante_domicilio')->nullable();
            $table->string('numero_de_seguro_social')->nullable();
            $table->string('boleta_primaria_secundaria')->nullable();
            $table->string('especialidad_1')->nullable();
            $table->string('especialidad_2')->nullable();
            $table->string('especialidad_3')->nullable();
            $table->string('lastname_materno')->nullable();
            $table->string('calle')->nullable();
            $table->string('numero')->nullable();
            $table->string('cruzamientos')->nullable();
            $table->string('colonia')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('municipio')->nullable();
            $table->string('entidad_federativa')->nullable();
        
            $table->enum(
                'tiene_ceneval',
                ['SI','NO']
            )->default('NO');
            $table->text('resultado_ceneval')->nullable();
            $table->text('folio_ceneval')->nullable();

            $table->enum(
                'escolaridad',
                [
                    'primaria',
                    'secundaria',
                    'bachillerato',
                    'universitario',
                ]
            )->nullable();
            $table->enum(
                'tipo_sanguineo',
                [
                    'A+',
                    'A-',
                    'B+',
                    'B-',
                    'AB+',
                    'AB-',
                    'O+',
                    'O-' 
                ]
            )->nullable();
            $table->enum(
                'status',
                [
                    'pendiente',
                    'aprobado',
                    'rechazado',
                    'resubir-documentos',
                ]
            )->default('pendiente');
            $table->date('fecha_nac')->nullable();
            $table->enum(
                'genero',
                ['F','M']
            )->nullable();
            $table->string('nombre_Secundaria_procedencia')->nullable();

            $table->enum(
                'tipo_de_secundaria',
                [
                    'GENERAL',
                    'TECNICA',
                    'ADULTOS',
                    'TELESECUNDARIA',
                ]
            )->default('GENERAL');
            //$table->string('promedio_secundaria')->nullable();
            $table->decimal('promedio_secundaria', 5, 4)->nullable();
            $table->enum(
                'posee_beca',
                ['SI','NO']
            )->nullable();

            $table->enum(
                'tiene_discapacidad',
                ['SI','NO']
            )->nullable();
            $table->text('tiene_discapacidad_tipo')->nullable();
            $table->enum(
                'tratamientos',
                ['SI','NO']
            )->nullable();

            $table->enum(
                'covid_has_tenido_calentura',
                ['SI','NO','TALVEZ']
            )->nullable();

            $table->enum(
                'covid_has_perdido_olfato',
                ['SI','NO','TALVEZ']
            )->nullable();

            $table->enum(
                'covid_has_tenido_contacto_directo_con_alguien_con_covid19',
                ['SI','NO','TALVEZ']
            )->nullable();

            $table->text('tratamientos_tipo')->nullable();


            $table->text('nombre_completo_padre')->nullable();
            $table->text('nombre_completo_madre')->nullable();
            $table->string('ocupacion_padre')->nullable();
            $table->string('ocupacion_madre')->nullable();
            $table->string('escolaridad_padre')->nullable();
            $table->string('escolaridad_madre')->nullable();
            $table->string('telefono_celular_padre')->nullable();
            $table->string('telefono_celular_madre')->nullable();
            $table->enum(
                'de_quien_depende',
                ['PADRE','CONYUGUE','OTRO']
            )->nullable();
            $table->enum(
                'estado_civil',
                ['soltero','casado','union-libre','divorciado','viudo']
            );       
    
            $table->string('clave_entidad_federativa')->nullable();
            $table->string('clave_estado_nac')->nullable();
            $table->string('clave_localidad_vive')->nullable();
            $table->string('clave_municipio_nac')->nullable();
            $table->string('clave_municipio_vive')->nullable();

            $table->string('representante_nombre')->nullable();
            $table->string('representante_parentesco')->nullable();
         
            $table->string('representante_calle')->nullable();
            $table->string('representante_numero')->nullable();
            $table->string('representante_cruzamientos')->nullable();
            $table->string('representante_colonia')->nullable();
            $table->string('representante_zip_code')->nullable();
            $table->string('representante_municipio')->nullable();
            $table->string('representante_telefono_fijo')->nullable();
            $table->string('representante_telefono_celular')->nullable();
            $table->string('representante_email')->nullable();

            $table->text('info')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->datetime('two_factor_expiry')->nullable();
            $table->datetime('two_factor_token_expiry')->nullable();
            $table->text('two_factor_token')->nullable();
            $table->string('logo')->nullable();
            $table->text('payment_info')->nullable();
            $table->integer('strikes')->default(0);
            $table->enum('role',
                [
                    'admin',
                    'aspirante',
                    'estudiante',
                    'editor',
                    'auxiliar',
                ]
            )->default('estudiante');
            $table->string('avatar')->nullable();
            $table->string('provider', 20)->nullable();
            $table->string('provider_id')->nullable();
            $table->string('access_token')->nullable();
            $table->boolean('confirmed')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
