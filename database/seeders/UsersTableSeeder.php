<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('users')->insert([
            'firstname' => Str::random(10),
            'midname'  => Str::random(10),
            'lastname' => Str::random(10),
            'role' => 'admin',
            'email' => 'a@g.com',
            'password' => bcrypt('demo'),
        ]);
        
        DB::table('users')->insert([
            'firstname' => Str::random(10),
            'midname'  => Str::random(10),
            'lastname' => Str::random(10),
            'role' => 'editor',
            'email' => 'a2@g.com',
            'password' => bcrypt('demo'),
        ]);
        
 		

    }

    private function loadBacth()
    {
        for($i=0;$i<100;$i++)
        {
            DB::table('users')->insert([
                'firstname' => Str::random(10),
                'midname'  => Str::random(10),
                'lastname' => Str::random(10),
                'role' => 'editor',
                'email' => Str::random(10).'@gmail.com',
                'password' => bcrypt('demo'),
            ]);
        }
    }
}
