<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      
        DB::table('users')->insert([
            'firstname' => Str::random(10),
            'midname'  => Str::random(10),
            'lastname' => Str::random(10),
            'role' => 'admin',
            'email' => 'a@g.com',
            'password' => bcrypt('demo'),
        ]);
        
        DB::table('users')->insert([
            'firstname' => Str::random(10),
            'midname'  => Str::random(10),
            'lastname' => Str::random(10),
            'role' => 'editor',
            'email' => 'a2@g.com',
            'password' => bcrypt('demo'),
        ]);
    }
}
